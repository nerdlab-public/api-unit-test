package com.api.cnrt.validations;

import java.util.List;

import com.api.cnrt.api.user.model.RegisterDeviceRequest;
import com.api.cnrt.fiscalizaciones.dto.*;
import com.api.cnrt.maestros.repository.IFaltaServicioRepository;

public abstract class AbstractFiscalizacionValidatorService {


    public void validarFiscalizacion(FiscalizacionDTORequest fiscalizacionDTO) {
        validarCamposCargasCabecera(fiscalizacionDTO);
        validarCamposCargasInfracciones(fiscalizacionDTO.getFiscalizacionInfraccionesList());
        validarCamposCargasEmpresa(fiscalizacionDTO.getFiscalizacionEmpresa());
        validarCamposCargasRegisterDevice(fiscalizacionDTO.getRegisterDeviceRequest());
        validarCamposCargasDominios(fiscalizacionDTO.getFiscalizacionDominiosList());
        validarCamposCargasChoferes(fiscalizacionDTO.getFiscalizacionChoferesList());
    }

    protected abstract void validarCamposCargasChoferes(List<FiscalizacionChoferDTORequest> fiscalizacionChoferesList);

    protected abstract void validarCamposCargasDominios(List<FiscalizacionDominioDTORequest> fiscalizacionDominiosList);

    protected abstract void validarCamposCargasRegisterDevice(RegisterDeviceRequest registerDeviceRequest);

    protected abstract void validarCamposCargasEmpresa(FiscalizacionEmpresaDTORequest fiscalizacionEmpresa);

    protected abstract void validarCamposCargasInfracciones(List<FiscalizacionInfraccionDTORequest> fiscalizacionInfraccionesList);

    protected abstract void validarCamposCargasCabecera(FiscalizacionDTORequest fiscalizacionDTO);


}
