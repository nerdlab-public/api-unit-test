package com.api.cnrt.validations;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ActionValidator implements ConstraintValidator<ActionValidation, String> {


    @Override
    public void initialize(ActionValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String action, ConstraintValidatorContext constraintValidatorContext) {
        return action != null && (action.equals("get_Maestros_d") || action.equals("syncro_db_d"));
    }
}
