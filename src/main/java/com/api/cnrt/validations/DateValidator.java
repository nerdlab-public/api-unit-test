package com.api.cnrt.validations;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<DateValidation, String> {

    private DateValidation validation;

    @Override
    public void initialize(DateValidation constraintAnnotation) {
        this.validation = constraintAnnotation;
    }
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return isValidDate(value, validation.pattern());
	}
	
    public static boolean isValidDate(String inDate, String format) {
        try {
        	LocalDate.parse(inDate, DateTimeFormatter.ofPattern(format));
        	return true;
		} catch (DateTimeParseException  e) {
			return false;
		}		
    }

}
