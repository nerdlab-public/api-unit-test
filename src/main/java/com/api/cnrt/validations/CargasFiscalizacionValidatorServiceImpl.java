package com.api.cnrt.validations;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.api.cnrt.api.user.model.RegisterDeviceRequest;
import com.api.cnrt.common.ConstantCommonUtil;
import com.api.cnrt.fiscalizaciones.dto.*;
import com.api.cnrt.maestros.model.FaltaServicio;
import com.api.cnrt.maestros.repository.IFaltaServicioRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CargasFiscalizacionValidatorServiceImpl extends AbstractFiscalizacionValidatorService{

    private final IFaltaServicioRepository faltaServicioRepository;
    private final RegisterDevicesValidatorServiceImpl registerDevicesValidator;


    public CargasFiscalizacionValidatorServiceImpl(IFaltaServicioRepository faltaServicioRepository, RegisterDevicesValidatorServiceImpl registerDevicesValidator) {
        this.faltaServicioRepository = faltaServicioRepository;
        this.registerDevicesValidator = registerDevicesValidator;
    }




    protected void validarCamposCargasDominios(List<FiscalizacionDominioDTORequest> fiscalizacionDominiosList) {
        List<String> errores = new ArrayList<>();

        if (fiscalizacionDominiosList != null && fiscalizacionDominiosList.size() > 0) {

            for (FiscalizacionDominioDTORequest dominioItem : fiscalizacionDominiosList) {

                if(dominioItem.getAcarreado() != null && dominioItem.getAcarreado() == true ) {
                    if (StringUtils.isBlank(dominioItem.getDominioAcarreo())) {
                        errores.add("El campo 'dominioAcarreo' es requerido.");
                    }
                    if (dominioItem.getIdPredioRetencion() == null) {

                        errores.add("El campo 'idPredioRetencion' es requerido.");
                    }
                }
                if (StringUtils.isBlank(dominioItem.getDominio())) {
                    errores.add("El campo 'dominio' es requerido.");
                }

                if (dominioItem.getOrden() == null) {
                    errores.add("El campo 'orden' es requerido.");
                }


                if (StringUtils.isBlank(dominioItem.getNumeroRtoNuevo())) {
                    errores.add("El campo 'numeroRtoNuevo' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getAseguradoraNuevo())) {
                    errores.add("El campo 'aseguradoraNuevo' es requerido.");
                }


                if (StringUtils.isBlank(dominioItem.getCategoriaNuevo())) {
                    errores.add("El campo 'categoriaNuevo' es requerido.");
                }


                if (StringUtils.isBlank(dominioItem.getCuitNuevo())) {
                    errores.add("El campo 'cuitNuevo' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getDomicilio())) {
                    errores.add("El campo 'domicilio' es requerido.");
                }


                if (dominioItem.getIdLocalidad() == null) {
                    errores.add("El campo 'idLocalidad' es requerido.");
                }

                if (dominioItem.getIdPais() == null) {
                    errores.add("El campo 'idPais' es requerido.");
                }

                if (dominioItem.getIdProvincia() == null) {
                    errores.add("El campo 'idProvincia' es requerido.");
                }

                if (dominioItem.getInternoOriginal() != null && !dominioItem.getInternoOriginal().isEmpty()) {
                    errores.add("El campo 'internoOriginal' debe ser vacio o nulo.");
                }

                if (dominioItem.getInternoNuevo() != null && !dominioItem.getInternoNuevo().isEmpty()) {
                    errores.add("El campo 'internoNuevo' debe ser vacio o nulo.");
                }

                if (dominioItem.getLineaOriginal() != null && !dominioItem.getLineaOriginal().isEmpty()) {
                    errores.add("El campo 'lineaOriginal' debe ser vacio o nulo..");
                }

                if (dominioItem.getLineaNuevo() != null && !dominioItem.getLineaNuevo().isEmpty()) {
                    errores.add("El campo 'lineaNuevo' debe ser vacio o nulo.");
                }

                if (StringUtils.isBlank(dominioItem.getNacionalidad())) {
                    errores.add("El campo 'nacionalidad' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getNroPolizaNuevo())) {
                    errores.add("El campo 'nroPolizaNuevo' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getPermisosNuevo())) {
                    errores.add("El campo 'permisosNuevo' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getRazonSocialNuevo())) {
                    errores.add("El campo 'razonSocialNuevo' es requerido.");
                }

                if (dominioItem.getRetenido() == null) {
                    errores.add("El campo 'retenido' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getRutaNuevo())) {
                    errores.add("El campo 'rutaNuevo' es requerido.");
                }


                if (StringUtils.isBlank(dominioItem.getServiciosNuevo())) {
                    errores.add("El campo 'serviciosNuevo' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getTacografoNroNuevo())) {
                    errores.add("El campo 'tacografoNroNuevo' es requerido.");
                }

                if (dominioItem.getTipo() == null) {
                    errores.add("El campo 'tipo' es requerido.");
                }

                if (StringUtils.isBlank(dominioItem.getUbicacion())) {
                    errores.add("El campo 'ubicacion' es requerido.");
                }


                if (StringUtils.isBlank(dominioItem.getVigenciaHastaInspeccionTecnicaNuevo())) {
                    errores.add("El campo 'vigenciaHastaInspeccionTecnicaNuevo' es requerido.");
                }


                if (StringUtils.isBlank(dominioItem.getVigenciaHastaPolizaNuevo())) {
                    errores.add("El campo 'vigenciaHastaPolizaNuevo' es requerido.");
                }

                if (dominioItem.getAcarreado() == null) {
                    errores.add("El campo 'acarreado' es requerido.");
                }

            }

            if (!errores.isEmpty()) {
                String mensajeError = String.join("\n", errores);
                throw new IllegalArgumentException(mensajeError);
            }

        } else {
            throw new IllegalArgumentException("FiscalizacionDominios es requerido ");
        }
    }

    protected void validarCamposCargasRegisterDevice(RegisterDeviceRequest registerDeviceRequest) {
        registerDevicesValidator.validarRegisterDevice(registerDeviceRequest);
    }


    protected void validarCamposCargasCabecera(FiscalizacionDTORequest fiscalizacionDTO) {
        List<String> errores = new ArrayList<>();

        if (fiscalizacionDTO.getIdLogin() == null) {
            errores.add("El campo 'idLogin' es obligatorio.");
        }

        if (fiscalizacionDTO.getIdDelegacion() == null) {
            errores.add("El campo 'idDelegacion' es obligatorio.");
        }

        if (fiscalizacionDTO.getIdLugar() != null) {
            errores.add("El campo 'idLugar' no es requerido.");
        }

        if (StringUtils.isBlank(fiscalizacionDTO.getNumeroOperativo())) {
            errores.add("El campo 'numeroOperativo' es obligatorio.");
        }

        if (fiscalizacionDTO.getIdVehiculoCNRT() == null) {
            errores.add("El campo 'idVehiculoCNRT' es obligatorio.");
        }

        if (fiscalizacionDTO.getIdResultado() == null) {
            errores.add("El campo 'idResultado' es obligatorio.");
        }

        if (StringUtils.isBlank(fiscalizacionDTO.getFechaCierre())) {
            errores.add("El campo 'fechaCierre' es obligatorio.");
        }

        // La fecha de saneamiento viene si todas las infracciones estan saneadas
        List<FiscalizacionInfraccionDTORequest> infraccionesList = fiscalizacionDTO.getFiscalizacionInfraccionesList();

        boolean todasSaneadas = infraccionesList.stream()
                .allMatch(infraccion -> infraccion.getIdEstadoFiscaInfraccion() != null && infraccion.getIdEstadoFiscaInfraccion() == ConstantCommonUtil.ESTADO_INFRACCION_SANEADA);

        if (todasSaneadas) {
            // Todas las infracciones están saneadas
            if (StringUtils.isBlank(fiscalizacionDTO.getFechaSaneamiento())) {
                errores.add("El campo 'fechaSaneamiento' es obligatorio si todas las infracciones están saneadas.");
            }
        } else {
            // No todas las infracciones están saneadas
            if (StringUtils.isNotBlank(fiscalizacionDTO.getFechaSaneamiento())) {
                errores.add("La fecha de saneamiento no debe proporcionarse si no todas las infracciones están saneadas.");
            }

        }

        if (fiscalizacionDTO.getIdResultado() == ConstantCommonUtil.ACTA_RETENER) {
            if (StringUtils.isBlank(fiscalizacionDTO.getFechaRetencion())) {
                errores.add("El campo 'fechaRetencion' es obligatorio.");
            }
        } else {
            if (StringUtils.isNotBlank(fiscalizacionDTO.getFechaRetencion())) {
                errores.add("El campo 'fechaRetencion' no debe proporcionarse si el vehiculo no fue retenido.");
            }
        }

        if (fiscalizacionDTO.getIdServicio() == null) {
            errores.add("El campo 'idServicio' es obligatorio.");
        }

        if (fiscalizacionDTO.getListaPasajeros() != null) {
            errores.add("El campo 'listaPasajeros' debe ser nulo.");
        }

        if (StringUtils.isBlank(fiscalizacionDTO.getLatitud())) {
            errores.add("El campo 'latitud' es obligatorio.");
        }

        if (StringUtils.isBlank(fiscalizacionDTO.getLongitud())) {
            errores.add("El campo 'longitud' es obligatorio.");
        }

        if (StringUtils.isBlank(fiscalizacionDTO.getOsi())) {
            errores.add("El campo 'osi' es obligatorio.");
        }

        if (fiscalizacionDTO.getIdResultado() != ConstantCommonUtil.FISCALIZACION_OK) {
            if (StringUtils.isBlank(fiscalizacionDTO.getActa())) {
                errores.add("El campo 'acta' es obligatorio.");
            }
        } else {
            if (fiscalizacionDTO.getActa() != null) {
                errores.add("El campo 'acta' no debe proporcionarse si la fiscalizacion no tuvo infracciones");
            }
        }


        if (StringUtils.isBlank(fiscalizacionDTO.getFechaCreacion())) {
            errores.add("El campo 'fechaCreacion' es obligatorio.");
        }

        if (fiscalizacionDTO.getIdLocalidadOrigen() == null) {
            errores.add("El campo 'idLocalidadOrigen' es obligatorio.");
        }

        if (fiscalizacionDTO.getIdLocalidadDestino() == null) {
            errores.add("El campo 'idLocalidadDestino' es obligatorio.");
        }


        if (fiscalizacionDTO.getIdOperativoApi() == null) {
            errores.add("El campo 'idOperativoApi' es obligatorio.");
        }

        if (fiscalizacionDTO.getType() == null) {
            errores.add("El campo 'type' es obligatorio.");
        }

        if (!errores.isEmpty()) {
            String mensajeError = String.join("\n", errores);
            throw new IllegalArgumentException(mensajeError);
        }
    }


    protected void validarCamposCargasInfracciones(List<FiscalizacionInfraccionDTORequest> fiscalizacionInfraccionesList) {
        List<String> errores = new ArrayList<>();

        if (fiscalizacionInfraccionesList != null && fiscalizacionInfraccionesList.size() > 0) {
            for (FiscalizacionInfraccionDTORequest infraccion : fiscalizacionInfraccionesList) {

                if (infraccion.getIdFaltaServicio() == null) {
                    errores.add("El campo 'idFaltaServicio' es obligatorio para cada infracción.");
                }

                Optional<FaltaServicio> falta = faltaServicioRepository.findById(infraccion.getIdFaltaServicio());

                if (!falta.isPresent()) {
                    errores.add("La falta servicio no existe");
                }

                if (falta.isPresent() && falta.get().getAplicaCantidad() != null && falta.get().getAplicaCantidad()) {
                    if (infraccion.getCantidad() == null) {
                        errores.add("El campo 'cantidad' es obligatorio para cada infracción si aplica cantidad según la falta de servicio.");
                    }
                }

                if (falta.isPresent() && falta.get().getAplicaCantidad() != null && !falta.get().getAplicaCantidad()) {
                    if (infraccion.getCantidad() != null) {
                        errores.add("El campo 'cantidad' no debe estar presente para cada infracción si no aplica cantidad según la falta de servicio.");
                    }
                }

                if (falta.get().getPorDominio() == null || !falta.get().getPorDominio()) {
                    if (StringUtils.isNotBlank(infraccion.getDominio())) {
                        errores.add("El campo 'dominio' no debe estar presente para cada infracción si no aplica por dominio según la falta de servicio.");
                    }
                }

                if (falta.get().getRelativoChofer() == null || !falta.get().getRelativoChofer()) {
                    if (StringUtils.isNotBlank(infraccion.getDni())) {
                        errores.add("El campo 'dni' no debe estar presente para cada infracción si no aplica por relativo chofer según la falta de servicio.");
                    }
                }

                if (falta.get().getPorDominio()) {
                    if (StringUtils.isBlank(infraccion.getDominio())) {
                        errores.add("El campo 'dominio' debe estar presente para cada infracción si  aplica por dominio según la falta de servicio.");
                    }
                }

                if (falta.get().getRelativoChofer()) {
                    if (StringUtils.isBlank(infraccion.getDni())) {
                        errores.add("El campo 'dni' debe estar presente para cada infracción si  aplica por relativo chofer según la falta de servicio.");
                    }
                }

                if (infraccion.getIdItemInfraccion() == null) {
                    errores.add("El campo 'idItemInfraccion' es obligatorio para cada infracción.");
                }

                if (StringUtils.isBlank(infraccion.getOsi())) {
                    errores.add("El campo 'osi' es obligatorio para cada infracción.");
                }

                if (infraccion.getIdEstadoFiscaInfraccion() == null) {
                    errores.add("El campo 'idEstadoFiscaInfraccion' es obligatorio para cada infracción.");
                }


            }

            if (!errores.isEmpty()) {
                String mensajeError = String.join("\n", errores);
                throw new IllegalArgumentException(mensajeError);
            }
        }
    }

    protected void validarCamposCargasEmpresa(FiscalizacionEmpresaDTORequest fiscalizacionEmpresa) {
        List<String> errores = new ArrayList<>();

        if (StringUtils.isBlank(fiscalizacionEmpresa.getEmpresaNro())) {
            errores.add("El campo 'empresaNro' es obligatorio.");
        }

        if (StringUtils.isBlank(fiscalizacionEmpresa.getOsi())) {
            errores.add("El campo 'osi' es obligatorio.");
        }

        if (StringUtils.isBlank(fiscalizacionEmpresa.getNombreEmpresa())) {
            errores.add("El campo 'nombreEmpresa' es obligatorio.");
        }

        if (StringUtils.isBlank(fiscalizacionEmpresa.getCuit())) {
            errores.add("El campo 'cuit' es obligatorio.");
        }

        if (StringUtils.isBlank(fiscalizacionEmpresa.getDomicilio())) {
            errores.add("El campo 'domicilio' es obligatorio.");
        }

        if (fiscalizacionEmpresa.getNombreDenunciante() != null) {
            errores.add("El campo 'nombreDenunciante' debe ser nulo.");
        }

        if (fiscalizacionEmpresa.getDniDenunciante() != null) {
            errores.add("El campo 'dniDenunciante' debe ser nulo.");
        }

        if (fiscalizacionEmpresa.getNombreRepresentante() != null) {
            errores.add("El campo 'nombreRepresentante' debe ser nulo.");
        }

        if (fiscalizacionEmpresa.getDniRepresentante() != null) {
            errores.add("El campo 'dniRepresentante' debe ser nulo.");
        }

        if (fiscalizacionEmpresa.getNumeroReserva() != null) {
            errores.add("El campo 'numeroReserva' debe ser nulo.");
        }
        if (fiscalizacionEmpresa.getCategoriaServicio() != null) {
            errores.add("El campo 'categoriaServicio' debe ser nulo.");
        }
        if (fiscalizacionEmpresa.getFechaReserva() != null) {
            errores.add("El campo 'fechaReserva' debe ser nulo.");
        }

        if (!errores.isEmpty()) {
            String mensajeError = String.join("\n", errores);
            throw new IllegalArgumentException(mensajeError);
        }
    }

    protected void validarCamposCargasChoferes(List<FiscalizacionChoferDTORequest> fiscalizacionChoferesList) {
        List<String> errores = new ArrayList<>();

        if (fiscalizacionChoferesList != null && fiscalizacionChoferesList.size() > 0) {
            for (FiscalizacionChoferDTORequest chofer : fiscalizacionChoferesList) {
                if (chofer.getDni() == null || chofer.getDni().isBlank()) {
                    errores.add("El campo 'dni' no debe ser nulo ni vacío.");
                }
                if (chofer.getOrden() == null) {
                    errores.add("El campo 'orden' no debe ser nulo.");
                }
                if (chofer.getOsi() == null || chofer.getOsi().isBlank()) {
                    errores.add("El campo 'osi' no debe ser nulo ni vacío.");
                }
                if (chofer.getAlcoholemiaInsitu() == null) {
                    errores.add("El campo 'alcoholemiaInsitu' no debe ser nulo.");
                }
                if (chofer.getConductorNuevo() == null || chofer.getConductorNuevo().isBlank()) {
                    errores.add("El campo 'conductorNuevo' no debe ser nulo ni vacío.");
                }
                if (chofer.getEstadoNuevo() == null || chofer.getEstadoNuevo().isBlank()) {
                    errores.add("El campo 'estadoNuevo' no debe ser nulo ni vacío.");
                }
                if (chofer.getResultadoAlcoholemia() == null) {
                    errores.add("El campo 'resultadoAlcoholemia' no debe ser nulo.");
                }
                if (chofer.getResultadoAlcoholemiaInsitu() == null || chofer.getResultadoAlcoholemiaInsitu().isBlank()) {
                    errores.add("El campo 'resultadoAlcoholemiaInsitu' no debe ser nulo ni vacío.");
                }
                if (chofer.getResultadoFisca() == null) {
                    errores.add("El campo 'resultadoFisca' no debe ser nulo.");
                }
                if (chofer.getResultadoSustancias() == null) {
                    errores.add("El campo 'resultadoSustancias' no debe ser nulo.");
                }
                if (chofer.getResultadoSustanciasInsitu() == null || chofer.getResultadoSustanciasInsitu().isBlank()) {
                    errores.add("El campo 'resultadoSustanciasInsitu' no debe ser nulo ni vacío.");
                }
                if (chofer.getSustanciasInsitu() == null) {
                    errores.add("El campo 'sustanciasInsitu' no debe ser nulo.");
                }

                if (chofer.getVtoCNuevo() == null || chofer.getVtoCNuevo().isBlank()) {
                    errores.add("El campo 'vtoCNuevo' no debe ser nulo ni vacío.");
                }


                if (chofer.getVtoGNuevo() == null || chofer.getVtoGNuevo().isBlank()) {
                    errores.add("El campo 'vtoGNuevo' no debe ser nulo ni vacío.");
                }


            }

            if (!errores.isEmpty()) {
                String mensajeError = String.join("\n", errores);
                throw new IllegalArgumentException(mensajeError);
            }
        } else {
            throw new IllegalArgumentException("FiscalizacionDominios es requerido ");
        }

    }

}
