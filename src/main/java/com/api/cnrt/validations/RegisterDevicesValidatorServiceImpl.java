package com.api.cnrt.validations;

import com.api.cnrt.api.user.model.RegisterDeviceRequest;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

@Service
public class RegisterDevicesValidatorServiceImpl {

    public void validarRegisterDevice(RegisterDeviceRequest registerDeviceRequest){

        if(registerDeviceRequest == null){
            throw new IllegalArgumentException("Register device object es obligatorio.");
        }

        if(Strings.isBlank(registerDeviceRequest.getUUID())){
            throw new IllegalArgumentException("El campo UUID es obligatorio.");
        }

        if(Strings.isBlank(registerDeviceRequest.getModel())){
            throw new IllegalArgumentException("El campo model es obligatorio.");
        }

        if(Strings.isBlank(registerDeviceRequest.getPlatform())){
            throw new IllegalArgumentException("El campo platform es obligatorio.");
        }

        if(Strings.isBlank(registerDeviceRequest.getVersion())){
            throw new IllegalArgumentException("El campo version es obligatorio.");
        }

        if(Strings.isBlank(registerDeviceRequest.getApkVersion())){
            throw new IllegalArgumentException("El campo apkVersion es obligatorio.");
        }

        if(Strings.isBlank(registerDeviceRequest.getLongitud())){
            throw new IllegalArgumentException("El campo longitud es obligatorio.");
        }

        if(Strings.isBlank(registerDeviceRequest.getLatitud())){
            throw new IllegalArgumentException("El campo latitud es obligatorio.");
        }

    }
}
