package com.api.cnrt.validations;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ActionValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ActionValidation  {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
