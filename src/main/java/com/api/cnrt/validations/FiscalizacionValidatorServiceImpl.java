package com.api.cnrt.validations;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.api.cnrt.common.CommonUtils;
import com.api.cnrt.common.ConstantCommonUtil;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionDTORequest;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionEmpresaDTORequest;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionInfraccionDTORequest;
import com.api.cnrt.fiscalizaciones.model.TipoFiscalizacionEnum;
import com.api.cnrt.fiscalizaciones.service.IFiscalizacionValidatorService;
import com.api.cnrt.maestros.model.FaltaServicio;
import com.api.cnrt.maestros.repository.IFaltaServicioRepository;

import java.util.List;
import java.util.Optional;

@Service
public class FiscalizacionValidatorServiceImpl implements IFiscalizacionValidatorService {
    private final IFaltaServicioRepository faltaServicioRepository;
    private final CargasFiscalizacionValidatorServiceImpl cargasFiscalizacionValidator;
    private final PasajerosFiscalizacionValidatorServiceImpl pasajerosFiscalizacionValidator;

    public FiscalizacionValidatorServiceImpl(IFaltaServicioRepository faltaServicioRepository, CargasFiscalizacionValidatorServiceImpl cargasFiscalizacionValidator, PasajerosFiscalizacionValidatorServiceImpl pasajerosFiscalizacionValidator) {
        this.faltaServicioRepository = faltaServicioRepository;
        this.cargasFiscalizacionValidator = cargasFiscalizacionValidator;
        this.pasajerosFiscalizacionValidator = pasajerosFiscalizacionValidator;
    }

    @Override
    public void validarFiscalizacion(FiscalizacionDTORequest fiscalizacionDTO, TipoFiscalizacionEnum tipoFiscalizacionEnum) {
        if (tipoFiscalizacionEnum == null) {
            throw new IllegalArgumentException("Tipo de fiscalización no puede ser nulo.");
        }

        switch (tipoFiscalizacionEnum) {
            case PASAJEROS:
                validarCamposPasajeros(fiscalizacionDTO);
                break;
            case CARGAS:
                validarCamposCargas(fiscalizacionDTO);
                break;
            case EMPRESA:
                validarCamposEmpresas(fiscalizacionDTO);
                break;
            case CALIDAD:
                validarCamposCalidad(fiscalizacionDTO);
                break;
            case COLEGIOS:
                validarCamposColegios(fiscalizacionDTO);
            default:
                throw new IllegalArgumentException("Tipo de fiscalizacion invalido.");
        }
    }

    private void validarCamposColegios(FiscalizacionDTORequest fiscalizacionDTO) {
        if (fiscalizacionDTO == null) {
            throw new IllegalArgumentException("Para fiscalización de tipo 'COLEGIOS', se requiere la información de la cabecera.");
        }
    }


    private void validarCamposCargas(FiscalizacionDTORequest fiscalizacionDTO) {
        if (fiscalizacionDTO == null) {
            throw new IllegalArgumentException("Para fiscalización de tipo 'CARGA', se requiere la información de cabecera.");
        }
        cargasFiscalizacionValidator.validarFiscalizacion(fiscalizacionDTO);

       }

    private void validarCamposEmpresas(FiscalizacionDTORequest fiscalizacionDTO) {
        if (fiscalizacionDTO == null) {
            throw new IllegalArgumentException("Para fiscalización de tipo 'EMPRESA', se requiere la información de cabecera.");
        }
    }

    private void validarCamposCalidad(FiscalizacionDTORequest fiscalizacionDTO) {
        if (fiscalizacionDTO == null) {
            throw new IllegalArgumentException("Para fiscalización de tipo 'CALIDAD', se requiere la información de la cabecera.");
        }
    }


    private void validarCamposPasajeros(FiscalizacionDTORequest fiscalizacionDTO) {
        if (fiscalizacionDTO == null) {
            throw new IllegalArgumentException("Para fiscalización de tipo 'PASAJEROS', se requiere la información de cabecera.");
        }

        pasajerosFiscalizacionValidator.validarFiscalizacion(fiscalizacionDTO);
    }

}
