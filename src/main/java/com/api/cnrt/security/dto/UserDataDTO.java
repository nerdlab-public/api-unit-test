package com.api.cnrt.security.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UserDataDTO {
    @NotBlank(message = "Username obligatorio")
	private String username;
    @NotBlank(message = "Password obligatorio")
    private String pass;

}
