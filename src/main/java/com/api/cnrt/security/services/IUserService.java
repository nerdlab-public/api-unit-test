package com.api.cnrt.security.services;

import com.api.cnrt.security.dto.UserDataDTO;
import com.api.cnrt.security.model.User;

public interface IUserService {
    
    User addUser(UserDataDTO user);

    User findBy(String username);

}
