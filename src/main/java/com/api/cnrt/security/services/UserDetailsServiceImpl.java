package com.api.cnrt.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.api.cnrt.security.dto.UserDataDTO;
import com.api.cnrt.security.model.User;
import com.api.cnrt.security.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService, IUserService {
	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

		return UserDetailsImpl.build(user);
	}
	
    @Override
    public User addUser(UserDataDTO userDTO) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPass()));

        return userRepository.save(user);
    }

	@Override
	public User findBy(String username) {
		User user = userRepository.findByUsername(username)
		.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
		return user;
	}

}


