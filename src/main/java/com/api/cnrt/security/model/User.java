package com.api.cnrt.security.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
@Table(name = "logins")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_logins")
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;


	@Column(name = "nombre")
	private String firstname;

	@NotBlank
	@Column(name = "apellido")
	private String lastname;

	private String email;

	@Column(name = "celular")
	private String mobilePhone;

	@Column(name = "delegacion")
	private String delegation;

	@Column(name = "calidad")
	private Integer quality;

	@Column(name= "id_perfiles")
	private Integer profileId;

	@NotBlank
	@Size(max = 120)
	private String pass;

	@Column(name= "id_app_devices")
	public Long idAppDevices;

/*	public int id_app_devices;
	public String pass;
	public String foto;
	public int activo;
	public String last_login;
	public java.sql.Date fecha_alta;
	public java.sql.Date fecha_modificacion;
	public String token;
	public long token_expired;
	public int id_blega_motivos;
	public long fecha_motivo;
	public int autoriza;
	public String dni;
	public java.sql.Date fmodif;
	public int passReset;
*/


	public User(String username, String password, String lastname) {
		this.username = username;
		this.pass = password;
		this.lastname = lastname;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return pass;
	}

	public void setPassword(String password) {
		this.pass = password;
	}

	public String getEmail() {
		return email;
	}


}