package com.api.cnrt.security.model;

public enum ERole {
	  
	  ROLE_USER,
	  ROLE_ADMIN
}
