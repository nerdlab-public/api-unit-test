package com.api.cnrt.security.controller;

import java.util.HashMap;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.api.cnrt.api.user.dto.RegisterDeviceDTO;
import com.api.cnrt.api.user.dto.UserDataDTO;
import com.api.cnrt.api.user.mapper.UserToUserDataDTOMapper;
import com.api.cnrt.api.user.model.ActionConstants;
import com.api.cnrt.api.user.model.AuthenticationAndRegistrationDeviceRequest;
import com.api.cnrt.api.user.model.AuthenticationAndRegistrationDeviceResponse;
import com.api.cnrt.api.user.model.SyncAppLog;
import com.api.cnrt.api.user.service.ISyncAppDeviceService;
import com.api.cnrt.api.user.service.ISyncAppLogService;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.security.model.JwtResponse;
import com.api.cnrt.security.model.User;
import com.api.cnrt.security.repository.UserRepository;
import com.api.cnrt.security.services.IUserService;
import com.api.cnrt.security.services.JwtUtilService;

import jakarta.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

	private final AuthenticationManager authenticationManager;
	private final UserDetailsService usuarioDetailsService;
	private final IUserService userService;
	private final JwtUtilService jwtUtilService;
	private final PasswordEncoder passwordEncoder;
	private final ISyncAppDeviceService syncAppDeviceService;
	private final ISyncAppLogService syncAppLogService;
	private final UserToUserDataDTOMapper userToUserDataDTOMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

	@GetMapping("/publico")
	public ResponseEntity<?> getMensajePublico() {
		var auth = SecurityContextHolder.getContext().getAuthentication();
		logger.info("Datos del Usuario: {}", auth.getPrincipal());
		logger.info("Datos de los Permisos {}", auth.getAuthorities());
		logger.info("Esta autenticado {}", auth.isAuthenticated());

		Map<String, String> mensaje = new HashMap<>();
		mensaje.put("contenido", "Hola. esto es publico");
		return ResponseEntity.ok(mensaje);
	}

	@PostMapping("/authenticate")
	public ResponseEntity<?> authenticate(@Valid @RequestBody AuthenticationAndRegistrationDeviceRequest request) throws EntityNotFoundException {

		// Autenticacion
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(),
				request.getPassword()));

		final UserDetails userDetails = usuarioDetailsService.loadUserByUsername(request.getUsername());
		final String jwt = jwtUtilService.generateToken(userDetails);

		// Registro dispositivo
		User user = userService.findBy(userDetails.getUsername());
		RegisterDeviceDTO dto = syncAppDeviceService.setLoggedDevice(request.getRegisterDeviceRequest(), user);

		// Registro evento en el log
		SyncAppLog log = syncAppLogService.createLog(ActionConstants.LOGIN_D, Long.valueOf(dto.getPrefijo()), user, null, null);
		syncAppLogService.saveLog(log);


		// Armado de respuesta
		AuthenticationAndRegistrationDeviceResponse response = AuthenticationAndRegistrationDeviceResponse.builder()
				.status("success")
				.message("Authenticacion y registro de dispositvo correcto")
				.token(new JwtResponse(jwt))
				.registerDeviceData(dto)
				.build();
		return ResponseEntity.ok(response);
	}
	

    //Exepcion para la validacion de los campos obligatorios
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
    
}
