package com.api.cnrt.fiscalizaciones.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.api.cnrt.fiscalizaciones.dto.OperativoDTORequest;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTOResponse;
import com.api.cnrt.fiscalizaciones.mapper.OperativoDTORequestToOperativoMapper;
import com.api.cnrt.fiscalizaciones.mapper.OperativoToOperativoDTOResponseMapper;
import com.api.cnrt.fiscalizaciones.model.Operativo;
import com.api.cnrt.fiscalizaciones.repository.IOperativoRepository;

@Service
@RequiredArgsConstructor
public class OperativoServiceImpl implements OperativoService{

    private final IOperativoRepository operativoRepository;
    private final OperativoDTORequestToOperativoMapper operativoDTORequestToOperativoMapper;
    private final OperativoToOperativoDTOResponseMapper operativoToOperativoDTOResponseMapper;
    @Override
    public OperativoDTOResponse guardarOperativo(OperativoDTORequest operativoDTO) {
        Operativo operativo = operativoDTORequestToOperativoMapper.map(operativoDTO);
        Operativo operativoGuardado = operativoRepository.save(operativo);
        return operativoToOperativoDTOResponseMapper.map(operativoGuardado);
    }

}
