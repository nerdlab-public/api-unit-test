package com.api.cnrt.fiscalizaciones.service;

import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTO;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTORequest;
import com.api.cnrt.security.model.User;

public interface IErrorSyncroService<T> {
    ErrorSyncroDTO saveErrorSyncro(ErrorSyncroDTORequest erroresDTORequest, User user, SyncAppDevice device) throws RepositoryException;
}
