package com.api.cnrt.fiscalizaciones.service;

import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.IllegalArgumentException;
import com.api.cnrt.fiscalizaciones.dto.SolucionErrorSyncroDTO;

public interface ISolucionErrorSyncroService<T> {
    SolucionErrorSyncroDTO markAsExecuted(SyncAppDevice request, Long solutionErrorId) throws EntityNotFoundException, IllegalArgumentException;
}
