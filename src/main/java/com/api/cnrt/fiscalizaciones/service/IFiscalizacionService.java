package com.api.cnrt.fiscalizaciones.service;

import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionDTORequest;

public interface IFiscalizacionService {
    void grabarFiscalizacion(FiscalizacionDTORequest fiscalizacionDTO) throws EntityNotFoundException, RepositoryException;
}
