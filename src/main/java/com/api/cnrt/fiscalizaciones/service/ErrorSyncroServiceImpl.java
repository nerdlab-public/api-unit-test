package com.api.cnrt.fiscalizaciones.service;

import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTO;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTORequest;
import com.api.cnrt.fiscalizaciones.mapper.ErrorSyncroDTORequestToErrorSyncroMapper;
import com.api.cnrt.fiscalizaciones.mapper.ErrorSyncroToErrorSyncroDTOMapper;
import com.api.cnrt.fiscalizaciones.model.ErrorSyncro;
import com.api.cnrt.fiscalizaciones.repository.IErrorSyncroRepository;
import com.api.cnrt.security.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ErrorSyncroServiceImpl implements IErrorSyncroService<ErrorSyncroDTO> {

    private final IErrorSyncroRepository errorSyncroRepository;
    private final ErrorSyncroDTORequestToErrorSyncroMapper errorSyncroDTORequestToErrorSyncroMapper;
    private final ErrorSyncroToErrorSyncroDTOMapper errorSyncroToErrorSyncroDTOMapper;

    @Override
    public ErrorSyncroDTO saveErrorSyncro(ErrorSyncroDTORequest erroresDTORequest, User user, SyncAppDevice device) throws RepositoryException {

        try {
            ErrorSyncro newErrorSyncro = errorSyncroDTORequestToErrorSyncroMapper.map(erroresDTORequest);
            newErrorSyncro.setIdDevice(device.getId());
            newErrorSyncro.setIdLogin(user.getId());
            ErrorSyncro createdErrorSyncro = errorSyncroRepository.save(newErrorSyncro);
            return errorSyncroToErrorSyncroDTOMapper.map(createdErrorSyncro);
        } catch (Exception e) {
            throw new RepositoryException("Error al grabar Error syncro", e);
        }
    }
}
