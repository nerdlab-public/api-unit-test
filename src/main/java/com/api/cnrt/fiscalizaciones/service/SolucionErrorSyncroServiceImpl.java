package com.api.cnrt.fiscalizaciones.service;

import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.IllegalArgumentException;
import com.api.cnrt.fiscalizaciones.dto.SolucionErrorSyncroDTO;
import com.api.cnrt.fiscalizaciones.mapper.SolucionErrorSyncroToSolucionErrorSyncroDTOMapper;
import com.api.cnrt.fiscalizaciones.model.SolucionErrorSyncro;
import com.api.cnrt.fiscalizaciones.repository.ISolucionErrorSyncroRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SolucionErrorSyncroServiceImpl implements ISolucionErrorSyncroService<SolucionErrorSyncroDTO> {

    private final ISolucionErrorSyncroRepository solucionErrorSyncroRepository;
    private final SolucionErrorSyncroToSolucionErrorSyncroDTOMapper mapper;


    public List<SolucionErrorSyncroDTO> getByDeviceId(Long deviceId) {
        List<SolucionErrorSyncro> solutions = solucionErrorSyncroRepository.findByDeviceId(deviceId);
        List<SolucionErrorSyncroDTO> solutionsDTO = solutions
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
        return solutionsDTO;
    }

    @Override
    public SolucionErrorSyncroDTO markAsExecuted(SyncAppDevice device, Long solutionErrorId) throws EntityNotFoundException, IllegalArgumentException {
        Optional<SolucionErrorSyncro> optionalSolucionErrorSyncro = solucionErrorSyncroRepository.findById(solutionErrorId);

        if(optionalSolucionErrorSyncro.isEmpty()){
            throw new EntityNotFoundException("Solución de error no encontrada con ID: " + solutionErrorId);
        }

        SolucionErrorSyncro solucionErrorSyncro = optionalSolucionErrorSyncro.get();

        // Verifica que el dispositivo coincida antes de marcar como ejecutado
        if (!Objects.equals(solucionErrorSyncro.getDeviceId(), device.getId())) {
            throw new IllegalArgumentException("El ID del dispositivo no coincide con la solución de error proporcionada");
        }

        solucionErrorSyncro.setExecuted(true);
        SolucionErrorSyncro solucionError = solucionErrorSyncroRepository.save(solucionErrorSyncro);
        SolucionErrorSyncroDTO solucionErrorSyncroDTO = mapper.map(solucionError);
        return solucionErrorSyncroDTO;
    }
}
