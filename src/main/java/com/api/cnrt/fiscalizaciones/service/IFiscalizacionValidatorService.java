package com.api.cnrt.fiscalizaciones.service;

import com.api.cnrt.fiscalizaciones.dto.FiscalizacionDTORequest;
import com.api.cnrt.fiscalizaciones.model.TipoFiscalizacionEnum;

public interface IFiscalizacionValidatorService {
    void validarFiscalizacion(FiscalizacionDTORequest fiscalizacionDTO, TipoFiscalizacionEnum tipoFiscalizacionEnum);
}