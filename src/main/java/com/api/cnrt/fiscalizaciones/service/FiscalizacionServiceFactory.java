package com.api.cnrt.fiscalizaciones.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import com.api.cnrt.fiscalizaciones.model.TipoFiscalizacionEnum;

@Component
@RequiredArgsConstructor
public class FiscalizacionServiceFactory {

    private final FiscalizacionPasajerosServiceImpl fiscalizacionPasajerosService;

    private final FiscalizacionCargasServiceImpl fiscalizacionCargasService;

    private final FiscalizacionCalidadServiceImpl fiscalizacionCalidadService;

    private final FiscalizacionEmpresaServiceImpl fiscalizacionEmpresaService;

    private final FiscalizacionColegiosServiceImpl fiscalizacionColegiosService;

    public IFiscalizacionService getService(TipoFiscalizacionEnum tipo) {
        switch (tipo) {
            case PASAJEROS:
                return fiscalizacionPasajerosService;
            case CARGAS:
                return fiscalizacionCargasService;
            case CALIDAD:
                return fiscalizacionCalidadService;
            case EMPRESA:
                return fiscalizacionEmpresaService;
            case COLEGIOS:
                return fiscalizacionColegiosService;
            default:
                throw new IllegalArgumentException("Tipo de fiscalización no soportado: " + tipo);
        }
    }

}
