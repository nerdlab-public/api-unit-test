package com.api.cnrt.fiscalizaciones.service;

import com.api.cnrt.fiscalizaciones.dto.OperativoDTORequest;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTOResponse;

public interface OperativoService {

    OperativoDTOResponse guardarOperativo(OperativoDTORequest operativoDTO);
}
