package com.api.cnrt.fiscalizaciones.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.api.user.repository.ISyncAppDeviceRepository;
import com.api.cnrt.common.ConstantCommonUtil;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionDTORequest;
import com.api.cnrt.fiscalizaciones.mapper.*;
import com.api.cnrt.fiscalizaciones.model.*;
import com.api.cnrt.fiscalizaciones.repository.*;
import com.api.cnrt.maestros.model.*;
import com.api.cnrt.maestros.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FiscalizacionPasajerosServiceImpl implements IFiscalizacionService {
    private final IOperativoRepository operativoRepository;
    private final FiscalizacionDTOPasajerosRequestToFiscalizacionMapper fiscalizacionDTORequestToFiscalizacionMapper;
    private final IFiscalizacionRepository fiscalizacionRepository;

    private final FiscalizacionChoferPasajerosDTORequestToFiscalizacionChoferMapper fiscalizacionChoferDTORequestToFiscalizacionChoferMapper;
    private final IFiscalizacionChoferRepository fiscalizacionChoferRepository;

    private final FiscalizacionDominioPasajerosDTORequestToFiscalizacionDominioMapper fiscalizacionDominioDTORequestToFiscalizacionDominioMapper;
    private final IFiscalizacionDominioRepository fiscalizacionDominioRepository;

    private final FiscalizacionEmpresaPasajerosDTORequestToFiscalizacionEmpresaMapper fiscalizacionEmpresaDTORequestToFiscalizacionEmpresaMapper;
    private final IFiscalizacionEmpresaRepository fiscalizacionEmpresaRepository;

    private final FiscalizacionInfraccionPasajerosDTORequestToFiscalizacionInfraccionMapper fiscalizacionInfraccionDTORequestToFiscalizacionInfraccionMapper;
    private final IFiscalizacionInfraccionRepository fiscalizacionInfraccionRepository;

    private final OrdenServicioServiceImpl ordenServicioService;


    private final OrdenServicioOldServiceImpl ordenServicioOldService;

    private final AlertaChoferServiceImpl alertaChoferService;

    private final LicenciaServiceImpl licenciaService;

    private final VehiculoCargaHabilitadoServiceImpl vehiculoCargaHabilitadoService;

    private final ISyncAppDeviceRepository syncAppDeviceRepository;


    @Override
    @Transactional
    public void grabarFiscalizacion(FiscalizacionDTORequest fiscalizacionDTORequest) throws RepositoryException, EntityNotFoundException {
        // El operativo ya fue cargado y obtengo el idLugar, esto es por si lo cargo manual
        Operativo operativo = operativoRepository.findById(fiscalizacionDTORequest.getIdOperativoApi())
                .orElseThrow(()->new RepositoryException("No se encontro operativo con id: "+ fiscalizacionDTORequest.getIdOperativoApi()));

        fiscalizacionDTORequest.setIdLugar(operativo.getLugar().getId());
        // Cabecera
        Fiscalizacion fiscaCabeceraSaved = grabarFiscalizacionCabecera(fiscalizacionDTORequest);

        // choferes
        List<FiscalizacionChofer> fiscaChoferesList =  grabarFiscaChoferes(fiscalizacionDTORequest, fiscaCabeceraSaved);

       // dominios
        List<FiscalizacionDominio> fiscaDominiosList = grabarFiscaDominios(fiscalizacionDTORequest, fiscaCabeceraSaved);

        // infracciones
        List<FiscalizacionInfraccion> fiscaInfracionList = grabarFiscaInfracciones(fiscalizacionDTORequest, fiscaCabeceraSaved);

        // empresa
        FiscalizacionEmpresa fiscaEmpresa = grabarFiscalizacionEmpresa(fiscalizacionDTORequest, fiscaCabeceraSaved);

        //obtengo las infracciones que generan orden de servicio, filtro por dominio distinto de null
        List<FiscalizacionInfraccion> infraccionToOrdenServicioList = ordenServicioService.generaOrdenServicio(fiscaInfracionList, fiscaCabeceraSaved);


        if (!infraccionToOrdenServicioList.isEmpty()) {
            // Si hay nuevas infracciones, busca las órdenes de servicio existentes. (busca por dominio y fecha de cierre != null)
            List<OrdenServicio> existingOsiList = ordenServicioService.buscarOrdenServicioExistente(fiscaDominiosList.get(0));

            if (!existingOsiList.isEmpty()) {
                // Si hay órdenes de servicio existentes, cierra las anteriores y carga las nuevas
                OrdenServicio createdOsi =  ordenServicioService.cerrarYCrearOrdenesServicio(existingOsiList, fiscaCabeceraSaved, infraccionToOrdenServicioList, fiscaDominiosList.get(0).getDominio());
            } else {
                // Si no hay órdenes de servicio nuevas existentes, debo buscar en las ordenes servicio old
                List<OrdenServicioOld> existingOsiOldList = ordenServicioOldService.buscarOrdenServicioOldExistente(fiscaDominiosList.get(0));
                if (!existingOsiOldList.isEmpty()) {
                    //si hay ordenes de servicio old debo cerrarlas y crear nuevas en ordendeservicio
                    OrdenServicio createdOsi = ordenServicioOldService.cerrarYCrearOrdenesServicioDesdeOld(existingOsiOldList, fiscaCabeceraSaved, infraccionToOrdenServicioList, fiscaDominiosList.get(0).getDominio());
                }else{
                    // sini existe orden de servicio, ni tampoco orden servicio old solo creo  la orden de servicio nueva
                    OrdenServicio createdOsi = ordenServicioService.crearNuevaOrdenServicio(fiscaDominiosList.get(0).getDominio(), infraccionToOrdenServicioList, fiscaCabeceraSaved, null);
                }
            }
        } else {
            // Si no hay nuevas infracciones, cierra la última orden de servicio (si la hay)
            List<OrdenServicio> existingOsiListToClose = ordenServicioService.buscarOrdenServicioExistente(fiscaDominiosList.get(0));
            if (!existingOsiListToClose.isEmpty()) {
                List<OrdenServicio> closedOsi  = ordenServicioService.cerrarOrdenesServicio(existingOsiListToClose, fiscaCabeceraSaved.getOsi());
            }else{
                List<OrdenServicioOld> existingOsiOLDListToClose = ordenServicioOldService.buscarOrdenServicioOldExistente(fiscaDominiosList.get(0));
                if (!existingOsiOLDListToClose.isEmpty()) {
                    List<OrdenServicioOld> closedOsiOld  = ordenServicioOldService.cerrarOrdenesServicio(existingOsiOLDListToClose, fiscaCabeceraSaved.getOsi());
                }
            }
        }

        // Busca por dni en lista de infracciones.
        List<FiscalizacionInfraccion> infraccionToAlertaChoferList = ordenServicioService.generaAlertaChofer(fiscaInfracionList, fiscaCabeceraSaved);

        if(!infraccionToAlertaChoferList.isEmpty()){
            try {
                List<AlertaChofer> alertaChoferCreated =  alertaChoferService.updateAlertaChofer(infraccionToAlertaChoferList);
            } catch (RepositoryException e) {
                throw new RepositoryException("Alerta chofer no pudo grabarse", e);
            }
        }

        // Actualizo tabla licencias

        List<Licencia> licenciasChanged = licenciaService.updateLicencias(fiscaCabeceraSaved, fiscaChoferesList);

        //Actualizo tabla vehiculo cargas
        List<VehiculoCargaHabilitado> vehiculosChanged = vehiculoCargaHabilitadoService.updateVehiculosCargas(fiscaCabeceraSaved, fiscaDominiosList);

        SyncAppDevice existsDevice = syncAppDeviceRepository.findByUUID(fiscalizacionDTORequest.getRegisterDeviceRequest().getUUID());
        if(existsDevice != null){
            if (fiscaCabeceraSaved.getResultado().getId() != ConstantCommonUtil.FISCALIZACION_OK) {
                existsDevice.setNumActas(existsDevice.getNumActas() + 1);
            }

            existsDevice.setNumFis(existsDevice.getNumFis() + 1);
        }else {
            throw new EntityNotFoundException("El dispositivo no existe");
        }

    }

    private FiscalizacionEmpresa grabarFiscalizacionEmpresa(FiscalizacionDTORequest fiscalizacionDTORequest, Fiscalizacion fiscaCabeceraSaved) {
        FiscalizacionEmpresa fiscalizacionEmpresa = fiscalizacionEmpresaDTORequestToFiscalizacionEmpresaMapper.map(fiscalizacionDTORequest.getFiscalizacionEmpresa());
        fiscalizacionEmpresa.setIdFiscaCabecera(fiscaCabeceraSaved.getId());
        return fiscalizacionEmpresaRepository.save(fiscalizacionEmpresa);
    }

    private List<FiscalizacionInfraccion> grabarFiscaInfracciones(FiscalizacionDTORequest fiscalizacionDTORequest, Fiscalizacion fiscaCabeceraSaved) {

        List<FiscalizacionInfraccion> fiscalizacionInfraciones = new ArrayList<>();

        // Check for null before processing the list
        if (fiscalizacionDTORequest != null && fiscalizacionDTORequest.getFiscalizacionInfraccionesList() != null) {
            fiscalizacionInfraciones = fiscalizacionDTORequest.getFiscalizacionInfraccionesList()
                    .stream()
                    .map(fiscalizacionInfraccionDTORequestToFiscalizacionInfraccionMapper::map)
                    .collect(Collectors.toList());

            fiscalizacionInfraciones.forEach(infraccion -> infraccion.setIdFiscaCabecera(fiscaCabeceraSaved.getId()));
            fiscalizacionInfraccionRepository.saveAll(fiscalizacionInfraciones);
        }

        return fiscalizacionInfraciones;
    }

    private List<FiscalizacionDominio> grabarFiscaDominios(FiscalizacionDTORequest fiscalizacionDTORequest, Fiscalizacion fiscaCabeceraSaved) {
        List<FiscalizacionDominio> fiscalizacionDominios = fiscalizacionDTORequest.getFiscalizacionDominiosList()
                .stream()
                .map(fiscalizacionDominioDTORequestToFiscalizacionDominioMapper::map)
                .collect(Collectors.toList());
        fiscalizacionDominios.forEach(dominio -> dominio.setIdFiscaCabecera(fiscaCabeceraSaved.getId()));
        fiscalizacionDominioRepository.saveAll(fiscalizacionDominios);
        return fiscalizacionDominios;

    }

    private Fiscalizacion grabarFiscalizacionCabecera(FiscalizacionDTORequest fiscalizacionDTORequest) {
        Fiscalizacion fiscalizacionCabecera = fiscalizacionDTORequestToFiscalizacionMapper.map(fiscalizacionDTORequest);
        return fiscalizacionRepository.save(fiscalizacionCabecera);
    }

    private List<FiscalizacionChofer> grabarFiscaChoferes(FiscalizacionDTORequest fiscalizacionDTORequest, Fiscalizacion fiscaCabeceraSaved) {

        List<FiscalizacionChofer> fiscalizacionChoferes = fiscalizacionDTORequest.getFiscalizacionChoferesList()
                .stream()
                .map(fiscalizacionChoferDTORequestToFiscalizacionChoferMapper::map)
                .collect(Collectors.toList());
        fiscalizacionChoferes.forEach(chofer -> chofer.setIdFiscaCabecera(fiscaCabeceraSaved.getId()));
        fiscalizacionChoferRepository.saveAll(fiscalizacionChoferes);
        return fiscalizacionChoferes;
    }




}
