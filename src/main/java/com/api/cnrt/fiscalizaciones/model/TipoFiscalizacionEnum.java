package com.api.cnrt.fiscalizaciones.model;

public enum TipoFiscalizacionEnum {

    // AGREGO DESCONOCIDA  porque enum en java inician desde cero
    DESCONOCIDA,
    CARGAS,
    PASAJEROS,
    CALIDAD,
    EMPRESA,

    COLEGIOS;



}
