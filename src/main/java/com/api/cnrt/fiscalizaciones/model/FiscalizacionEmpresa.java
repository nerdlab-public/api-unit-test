package com.api.cnrt.fiscalizaciones.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "fisca_empresa")
public class FiscalizacionEmpresa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_fisca_cabecera")
    private Long idFiscaCabecera;

    @Column(name = "empresa_nro", length = 50)
    private String empresaNro;

    @Column(name = "osi", length = 20)
    private String osi;

    @Column(name = "nombre_empresa")
    private String nombreEmpresa;

    @Column(name = "cuit")
    private String cuit;

    @Column(name = "domicilio")
    private String domicilio;

    @Column(name = "nombre_denunciante")
    private String nombreDenunciante;

    @Column(name = "dni_denunciante")
    private String dniDenunciante;

    @Column(name = "nombre_representante")
    private String nombreRepresentante;

    @Column(name = "dni_representante")
    private String dniRepresentante;

    @Column(name = "numero_reserva")
    private String numeroReserva;

    @Column(name = "categoria_servicio")
    private String categoriaServicio;

    @Column(name = "fecha_reserva")
    private LocalDateTime fechaReserva;

}
