package com.api.cnrt.fiscalizaciones.model;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "fisca_infraccion")
public class FiscalizacionInfraccion {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "id_fisca_cabecera")
        private Long idFiscaCabecera;

        @Column(name = "id_item_infraccion")
        private Long idItemInfraccion;

        @Column(name = "osi", length = 20)
        private String osi;

        @Column(name = "observacion", length = 100)
        private String observacion;

        @Column(name = "cantidad")
        private Integer cantidad;

        @Column(name = "id_estado_fisca_infraccion")
        private Long idEstadoFiscaInfraccion;

        @Column(name = "id_falta_servicio")
        private Long idFaltaServicio;

        @Column(name = "dominio", length = 20)
        private String dominio;

        @Column(name = "dni", length = 20)
        private String dni;
    }

