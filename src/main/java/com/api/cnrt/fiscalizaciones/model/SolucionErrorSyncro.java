package com.api.cnrt.fiscalizaciones.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "soluciones_errores_sincronizacion")
public class SolucionErrorSyncro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 300, nullable = false)
    private String descripcion;

    @Column(name = "device_id", nullable = false)
    private Long deviceId;

    @Column(nullable = false)
    private String UUID;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String script;

    @Column(name = "created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime created;


    @Column(name = "executed", columnDefinition = "BOOLEAN DEFAULT FALSE")
    private boolean executed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "error_sincronizacion_id")
    private ErrorSyncro errorSincronizacion;

}
