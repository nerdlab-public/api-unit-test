package com.api.cnrt.fiscalizaciones.model;

import lombok.Data;

@Data
public class DescripcionFiscalizacionType {
    private String descripcion;

    public DescripcionFiscalizacionType(String descripcion) {
        this.descripcion = descripcion;
    }
}
