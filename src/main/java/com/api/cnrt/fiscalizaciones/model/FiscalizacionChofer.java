package com.api.cnrt.fiscalizaciones.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "fisca_chofer")
public class FiscalizacionChofer {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "id_fisca_cabecera")
        private Long idFiscaCabecera;

        @Column(name = "dni", length = 10)
        private String dni;

        @Column(name = "orden")
        private Integer orden;

        @Column(name = "osi", length = 255)
        private String osi;

        @Column(name = "alcoholemia_insitu")
        private Integer alcoholemiaInsitu;

        @Column(name = "conductor_original", length = 255)
        private String conductorOriginal;

        @Column(name = "conductor_nuevo", length = 255)
        private String conductorNuevo;

        @Column(name = "estado_original", length = 255)
        private String estadoOriginal;

        @Column(name = "estado_nuevo", length = 255)
        private String estadoNuevo;

        @Column(name = "fecha_alcoholemia_anterior")
        private LocalDateTime fechaAlcoholemiaAnterior;

        @Column(name = "fecha_fisca_anterior")
        private LocalDateTime fechaFiscaAnterior;

        @Column(name = "fecha_psicofisico")
        private LocalDateTime fechaPsicofisico;

        @Column(name = "fecha_sustancias_anterior")
        private LocalDateTime fechaSustanciasAnterior;

        @Column(name = "resultado_alcoholemia")
        private Integer resultadoAlcoholemia;

        @Column(name = "resultado_alcoholemia_insitu", length = 255)
        private String resultadoAlcoholemiaInsitu;

        @Column(name = "resultado_fisca")
        private Integer resultadoFisca;

        @Column(name = "resultado_psicofisico", length = 255)
        private String resultadoPsicofisico;

        @Column(name = "resultado_sustancias")
        private Integer resultadoSustancias;

        @Column(name = "resultado_sustancias_insitu", length = 255)
        private String resultadoSustanciasInsitu;

        @Column(name = "sustancias_insitu")
        private Integer sustanciasInsitu;

        @Column(name = "vto_c_original")
        private LocalDateTime vtoCOriginal;

        @Column(name = "vto_c_nuevo")
        private LocalDateTime vtoCNuevo;

        @Column(name = "vto_ca_original")
        private LocalDateTime vtoCAOriginal;

        @Column(name = "vto_ca_nuevo")
        private LocalDateTime vtoCANuevo;

        @Column(name = "vto_g_original")
        private LocalDateTime vtoGOriginal;

        @Column(name = "vto_g_nuevo")
        private LocalDateTime vtoGNuevo;

        @Column(name = "vto_p_original")
        private LocalDateTime vtoPOriginal;

        @Column(name = "vto_p_nuevo")
        private LocalDateTime vtoPNuevo;
    }

