package com.api.cnrt.fiscalizaciones.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import com.api.cnrt.maestros.model.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "fisca_cabecera")
public class Fiscalizacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_login")
    private Login login;

    @ManyToOne
    @JoinColumn(name = "id_delegacion")
    private Delegacion delegacion;

    @ManyToOne
    @JoinColumn(name = "id_lugar")
    private Lugar lugar;

    @Column(name = "numero_operatico")
    private String numeroOperatico;

    @ManyToOne
    @JoinColumn(name = "id_vehiculo_cnrt")
    private VehiculoCNRT vehiculoCNRT;

    @ManyToOne
    @JoinColumn(name = "id_resultado")
    private ResultadoFiscalizacion resultado;

    @Column(name = "fecha_alta")
    private LocalDateTime fechaAlta;

    @Column(name = "fecha_cierre")
    private LocalDateTime fechaCierre;

    @Column(name = "fecha_saneamiento")
    private LocalDateTime fechaSaneamiento;

    @Column(name = "fecha_retencion")
    private LocalDateTime fechaRetencion;

    @ManyToOne
    @JoinColumn(name = "id_servicio")
    private Servicio servicio;

    @Column(name = "lista_pasajeros")
    private String listaPasajeros;

    @ManyToOne
    @JoinColumn(name = "id_login_colaborador")
    private Login loginColaborador;

    @Column(name = "observacion", length = 500)
    private String observacion;

    @Column(name = "latitud")
    private String latitud;

    @Column(name = "longitud")
    private String longitud;

    @Column(name = "osi")
    private String osi;

    @Column(name = "acta")
    private String acta;

    @ManyToOne
    @JoinColumn(name = "id_no_retener")
    private MotivoNoRetener motivoNoRetener;

    @Column(name = "observacion_no_retener", length = 500)
    private String observacionNoRetener;

    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;

    @ManyToOne
    @JoinColumn(name = "id_localidad_origen")
    private Localidad localidadOrigen;

    @ManyToOne
    @JoinColumn(name = "id_localidad_destino")
    private Localidad localidadDestino;

    @ManyToOne
    @JoinColumn(name = "id_estado_viaje")
    private EstadoViaje estadoViaje;

    @ManyToOne
    @JoinColumn(name = "id_clase_modalidad")
    private ClaseModalidad claseModalidad;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type")
    private TipoFiscalizacionEnum type;

    @Transient
    private DescripcionFiscalizacionType informacionType;

    @ManyToOne
    @JoinColumn(name = "id_operativo_api")
    private Operativo operativo;

}
