package com.api.cnrt.fiscalizaciones.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.beanvalidation.OptionalValidatorFactoryBean;

import com.api.cnrt.maestros.model.Delegacion;
import com.api.cnrt.maestros.model.Localidad;
import com.api.cnrt.maestros.model.Lugar;
import com.api.cnrt.maestros.model.Provincia;

import java.time.LocalDateTime;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "operativos_telefono")
public class Operativo {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_operativo_telefono")
    private Long id;

    @Column(name = "id_operativo")
    private Long idOperativo;

    @Column(name = "nro_operativo")
    private String nroOperativo;

    @Column(name = "id_fiscalizador")
    private Long idFiscalizador;

    @ManyToOne
    @JoinColumn(name = "id_delegacion")
    private Delegacion delegacion;

    @Column(name = "id_vehiculo")
    private Long idVehiculo;

    @ManyToOne
    @JoinColumn(name = "id_provincia")
    private Provincia provincia;

    @ManyToOne
    @JoinColumn(name = "id_localidad")
    private Localidad localidad;

    @Column(name = "lugar", length = 1000)
    private String lugarDescripcion;

    @Column(name = "latitud", length = 100)
    private String latitud;

    @Column(name = "longitud", length = 100)
    private String longitud;

    @Column(name = "status")
    private Integer status;

    @Column(name = "fecha_alta")
    private String fechaAlta;

    @Column(name = "fecha_cierre")
    private String fechaCierre;

    @Column(name = "ts")
    private String ts;

    @Column(name = "fecha")
    private LocalDateTime fecha;

    @Column(name = "export")
    private Boolean export;

    @Column(name = "inicio_export")
    private LocalDateTime inicioExport;

    @Column(name = "fin_export")
    private LocalDateTime finExport;

    @Column(name = "device")
    private Long devicePrefijo;

    @ManyToOne
    @JoinColumn(name = "id_lugar")
    private Lugar lugar;

}
