package com.api.cnrt.fiscalizaciones.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "errores_sincronizacion")
public class ErrorSyncro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_device")
    private Long idDevice;

    @Column(name = "UUID")
    public String UUID;

    @Column(name = "serial")
    public String serial;

    @Column(name = "model")
    public String model;

    @Column(name = "platform")
    public String platform;

    @Column(name = "version")
    public String version;

    @Column(name = "apk_version")
    public String apkVersion;

    @Column(name = "longitud")
    public String longitud;

    @Column(name = "latitud")
    public String latitud;

    @Column(name = "id_login")
    private Long idLogin;

    @Column(name = "id_operativo")
    private Long idOperativo;

    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "fiscalizacion_dto_json", columnDefinition = "TEXT")
    private String fiscalizacionDTOJson;

    @Column(name = "operativo_dto_json", columnDefinition = "TEXT")
    private String operativoDTOJson;

    @Column(name = "mensaje_error", columnDefinition = "TEXT")
    private String mensajeError;

    @PrePersist
    protected void onCreate() {
        created = LocalDateTime.now();
    }
}
