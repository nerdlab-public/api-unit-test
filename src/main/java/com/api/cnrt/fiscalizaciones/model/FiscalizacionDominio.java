package com.api.cnrt.fiscalizaciones.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "fisca_dominio")
public class FiscalizacionDominio {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "id_fisca_cabecera")
        private Long idFiscaCabecera;

        @Column(name = "dominio", length = 20)
        private String dominio;

        @Column(name = "orden")
        private Integer orden;

        @Column(name = "numero_rto", length = 20)
        private String numeroRto;

        @Column(name = "numero_rto_nuevo", length = 20)
        private String numeroRtoNuevo;

        @Column(name = "aseguradora_original", length = 255)
        private String aseguradoraOriginal;

        @Column(name = "aseguradora_nuevo", length = 255)
        private String aseguradoraNuevo;

        @Column(name = "categoria_original", length = 255)
        private String categoriaOriginal;

        @Column(name = "categoria_nuevo", length = 255)
        private String categoriaNuevo;

        @Column(name = "cuit_original", length = 20)
        private String cuitOriginal;

        @Column(name = "cuit_nuevo", length = 20)
        private String cuitNuevo;

        @Column(name = "domicilio", length = 255)
        private String domicilio;

        @Column(name = "fecha_ultimo_rodillo")
        private LocalDateTime fechaUltimoRodillo;

        @Column(name = "id_localidad")
        private Long idLocalidad;

        @Column(name = "id_pais")
        private Long idPais;

        @Column(name = "id_provincia")
        private Long idProvincia;

        @Column(name = "interno_original", length = 255)
        private String internoOriginal;

        @Column(name = "interno_nuevo", length = 255)
        private String internoNuevo;

        @Column(name = "linea_original", length = 255)
        private String lineaOriginal;

        @Column(name = "linea_nuevo", length = 255)
        private String lineaNuevo;

        @Column(name = "nacionalidad", length = 255)
        private String nacionalidad;

        @Column(name = "nro_poliza_original", length = 255)
        private String nroPolizaOriginal;

        @Column(name = "nro_poliza_nuevo", length = 255)
        private String nroPolizaNuevo;

        @Column(name = "permisos_original", length = 255)
        private String permisosOriginal;

        @Column(name = "permisos_nuevo", length = 255)
        private String permisosNuevo;

        @Column(name = "razon_social_original", length = 255)
        private String razonSocialOriginal;

        @Column(name = "razon_social_nuevo", length = 255)
        private String razonSocialNuevo;

        @Column(name = "retenido")
        private Boolean retenido;

        @Column(name = "ruta_original", length = 255)
        private String rutaOriginal;

        @Column(name = "ruta_nuevo", length = 255)
        private String rutaNuevo;

        @Column(name = "servicios_original", length = 255)
        private String serviciosOriginal;

        @Column(name = "servicios_nuevo", length = 255)
        private String serviciosNuevo;

        @Column(name = "tacografo_nro_original", length = 255)
        private String tacografoNroOriginal;

        @Column(name = "tacografo_nro_nuevo", length = 255)
        private String tacografoNroNuevo;

        @Column(name = "tecnica_nro_original", length = 255)
        private String tecnicaNroOriginal;

        @Column(name = "tecnica_nro_nuevo", length = 255)
        private String tecnicaNroNuevo;

        @Column(name = "tipo")
        private Long tipo;

        @Column(name = "ubicacion", length = 255)
        private String ubicacion;

        @Column(name = "ultima_fisca_fecha")
        private LocalDateTime ultimaFiscaFecha;

        @Column(name = "ultima_fisca_resultado")
        private Integer ultimaFiscaResultado;

        @Column(name = "vigencia_hasta_inspeccion_tecnica_original")
        private LocalDateTime vigenciaHastaInspeccionTecnicaOriginal;

        @Column(name = "vigencia_hasta_inspeccion_tecnica_nuevo")
        private LocalDateTime vigenciaHastaInspeccionTecnicaNuevo;

        @Column(name = "vigencia_hasta_poliza_original")
        private LocalDateTime vigenciaHastaPolizaOriginal;

        @Column(name = "vigencia_hasta_poliza_nuevo")
        private LocalDateTime vigenciaHastaPolizaNuevo;

        @Column(name = "acarreado")
        private Boolean acarreado;

        @Column(name = "dom_acarreo")
        private String dominioAcarreo;

        @Column(name = "id_predio_retencion")
        private Long idPredioRetencion;


    }

