package com.api.cnrt.fiscalizaciones.repository;

import com.api.cnrt.fiscalizaciones.model.ErrorSyncro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IErrorSyncroRepository extends JpaRepository<ErrorSyncro, Long> {
}
