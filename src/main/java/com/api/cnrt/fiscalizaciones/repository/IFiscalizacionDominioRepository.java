package com.api.cnrt.fiscalizaciones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.cnrt.fiscalizaciones.model.FiscalizacionChofer;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionDominio;

public interface IFiscalizacionDominioRepository extends JpaRepository<FiscalizacionDominio, Long> {
}
