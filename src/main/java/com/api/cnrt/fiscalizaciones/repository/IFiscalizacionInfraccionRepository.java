package com.api.cnrt.fiscalizaciones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.cnrt.fiscalizaciones.model.FiscalizacionChofer;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionInfraccion;

public interface IFiscalizacionInfraccionRepository extends JpaRepository<FiscalizacionInfraccion, Long> {
}
