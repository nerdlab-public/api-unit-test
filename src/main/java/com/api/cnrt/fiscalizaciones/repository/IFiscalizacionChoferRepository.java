package com.api.cnrt.fiscalizaciones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.cnrt.fiscalizaciones.model.FiscalizacionChofer;

public interface IFiscalizacionChoferRepository extends JpaRepository<FiscalizacionChofer, Long> {
}
