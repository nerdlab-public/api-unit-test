package com.api.cnrt.fiscalizaciones.repository;

import com.api.cnrt.fiscalizaciones.model.SolucionErrorSyncro;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ISolucionErrorSyncroRepository extends JpaRepository<SolucionErrorSyncro, Long> {

    List<SolucionErrorSyncro> findByDeviceId(Long deviceId);
}
