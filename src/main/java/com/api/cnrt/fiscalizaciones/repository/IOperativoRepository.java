package com.api.cnrt.fiscalizaciones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.cnrt.fiscalizaciones.model.Operativo;

public interface IOperativoRepository extends JpaRepository<Operativo, Long> {
}