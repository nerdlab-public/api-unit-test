package com.api.cnrt.fiscalizaciones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.cnrt.fiscalizaciones.model.Fiscalizacion;
import com.api.cnrt.fiscalizaciones.model.Operativo;

public interface IFiscalizacionRepository extends JpaRepository<Fiscalizacion, Long> {
}
