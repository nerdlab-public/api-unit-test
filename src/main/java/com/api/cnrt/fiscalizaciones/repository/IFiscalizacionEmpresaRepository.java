package com.api.cnrt.fiscalizaciones.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.cnrt.fiscalizaciones.model.FiscalizacionChofer;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionEmpresa;

public interface IFiscalizacionEmpresaRepository extends JpaRepository<FiscalizacionEmpresa, Long> {
}
