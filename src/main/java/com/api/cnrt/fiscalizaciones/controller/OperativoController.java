package com.api.cnrt.fiscalizaciones.controller;

import com.api.cnrt.exceptions.BusinessException;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTORequest;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTOResponse;
import com.api.cnrt.fiscalizaciones.service.OperativoServiceImpl;
import com.api.cnrt.maestros.service.LocalidadServiceImpl;
import com.api.cnrt.maestros.service.LugarServiceImpl;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/operativo")
public class OperativoController {

    private final OperativoServiceImpl operativoService;
    private final LocalidadServiceImpl localidadService;
    private final LugarServiceImpl lugarService;

    public OperativoController(OperativoServiceImpl operativoService, LocalidadServiceImpl localidadService, LugarServiceImpl lugarService) {
        this.operativoService = operativoService;
        this.localidadService = localidadService;
        this.lugarService = lugarService;
    }

    @PostMapping("/grabar-operativo")
    @Transactional
    public ResponseEntity<?> guardarOperativo(@Valid @RequestBody OperativoDTORequest operativoDTO) throws BusinessException {
        boolean exists = localidadService.isIdLocalidadInProvincia(operativoDTO.getIdLocalidad(), operativoDTO.getIdProvincia());
        if (!exists) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("El idLocalidad no pertenece a la provincia especificada");
        }

        if(operativoDTO.getIdLugar() != null && operativoDTO.getLugarDescripcion() != null){
            throw new BusinessException("idLugar | lugarDescripcion es requerido, no ambos.");
        }

        if (operativoDTO.getIdLugar() == null && operativoDTO.getLugarDescripcion() != null) {
            Long nuevoIdLugar = lugarService.grabarNuevoLugar(operativoDTO.getLugarDescripcion(), operativoDTO.getIdLocalidad());
            operativoDTO.setIdLugar(nuevoIdLugar);
        }

        OperativoDTOResponse operativoGuardado = operativoService.guardarOperativo(operativoDTO);
        return new ResponseEntity<>(operativoGuardado, HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
