package com.api.cnrt.fiscalizaciones.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionDTORequest;
import com.api.cnrt.fiscalizaciones.model.TipoFiscalizacionEnum;
import com.api.cnrt.fiscalizaciones.service.FiscalizacionServiceFactory;
import com.api.cnrt.fiscalizaciones.service.IFiscalizacionService;
import com.api.cnrt.validations.FiscalizacionValidatorServiceImpl;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/fiscalizacion")
public class FiscalizacionController {

    private final FiscalizacionServiceFactory serviceFactory;
    private final FiscalizacionValidatorServiceImpl validatorService;

    public FiscalizacionController(FiscalizacionServiceFactory serviceFactory, FiscalizacionValidatorServiceImpl validatorService) {
        this.serviceFactory = serviceFactory;
        this.validatorService = validatorService;
    }

    @PostMapping("/grabar-fiscalizacion")
    public ResponseEntity<String> grabarFiscalizacion(@RequestBody FiscalizacionDTORequest fiscalizacionDTO) {
        try {
            TipoFiscalizacionEnum tipoFiscalizacion = TipoFiscalizacionEnum.values()[fiscalizacionDTO.getType()];
            // TODO: Controlar si isValid
            validatorService.validarFiscalizacion(fiscalizacionDTO, tipoFiscalizacion);
            IFiscalizacionService fiscalizacionService = serviceFactory.getService(tipoFiscalizacion);
            fiscalizacionService.grabarFiscalizacion(fiscalizacionDTO);
            return ResponseEntity.ok("Fiscalización grabada exitosamente.");
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body("Error: " + e.getMessage());
        } catch (RepositoryException e) {
            throw new RuntimeException(e);
        } catch (EntityNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
