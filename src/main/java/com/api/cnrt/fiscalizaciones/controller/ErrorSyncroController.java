package com.api.cnrt.fiscalizaciones.controller;

import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.api.user.service.ISyncAppDeviceService;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.IllegalArgumentException;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.dto.ConfirmExecutionSolutionErrorSyncroDTORequest;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTO;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTORequest;
import com.api.cnrt.fiscalizaciones.dto.SolucionErrorSyncroDTO;
import com.api.cnrt.fiscalizaciones.service.ErrorSyncroServiceImpl;
import com.api.cnrt.fiscalizaciones.service.SolucionErrorSyncroServiceImpl;
import com.api.cnrt.security.model.User;
import com.api.cnrt.security.services.IUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/sincro")
@RequiredArgsConstructor
public class ErrorSyncroController {

    private final ErrorSyncroServiceImpl errorSyncroService;
    private final SolucionErrorSyncroServiceImpl solucionErrorSyncroService;
    private final ISyncAppDeviceService syncAppDeviceService;
    private final IUserService userService;


    @PostMapping("/errores")
    public ResponseEntity<?> saveErrorSyncro(@Valid @RequestBody ErrorSyncroDTORequest erroresDTORequest) throws RepositoryException, EntityNotFoundException {

        if (erroresDTORequest.getFiscalizacion() != null || erroresDTORequest.getOperativo() != null) {
            String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
            User user = userService.findBy(username);

            SyncAppDevice device = syncAppDeviceService.findBy(erroresDTORequest.getRegisterDeviceRequest().getUUID());
            if(device == null){
                throw new EntityNotFoundException("Device no encontrado");
            }
            ErrorSyncroDTO errorCreated = errorSyncroService.saveErrorSyncro(erroresDTORequest, user, device);
            return ResponseEntity.status(HttpStatus.CREATED).body(errorCreated);
        } else {
            return ResponseEntity.badRequest().body("Error: ninguno de los DTOs está presente");
        }
    }

    @GetMapping("/scripts")
    public ResponseEntity<?> getScriptSolucionesErroresSincronizacion(@RequestParam("UUID") String uuid) {
        try {

            SyncAppDevice device = syncAppDeviceService.findBy(uuid);
            if (device == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Dispositivo no encontrado con UUID:" + uuid);
            }

            List<SolucionErrorSyncroDTO> errores = solucionErrorSyncroService.getByDeviceId(device.getId());
            if (errores.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("No tiene scripts para descargar");
            }

            return ResponseEntity.status(HttpStatus.FOUND).body(errores);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error al procesar la solicitud", e);
        }
    }

    @PutMapping("/scripts")
    public ResponseEntity<?> confirmQueryExecution(@RequestBody ConfirmExecutionSolutionErrorSyncroDTORequest confirm) throws EntityNotFoundException, IllegalArgumentException {
            SyncAppDevice device = syncAppDeviceService.findBy(confirm.getRegisterDeviceRequest().getUUID());
            SolucionErrorSyncroDTO solucionErrorSyncroDTO = solucionErrorSyncroService.markAsExecuted(device, confirm.getSolutionErrorId());
            return ResponseEntity.ok(solucionErrorSyncroDTO);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
