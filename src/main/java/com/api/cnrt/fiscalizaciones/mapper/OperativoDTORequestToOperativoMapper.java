package com.api.cnrt.fiscalizaciones.mapper;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTORequest;
import com.api.cnrt.fiscalizaciones.model.Operativo;
import com.api.cnrt.maestros.repository.IDelegacionRepository;
import com.api.cnrt.maestros.repository.ILocalidadRepository;
import com.api.cnrt.maestros.repository.ILugarRepository;
import com.api.cnrt.maestros.repository.IProvinciaRepository;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class OperativoDTORequestToOperativoMapper implements IMapper<OperativoDTORequest, Operativo> {

    private final IDelegacionRepository delegacionRepository;
    private final IProvinciaRepository provinciaRepository;
    private final ILocalidadRepository localidadRepository;
    private final ILugarRepository lugarRepository;

    @Override
    public Operativo map(OperativoDTORequest input) {
        Operativo operativo = new Operativo();
        operativo.setIdOperativo(input.getIdOperativo());
        operativo.setNroOperativo(input.getNroOperativo());
        operativo.setIdFiscalizador(input.getIdFiscalizador());
        operativo.setDelegacion(delegacionRepository.findById(input.getIdDelegacion())
                .orElseThrow(() -> new EntityNotFoundException("Delegacion no encontrada con ID: " + input.getIdDelegacion())));
        operativo.setIdVehiculo(input.getIdVehiculo());
        operativo.setProvincia(provinciaRepository.findById(input.getIdProvincia())
                .orElseThrow(() -> new EntityNotFoundException("Provincia no encontrada con ID: " + input.getIdProvincia())));
        operativo.setLocalidad(localidadRepository.findById(input.getIdLocalidad())
                .orElseThrow(() -> new EntityNotFoundException("Localidad no encontrada con ID: " + input.getIdLocalidad())));
        operativo.setLugarDescripcion(input.getLugarDescripcion());
        operativo.setLatitud(input.getLatitud());
        operativo.setLongitud(input.getLongitud());
        operativo.setStatus(input.getStatus());
        operativo.setFechaAlta(input.getFechaAlta());
        operativo.setFechaCierre(input.getFechaCierre());
       //TODO: VER esto, lo usaban antes para hacer un seguimiento del error, talvez ahora caresca de sentido.
        // operativo.setTs(input.getTs());
        operativo.setFecha(LocalDateTime.now());
        operativo.setDevicePrefijo(input.getDevicePrefijo());
        operativo.setLugar(lugarRepository.findById(input.getIdLugar())
                .orElseThrow(() -> new EntityNotFoundException("Lugar no encontrado con ID: " + input.getIdLugar())));
        return operativo;
    }
}
