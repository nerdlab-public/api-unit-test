package com.api.cnrt.fiscalizaciones.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionChoferDTORequest;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionChofer;

@Component
@RequiredArgsConstructor
public class FiscalizacionChoferPasajerosDTORequestToFiscalizacionChoferMapper implements IMapper<FiscalizacionChoferDTORequest, FiscalizacionChofer> {
    @Override
    public FiscalizacionChofer map(FiscalizacionChoferDTORequest in) {
        FiscalizacionChofer fiscalizacionChofer = new FiscalizacionChofer();
        fiscalizacionChofer.setDni(in.getDni());
        fiscalizacionChofer.setOrden(in.getOrden());
        fiscalizacionChofer.setOsi(in.getOsi());
        fiscalizacionChofer.setAlcoholemiaInsitu(in.getAlcoholemiaInsitu());
        fiscalizacionChofer.setConductorOriginal(in.getConductorOriginal());
        fiscalizacionChofer.setConductorNuevo(in.getConductorNuevo());
        fiscalizacionChofer.setEstadoOriginal(in.getEstadoOriginal());
        fiscalizacionChofer.setEstadoNuevo(in.getEstadoNuevo());
        fiscalizacionChofer.setResultadoAlcoholemia(in.getResultadoAlcoholemia());
        fiscalizacionChofer.setFechaAlcoholemiaAnterior(DateUtils.convertStringToLocalDateTime(in.getFechaAlcoholemiaAnterior()));
        fiscalizacionChofer.setFechaFiscaAnterior(DateUtils.convertStringToLocalDateTime(in.getFechaFiscaAnterior()));
        fiscalizacionChofer.setFechaPsicofisico(DateUtils.convertStringToLocalDateTime(in.getFechaPsicofisico()));
        fiscalizacionChofer.setFechaSustanciasAnterior(DateUtils.convertStringToLocalDateTime(in.getFechaSustanciasAnterior()));
        fiscalizacionChofer.setResultadoAlcoholemiaInsitu(in.getResultadoAlcoholemiaInsitu());
        fiscalizacionChofer.setResultadoFisca(in.getResultadoFisca());
        fiscalizacionChofer.setResultadoPsicofisico(in.getResultadoPsicofisico());
        fiscalizacionChofer.setResultadoSustancias(in.getResultadoSustancias());
        fiscalizacionChofer.setResultadoSustanciasInsitu(in.getResultadoSustanciasInsitu());
        fiscalizacionChofer.setSustanciasInsitu(in.getSustanciasInsitu());
        fiscalizacionChofer.setVtoPOriginal(DateUtils.convertStringToLocalDateTime(in.getVtoPOriginal()));
        fiscalizacionChofer.setVtoPNuevo(DateUtils.convertStringToLocalDateTime(in.getVtoPNuevo()));

        return fiscalizacionChofer;
    }
}
