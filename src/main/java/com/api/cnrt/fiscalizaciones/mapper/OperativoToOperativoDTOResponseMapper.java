package com.api.cnrt.fiscalizaciones.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTOResponse;
import com.api.cnrt.fiscalizaciones.model.Operativo;

@Component
public class OperativoToOperativoDTOResponseMapper implements IMapper<Operativo, OperativoDTOResponse> {

    @Override
    public OperativoDTOResponse map(Operativo in) {
      return OperativoDTOResponse.builder()
              .id(in.getId())
              .idOperativo(in.getIdOperativo())
              .nroOperativo(in.getNroOperativo())
              .idFiscalizador(in.getIdFiscalizador())
              .idDelegacion(in.getDelegacion().getId())
              .idVehiculo(in.getIdVehiculo())
              .idProvincia(in.getProvincia().getId())
              .idLocalidad(in.getLocalidad().getId())
              .lugarDescripcion(in.getLugarDescripcion())
              .latitud(in.getLatitud())
              .longitud(in.getLongitud())
              .status(in.getStatus())
              .fechaAlta(in.getFechaAlta())
              .fechaCierre(in.getFechaCierre())
              .ts(in.getTs())
              .devicePrefijo(in.getDevicePrefijo())
              .idLugar(in.getLugar().getId())
              .build();
    }
}
