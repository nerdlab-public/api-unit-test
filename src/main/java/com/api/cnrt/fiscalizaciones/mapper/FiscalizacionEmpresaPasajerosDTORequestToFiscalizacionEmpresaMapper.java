package com.api.cnrt.fiscalizaciones.mapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionEmpresaDTORequest;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionEmpresa;

@Component
public class FiscalizacionEmpresaPasajerosDTORequestToFiscalizacionEmpresaMapper implements IMapper<FiscalizacionEmpresaDTORequest, FiscalizacionEmpresa> {
    @Override
    public FiscalizacionEmpresa map(FiscalizacionEmpresaDTORequest in) {
        FiscalizacionEmpresa fiscalizacionEmpresa = new FiscalizacionEmpresa();
        fiscalizacionEmpresa.setEmpresaNro(in.getEmpresaNro());
        fiscalizacionEmpresa.setOsi(in.getOsi());
        fiscalizacionEmpresa.setNombreEmpresa(in.getNombreEmpresa());
        fiscalizacionEmpresa.setCuit(in.getCuit());
        fiscalizacionEmpresa.setDomicilio(in.getDomicilio());
        fiscalizacionEmpresa.setNombreDenunciante(in.getNombreDenunciante());
        fiscalizacionEmpresa.setDniDenunciante(in.getDniDenunciante());
        fiscalizacionEmpresa.setNombreRepresentante(in.getNombreRepresentante());
        fiscalizacionEmpresa.setDniRepresentante(in.getDniRepresentante());
        fiscalizacionEmpresa.setNumeroReserva(in.getNumeroReserva());
        fiscalizacionEmpresa.setCategoriaServicio(in.getCategoriaServicio());
        fiscalizacionEmpresa.setFechaReserva(StringUtils.isNotBlank(in.getFechaReserva())
                ? DateUtils.convertStringToLocalDateTime(in.getFechaReserva())
                : null);

        return fiscalizacionEmpresa;
    }
}
