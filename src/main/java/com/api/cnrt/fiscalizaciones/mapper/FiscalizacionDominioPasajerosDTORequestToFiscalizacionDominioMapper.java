package com.api.cnrt.fiscalizaciones.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionDominioDTORequest;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionDominio;

@Component
public class FiscalizacionDominioPasajerosDTORequestToFiscalizacionDominioMapper implements IMapper<FiscalizacionDominioDTORequest, FiscalizacionDominio> {
    @Override
    public FiscalizacionDominio map(FiscalizacionDominioDTORequest in) {
        FiscalizacionDominio fiscalizacionDominio = new FiscalizacionDominio();
        fiscalizacionDominio.setDominio(in.getDominio());
        fiscalizacionDominio.setOrden(in.getOrden());
        fiscalizacionDominio.setAseguradoraOriginal(in.getAseguradoraOriginal());
        fiscalizacionDominio.setAseguradoraNuevo(in.getAseguradoraNuevo());
        fiscalizacionDominio.setCategoriaOriginal(in.getCategoriaOriginal());
        fiscalizacionDominio.setCategoriaNuevo(in.getCategoriaNuevo());
        fiscalizacionDominio.setCuitOriginal(in.getCuitOriginal());
        fiscalizacionDominio.setCuitNuevo(in.getCuitNuevo());
        fiscalizacionDominio.setDomicilio(in.getDomicilio());
        fiscalizacionDominio.setFechaUltimoRodillo(DateUtils.convertStringToLocalDateTime(in.getFechaUltimoRodillo()));
        fiscalizacionDominio.setIdLocalidad(in.getIdLocalidad());
        fiscalizacionDominio.setIdPais(in.getIdPais());
        fiscalizacionDominio.setIdProvincia(in.getIdProvincia());
        fiscalizacionDominio.setInternoOriginal(in.getInternoOriginal());
        fiscalizacionDominio.setInternoNuevo(in.getInternoNuevo());
        fiscalizacionDominio.setLineaOriginal(in.getLineaOriginal());
        fiscalizacionDominio.setLineaNuevo(in.getLineaNuevo());
        fiscalizacionDominio.setNacionalidad(in.getNacionalidad());
        fiscalizacionDominio.setNroPolizaOriginal(in.getNroPolizaOriginal());
        fiscalizacionDominio.setNroPolizaNuevo(in.getNroPolizaNuevo());
        fiscalizacionDominio.setPermisosOriginal(in.getPermisosOriginal());
        fiscalizacionDominio.setPermisosNuevo(in.getPermisosNuevo());
        fiscalizacionDominio.setRazonSocialOriginal(in.getRazonSocialOriginal());
        fiscalizacionDominio.setRazonSocialNuevo(in.getRazonSocialNuevo());
        fiscalizacionDominio.setRetenido(in.getRetenido());
        fiscalizacionDominio.setServiciosOriginal(in.getServiciosOriginal());
        fiscalizacionDominio.setServiciosNuevo(in.getServiciosNuevo());
        fiscalizacionDominio.setTacografoNroOriginal(in.getTacografoNroOriginal());
        fiscalizacionDominio.setTacografoNroNuevo(in.getTacografoNroNuevo());
        fiscalizacionDominio.setTipo(in.getTipo());
        fiscalizacionDominio.setUbicacion(in.getUbicacion());
        fiscalizacionDominio.setUltimaFiscaFecha(DateUtils.convertStringToLocalDateTime(in.getUltimaFiscaFecha()));
        fiscalizacionDominio.setUltimaFiscaResultado(in.getUltimaFiscaResultado());
         fiscalizacionDominio.setVigenciaHastaPolizaOriginal(DateUtils.convertStringToLocalDateTime(in.getVigenciaHastaPolizaOriginal()));
        fiscalizacionDominio.setVigenciaHastaPolizaNuevo(DateUtils.convertStringToLocalDateTime(in.getVigenciaHastaPolizaNuevo()));
        fiscalizacionDominio.setAcarreado(in.getAcarreado());
        return fiscalizacionDominio;
    }
}
