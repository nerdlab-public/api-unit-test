package com.api.cnrt.fiscalizaciones.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionInfraccionDTORequest;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionInfraccion;

@Component
public class FiscalizacionInfraccionCargasDTORequestToFiscalizacionInfraccionMapper implements IMapper<FiscalizacionInfraccionDTORequest, FiscalizacionInfraccion> {
    @Override
    public FiscalizacionInfraccion map(FiscalizacionInfraccionDTORequest in) {
        FiscalizacionInfraccion fiscalizacionInfraccion = new FiscalizacionInfraccion();
        fiscalizacionInfraccion.setIdItemInfraccion(in.getIdItemInfraccion());
        fiscalizacionInfraccion.setOsi(in.getOsi());
        fiscalizacionInfraccion.setObservacion(in.getObservacion());
        fiscalizacionInfraccion.setCantidad(in.getCantidad());
        fiscalizacionInfraccion.setIdEstadoFiscaInfraccion(in.getIdEstadoFiscaInfraccion());
        fiscalizacionInfraccion.setIdFaltaServicio(in.getIdFaltaServicio());
        fiscalizacionInfraccion.setDominio(in.getDominio());
        fiscalizacionInfraccion.setDni(in.getDni());
        return fiscalizacionInfraccion;
    }
}
