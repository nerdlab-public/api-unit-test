package com.api.cnrt.fiscalizaciones.mapper;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.FiscalizacionDTORequest;
import com.api.cnrt.fiscalizaciones.model.Fiscalizacion;
import com.api.cnrt.fiscalizaciones.model.TipoFiscalizacionEnum;
import com.api.cnrt.fiscalizaciones.repository.IOperativoRepository;
import com.api.cnrt.maestros.repository.*;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
@RequiredArgsConstructor
public class FiscalizacionDTOPasajerosRequestToFiscalizacionMapper implements IMapper<FiscalizacionDTORequest, Fiscalizacion> {

    private final ILoginRepository loginRepository;
    private final IDelegacionRepository delegacionRepository;
    private final ILugarRepository lugarRepository;
    private final IVehiculoCNRTRepository vehiculoCNRTRepository;
    private final IResultadoFiscalizacionRepository resultadoFiscalizacionRepository;
    private final IServicioRepository servicioRepository;
    private final IMotivoNoRetenerRepository motivoNoRetenerRepository;
    private final ILocalidadRepository localidadRepository;
    private final IEstadoViajeRepository estadoViajeRepository;
    private final IClaseModalidadRepository claseModalidadRepository;
    private final IOperativoRepository operativoRepository;



    @Override
    public Fiscalizacion map(FiscalizacionDTORequest in) {
        Fiscalizacion fiscalizacion = new Fiscalizacion();
        fiscalizacion.setLogin(loginRepository.findById(in.getIdLogin())
                .orElseThrow(() -> new EntityNotFoundException("Login no encontrada con ID: " + in.getIdLogin())));
        fiscalizacion.setDelegacion(delegacionRepository.findById(in.getIdDelegacion())
                .orElseThrow(() -> new EntityNotFoundException("Delegacion no encontrada con ID: " + in.getIdDelegacion())));
        fiscalizacion.setLugar(lugarRepository.findById(in.getIdLugar())
                .orElseThrow(() -> new EntityNotFoundException("Lugar no encontrada con ID: " + in.getIdLugar())));
        fiscalizacion.setNumeroOperatico(in.getNumeroOperativo());
        fiscalizacion.setVehiculoCNRT(vehiculoCNRTRepository.findById(in.getIdVehiculoCNRT())
                .orElseThrow(() -> new EntityNotFoundException("VehiculoCNRT no encontrada con ID: " + in.getIdVehiculoCNRT())));
        fiscalizacion.setResultado(resultadoFiscalizacionRepository.findById(in.getIdResultado())
                .orElseThrow(() -> new EntityNotFoundException("Resultado Fiscalizacion no encontrada con ID: " + in.getIdResultado())));
        fiscalizacion.setFechaAlta(LocalDateTime.now(ZoneId.systemDefault()));
        fiscalizacion.setFechaCierre(DateUtils.convertStringToLocalDateTime(in.getFechaCierre()));
        fiscalizacion.setFechaSaneamiento(in.getFechaSaneamiento() != null ? DateUtils.convertStringToLocalDateTime(in.getFechaSaneamiento()) : null);
        fiscalizacion.setFechaRetencion(in.getFechaRetencion() != null ? DateUtils.convertStringToLocalDateTime(in.getFechaRetencion()) : null);
        fiscalizacion.setServicio(servicioRepository.findById(in.getIdServicio())
                .orElseThrow(() -> new EntityNotFoundException("Servicio no encontrada con ID: " + in.getIdServicio())));
        fiscalizacion.setListaPasajeros(in.getListaPasajeros());
        fiscalizacion.setObservacion(in.getObservacion());
        fiscalizacion.setLatitud(in.getLatitud());
        fiscalizacion.setLongitud(in.getLongitud());
        fiscalizacion.setOsi(in.getOsi());
        fiscalizacion.setActa(in.getActa());
        fiscalizacion.setMotivoNoRetener(in.getIdNoRetener() != null ?
                motivoNoRetenerRepository.findById(in.getIdNoRetener())
                        .orElseThrow(() -> new EntityNotFoundException("Motivo no encontrada con ID: " + in.getIdNoRetener())) : null);

        fiscalizacion.setObservacionNoRetener(in.getObservacionNoRetener() != null && !in.getObservacionNoRetener().isEmpty() ?
                in.getObservacionNoRetener() : null);

        fiscalizacion.setLoginColaborador(in.getIdLoginColaborador() != null ? loginRepository.findById(in.getIdLogin())
                .orElseThrow(() -> new EntityNotFoundException("Login no encontrada con ID: " + in.getIdLogin())) : null);
        fiscalizacion.setFechaCreacion(DateUtils.convertStringToLocalDateTime(in.getFechaCreacion()));
        fiscalizacion.setLocalidadOrigen(localidadRepository.findById(in.getIdLocalidadOrigen())
                .orElseThrow(() -> new EntityNotFoundException("Localidad origen no encontrada con ID: " + in.getIdLocalidadOrigen())));
        fiscalizacion.setLocalidadDestino(localidadRepository.findById(in.getIdLocalidadDestino())
                .orElseThrow(() -> new EntityNotFoundException("Localidad destino no encontrada con ID: " + in.getIdLocalidadDestino())));
        fiscalizacion.setEstadoViaje(estadoViajeRepository.findById(in.getIdEstadoViaje())
                .orElseThrow(() -> new EntityNotFoundException("Estado de Viaje no encontrada con ID: " + in.getIdEstadoViaje())));
        fiscalizacion.setClaseModalidad(claseModalidadRepository.findById(in.getIdClaseModalidad())
                .orElseThrow(() -> new EntityNotFoundException("Clase Modalidad no encontrada con ID: " + in.getIdClaseModalidad())));
        fiscalizacion.setType(TipoFiscalizacionEnum.values()[in.getType()]);
        fiscalizacion.setOperativo(operativoRepository.findById(in.getIdOperativoApi())
                .orElseThrow(() -> new EntityNotFoundException("Operativo no encontrada con ID: " + in.getIdOperativoApi())));
        return fiscalizacion;
    }
}
