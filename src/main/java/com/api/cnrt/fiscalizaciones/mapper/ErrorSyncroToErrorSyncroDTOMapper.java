package com.api.cnrt.fiscalizaciones.mapper;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTO;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTOResponse;
import com.api.cnrt.fiscalizaciones.model.ErrorSyncro;
import com.api.cnrt.fiscalizaciones.model.Operativo;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ErrorSyncroToErrorSyncroDTOMapper implements IMapper<ErrorSyncro, ErrorSyncroDTO> {

    @Override
    public ErrorSyncroDTO map(ErrorSyncro in) {
      return ErrorSyncroDTO.builder()
              .id(in.getId())
              .idDevice(in.getIdDevice())
              .UUID(in.getUUID())
              .serial(in.getSerial())
              .model(in.getModel())
              .platform(in.getPlatform())
              .version(in.getVersion())
              .apkVersion(in.getApkVersion())
              .longitud(in.getLongitud())
              .latitud(in.getLatitud())
              .idLogin(in.getIdLogin())
              .idOperativo(in.getIdOperativo())
              .created(in.getCreated())
              .fiscalizacionDTOJson(in.getFiscalizacionDTOJson())
              .operativoDTOJson(in.getOperativoDTOJson())
              .mensajeError(in.getMensajeError())
              .build();
    }
}