package com.api.cnrt.fiscalizaciones.mapper;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.ErrorSyncroDTORequest;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTORequest;
import com.api.cnrt.fiscalizaciones.model.ErrorSyncro;
import com.api.cnrt.fiscalizaciones.model.Operativo;
import com.api.cnrt.maestros.repository.IDelegacionRepository;
import com.api.cnrt.maestros.repository.ILocalidadRepository;
import com.api.cnrt.maestros.repository.ILugarRepository;
import com.api.cnrt.maestros.repository.IProvinciaRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class ErrorSyncroDTORequestToErrorSyncroMapper implements IMapper<ErrorSyncroDTORequest, ErrorSyncro> {

    private final ObjectMapper objectMapper;

    @Override
    public ErrorSyncro map(ErrorSyncroDTORequest input) {
        ErrorSyncro errorSyncro = new ErrorSyncro();
        errorSyncro.setUUID(input.getRegisterDeviceRequest().getUUID());
        errorSyncro.setSerial(input.getRegisterDeviceRequest().getSerial());
        errorSyncro.setModel(input.getRegisterDeviceRequest().getModel());
        errorSyncro.setPlatform(input.getRegisterDeviceRequest().getPlatform());
        errorSyncro.setVersion(input.getRegisterDeviceRequest().getVersion());
        errorSyncro.setApkVersion(input.getRegisterDeviceRequest().getApkVersion());
        errorSyncro.setLongitud(input.getRegisterDeviceRequest().getLongitud());
        errorSyncro.setLatitud(input.getRegisterDeviceRequest().getLatitud());
        if (input.getFiscalizacion() != null && input.getFiscalizacion().getIdOperativoApi() != null) {
            errorSyncro.setIdOperativo(input.getFiscalizacion().getIdOperativoApi());
        }
        try {
            if (input.getFiscalizacion() != null) {
                String fiscalizacionJson = objectMapper.writeValueAsString(input.getFiscalizacion());
                errorSyncro.setFiscalizacionDTOJson(fiscalizacionJson);
            }

            if (input.getOperativo() != null) {
                String operativoJson = objectMapper.writeValueAsString(input.getOperativo());
                errorSyncro.setOperativoDTOJson(operativoJson);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        errorSyncro.setMensajeError(input.getErrorMessages());
        return errorSyncro;
    }
}
