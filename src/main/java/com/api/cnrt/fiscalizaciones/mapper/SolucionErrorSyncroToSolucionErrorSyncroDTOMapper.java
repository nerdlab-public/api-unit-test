package com.api.cnrt.fiscalizaciones.mapper;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.fiscalizaciones.dto.OperativoDTOResponse;
import com.api.cnrt.fiscalizaciones.dto.SolucionErrorSyncroDTO;
import com.api.cnrt.fiscalizaciones.model.Operativo;
import com.api.cnrt.fiscalizaciones.model.SolucionErrorSyncro;
import org.springframework.stereotype.Component;

@Component
public class SolucionErrorSyncroToSolucionErrorSyncroDTOMapper implements IMapper<SolucionErrorSyncro, SolucionErrorSyncroDTO> {

    @Override
    public SolucionErrorSyncroDTO map(SolucionErrorSyncro in) {
      return SolucionErrorSyncroDTO.builder()
              .id(in.getId())
              .deviceId(in.getDeviceId())
              .UUID(in.getUUID())
              .created(in.getCreated())
              .descripcion(in.getDescripcion())
              .executed(in.isExecuted())
              .script(in.getScript())
              .errorSincronizacionId(in.getErrorSincronizacion().getId())
              .build();
    }
}
