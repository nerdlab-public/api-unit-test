package com.api.cnrt.fiscalizaciones.dto;

import com.api.cnrt.api.user.model.RegisterDeviceRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorSyncroDTORequest {

    private FiscalizacionDTORequest fiscalizacion;
    private OperativoDTORequest operativo;
    @Valid
    private RegisterDeviceRequest registerDeviceRequest;
    private String errorMessages;
}
