package com.api.cnrt.fiscalizaciones.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FiscalizacionChoferDTORequest {


    private String dni;


    private Integer orden;


    private String osi;


    private Integer alcoholemiaInsitu;


    private String conductorOriginal;


    private String conductorNuevo;


    private String estadoOriginal;


    private String estadoNuevo;


    private String fechaAlcoholemiaAnterior;


    private String fechaFiscaAnterior;


    private String fechaPsicofisico;


    private String fechaSustanciasAnterior;


    private Integer resultadoAlcoholemia;


    private String resultadoAlcoholemiaInsitu;


    private Integer resultadoFisca;


    private String resultadoPsicofisico;


    private Integer resultadoSustancias;


    private String resultadoSustanciasInsitu;


    private Integer sustanciasInsitu;


    private String vtoCOriginal;


    private String vtoCNuevo;

    private String vtoCAOriginal;


    private String vtoCANuevo;


    private String vtoGOriginal;


    private String vtoGNuevo;


    private String vtoPOriginal;


    private String vtoPNuevo;
}
