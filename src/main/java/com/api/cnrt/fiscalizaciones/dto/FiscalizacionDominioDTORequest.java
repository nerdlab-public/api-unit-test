package com.api.cnrt.fiscalizaciones.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FiscalizacionDominioDTORequest {

    private String dominio;
    private Integer orden;
    private String numeroRto;
    private String numeroRtoNuevo;
    private String aseguradoraOriginal;
    private String aseguradoraNuevo;
    private String categoriaOriginal;
    private String categoriaNuevo;
    private String cuitOriginal;
    private String cuitNuevo;
    private String domicilio;
    private String fechaUltimoRodillo;
    private Long idLocalidad;
    private Long idPais;
    private Long idProvincia;
    private String internoOriginal;
    private String internoNuevo;
    private String lineaOriginal;
    private String lineaNuevo;
    private String nacionalidad;
    private String nroPolizaOriginal;
    private String nroPolizaNuevo;
    private String permisosOriginal;
    private String permisosNuevo;
    private String razonSocialOriginal;
    private String razonSocialNuevo;
    private Boolean retenido;
    private String rutaOriginal;
    private String rutaNuevo;
    private String serviciosOriginal;
    private String serviciosNuevo;
    private String tacografoNroOriginal;
    private String tacografoNroNuevo;
    private String tecnicaNroOriginal;
    private String tecnicaNroNuevo;
    private Long tipo;
    private String ubicacion;
    private String ultimaFiscaFecha;
    private Integer ultimaFiscaResultado;
    private String vigenciaHastaInspeccionTecnicaOriginal;
    private String vigenciaHastaInspeccionTecnicaNuevo;
    private String vigenciaHastaPolizaOriginal;
    private String vigenciaHastaPolizaNuevo;
    private Boolean acarreado;
    private Long idPredioRetencion;
    private String dominioAcarreo;
}
