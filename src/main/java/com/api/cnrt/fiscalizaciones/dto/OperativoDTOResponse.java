package com.api.cnrt.fiscalizaciones.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OperativoDTOResponse {

    private Long id;
    private Long idOperativo;
    private String nroOperativo;
    private Long idFiscalizador;
    private Long idDelegacion;
    private Long idVehiculo;
    private Long idProvincia;
    private Long idLocalidad;
    private String lugarDescripcion;
    private String latitud;
    private String longitud;
    private Integer status;
    private String fechaAlta;
    private String fechaCierre;
    private String ts;
    private String fecha;
    private Long devicePrefijo;
    private Long idLugar;

}
