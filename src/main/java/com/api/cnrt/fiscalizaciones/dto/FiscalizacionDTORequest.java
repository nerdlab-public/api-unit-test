package com.api.cnrt.fiscalizaciones.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import com.api.cnrt.api.user.model.RegisterDeviceRequest;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FiscalizacionDTORequest {

    private Long idLogin;
    private Long idDelegacion;
    private Long idLugar;
    private String numeroOperativo;
    private Long idVehiculoCNRT;
    private Long idResultado;
    private String fechaAlta;
    private String fechaCierre;
    private String fechaSaneamiento;
    private String fechaRetencion;
    private Long idServicio;
    private String listaPasajeros;
    private Long idLoginColaborador;
    private String observacion;
    private String latitud;
    private String longitud;
    private String osi;
    private String acta;
    private Long idNoRetener;
    private String observacionNoRetener;
    private String fechaCreacion;
    private Long idLocalidadOrigen;
    private Long idLocalidadDestino;
    private Long idEstadoViaje;
    private Long idClaseModalidad;
    private Long idOperativoApi;
    private Integer type;
    private List<FiscalizacionDominioDTORequest> fiscalizacionDominiosList;
    private List<FiscalizacionChoferDTORequest> fiscalizacionChoferesList;
    private List<FiscalizacionInfraccionDTORequest> fiscalizacionInfraccionesList;
    private FiscalizacionEmpresaDTORequest fiscalizacionEmpresa;
    private RegisterDeviceRequest registerDeviceRequest;

}
