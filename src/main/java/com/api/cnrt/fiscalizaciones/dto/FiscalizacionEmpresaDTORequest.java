package com.api.cnrt.fiscalizaciones.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FiscalizacionEmpresaDTORequest {

    private String empresaNro;
    private String osi;
    private String nombreEmpresa;
    private String cuit;
    private String domicilio;
    private String nombreDenunciante;
    private String dniDenunciante;
    private String nombreRepresentante;
    private String dniRepresentante;
    private String numeroReserva;
    private String categoriaServicio;
    private String fechaReserva;
}
