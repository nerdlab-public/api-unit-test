package com.api.cnrt.fiscalizaciones.dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorSyncroDTO {

    private Long id;
    private Long idDevice;
    public String UUID;
    public String serial;
    public String model;
    public String platform;
    public String version;
    public String apkVersion;
    public String longitud;
    public String latitud;
    private Long idLogin;
    private Long idOperativo;
    private LocalDateTime created;
    private String fiscalizacionDTOJson;
    private String operativoDTOJson;
    private String mensajeError;

}
