package com.api.cnrt.fiscalizaciones.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FiscalizacionInfraccionDTORequest {

    private Long idItemInfraccion;
    private String osi;
    private String observacion;
    private Integer cantidad;
    private Long idEstadoFiscaInfraccion;
    private Long idFaltaServicio;
    private String dominio;
    private String dni;

}
