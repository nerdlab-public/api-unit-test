package com.api.cnrt.fiscalizaciones.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SolucionErrorSyncroDTO {

    private Long id;
    private String descripcion;
    private Long deviceId;
    private String UUID;
    private String script;
    private LocalDateTime created;
    private boolean executed;
    private Long errorSincronizacionId;
}
