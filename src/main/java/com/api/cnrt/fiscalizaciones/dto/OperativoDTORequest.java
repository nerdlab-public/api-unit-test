package com.api.cnrt.fiscalizaciones.dto;

import com.api.cnrt.maestros.dto.LocalidadDTO;
import com.api.cnrt.maestros.service.ILocalidadService;
import com.api.cnrt.maestros.service.LocalidadServiceImpl;

import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class OperativoDTORequest {

    private final LocalidadServiceImpl localidadService;


    @NotNull
    private Long idOperativo;

    @NotNull
    @NotBlank(message = "El campo no puede estar en blanco")
    private String nroOperativo;

    @NotNull
    private Long idFiscalizador;

    @NotNull
    private Long idDelegacion;

    @NotNull
    private Long idVehiculo;

    @NotNull
    private Long idProvincia;

    @NotNull
    private Long idLocalidad;

    private String lugarDescripcion;

    @NotNull
    @NotBlank(message = "El campo no puede estar en blanco")
    private String latitud;

    @NotNull
    @NotBlank(message = "El campo no puede estar en blanco")
    private String longitud;

    @NotNull
    private Integer status;

    @NotNull
    @NotBlank(message = "El campo no puede estar en blanco")
    private String fechaAlta;

    @NotNull
    @NotBlank(message = "El campo no puede estar en blanco")
    private String fechaCierre;


    @NotNull
    private Long devicePrefijo;

    private Long idLugar;


    @AssertTrue(message = "Si idLugar is nulo, lugarDescripcion debe tener un valor")
    private boolean isLugarDescripcionValid() {
        return idLugar != null || (lugarDescripcion != null && !lugarDescripcion.isEmpty());
    }

}
