package com.api.cnrt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

@Configuration
@OpenAPIDefinition

public class SwaggerConfig {
    
	@Bean
	public OpenAPI customOpenAPI () {
		return new OpenAPI().addSecurityItem(new SecurityRequirement().addList("Bearer Authentication"))
	            .components(new Components().addSecuritySchemes("Bearer Authentication", createAPIKeyScheme()))
				.info(new Info()
						.title("CNRT - API BACKEND")
						.version("0.1")
						.description("Api backend de la CNRT")
						.license(new License().name("Apache 2.0").url("http://springdoc.org")));
	}
	
    private SecurityScheme createAPIKeyScheme() {
        return new SecurityScheme().type(SecurityScheme.Type.HTTP)
            .bearerFormat("JWT")
            .scheme("bearer");
    }
}
