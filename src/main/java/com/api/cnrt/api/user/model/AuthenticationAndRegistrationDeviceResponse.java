package com.api.cnrt.api.user.model;

import com.api.cnrt.api.user.dto.RegisterDeviceDTO;
import com.api.cnrt.api.user.dto.UserDataDTO;
import com.api.cnrt.security.model.JwtResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenticationAndRegistrationDeviceResponse {
    private String status;
    private String message;
    private JwtResponse token;
    private RegisterDeviceDTO registerDeviceData; //Datos de dispositivo
}
