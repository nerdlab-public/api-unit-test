package com.api.cnrt.api.user.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "blega_sync_app_devices_tipos" )
public class TypeDevice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sync_app_devices_tipos")
    public Long id;

    @Column(name = "descripcion")
    public String description;

    @Column(name = "url")
    public String url;

}
