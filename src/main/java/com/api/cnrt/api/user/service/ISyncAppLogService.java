package com.api.cnrt.api.user.service;

import com.api.cnrt.api.user.model.SyncAppLog;
import com.api.cnrt.security.model.User;

public interface ISyncAppLogService {

    public SyncAppLog saveLog(SyncAppLog syncAppLog);
    public SyncAppLog createLog(String action, Long prefijo, User user, Long duracion, String datosSyncro);
}
