package com.api.cnrt.api.user.model;

public class ActionConstants {
    public static final String GET_MAESTROS_D = "get_Maestros_d";
    public static final String SINCRO_DB_D = "syncro_db_d";
    public static final String LOGIN_D = "login_d";
}
