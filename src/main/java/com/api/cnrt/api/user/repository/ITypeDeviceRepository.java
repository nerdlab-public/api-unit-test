package com.api.cnrt.api.user.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.cnrt.api.user.model.TypeDevice;

@Repository
public interface ITypeDeviceRepository extends JpaRepository<TypeDevice, Long> {
}
