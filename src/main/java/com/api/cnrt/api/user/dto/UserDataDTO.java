package com.api.cnrt.api.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDataDTO {

    private Long id;
    private String username;
    private String email;
    private String firstname;
    private String lastname;
    private Integer profileId;
    private String mobilePhone;
    private String delegation;
    private Integer quality;

}
