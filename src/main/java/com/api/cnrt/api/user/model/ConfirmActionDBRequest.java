package com.api.cnrt.api.user.model;

import com.api.cnrt.validations.ActionValidation;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfirmActionDBRequest {

    @NotNull(message = "El campo 'duración' no puede ser nulo ni estar en blanco")
    private Long duracion;

    @ActionValidation(message = "El campo 'action' no es válido")
    private String action;

    @NotNull(message = "El campo 'registerDeviceRequest' no es válido")
    @Valid
    private RegisterDeviceRequest registerDeviceRequest;

}
