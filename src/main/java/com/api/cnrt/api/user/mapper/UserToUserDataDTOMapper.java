package com.api.cnrt.api.user.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.api.user.dto.UserDataDTO;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.security.model.User;

@Component
public class UserToUserDataDTOMapper implements IMapper<User, UserDataDTO> {

    @Override
    public UserDataDTO map(User user) {
        
       return UserDataDTO.builder()
        		.id(user.getId())
        		.username(user.getUsername())
        		.email(user.getEmail())
        		.lastname(user.getLastname())
        		.firstname(user.getFirstname())
        		.profileId(user.getProfileId())
        		.delegation(user.getDelegation())
        		.quality(user.getQuality())
        		.mobilePhone(user.getMobilePhone())
        		.build();
        
    }
}

