package com.api.cnrt.api.user.controller;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.api.cnrt.api.user.dto.RegisterDeviceDTO;
import com.api.cnrt.api.user.model.*;
import com.api.cnrt.api.user.service.ISyncAppDeviceService;
import com.api.cnrt.api.user.service.ISyncAppLogService;
import com.api.cnrt.api.user.service.IUserDataService;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.security.model.User;
import com.api.cnrt.security.services.IUserService;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/register")
public class SyncAppDeviceController {


    private final IUserDataService userDataService;
    private final IUserService userService;
    private final ISyncAppDeviceService syncAppDeviceService;
    private final ISyncAppLogService syncAppLogService;

    public SyncAppDeviceController(IUserDataService userDataService, IUserService userService, ISyncAppDeviceService syncAppDeviceService, ISyncAppLogService syncAppLogService) {
        this.userDataService = userDataService;
        this.userService = userService;
        this.syncAppDeviceService = syncAppDeviceService;
        this.syncAppLogService = syncAppLogService;
    }


    @PostMapping("/confirm-action")
    public ResponseEntity<?> confirmAction(@Valid @RequestBody ConfirmActionDBRequest request) throws EntityNotFoundException {
        try {
        // obtengo usuario
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        User user = userService.findBy(username);

        RegisterDeviceDTO dto = null;

        // grabo el confirm de actualizacion de dispositivo
        if(ActionConstants.GET_MAESTROS_D.equals(request.getAction())){
            dto = syncAppDeviceService.setUpdatedDBDevice(request, user);
        }

        // grabo el confirm de sincronizacion de dispositivo
        if(ActionConstants.SINCRO_DB_D.equals(request.getAction())){
                dto = syncAppDeviceService.setSincroDBDevice(request, user);
        }


        // grabo log
        SyncAppLog log = syncAppLogService.createLog(request.getAction(), Long.valueOf(dto.getPrefijo()), user, request.getDuracion(), null);
        syncAppLogService.saveLog(log);

        // devuelvo response
        ConfirmActionDBResponse response = ConfirmActionDBResponse.builder()
                .status("success")
                .message("Se actualizo fecha de ultima "+request.getAction()+" de DB local exitosamente")
                .registerDeviceData(dto)
                .build();
        return ResponseEntity.ok(response);
        } catch (EntityNotFoundException e) {
            // Manejar la excepción específica EntityNotFoundException
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No se encontró la entidad solicitada");
        } catch (Exception e) {
            // Manejar otras excepciones no esperadas
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Se produjo un error interno");
        }
    }

    //Exepcion para la validacion de los campos obligatorios
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
