package com.api.cnrt.api.user.dto;

import com.api.cnrt.maestros.dto.PermisosPDADTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterDeviceDTO {

    private String numFis;
    private String numActa;
    private String prefijo;
    private Long lastUpdateDB;
    private UserDataDTO userDataDTO;
    private PermisosPDADTO permisosPDADTO;

}
