package com.api.cnrt.api.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.cnrt.api.user.model.SyncAppLog;

@Repository
public interface ISyncAppLogRepository extends JpaRepository<SyncAppLog, Long> {

}
