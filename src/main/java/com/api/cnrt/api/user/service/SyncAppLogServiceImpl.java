package com.api.cnrt.api.user.service;

import org.springframework.stereotype.Service;

import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.api.user.model.SyncAppLog;
import com.api.cnrt.api.user.repository.ISyncAppLogRepository;
import com.api.cnrt.common.CommonUtils;
import com.api.cnrt.security.model.User;

@Service
public class SyncAppLogServiceImpl implements ISyncAppLogService{

    private final ISyncAppLogRepository syncAppLogRepository;

    public SyncAppLogServiceImpl(ISyncAppLogRepository syncAppLogRepository) {
        this.syncAppLogRepository = syncAppLogRepository;
    }

    @Override
    public SyncAppLog saveLog(SyncAppLog syncAppLog) {
        return syncAppLogRepository.save(syncAppLog);
    }

    @Override
    public SyncAppLog createLog(String action,Long prefijo, User user, Long duracion, String datosSyncro) {
        SyncAppLog log = new SyncAppLog();
        log.setFechaSync(CommonUtils.getUnixTime());
        log.setToken("");
        log.setPrefijo(prefijo);
        log.setAccion(action);
        log.setIdLogins(user.getId());
        log.setDuracion(duracion);
        log.setDatosSincro(datosSyncro);
        return log;
    }


}
