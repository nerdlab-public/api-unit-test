package com.api.cnrt.api.user.service;

import com.api.cnrt.api.user.dto.UserDataDTO;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.RepositoryException;

public interface IUserDataService {
	
	 public UserDataDTO getUserDataByUsername(String username) throws EntityNotFoundException, RepositoryException;

}
