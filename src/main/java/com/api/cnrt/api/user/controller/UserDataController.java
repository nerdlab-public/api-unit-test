package com.api.cnrt.api.user.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.cnrt.api.user.dto.UserDataDTO;
import com.api.cnrt.api.user.service.IUserDataService;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.RepositoryException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("/api/user")
public class UserDataController {

    private final IUserDataService userDataService;

    public UserDataController(IUserDataService userDataService) {
      this.userDataService = userDataService;
    }
    
    //Ejemplo para customizar la documentacion
    @Operation(summary = "Obtiene datos de un usuario")
    @ApiResponses(value = { 
      @ApiResponse(responseCode = "200", description = "Ususario encontrado", 
        content = { @Content(mediaType = "application/json", 
          schema = @Schema(implementation = UserDataDTO.class)) }),
      @ApiResponse(responseCode = "400", description = "Datos invalidos", 
        content = @Content), 
      @ApiResponse(responseCode = "404", description = "Usuario no encontrado", 
        content = @Content) })
    
    @GetMapping("/user-detail")
    public UserDataDTO userData() throws EntityNotFoundException, RepositoryException{
    	//Obtenemos el username desde el contexto de autenticacion que construyo en una paso anterior el filtro (JwtRequestFilter)
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        UserDataDTO userDTO = userDataService.getUserDataByUsername(username);
        return ResponseEntity.ok(userDTO).getBody();
    }

}
