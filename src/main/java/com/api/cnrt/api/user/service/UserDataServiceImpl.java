package com.api.cnrt.api.user.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.api.cnrt.api.user.dto.UserDataDTO;
import com.api.cnrt.api.user.mapper.UserToUserDataDTOMapper;
import com.api.cnrt.api.user.repository.IUserDataRepository;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.security.model.User;

@Service
public class UserDataServiceImpl implements IUserDataService{
    
    private final IUserDataRepository userDataRepository;
    
    private final UserToUserDataDTOMapper userToUserDataDTOMapper;

    public UserDataServiceImpl(IUserDataRepository userDataRepository, UserToUserDataDTOMapper userToUserDataDTOMapper) {
      this.userDataRepository = userDataRepository;
      this.userToUserDataDTOMapper = userToUserDataDTOMapper;
    }
    
    @Override
	public UserDataDTO getUserDataByUsername(String username) throws EntityNotFoundException, RepositoryException {
    	Optional<User> user;
    	try {
    		user  = userDataRepository.findByUsername(username);
		} catch (Exception ex) {
			throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
		}
        
        if(!user.isPresent()){
            throw new EntityNotFoundException("User inexistente.");
        }
        
        UserDataDTO userData = userToUserDataDTOMapper.map(user.get());
        return userData;
	}



}


