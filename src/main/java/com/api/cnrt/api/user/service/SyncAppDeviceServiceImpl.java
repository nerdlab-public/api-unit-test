package com.api.cnrt.api.user.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.api.cnrt.api.user.dto.RegisterDeviceDTO;
import com.api.cnrt.api.user.dto.UserDataDTO;
import com.api.cnrt.api.user.mapper.UserToUserDataDTOMapper;
import com.api.cnrt.api.user.model.ConfirmActionDBRequest;
import com.api.cnrt.api.user.model.RegisterDeviceRequest;
import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.api.user.model.TypeDevice;
import com.api.cnrt.api.user.repository.ISyncAppDeviceRepository;
import com.api.cnrt.api.user.repository.ITypeDeviceRepository;
import com.api.cnrt.api.user.repository.IUserDataRepository;
import com.api.cnrt.common.CommonUtils;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.maestros.dto.PermisosPDADTO;
import com.api.cnrt.maestros.mapper.PermisosPDAToPermisosPDADTOMapper;
import com.api.cnrt.maestros.model.PermisosPDA;
import com.api.cnrt.maestros.repository.IPermisosPDARepository;
import com.api.cnrt.security.model.User;
import com.api.cnrt.security.repository.UserRepository;

@Service
public class SyncAppDeviceServiceImpl implements ISyncAppDeviceService {


    private final IUserDataRepository userDataRepository;

    private final ITypeDeviceRepository typeDeviceRepository;

    private final UserToUserDataDTOMapper userToUserDataDTOMapper;

    private final UserRepository userRepository;

    private final ISyncAppDeviceRepository syncAppDeviceRepository;

    private final IPermisosPDARepository permisosPDARepository;
    private final PermisosPDAToPermisosPDADTOMapper permisosPDAToPermisosPDADTOMapper;

    public SyncAppDeviceServiceImpl(IUserDataRepository userDataRepository, ITypeDeviceRepository typeDeviceRepository, UserToUserDataDTOMapper userToUserDataDTOMapper, UserRepository userRepository, ISyncAppDeviceRepository syncAppDeviceRepository, IPermisosPDARepository permisosPDARepository, PermisosPDAToPermisosPDADTOMapper permisosPDAToPermisosPDADTOMapper) {
        this.userDataRepository = userDataRepository;
        this.typeDeviceRepository = typeDeviceRepository;
        this.userToUserDataDTOMapper = userToUserDataDTOMapper;
        this.userRepository = userRepository;
        this.syncAppDeviceRepository = syncAppDeviceRepository;
        this.permisosPDARepository = permisosPDARepository;
        this.permisosPDAToPermisosPDADTOMapper = permisosPDAToPermisosPDADTOMapper;
    }


    @Override
    @Transactional
    public RegisterDeviceDTO setLoggedDevice(RegisterDeviceRequest registerDeviceRequest, User user) throws EntityNotFoundException {
        // BUSCA EL DISPOSITIVO POR UUID
        SyncAppDevice existsDevice = syncAppDeviceRepository.findByUUID(registerDeviceRequest.getUUID());
        SyncAppDevice device;
        if (existsDevice != null){
            device = updateLoggedDevice(existsDevice, registerDeviceRequest, user);
        }else{
            device = createLoggedDevice(registerDeviceRequest, user);
        }

        //actualiza tabla logins
        user.setIdAppDevices(device.getId());
        userRepository.save(user);
        
        UserDataDTO userDTO = userToUserDataDTOMapper.map(user);

        PermisosPDA permisosEntity = permisosPDARepository.findByIdLogin(user.getId());

        PermisosPDADTO permisosDTO = (permisosEntity != null) ? permisosPDAToPermisosPDADTOMapper.map(permisosEntity) : null;

        //DEVUELVE PREFIJO NUM_FIS NUM ACT MAS DATOS DEL USUARIO
        return RegisterDeviceDTO.builder()
        		.userDataDTO(userDTO)
        		.prefijo(device.getPrefijo())
        		.numActa(String.valueOf(device.getNumActas()))
        		.numFis(String.valueOf(device.getNumFis()))
                .lastUpdateDB(device.getFechaUpdatedDb())
                .permisosPDADTO(permisosDTO)
        		.build();
    }

    @Override
    @Transactional
    public RegisterDeviceDTO setUpdatedDBDevice(ConfirmActionDBRequest request, User user) throws EntityNotFoundException {
        SyncAppDevice existsDevice = syncAppDeviceRepository.findByUUID(request.getRegisterDeviceRequest().getUUID());
        SyncAppDevice device = updateUpdatedDBDevice(existsDevice, request.getRegisterDeviceRequest(), user);
        user.setIdAppDevices(device.getId());
        userRepository.save(user);
        UserDataDTO userDTO = userToUserDataDTOMapper.map(user);
        return RegisterDeviceDTO.builder()
                .userDataDTO(userDTO)
                .prefijo(device.getPrefijo())
                .numActa(String.valueOf(device.getNumActas()))
                .numFis(String.valueOf(device.getNumFis()))
                .lastUpdateDB(device.getFechaUpdatedDb())
                .build();
    }

    @Override
    @Transactional
    public RegisterDeviceDTO setSincroDBDevice(ConfirmActionDBRequest request, User user) {
        SyncAppDevice existsDevice = syncAppDeviceRepository.findByUUID(request.getRegisterDeviceRequest().getUUID());
        SyncAppDevice device = updateSincroDBDevice(existsDevice, request.getRegisterDeviceRequest(), user);
        user.setIdAppDevices(device.getId());
        userRepository.save(user);
        UserDataDTO userDTO = userToUserDataDTOMapper.map(user);
        return RegisterDeviceDTO.builder()
                .userDataDTO(userDTO)
                .prefijo(device.getPrefijo())
                .numActa(String.valueOf(device.getNumActas()))
                .numFis(String.valueOf(device.getNumFis()))
                .build();
    }

    @Override
    public SyncAppDevice findBy(String uuid) {
        return syncAppDeviceRepository.findByUUID(uuid);
    }


    private SyncAppDevice updateLoggedDevice(SyncAppDevice existsDevice, RegisterDeviceRequest registerDeviceRequest, User user) {
        Long unixTime = CommonUtils.getUnixTime();
        existsDevice.setModel(registerDeviceRequest.getModel());
        existsDevice.setSerial(registerDeviceRequest.getSerial());
        existsDevice.setPlatform(registerDeviceRequest.getPlatform());
        existsDevice.setVersion(registerDeviceRequest.getVersion());
        existsDevice.setIdLogins(user.getId());
        existsDevice.setFechaLogin(unixTime);
        existsDevice.setApkVersion(registerDeviceRequest.getApkVersion());
        existsDevice.setLng(registerDeviceRequest.getLongitud());
        existsDevice.setLat(registerDeviceRequest.getLatitud());
        return syncAppDeviceRepository.save(existsDevice);
    }




    private SyncAppDevice createLoggedDevice(RegisterDeviceRequest registerDeviceRequest, User user) throws EntityNotFoundException {

        Long unixTime = CommonUtils.getUnixTime();
        SyncAppDevice newDevice = new SyncAppDevice();
        newDevice.setDescri("Alta Automatica");
        newDevice.setModel(registerDeviceRequest.getModel());
        newDevice.setSerial(registerDeviceRequest.getSerial());
        newDevice.setUUID(registerDeviceRequest.getUUID());
        newDevice.setPlatform(registerDeviceRequest.getPlatform());
        newDevice.setVersion(registerDeviceRequest.getVersion());
        newDevice.setFechaAlta(unixTime);
        newDevice.setFechaLogin(unixTime);
        newDevice.setReset(0);
        newDevice.setEstado("0");
        newDevice.setNumFis(0);
        TypeDevice typeDevice = typeDeviceRepository.findById(1L)
                .orElseThrow(() -> new EntityNotFoundException("Type device not found"));
        newDevice.setTypeDevice(typeDevice);
        newDevice.setNumActas(0);
        newDevice.setIdLogins(user.getId());
        newDevice.setLng(registerDeviceRequest.getLongitud());
        newDevice.setLat(registerDeviceRequest.getLatitud());
        newDevice.setApkVersion(registerDeviceRequest.getApkVersion());
        SyncAppDevice savedDevice = syncAppDeviceRepository.save(newDevice);
        savedDevice.setPrefijo(CommonUtils.getPrefijo(savedDevice.getId()));
        return syncAppDeviceRepository.save(savedDevice);
    }
    private SyncAppDevice updateUpdatedDBDevice(SyncAppDevice existsDevice, RegisterDeviceRequest registerDeviceRequest, User user) {
        Long unixTime = CommonUtils.getUnixTime();
        existsDevice.setModel(registerDeviceRequest.getModel());
        existsDevice.setSerial(registerDeviceRequest.getSerial());
        existsDevice.setPlatform(registerDeviceRequest.getPlatform());
        existsDevice.setVersion(registerDeviceRequest.getVersion());
        existsDevice.setIdLogins(user.getId());
        existsDevice.setFechaUpdatedDb(unixTime);
        existsDevice.setApkVersion(registerDeviceRequest.getApkVersion());
        existsDevice.setLng(registerDeviceRequest.getLongitud());
        existsDevice.setLat(registerDeviceRequest.getLatitud());
        return syncAppDeviceRepository.save(existsDevice);
    }

    private SyncAppDevice updateSincroDBDevice(SyncAppDevice existsDevice, RegisterDeviceRequest registerDeviceRequest, User user) {
        Long unixTime = CommonUtils.getUnixTime();
        existsDevice.setModel(registerDeviceRequest.getModel());
        existsDevice.setSerial(registerDeviceRequest.getSerial());
        existsDevice.setPlatform(registerDeviceRequest.getPlatform());
        existsDevice.setVersion(registerDeviceRequest.getVersion());
        existsDevice.setIdLogins(user.getId());
        existsDevice.setFechaSincroDb(unixTime);
        existsDevice.setApkVersion(registerDeviceRequest.getApkVersion());
        existsDevice.setLng(registerDeviceRequest.getLongitud());
        existsDevice.setLat(registerDeviceRequest.getLatitud());
        return syncAppDeviceRepository.save(existsDevice);

    }

}
