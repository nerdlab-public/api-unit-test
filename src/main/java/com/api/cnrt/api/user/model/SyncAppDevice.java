package com.api.cnrt.api.user.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "blega_sync_app_devices")
public class SyncAppDevice {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id_app_devices")
        public Long id;

        @ManyToOne(optional = true)
        @JoinColumn(name = "id_app_devices_tipos")
        public TypeDevice typeDevice;

        @Column(name = "id_logins")
        public Long idLogins;

        @Column(name = "descri")
        public String descri;

        @Column(name = "model")
        public String model;

        @Column(name = "serial")
        public String serial;

        @Column(name = "UUID")
        public String UUID;

        @Column(name = "platform")
        public String platform;

        @Column(name = "version")
        public String version;

        @Column(name = "fecha_alta")
        public Long fechaAlta;

        @Column(name = "fecha_login")
        public Long fechaLogin;

        @Column(name = "fecha_sincro_db")
        public Long fechaSincroDb;

        @Column(name = "fecha_updated_db")
        public Long fechaUpdatedDb;

        @Column(name = "fecha_created_db")
        public Long fechaCreatedDb;

        @Column(name = "reset")
        public Integer reset;

        @Column(name = "prefijo")
        public String prefijo;

        @Column(name = "num_actas")
        public Integer numActas;

        @Column(name = "num_fis")
        public Integer numFis;

        @Column(name = "lng")
        public String lng;

        @Column(name = "lat")
        public String lat;

        @Column(name = "direccion")
        public String direccion;

        @Column(name = "id_organismo")
        public String idOrganismo;

        @Column(name = "provincia")
        public String provincia;

        @Column(name = "estado")
        public String estado;

        @Column(name = "apk_version")
        public String apkVersion;

        @Column(name = "db_version")
        public String dbVersion;

        @Column(name = "build")
        public String build;
}
