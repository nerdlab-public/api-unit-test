package com.api.cnrt.api.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.cnrt.api.user.model.SyncAppDevice;

@Repository
public interface ISyncAppDeviceRepository extends JpaRepository<SyncAppDevice, Long> {

    SyncAppDevice findByUUID(String UUID);

}
