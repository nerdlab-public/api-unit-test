package com.api.cnrt.api.user.model;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenticationAndRegistrationDeviceRequest {
    @NotBlank(message = "El campo 'username' no puede estar en blanco")
    private String username;
    @NotBlank(message = "El campo 'password' no puede estar en blanco")
    private String password;
    @Valid
    private RegisterDeviceRequest registerDeviceRequest;
}
