package com.api.cnrt.api.user.service;

import com.api.cnrt.api.user.dto.RegisterDeviceDTO;
import com.api.cnrt.api.user.model.ConfirmActionDBRequest;
import com.api.cnrt.api.user.model.RegisterDeviceRequest;
import com.api.cnrt.api.user.model.SyncAppDevice;
import com.api.cnrt.exceptions.EntityNotFoundException;
import com.api.cnrt.security.model.User;

public interface ISyncAppDeviceService {

    public RegisterDeviceDTO setLoggedDevice(RegisterDeviceRequest registerDeviceRequest, User user) throws EntityNotFoundException;
    public RegisterDeviceDTO setUpdatedDBDevice(ConfirmActionDBRequest request, User user) throws EntityNotFoundException;

    RegisterDeviceDTO setSincroDBDevice(ConfirmActionDBRequest request, User user);

    SyncAppDevice findBy(String uuid);
}
