package com.api.cnrt.api.user.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "blega_sync_logs")
public class SyncAppLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sync_logs")
    private Long id;

    @Column(name = "fecha_sync")
    private Long fechaSync;

    @Column(name = "token")
    private String token;

    @Column(name = "id_devices")
    private Long prefijo;

    @Column(name = "accion")
    private String accion;

    @Column(name = "id_logins")
    private Long idLogins;

    @Column(name = "duracion")
    private Long duracion;

    @Column(name = "datos_sincro", columnDefinition = "MEDIUMTEXT")
    private String datosSincro;

}