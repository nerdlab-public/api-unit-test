package com.api.cnrt.api.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterDeviceRequest {
    @JsonProperty("UUID")
    @NotNull(message = "El campo 'UUID' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'UUID' no puede estar en blanco")
    private String UUID;

    @NotNull(message = "El campo 'serial' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'serial' no puede estar en blanco")
    private String serial;

    @NotNull(message = "El campo 'model' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'model' no puede estar en blanco")
    private String model;

    @NotNull(message = "El campo 'platform' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'platform' no puede estar en blanco")
    private String platform;

    @NotNull(message = "El campo 'version' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'version' no puede estar en blanco")
    private String version;

    @NotNull(message = "El campo 'apk-version' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'apk-version' no puede estar en blanco")
    private String apkVersion;

    @NotNull(message = "El campo 'longitud' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'longitud' no puede estar en blanco")
    private String longitud;

    @NotNull(message = "El campo 'latitud' no puede estar en blanco ni ser nulo")
    @NotBlank(message = "El campo 'latitud' no puede estar en blanco")
    private String latitud;

}
