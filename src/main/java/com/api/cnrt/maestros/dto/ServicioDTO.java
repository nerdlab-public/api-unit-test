package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ServicioDTO {

    private Long id;
    private String servicio;
    private String tipo;
    private String abrev;
    private Long tipoOrdenControlId;
    private Long tipoTransporteId;
    private Long ambitoId;
    private Long claseModalidadId;
    private String colItems;
    private Integer listaPasajeros;
    private Integer conductor;
    private Integer contrato;
    private Integer declaracion;
    private Integer sisElectronico;
    private Integer comprobante;
    private Integer tacografo;
    private Integer activo;


}
