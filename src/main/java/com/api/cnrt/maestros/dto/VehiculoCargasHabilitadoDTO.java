package com.api.cnrt.maestros.dto;

import com.api.cnrt.maestros.model.CategoriaVehiculo;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VehiculoCargasHabilitadoDTO {


    private Long id;

    private String dominio;

    private String anioModelo;

    private String empresaNumero;

    private String paut;

    private String numeroChasis;

    private String cantidadEjes;

    private String tipoVehiculo;

    private String chasisMarca;

    private String tipoCarroceria;

    private String razonSocial;

    private String tipoDocumentoAbrev;

    private String numeroDocumento;

    private String vigenciaHastaInspeccionTecnica;

    private String tipoTecnica;

    private String tecnicaNumero;

    private String numeroRuta;

    private String fechaEmision;

    private String tacografoNumero;


    private Long categoriaVehiculoId;

    private String ultimaFiscaFecha;

    private Integer ultimaFiscaResultado;

    private Integer estado;

    private String permisos;

    private String pais;

}
