package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PermisosPDADTO {

    private Long id;
    private Long idLogins;
    private Boolean actaCalidad;
    private Boolean actaEmpresa;
    private Boolean actaPasajeros;
    private Boolean actaCargas;

}
