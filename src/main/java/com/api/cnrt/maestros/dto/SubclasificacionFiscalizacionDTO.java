package com.api.cnrt.maestros.dto;

import com.api.cnrt.maestros.model.ClasificacionFiscalizacion;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubclasificacionFiscalizacionDTO {

    private Long id;
    private String descripcion;
    private Long clasificacionFiscalizacionId;
    private Integer orden;
    private Integer ingresoManual;
}
