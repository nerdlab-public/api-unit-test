package com.api.cnrt.maestros.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CategoriaVehiculoDTO {

    private Long id;
    private String description;
    private String abrev;
    private Integer activo;
}
