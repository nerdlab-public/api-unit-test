package com.api.cnrt.maestros.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import com.api.cnrt.maestros.model.Delegacion;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginDTO {

    private Long idLogin;

    private Long idPerfil;

    private Long idAPPDevice;

    private String username;

    private String email;

    private String pass;

    private String nombre;

    private String apellido;

    private String celular;

    private Integer activo;

    private String lastLogin;

    private Integer autoriza;

    private String dni;

    private Long delegacionId;

    private Integer calidad;


}
