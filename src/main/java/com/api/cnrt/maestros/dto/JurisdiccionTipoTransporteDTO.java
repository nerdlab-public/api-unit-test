package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JurisdiccionTipoTransporteDTO {

    private Long id;
    private Long tipoTransporteId;
    private Long jurisdiccionId;

}
