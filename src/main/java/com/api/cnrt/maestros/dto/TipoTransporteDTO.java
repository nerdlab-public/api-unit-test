package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TipoTransporteDTO {

    private Long id;
    private String descripcion;
    private Integer activo;
    private Integer dominio1;
    private Integer dominio2;
    private Integer dominio3;
    private Integer chofer1;
    private Integer chofer2;
    private String abrev;
}
