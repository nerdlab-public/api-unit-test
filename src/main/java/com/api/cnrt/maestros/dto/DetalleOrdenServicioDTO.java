package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetalleOrdenServicioDTO {

    private Long id;

    private Long ordenServicioId;

    private String osi;

    private String observacion;

    private Integer cantidad;

    private Long idFaltaServicio;

}
