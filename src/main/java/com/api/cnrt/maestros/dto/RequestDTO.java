package com.api.cnrt.maestros.dto;

import org.springframework.beans.factory.annotation.Value;

import com.api.cnrt.validations.DateValidation;

import lombok.Data;

@Data
public class RequestDTO {

	@Value("false")
	private boolean initial;

	@DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.")
    private String lastDateDb;
}
