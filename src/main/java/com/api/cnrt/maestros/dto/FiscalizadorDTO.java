package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FiscalizadorDTO {

    private Long id;
    private String nombre;
    private String apellido;
    private String dni;
    private Integer autoriza;
    private Long delegacion;
    private Integer calidad;
    private Integer soloHistorico;
    private Integer activo;
    private String password;
    private String mail;
}
