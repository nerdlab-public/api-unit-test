package com.api.cnrt.maestros.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DelegacionDTO {

    public Long id;

    public String organismoId;

    public String descripcion;

    public String delegado;

    public Integer activo;
}
