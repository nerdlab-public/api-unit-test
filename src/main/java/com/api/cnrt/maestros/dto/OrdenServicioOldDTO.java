package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrdenServicioOldDTO {

    private String dominio;

    private LocalDateTime fechaAlta;

    private String ubicacionOrigen;

    private String ubicacionDestino;

    private Integer estado;

    private String ubicacion;

    private String osiAlta;

    private String osiOriginal;

    private String osiCierre;

    private LocalDateTime fechaCierre;

    private String abrev;
}
