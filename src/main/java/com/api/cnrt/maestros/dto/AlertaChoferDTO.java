package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AlertaChoferDTO {

    private String dni;

    public String fecha;

    public Integer infraccion1;
    
    public Integer infraccion2;

    public Integer infraccion3;

    public Long idFaltaServicioUno;
    public Long idFaltaServicioDos;
    public Long idFaltaServicioTres;

}
