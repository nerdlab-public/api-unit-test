package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VehiculoJNHabilitadoDTO {

    private String dominio;

    private String interno;

    private String servicios;

    private String anioModelo;

    private Integer cantidadAsientos;

    private String empresaNumero;

    private String razonSocial;

    private String vigenciaHasta;

    private String modelo;

    private String marca;

    private String vigenciaHastaInspeccionTecnica;

    private String tipoTecnica;

    private String categoriaAprob;

    private String expOrig;

    private String expModif;

    private String disposicion;

    private String tacografoMarca;

    private String tacografoNumero;

    private String tecnicaNumero;

    private String tipoDocumentoAbrev;

    private String cuit;

    private String categoriaTecnica;

    private String aseguradora;

    private String numeroPoliza;

    private String vigenciaHastaPoliza;

    private String utimaFiscaFecha;

    private String ultimaFiscaResultado;

    private String pais;

    private String fechaUltimoRodillo;

    private Integer resultadoUltimoRodillo;

}
