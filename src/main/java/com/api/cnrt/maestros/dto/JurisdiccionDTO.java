package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JurisdiccionDTO {

    private Long id;
    private String descripcion;
    private Integer fiscaliza;
    private Integer internacional;
    private String abrev;

}
