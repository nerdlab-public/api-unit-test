package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetalleSubclasificacionFiscalizacionDTO {

    private Long id;
    private String descripcion;
    private Long subclasificacionFiscalizacionId;
}
