package com.api.cnrt.maestros.dto;

import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import com.api.cnrt.maestros.model.EstadoOrdenServicio;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.model.Localidad;
import com.api.cnrt.maestros.model.Servicio;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrdenServicioDTO {

    private Long id;

    private String dominio;

    private LocalDateTime fechaAlta;

    private Long localidadOrigenId;

    private Long localidadDestinoId;

    private Long estadoOrdenServicioId;

    private Long estadoViajeId;

    private String osiAlta;

    private String osiOriginal;

    private String osiCierre;

    private LocalDateTime fechaCierre;

    private Long servicioId;

}
