package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PredioDTO {

    private Long id;
    private String descripcion;
    private Integer generaRetencion;
    private String provincia;
    private Integer activo;


}
