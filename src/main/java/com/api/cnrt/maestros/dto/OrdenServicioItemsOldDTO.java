package com.api.cnrt.maestros.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrdenServicioItemsOldDTO {

    @Id
    @Column(name = "osi")
    private String osi;

    @Id
    @Column(name = "id_campo")
    private Integer idCampo;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "cantidad")
    private Integer cantidad;
}
