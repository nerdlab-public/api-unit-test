package com.api.cnrt.maestros.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LicenciaDTO {

    private Long id;
    private String dni;
    private String apellido;
    private String nombre;
    private String vencimientoCategoriaP;
    private String vencimientoCategoriaG;
    private String vencimientoCategoriaC;
    private String fechaUltimaFiscalizacion;
    private Integer resultadoUltimaFiscalizacion;
    private String fechaUltimoPsicofisico;
    private Integer resultadoUltimoPsicofisico;
    private String fechaUltimaAlcoholemia;
    private Integer resultadoUltimaAlcoholemia;
    private String fechaUltimoTestSustancias;
    private Integer resultadoUltimoTestSustancias;
    private String inhabilitadoHasta;
    private String estado;
    private String vigenciaCursoP;
    private String vigenciaCursoG;
    private String vigenciaCursoC;

}
