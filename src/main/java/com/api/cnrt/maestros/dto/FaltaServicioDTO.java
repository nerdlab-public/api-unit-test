package com.api.cnrt.maestros.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FaltaServicioDTO {

    private Long id;
    private Long tipoTransporteId;
    private Long jurisdiccionId;
    private Long itemInfraccionId;
    private Boolean porEmpresa;
    private Boolean porDominio;
    private Boolean actaCalidad;
    private Long clasificacionFiscalizacionId;
    private Long subclasificacionFiscalizacionId;
    private Long detalleSubclasificacionFiscalizacionId;
    private Boolean enPartida;
    private Boolean enViaje;
    private Boolean enArribo;
    private Boolean agrupa;
    private Boolean retiene;
    private Boolean paraliza;
    private Boolean desafectaVehiculo;
    private Boolean desafectaChofer;
    private Integer cantTope1;
    private Long inf1;
    private Integer cantTope2;
    private Long inf2;
    private Integer cantTope3;
    private Long inf3;
    private Boolean aplicaCantidad;
    private Long cnrtServiciosId;
    private Boolean relativoChofer;
}
