package com.api.cnrt.maestros.dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VehiculoCNRTDTO {


    private Long id;

    private String dominio;

    private String marca;

    private String modelo;

    private String anio;

    private String ubicacion;

    private String numeroMotor;

    private String numeroChasis;

    private String titular;

    private String cobertura;

    private Integer pointer;

    private String estado;

    private Integer activo;
}
