package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.AmbitoClaseModalidadDTO;
import com.api.cnrt.maestros.dto.DelegacionDTO;
import com.api.cnrt.maestros.model.AmbitoClaseModalidad;
import com.api.cnrt.maestros.model.Delegacion;

@Component
public class AmbitoClaseModalidadToAmbitoClaseModalidadDTOMapper implements IMapper<AmbitoClaseModalidad, AmbitoClaseModalidadDTO> {


    @Override
    public AmbitoClaseModalidadDTO map(AmbitoClaseModalidad in) {
        return AmbitoClaseModalidadDTO.builder()
                .id(in.getId())
                .ambito(in.getAmbito().getId())
                .claseModalidad(in.getClaseModalidad().getId())
                .build();
    }
}
