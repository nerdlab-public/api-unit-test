package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.dto.TipoDocumentoEmpresaDTO;
import com.api.cnrt.maestros.model.Pais;
import com.api.cnrt.maestros.model.TipoDocumentoEmpresa;

@Component
public class TipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper implements IMapper<TipoDocumentoEmpresa, TipoDocumentoEmpresaDTO> {


	@Override
	public TipoDocumentoEmpresaDTO map(TipoDocumentoEmpresa in) {
		return TipoDocumentoEmpresaDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.mascara(in.getMascara())
				.build();

	}
}
