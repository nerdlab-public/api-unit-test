package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ZonaOperativoDTO;
import com.api.cnrt.maestros.model.ZonaOperativo;

@Component
public class ZonaOperativoToZonaOperativoDTOMapper implements IMapper<ZonaOperativo, ZonaOperativoDTO> {

	@Override
	public ZonaOperativoDTO map(ZonaOperativo in) {
		return ZonaOperativoDTO.builder()
        .id(in.getId())
        .description(in.getDescripcion())
        .activo(in.getActivo())
        .build();
	}

}
