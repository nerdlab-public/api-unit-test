package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.dto.ServicioDTO;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.model.Servicio;

@Component
public class ServicioToServicioDTOMapper implements IMapper<Servicio, ServicioDTO> {


    @Override
    public ServicioDTO map(Servicio in) {
        return ServicioDTO.builder()
                .id(in.getId())
                .abrev(in.getAbrev())
                .servicio(in.getServicio())
                .activo(in.getActivo())
                .ambitoId(in.getAmbito().getId())
                .tipoOrdenControlId(in.getTipoOrdenControl().getId())
                .tipoTransporteId(in.getTipoTransporte().getId())
                .claseModalidadId(in.getClaseModalidad().getId())
                .colItems(in.getColItems())
                .comprobante(in.getComprobante())
                .conductor(in.getConductor())
                .contrato(in.getContrato())
                .declaracion(in.getDeclaracion())
                .listaPasajeros(in.getListaPasajeros())
                .sisElectronico(in.getSisElectronico())
                .tacografo(in.getTacografo())
                .tipo(in.getTipo())
                .build();

    }
}
