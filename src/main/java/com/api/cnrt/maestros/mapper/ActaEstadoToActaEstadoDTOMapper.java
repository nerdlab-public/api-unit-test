package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.model.ActaEstado;

@Component
public class ActaEstadoToActaEstadoDTOMapper implements IMapper<ActaEstado, ActaEstadoDTO> {


	@Override
	public ActaEstadoDTO map(ActaEstado in) {
		return ActaEstadoDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.build();
	}
}
