package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.AmbitoDTO;
import com.api.cnrt.maestros.dto.OrdenServicioOldDTO;
import com.api.cnrt.maestros.model.Ambito;
import com.api.cnrt.maestros.model.OrdenServicioOld;

@Component
public class OrdenServicioOldToOrdenServicioOldDTOMapper implements IMapper<OrdenServicioOld, OrdenServicioOldDTO> {


	@Override
	public OrdenServicioOldDTO map(OrdenServicioOld in) {
		return OrdenServicioOldDTO.builder()
				.dominio(in.getDominio())
				.estado(in.getEstado())
				.fechaAlta(in.getFechaAlta())
				.fechaCierre(in.getFechaCierre())
				.osiAlta(in.getOsiAlta())
				.osiOriginal(in.getOsiOriginal())
				.osiCierre(in.getOsiCierre())
				.ubicacion(in.getUbicacion())
				.ubicacionDestino(in.getUbicacionDestino())
				.ubicacionOrigen(in.getUbicacionOrigen())
				.abrev(in.getAbrev())
				.build();
	}
}
