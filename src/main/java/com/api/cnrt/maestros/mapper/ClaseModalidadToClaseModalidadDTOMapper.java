package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ClaseModalidadDTO;
import com.api.cnrt.maestros.model.ClaseModalidad;

@Component
public class ClaseModalidadToClaseModalidadDTOMapper implements IMapper<ClaseModalidad, ClaseModalidadDTO> {


	@Override
	public ClaseModalidadDTO map(ClaseModalidad in) {
		return ClaseModalidadDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.abrev(in.getAbrev())
				.activo(in.getActivo())
				.build();
	}
}
