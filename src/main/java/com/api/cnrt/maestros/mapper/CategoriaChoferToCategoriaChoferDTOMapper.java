package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.CategoriaChoferDTO;
import com.api.cnrt.maestros.model.CategoriaChofer;

@Component
public class CategoriaChoferToCategoriaChoferDTOMapper implements IMapper<CategoriaChofer, CategoriaChoferDTO> {


    @Override
    public CategoriaChoferDTO map(CategoriaChofer in) {
        return CategoriaChoferDTO.builder()
                .id(in.getId())
                .description(in.getDescripcion())
                .abrev(in.abrev)
                .build();
    }
}
