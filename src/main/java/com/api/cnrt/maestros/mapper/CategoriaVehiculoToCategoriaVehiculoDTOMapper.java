package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.CategoriaVehiculoDTO;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.model.CategoriaVehiculo;

@Component
public class CategoriaVehiculoToCategoriaVehiculoDTOMapper implements IMapper<CategoriaVehiculo, CategoriaVehiculoDTO> {


    @Override
    public CategoriaVehiculoDTO map(CategoriaVehiculo in) {
        return CategoriaVehiculoDTO.builder()
                .id(in.getId())
                .description(in.getDescripcion())
                .abrev(in.abrev)
                .activo(in.getActivo())
                .build();
    }
}
