package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ClasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.model.ClasificacionFiscalizacion;
import com.api.cnrt.maestros.model.EstadoViaje;

@Component
public class ClasificacionFiscalizacionToClasificacionFiscalizacionDTOMapper implements IMapper<ClasificacionFiscalizacion, ClasificacionFiscalizacionDTO> {


	@Override
	public ClasificacionFiscalizacionDTO map(ClasificacionFiscalizacion in) {
		return ClasificacionFiscalizacionDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.orden(in.getOrden())
				.build();
	}
}
