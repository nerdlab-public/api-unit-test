package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.dto.EstadoOrdenServicioDTO;
import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.model.EstadoOrdenServicio;

@Component
public class EstadoOrdenServicioToEstadoOrdenServicioDTOMapper implements IMapper<EstadoOrdenServicio, EstadoOrdenServicioDTO> {


	@Override
	public EstadoOrdenServicioDTO map(EstadoOrdenServicio in) {
		return EstadoOrdenServicioDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.build();
	}
}
