package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.dto.TipoTransporteAmbitoDTO;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.model.TipoTransporteAmbito;

@Component
public class TipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper implements IMapper<TipoTransporteAmbito, TipoTransporteAmbitoDTO> {


    @Override
    public TipoTransporteAmbitoDTO map(TipoTransporteAmbito in) {
        return TipoTransporteAmbitoDTO.builder()
                .id(in.getId())
                .tipoTransporteId(in.getTipoTransporte().getId())
                .ambitoId(in.getAmbito().getId())
                .build();

    }
}
