package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.TipoDocumentoCobrarDTO;
import com.api.cnrt.maestros.model.TipoDocumentoCobrar;

@Component
public class TipoDocumentoCobrarToTipoDocumentoCobrarDTOMapper implements IMapper<TipoDocumentoCobrar, TipoDocumentoCobrarDTO> {

	@Override
	public TipoDocumentoCobrarDTO map(TipoDocumentoCobrar in) {
		return TipoDocumentoCobrarDTO.builder()
        .id(in.getId())
        .description(in.getDescripcion())
        .tipoDocumento(in.getTipoDocumento())
        .build();
	}

}
