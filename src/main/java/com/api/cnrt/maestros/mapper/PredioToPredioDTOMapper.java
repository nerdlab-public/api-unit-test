package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.model.Pais;
import com.api.cnrt.maestros.model.Predio;

@Component
public class PredioToPredioDTOMapper implements IMapper<Predio, PredioDTO> {


    @Override
    public PredioDTO map(Predio in) {
        return PredioDTO.builder()
                .id(in.getId())
                .descripcion(in.getDescripcion())
                .provincia(in.getProvincia())
                .generaRetencion(in.getGeneraRetencion())
                .activo(in.getActivo())
                .build();

    }
}
