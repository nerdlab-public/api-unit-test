package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.LocalidadDTO;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.model.Localidad;
import com.api.cnrt.maestros.model.TipoCategoria;

@Component
public class LocalidadToLocalidadDTOMapper implements IMapper<Localidad, LocalidadDTO> {

    @Override
    public LocalidadDTO map(Localidad in) {
        return LocalidadDTO.builder()
                .id(in.getId())
                .description(in.getDescripcion())
                .activo(in.getActivo())
                .provinciaId(in.getProvinciaId())
                .build();
    }

}
