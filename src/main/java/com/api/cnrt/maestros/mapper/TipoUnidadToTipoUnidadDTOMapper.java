package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.TipoUnidadDTO;
import com.api.cnrt.maestros.model.TipoUnidad;

@Component
public class TipoUnidadToTipoUnidadDTOMapper implements IMapper<TipoUnidad, TipoUnidadDTO> {

	@Override
	public TipoUnidadDTO map(TipoUnidad in) {
		return TipoUnidadDTO.builder()
        .id(in.getId())
        .description(in.getDescripcion())
        .diaCoeficiente(in.getDiaCoeficiente())
        .build();
	}

}
