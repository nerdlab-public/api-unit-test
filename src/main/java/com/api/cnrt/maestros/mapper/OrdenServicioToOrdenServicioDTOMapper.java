package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.AlertaChoferDTO;
import com.api.cnrt.maestros.dto.OrdenServicioDTO;
import com.api.cnrt.maestros.model.AlertaChofer;
import com.api.cnrt.maestros.model.OrdenServicio;

@Component
public class OrdenServicioToOrdenServicioDTOMapper implements IMapper<OrdenServicio, OrdenServicioDTO> {


	@Override
	public OrdenServicioDTO map(OrdenServicio in) {
		return OrdenServicioDTO.builder()
				.id(in.getId())
				.dominio(in.getDominio())
				.fechaAlta(in.getFechaAlta())
				.estadoOrdenServicioId(in.getEstadoOrdenServicio().getId())
				.servicioId(in.getServicio().getId())
				.estadoViajeId(in.getEstadoViaje().getId())
				.fechaCierre(in.getFechaCierre())
				.localidadDestinoId(in.getLocalidadDestino().getId())
				.localidadOrigenId(in.getLocalidadOrigen().getId())
				.osiAlta(in.getOsiAlta())
				.osiCierre(in.getOsiCierre())
				.osiOriginal(in.getOsiOriginal())
				.build();
	}
}
