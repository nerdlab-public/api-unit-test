package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.LicenciaDTO;
import com.api.cnrt.maestros.dto.LoginDTO;
import com.api.cnrt.maestros.model.Licencia;
import com.api.cnrt.maestros.model.Login;

@Component
public class LoginToLoginDTOMapper implements IMapper<Login, LoginDTO> {


    @Override
    public LoginDTO map(Login in) {
        return LoginDTO.builder()
                .idLogin(in.getIdLogin())
                .activo(in.getActivo())
                .dni(in.getDni())
                .email(in.getEmail())
                .calidad(in.getCalidad())
                .celular(in.getCelular())
                .nombre(in.getNombre())
                .lastLogin(in.getLastLogin())
                .pass(in.getPass())
                .autoriza(in.getAutoriza())
                .idAPPDevice(in.getIdAPPDevice())
                .idPerfil(in.getIdPerfil())
                .username(in.getUsername())
                .delegacionId(in.getDelegacion().getId())
                .build();

    }
}
