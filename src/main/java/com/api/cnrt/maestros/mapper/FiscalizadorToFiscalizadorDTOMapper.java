package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.model.Fiscalizador;

@Component
public class FiscalizadorToFiscalizadorDTOMapper implements IMapper<Fiscalizador, FiscalizadorDTO> {



    @Override
    public FiscalizadorDTO map(Fiscalizador in) {
        return FiscalizadorDTO.builder()
                .id(in.getId())
                .nombre(in.getNombre())
                .apellido(in.getApellido())
                .dni(in.getDni())
                .autoriza(in.getAutoriza())
                .delegacion(in.getDelegacion().getId())
                .calidad(in.getCalidad())
                .activo(in.getActivo())
                .soloHistorico(in.getSoloHistorico())
                .password(in.getPassword())
                .mail(in.getMail())
                .build();
    }
}
