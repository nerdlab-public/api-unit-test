package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.MotivoNoRetenerDTO;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.model.MotivoNoRetener;
import com.api.cnrt.maestros.model.Pais;

@Component
public class MotivoNoRetenerToMotivoNoRetenerDTOMapper implements IMapper<MotivoNoRetener, MotivoNoRetenerDTO> {


	@Override
	public MotivoNoRetenerDTO map(MotivoNoRetener in) {
		return MotivoNoRetenerDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.activo(in.getActivo())
				.build();

	}
}
