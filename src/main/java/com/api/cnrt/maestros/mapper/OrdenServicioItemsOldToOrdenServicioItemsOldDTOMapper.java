package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.dto.OrdenServicioItemsOldDTO;
import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.model.OrdenServicioItemsOld;

@Component
public class OrdenServicioItemsOldToOrdenServicioItemsOldDTOMapper implements IMapper<OrdenServicioItemsOld, OrdenServicioItemsOldDTO> {


	@Override
	public OrdenServicioItemsOldDTO map(OrdenServicioItemsOld in) {
		return OrdenServicioItemsOldDTO.builder()
				.osi(in.getOsi())
				.idCampo(in.getIdCampo())
				.observaciones(in.getObservaciones())
				.cantidad(in.getCantidad())
				.build();
	}
}
