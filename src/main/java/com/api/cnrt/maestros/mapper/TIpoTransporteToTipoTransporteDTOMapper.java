package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.TipoTransporteDTO;
import com.api.cnrt.maestros.model.TipoTransporte;

@Component
public class TIpoTransporteToTipoTransporteDTOMapper implements IMapper<TipoTransporte, TipoTransporteDTO> {
    @Override
    public TipoTransporteDTO map(TipoTransporte in) {
        return TipoTransporteDTO.builder()
                .id(in.getId())
                .descripcion(in.getDescripcion())
                .activo(in.getActivo())
                .dominio1(in.getDominio1())
                .dominio2(in.getDominio2())
                .dominio3(in.getDominio3())
                .chofer1(in.getChofer1())
                .chofer2(in.getChofer2())
                .abrev(in.getAbrev())
                .build();
    }
}
