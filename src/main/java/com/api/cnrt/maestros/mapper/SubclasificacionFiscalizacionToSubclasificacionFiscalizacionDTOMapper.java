package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.dto.SubclasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.model.SubclasificacionFiscalizacion;

@Component
public class SubclasificacionFiscalizacionToSubclasificacionFiscalizacionDTOMapper implements IMapper<SubclasificacionFiscalizacion, SubclasificacionFiscalizacionDTO> {


	@Override
	public SubclasificacionFiscalizacionDTO map(SubclasificacionFiscalizacion in) {
		return SubclasificacionFiscalizacionDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.orden(in.getOrden())
				.clasificacionFiscalizacionId(in.getClasificacionFiscalizacion().getId())
				.ingresoManual(in.getIngresoManual())
				.build();
	}
}
