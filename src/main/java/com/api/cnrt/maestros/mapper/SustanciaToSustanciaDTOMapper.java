package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.SustanciaDTO;
import com.api.cnrt.maestros.model.Sustancia;

@Component
public class SustanciaToSustanciaDTOMapper implements IMapper<Sustancia, SustanciaDTO> {


    @Override
    public SustanciaDTO map(Sustancia in) {
        return SustanciaDTO.builder()
                .id(in.getId())
                .descripcion(in.getDescripcion())
                .abrev(in.getAbrev())
                .activo(in.getActivo())
                .build();

    }
}
