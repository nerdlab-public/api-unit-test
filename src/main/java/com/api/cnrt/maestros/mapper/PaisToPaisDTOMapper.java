package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.model.Pais;

@Component
public class PaisToPaisDTOMapper implements IMapper<Pais, PaisDTO> {


	@Override
	public PaisDTO map(Pais in) {
		return PaisDTO.builder()
				.id(in.getId())
				.description(in.getDescripcion())
				.abrev(in.abrev)
				.activo(in.getActivo())
				.build();

	}
}
