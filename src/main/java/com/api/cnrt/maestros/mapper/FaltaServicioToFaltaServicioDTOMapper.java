package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.FaltaServicioDTO;
import com.api.cnrt.maestros.model.FaltaServicio;

@Component
public class FaltaServicioToFaltaServicioDTOMapper implements IMapper<FaltaServicio, FaltaServicioDTO> {
    @Override
    public FaltaServicioDTO map(FaltaServicio in) {
        return FaltaServicioDTO.builder()
                .id(in.getId())
                .tipoTransporteId(in.getTipoTransporteId())
                .clasificacionFiscalizacionId(in.getClasificacionFiscalizacion().getId())
                .subclasificacionFiscalizacionId(in.getSubclasificacionFiscalizacion().getId())
                .detalleSubclasificacionFiscalizacionId(in.getDetalleSubclasificacionFiscalizacion().getId())
                .actaCalidad(in.getActaCalidad())
                .agrupa(in.getAgrupa())
                .aplicaCantidad(in.getAplicaCantidad())
                .cantTope1(in.getCantTope1())
                .cnrtServiciosId(in.getCnrtServiciosId())
                .cantTope2(in.getCantTope2())
                .cantTope3(in.getCantTope3())
                .desafectaChofer(in.getDesafectaChofer())
                .desafectaVehiculo(in.getDesafectaVehiculo())
                .enArribo(in.getEnArribo())
                .enPartida(in.getEnPartida())
                .enViaje(in.getEnViaje())
                .inf1(in.getInf1()).inf3(in.getInf3())
                .inf2(in.getInf2())
                .jurisdiccionId(in.getJurisdiccionId())
                .itemInfraccionId(in.getItemInfraccionId())
                .paraliza(in.getParaliza())
                .porDominio(in.getPorDominio())
                .porEmpresa(in.getPorEmpresa())
                .retiene(in.getRetiene())
                .relativoChofer(in.getRelativoChofer())
                .build();
    }
}
