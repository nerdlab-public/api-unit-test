package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PermisosPDADTO;
import com.api.cnrt.maestros.model.PermisosPDA;

@Component
public class PermisosPDAToPermisosPDADTOMapper implements IMapper<PermisosPDA, PermisosPDADTO> {

	@Override
	public PermisosPDADTO map(PermisosPDA in) {
		return PermisosPDADTO.builder()
				.id(in.getId())
				.actaCalidad(in.getActaCalidad())
				.actaCargas(in.getActaCargas())
				.actaPasajeros(in.getActaPasajeros())
				.actaEmpresa(in.getActaEmpresa())
				.idLogins(in.getIdLogins())
				.build();
	}
}
