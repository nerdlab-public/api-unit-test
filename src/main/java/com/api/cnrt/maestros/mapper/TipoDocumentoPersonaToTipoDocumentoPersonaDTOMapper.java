package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.dto.TipoDocumentoPersonaDTO;
import com.api.cnrt.maestros.model.Pais;
import com.api.cnrt.maestros.model.TipoDocumentoPersona;

@Component
public class TipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper implements IMapper<TipoDocumentoPersona, TipoDocumentoPersonaDTO> {


	@Override
	public TipoDocumentoPersonaDTO map(TipoDocumentoPersona in) {
		return TipoDocumentoPersonaDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.mascara(in.getMascara())
				.build();

	}
}
