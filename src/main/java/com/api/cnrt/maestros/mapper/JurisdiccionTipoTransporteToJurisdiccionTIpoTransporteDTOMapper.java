package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.JurisdiccionTipoTransporteDTO;
import com.api.cnrt.maestros.model.JurisdiccionTipoTransporte;

@Component
public class JurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper implements IMapper<JurisdiccionTipoTransporte, JurisdiccionTipoTransporteDTO> {
    @Override
    public JurisdiccionTipoTransporteDTO map(JurisdiccionTipoTransporte in) {
        return JurisdiccionTipoTransporteDTO.builder()
                .id(in.getId())
                .jurisdiccionId(in.getJurisdiccion().getId())
                .tipoTransporteId(in.getTipoTransporte().getId())
                .build();
    }
}
