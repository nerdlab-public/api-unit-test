package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.dto.VehiculoJNHabilitadoDTO;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.model.VehiculoJNHabilitado;

@Component
public class VehiculoJNHabilitadoToVehiculoJNHabilitadoDTOMapper implements IMapper< VehiculoJNHabilitado,  VehiculoJNHabilitadoDTO> {


    @Override
    public  VehiculoJNHabilitadoDTO map( VehiculoJNHabilitado in) {
        return  VehiculoJNHabilitadoDTO.builder()
                .anioModelo(in.getAnioModelo())
                .aseguradora(in.getAseguradora())
                .cantidadAsientos(in.getCantidadAsientos())
                .categoriaAprob(in.getCategoriaAprob())
                .dominio(in.getDominio())
                .categoriaTecnica(in.getCategoriaTecnica())
                .cuit(in.getCuit())
                .disposicion(in.getDisposicion())
                .empresaNumero(in.getEmpresaNumero())
                .expModif(in.getExpModif())
                .expOrig(in.getExpOrig())
                .marca(in.getMarca())
                .modelo(in.getModelo())
                .fechaUltimoRodillo(in.getFechaUltimoRodillo())
                .interno(in.getInterno())
                .numeroPoliza(in.getNumeroPoliza())
                .pais(in.getPais())
                .razonSocial(in.getRazonSocial())
                .servicios(in.getServicios())
                .resultadoUltimoRodillo(in.getResultadoUltimoRodillo())
                .tacografoMarca(in.getTacografoMarca())
                .tacografoNumero(in.getTacografoNumero())
                .tecnicaNumero(in.getTecnicaNumero())
                .tipoDocumentoAbrev(in.getTipoDocumentoAbrev())
                .tipoTecnica(in.getTipoTecnica())
                .vigenciaHasta(in.getVigenciaHasta())
                .ultimaFiscaResultado(in.getUltimaFiscaResultado())
                .utimaFiscaFecha(in.getUtimaFiscaFecha())
                .vigenciaHastaPoliza(in.getVigenciaHastaPoliza())
                .vigenciaHastaInspeccionTecnica(in.getVigenciaHastaInspeccionTecnica())
                .vigenciaHasta(in.getVigenciaHasta())
                .build();

    }
}
