package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.DelegacionDTO;
import com.api.cnrt.maestros.model.Delegacion;

@Component
public class DelegacionToDelegacionDTOMapper implements IMapper<Delegacion, DelegacionDTO> {


    @Override
    public DelegacionDTO map(Delegacion in) {
        return DelegacionDTO.builder()
                .id(in.getId())
                .organismoId(in.getOrganismoId())
                .descripcion(in.getDescripcion())
                .delegado(in.getDelegado())
                .activo(in.getActivo())
                .build();
    }
}
