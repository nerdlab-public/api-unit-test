package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.LugarDTO;
import com.api.cnrt.maestros.model.Lugar;

@Component
public class LugaresToLugarDTOMapper implements IMapper<Lugar, LugarDTO> {


    @Override
    public LugarDTO map(Lugar in) {
        return LugarDTO.builder()
                .id(in.getId())
                .descripcion(in.getDescripcion())
                .localidadId(in.getLocalidad().getId())
                .build();
    }
}
