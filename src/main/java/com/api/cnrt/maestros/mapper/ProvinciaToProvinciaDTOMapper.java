package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ProvinciaDTO;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.model.Provincia;
import com.api.cnrt.maestros.model.TipoCategoria;

@Component
public class ProvinciaToProvinciaDTOMapper implements IMapper<Provincia, ProvinciaDTO> {

    @Override
    public ProvinciaDTO map(Provincia in) {
        return ProvinciaDTO.builder()
                .id(in.getId())
                .paisId(in.getPaisId())
                .description(in.getDescripcion())
                .abrev(in.abrev)
                .activo(in.getActivo())
                .build();
    }

}
