package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.dto.VehiculoCargasHabilitadoDTO;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.model.VehiculoCargaHabilitado;

@Component
public class VehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper implements IMapper<VehiculoCargaHabilitado, VehiculoCargasHabilitadoDTO> {


    @Override
    public VehiculoCargasHabilitadoDTO map(VehiculoCargaHabilitado in) {
        return VehiculoCargasHabilitadoDTO.builder()
                .id(in.getId())
                .anioModelo(in.getAnioModelo())
                .cantidadEjes(in.getCantidadEjes())
                .categoriaVehiculoId((in.getCategoriaVehiculo() != null) ? in.getCategoriaVehiculo().getId() : null)
                .chasisMarca(in.getChasisMarca())
                .dominio(in.getDominio())
                .empresaNumero(in.getEmpresaNumero())
                .estado(in.getEstado())
                .fechaEmision(in.getFechaEmision())
                .numeroChasis(in.getNumeroChasis())
                .numeroDocumento(in.getNumeroDocumento())
                .paut(in.getPaut())
                .numeroRuta(in.getNumeroRuta())
                .pais(in.getPais())
                .permisos(in.getPermisos())
                .razonSocial(in.getRazonSocial())
                .tacografoNumero(in.getTacografoNumero())
                .tecnicaNumero(in.getTecnicaNumero())
                .tipoCarroceria(in.getTipoCarroceria())
                .tipoVehiculo(in.getTipoVehiculo())
                .tipoDocumentoAbrev(in.getTipoDocumentoAbrev())
                .tipoTecnica(in.getTipoTecnica())
                .ultimaFiscaFecha(in.getUltimaFiscaFecha())
                .ultimaFiscaResultado(in.getUltimaFiscaResultado())
                .vigenciaHastaInspeccionTecnica(in.getVigenciaHastaInspeccionTecnica())
                .build();

    }
}
