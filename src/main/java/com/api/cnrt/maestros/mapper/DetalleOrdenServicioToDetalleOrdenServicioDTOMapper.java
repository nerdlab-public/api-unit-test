package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.AlertaChoferDTO;
import com.api.cnrt.maestros.dto.DetalleOrdenServicioDTO;
import com.api.cnrt.maestros.model.AlertaChofer;
import com.api.cnrt.maestros.model.DetalleOrdenServicio;

@Component
public class DetalleOrdenServicioToDetalleOrdenServicioDTOMapper implements IMapper<DetalleOrdenServicio, DetalleOrdenServicioDTO> {


	@Override
	public DetalleOrdenServicioDTO map(DetalleOrdenServicio in) {
		return DetalleOrdenServicioDTO.builder()
				.id(in.getId())
				.ordenServicioId(in.getOrdenServicio().getId())
				.cantidad(in.getCantidad())
				.osi(in.getOsi())
				.observacion(in.getObservacion())
				.idFaltaServicio(in.getIdFaltaServicio())
				.build();
	}
}
