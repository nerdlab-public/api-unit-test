package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.model.EstadoViaje;

@Component
public class EstadoViajeToEstadoViajeDTOMapper implements IMapper<EstadoViaje, EstadoViajeDTO> {


	@Override
	public EstadoViajeDTO map(EstadoViaje in) {
		return EstadoViajeDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.build();
	}
}
