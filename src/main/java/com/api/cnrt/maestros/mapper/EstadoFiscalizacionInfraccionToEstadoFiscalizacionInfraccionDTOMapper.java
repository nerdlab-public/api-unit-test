package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.dto.EstadoFiscalizacionInfraccionDTO;
import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.model.EstadoFiscalizacionInfraccion;

@Component
public class EstadoFiscalizacionInfraccionToEstadoFiscalizacionInfraccionDTOMapper implements IMapper<EstadoFiscalizacionInfraccion, EstadoFiscalizacionInfraccionDTO> {


	@Override
	public EstadoFiscalizacionInfraccionDTO map(EstadoFiscalizacionInfraccion in) {
		return EstadoFiscalizacionInfraccionDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.build();
	}
}
