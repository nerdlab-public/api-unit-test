package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.dto.TipoOrdenControlDTO;
import com.api.cnrt.maestros.model.Pais;
import com.api.cnrt.maestros.model.TipoOrdenControl;

@Component
public class TipoOrdenControlToTipoOrdenControlDTOMapper implements IMapper<TipoOrdenControl, TipoOrdenControlDTO> {


	@Override
	public TipoOrdenControlDTO map(TipoOrdenControl in) {
		return TipoOrdenControlDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.abrev(in.getAbrev())
				.tipoTransporteId(in.getTipoTransporte().getId())
				.build();

	}
}
