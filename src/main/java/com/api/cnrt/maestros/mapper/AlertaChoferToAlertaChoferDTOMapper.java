package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.AlertaChoferDTO;
import com.api.cnrt.maestros.model.AlertaChofer;

@Component
public class AlertaChoferToAlertaChoferDTOMapper implements IMapper<AlertaChofer, AlertaChoferDTO> {


	@Override
	public AlertaChoferDTO map(AlertaChofer in) {
		return AlertaChoferDTO.builder()
				.dni(in.getDni())
				.fecha(DateUtils.convertLocalDateTimeToString(in.getFecha()))
				.infraccion1(in.getInfraccion1())
				.infraccion2(in.getInfraccion2())
				.infraccion3(in.getInfraccion3())
				.idFaltaServicioUno(in.getIdFaltaServicioUno())
				.idFaltaServicioDos(in.getIdFaltaServicioDos())
				.idFaltaServicioTres(in.getIdFaltaServicioTres())
				.build();
	}
}
