package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.LicenciaDTO;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.model.Licencia;
import com.api.cnrt.maestros.model.Predio;

@Component
public class LicenciaToLicenciaDTOMapper implements IMapper<Licencia, LicenciaDTO> {


    @Override
    public LicenciaDTO map(Licencia in) {
        return LicenciaDTO.builder()
                .id(in.getId())
                .nombre(in.getNombre())
                .apellido(in.getApellido())
                .dni(in.getDni())
                .estado(in.getEstado())
                .fechaUltimaAlcoholemia(in.getFechaUltimaAlcoholemia())
                .fechaUltimaFiscalizacion(in.getFechaUltimaFiscalizacion())
                .fechaUltimoPsicofisico(in.getFechaUltimoPsicofisico())
                .fechaUltimoTestSustancias(in.getFechaUltimoTestSustancias())
                .inhabilitadoHasta(in.getInhabilitadoHasta())
                .resultadoUltimaAlcoholemia(in.getResultadoUltimaAlcoholemia())
                .resultadoUltimaFiscalizacion(in.getResultadoUltimaFiscalizacion())
                .resultadoUltimoPsicofisico(in.getResultadoUltimoPsicofisico())
                .resultadoUltimoTestSustancias(in.getResultadoUltimoTestSustancias())
                .vencimientoCategoriaC(in.getVencimientoCategoriaC())
                .vencimientoCategoriaG(in.getVencimientoCategoriaG())
                .vencimientoCategoriaP(in.getVencimientoCategoriaP())
                .vigenciaCursoC(in.getVigenciaCursoC())
                .vigenciaCursoG(in.getVigenciaCursoG())
                .vigenciaCursoP(in.getVigenciaCursoP())
                .build();

    }
}
