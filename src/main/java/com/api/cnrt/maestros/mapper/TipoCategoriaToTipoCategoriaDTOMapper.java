package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.model.TipoCategoria;

@Component
public class TipoCategoriaToTipoCategoriaDTOMapper implements IMapper<TipoCategoria, TipoCategoriaDTO> {

	@Override
	public TipoCategoriaDTO map(TipoCategoria in) {
		return TipoCategoriaDTO.builder()
        .id(in.getId())
        .description(in.getDescripcion())
        .abrev(in.abrev)
        .activo(in.getActivo())
        .build();
	}

}
