package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.dto.VehiculoCNRTDTO;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.model.VehiculoCNRT;

@Component
public class VehiculoCNRTToVehiculoCNRTDTOMapper implements IMapper<VehiculoCNRT, VehiculoCNRTDTO> {


    @Override
    public VehiculoCNRTDTO map(VehiculoCNRT in) {
        return VehiculoCNRTDTO.builder()
                .id(in.getId())
                .activo(in.getActivo())
                .anio(in.getAnio())
                .dominio(in.getDominio())
                .cobertura(in.getCobertura())
                .estado(in.getEstado())
                .marca(in.getMarca())
                .modelo(in.getModelo())
                .numeroChasis(in.getNumeroChasis())
                .numeroMotor(in.getNumeroMotor())
                .pointer(in.getPointer())
                .titular(in.getTitular())
                .ubicacion(in.getUbicacion())
                .build();

    }
}
