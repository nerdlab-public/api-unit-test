package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.JurisdiccionDTO;
import com.api.cnrt.maestros.model.Jurisdiccion;

@Component
public class JurisdiccionToJurisdiccionDTOMapper implements IMapper<Jurisdiccion, JurisdiccionDTO> {
    @Override
    public JurisdiccionDTO map(Jurisdiccion in) {
        return JurisdiccionDTO.builder()
                .id(in.getId())
                .descripcion(in.getDescripcion())
                .fiscaliza(in.getFiscaliza())
                .internacional(in.getInternacional())
                .abrev(in.getAbrev())
                .build();
    }
}
