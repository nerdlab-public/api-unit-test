package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.ResultadoFiscalizacionDTO;
import com.api.cnrt.maestros.model.ResultadoFiscalizacion;

@Component
public class ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper implements IMapper<ResultadoFiscalizacion, ResultadoFiscalizacionDTO> {


	@Override
	public ResultadoFiscalizacionDTO map(ResultadoFiscalizacion in) {
		return ResultadoFiscalizacionDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.build();
	}
}
