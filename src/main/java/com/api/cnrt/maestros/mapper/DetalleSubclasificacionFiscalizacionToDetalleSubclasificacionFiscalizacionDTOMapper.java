package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.DetalleSubclasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.model.DetalleSubclasificacionFiscalizacion;
import com.api.cnrt.maestros.model.EstadoViaje;

@Component
public class DetalleSubclasificacionFiscalizacionToDetalleSubclasificacionFiscalizacionDTOMapper implements IMapper<DetalleSubclasificacionFiscalizacion, DetalleSubclasificacionFiscalizacionDTO> {


	@Override
	public DetalleSubclasificacionFiscalizacionDTO map(DetalleSubclasificacionFiscalizacion in) {
		return DetalleSubclasificacionFiscalizacionDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.subclasificacionFiscalizacionId(in.getSubclasificacionFiscalizacion().getId())
				.build();
	}
}
