package com.api.cnrt.maestros.mapper;

import org.springframework.stereotype.Component;

import com.api.cnrt.common.IMapper;
import com.api.cnrt.maestros.dto.AmbitoDTO;
import com.api.cnrt.maestros.model.Ambito;

@Component
public class AmbitoToAmbitoDTOMapper implements IMapper<Ambito, AmbitoDTO> {


	@Override
	public AmbitoDTO map(Ambito in) {
		return AmbitoDTO.builder()
				.id(in.getId())
				.descripcion(in.getDescripcion())
				.abrev(in.getAbrev())
				.activo(in.getActivo())
				.build();
	}
}
