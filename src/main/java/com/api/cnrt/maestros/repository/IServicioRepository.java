package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.Servicio;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IServicioRepository extends JpaRepository<Servicio, Long> {

    @Query("SELECT tc FROM Servicio tc WHERE tc.fmod > :lastDateDb")
    Page<Servicio> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
