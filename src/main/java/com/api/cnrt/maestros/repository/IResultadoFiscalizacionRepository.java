package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.model.ResultadoFiscalizacion;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IResultadoFiscalizacionRepository extends JpaRepository<ResultadoFiscalizacion, Long> {

    @Query("SELECT ae FROM ResultadoFiscalizacion ae WHERE ae.fmod > :lastSyncro")
    Page<ResultadoFiscalizacion> findFromLastSincro(@Param("lastSyncro") LocalDateTime lastSyncro, Pageable pageable);
}
