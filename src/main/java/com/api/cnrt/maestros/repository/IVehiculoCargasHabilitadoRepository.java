package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.VehiculoCargaHabilitado;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IVehiculoCargasHabilitadoRepository extends JpaRepository<VehiculoCargaHabilitado, Long> {

    @Query("SELECT tc FROM VehiculoCargaHabilitado tc WHERE tc.fmod > :lastDateDb and tc.estado = 0")
    Page<VehiculoCargaHabilitado> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);

    Page<VehiculoCargaHabilitado> findByEstado(int i, Pageable pageable);

    VehiculoCargaHabilitado findByDominio(String dominioVehiculo);
}
