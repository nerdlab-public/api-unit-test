package com.api.cnrt.maestros.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.TipoDocumentoCobrar;

@Repository
public interface ITipoDocumentoCobrarRepository extends JpaRepository<TipoDocumentoCobrar, Long> {

    @Query("SELECT tc FROM TipoDocumentoCobrar tc WHERE tc.fmod > :lastDateDb")
    Page<TipoDocumentoCobrar> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
