package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.DetalleSubclasificacionFiscalizacion;
import com.api.cnrt.maestros.model.EstadoViaje;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IDetalleSubclasificacionFiscalizacionRepository extends JpaRepository<DetalleSubclasificacionFiscalizacion, Long> {

    @Query("SELECT ae FROM DetalleSubclasificacionFiscalizacion ae WHERE ae.fmod > :lastSyncro")
    Page<DetalleSubclasificacionFiscalizacion> findFromLastSincro(@Param("lastSyncro") LocalDateTime lastSyncro, Pageable pageable);
}
