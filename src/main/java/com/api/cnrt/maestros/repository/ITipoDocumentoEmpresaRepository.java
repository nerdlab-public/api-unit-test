package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.model.TipoDocumentoEmpresa;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ITipoDocumentoEmpresaRepository extends JpaRepository<TipoDocumentoEmpresa, Long> {

    @Query("SELECT tc FROM TipoDocumentoEmpresa tc WHERE tc.fmod > :lastDateDb")
    Page<TipoDocumentoEmpresa> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
