package com.api.cnrt.maestros.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.TipoCategoria;

@Repository
public interface ITipoCategoriaRepository extends JpaRepository<TipoCategoria, Long> {

    @Query("SELECT tc FROM TipoCategoria tc WHERE tc.fmod > :lastDateDb")
    Page<TipoCategoria> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
