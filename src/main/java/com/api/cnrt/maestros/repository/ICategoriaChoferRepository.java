package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.CategoriaChofer;
import com.api.cnrt.maestros.model.TipoCategoria;

import java.time.LocalDateTime;
import java.util.List;
@Repository
public interface ICategoriaChoferRepository extends JpaRepository<CategoriaChofer, Long> {

    @Query("SELECT tc FROM CategoriaChofer tc WHERE tc.fmod > :lastDateDb")
    Page<CategoriaChofer> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
