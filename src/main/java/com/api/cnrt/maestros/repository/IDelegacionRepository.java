package com.api.cnrt.maestros.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Delegacion;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IDelegacionRepository extends JpaRepository<Delegacion, Long> {

    @Query("SELECT tc FROM Delegacion tc WHERE tc.fmod > :lastDateDb")
    Page<Delegacion> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);

}
