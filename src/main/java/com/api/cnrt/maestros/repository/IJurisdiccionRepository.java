package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Jurisdiccion;
import com.api.cnrt.maestros.model.Pais;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IJurisdiccionRepository extends JpaRepository<Jurisdiccion, Long> {

    @Query("SELECT tc FROM Jurisdiccion tc WHERE tc.fmod > :lastDateDb")
    Page<Jurisdiccion> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
