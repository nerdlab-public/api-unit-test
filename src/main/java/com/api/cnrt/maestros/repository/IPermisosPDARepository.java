package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.PermisosPDA;

import java.time.LocalDateTime;

@Repository
public interface IPermisosPDARepository extends JpaRepository<PermisosPDA, Long> {

    @Query("SELECT ae FROM PermisosPDA ae WHERE ae.fmod > :lastSyncro")
    Page<PermisosPDA> findFromLastSincro(@Param("lastSyncro") LocalDateTime lastSyncro, Pageable pageable);


    @Query("SELECT p FROM PermisosPDA p WHERE p.idLogins = :login")
    PermisosPDA findByIdLogin(@Param("login") Long login);
}
