package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.DetalleOrdenServicio;
import com.api.cnrt.maestros.model.OrdenServicio;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IDetalleOrdenServicioRepository extends JpaRepository<DetalleOrdenServicio, Long> {

    @Query("SELECT tc FROM DetalleOrdenServicio tc WHERE tc.fmod > :lastDateDb")
    Page<DetalleOrdenServicio> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
