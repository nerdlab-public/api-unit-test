package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.fiscalizaciones.model.FiscalizacionDominio;
import com.api.cnrt.maestros.model.ClaseModalidad;
import com.api.cnrt.maestros.model.OrdenServicioOld;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IOrdenServicioOldRepository extends JpaRepository<OrdenServicioOld, Long> {

    @Query("SELECT ae FROM OrdenServicioOld ae WHERE ae.fmod > :lastSyncro")
    Page<OrdenServicioOld> findFromLastSincro(@Param("lastSyncro") LocalDateTime lastSyncro, Pageable pageable);

    Page<OrdenServicioOld> findByEstado(int i, Pageable pageable);

    List<OrdenServicioOld> findByDominioAndFechaCierreIsNull(String fiscaDominio);
}
