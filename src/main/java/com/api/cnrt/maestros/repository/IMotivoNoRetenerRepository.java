package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.MotivoNoRetener;
import com.api.cnrt.maestros.model.Predio;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IMotivoNoRetenerRepository extends JpaRepository<MotivoNoRetener, Long> {

    @Query("SELECT tc FROM MotivoNoRetener tc WHERE tc.fmod > :lastDateDb")
    Page<MotivoNoRetener> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
