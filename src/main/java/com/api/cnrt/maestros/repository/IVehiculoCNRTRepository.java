package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.VehiculoCNRT;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IVehiculoCNRTRepository extends JpaRepository<VehiculoCNRT, Long> {

    @Query("SELECT tc FROM VehiculoCNRT tc WHERE tc.fmod > :lastDateDb")
    Page<VehiculoCNRT> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
