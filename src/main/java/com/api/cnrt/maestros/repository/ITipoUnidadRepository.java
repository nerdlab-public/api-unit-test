package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.TipoUnidad;

import java.time.LocalDateTime;

@Repository
public interface ITipoUnidadRepository extends JpaRepository<TipoUnidad, Long> {

    @Query("SELECT tu FROM TipoUnidad tu WHERE tu.fmod > :lastDateDb")
    Page<TipoUnidad> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
