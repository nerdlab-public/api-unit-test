package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.Login;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ILoginRepository extends JpaRepository<Login, Long> {

    @Query("SELECT tc FROM Login tc WHERE tc.fmodif > :lastDateDb")
    Page<Login> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);

    @Query("SELECT l FROM Login l WHERE l.idLogin > 0")
    Page<Login> findAllByIdGreaterThanOne(Pageable pageable);
}
