package com.api.cnrt.maestros.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.ActaEstado;

@Repository
public interface IActaEstadoRepository extends JpaRepository<ActaEstado, Long> {

    @Query("SELECT ae FROM ActaEstado ae WHERE ae.fmod > :lastSyncro")
    Page<ActaEstado> findFromLastSincro(@Param("lastSyncro") LocalDateTime lastSyncro, Pageable pageable);
}
