package com.api.cnrt.maestros.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.ZonaOperativo;

@Repository
public interface IZonaOperativoRepository extends JpaRepository<ZonaOperativo, Long> {

    @Query("SELECT zo FROM ZonaOperativo zo WHERE zo.fmod > :lastDateDb")
    Page<ZonaOperativo> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
