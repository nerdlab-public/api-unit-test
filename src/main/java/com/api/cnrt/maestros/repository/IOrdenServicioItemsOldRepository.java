package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.OrdenServicioItemsOld;
import com.api.cnrt.maestros.model.OrdenServicioOld;

import java.time.LocalDateTime;

@Repository
public interface IOrdenServicioItemsOldRepository extends JpaRepository<OrdenServicioItemsOld, Long> {

    @Query("SELECT ae FROM OrdenServicioItemsOld ae " +
            "JOIN OrdenServicioOld ss ON ae.osi = ss.osiAlta " +
            "WHERE ss.estado <> 2")
    Page<OrdenServicioItemsOld>  findAllActive(Pageable pageable);
}
