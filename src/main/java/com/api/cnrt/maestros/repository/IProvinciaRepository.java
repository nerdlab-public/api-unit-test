package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.Pais;
import com.api.cnrt.maestros.model.Provincia;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IProvinciaRepository extends JpaRepository<Provincia, Long> {

    @Query("SELECT tc FROM Provincia tc WHERE tc.fmod > :lastDateDb")
    Page<Provincia> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
