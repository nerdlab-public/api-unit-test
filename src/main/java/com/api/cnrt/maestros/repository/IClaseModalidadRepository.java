package com.api.cnrt.maestros.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.ClaseModalidad;

@Repository
public interface IClaseModalidadRepository extends JpaRepository<ClaseModalidad, Long> {

    @Query("SELECT ae FROM ClaseModalidad ae WHERE ae.fmod > :lastSyncro")
    Page<ClaseModalidad> findFromLastSincro(@Param("lastSyncro") LocalDateTime lastSyncro, Pageable pageable);
}
