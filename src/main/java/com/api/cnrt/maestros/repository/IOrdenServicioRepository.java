package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.fiscalizaciones.model.FiscalizacionDominio;
import com.api.cnrt.maestros.model.OrdenServicio;
import com.api.cnrt.maestros.model.Pais;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IOrdenServicioRepository extends JpaRepository<OrdenServicio, Long> {

    @Query("SELECT tc FROM OrdenServicio tc WHERE tc.fmod > :lastDateDb")
    Page<OrdenServicio> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);

    List<OrdenServicio> findByDominioAndFechaCierreIsNull(String fiscaDominio);
}
