package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.model.FaltaServicio;

import java.time.LocalDateTime;

public interface IFaltaServicioRepository extends JpaRepository<FaltaServicio, Long> {

    @Query("SELECT ae FROM FaltaServicio ae WHERE ae.fechaModificacion > :lastSyncro")
    Page<FaltaServicio> findFromLastSincro(@Param("lastSyncro") LocalDateTime lastSyncro, Pageable pageable);
}
