package com.api.cnrt.maestros.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.cnrt.maestros.model.TipoTransporteAmbito;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ITipoTransporteAmbitoRepository extends JpaRepository<TipoTransporteAmbito, Long> {

    @Query("SELECT tc FROM TipoTransporteAmbito tc WHERE tc.fmod > :lastDateDb")
    Page<TipoTransporteAmbito> findFromLastSincro(@Param("lastDateDb") LocalDateTime lastDateDb, Pageable pageable);
}
