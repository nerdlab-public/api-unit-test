package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_localidad")
public class Localidad {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "provincia_id")
    public Long provinciaId;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "activo")
    public Integer activo;
    
    @Column(name = "fmod")
    public LocalDateTime fmod;

}
