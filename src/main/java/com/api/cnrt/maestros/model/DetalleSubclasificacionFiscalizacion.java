package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name  = "cnrt_detalle_subclasificacion_fiscalizacion")
public class DetalleSubclasificacionFiscalizacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "descripcion")
    public String descripcion;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_subclasificacion_fiscalizacion")
    private SubclasificacionFiscalizacion subclasificacionFiscalizacion;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
