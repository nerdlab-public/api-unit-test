package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_delegacion")
public class Delegacion {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "organismo_id")
    public String organismoId;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "delegado")
    public String delegado;

    @Column(name = "activo")
    public Integer activo;
    
    @Column(name = "fmod")
    public LocalDateTime fmod;

}
