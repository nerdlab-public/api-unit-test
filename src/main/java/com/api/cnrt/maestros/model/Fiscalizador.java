package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cnrt_fiscalizador")
public class Fiscalizador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "apellido")
    public String apellido;

    @Column(name = "dni")
    public String dni;

    @Column(name = "autoriza")
    public Integer autoriza;

    @ManyToOne(optional = true)
    @JoinColumn(name = "delegacion")
    public Delegacion delegacion;

    @Column(name = "calidad")
    public Integer calidad;

    @Column(name = "activo")
    public Integer activo;

    @Column(name = "solo_historico")
    public Integer soloHistorico;

    @Column(name = "password")
    public String password;

    @Column(name = "mail")
    public String mail;

    @Column(name = "fmod")
    public LocalDateTime fmod;


}
