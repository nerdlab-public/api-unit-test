package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_licencias")
public class Licencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "dni")
    public String dni;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "apellido")
    public String apellido;

    @Column(name = "vencimiento_cat_p")
    public String vencimientoCategoriaP;

    @Column(name = "vencimiento_cat_g")
    public String vencimientoCategoriaG;

    @Column(name = "vencimiento_cat_c")
    public String vencimientoCategoriaC;

    @Column(name = "fecha_ultima_fiscalizacion")
    public String fechaUltimaFiscalizacion;

    @Column(name = "resultado_ultima_fiscalizacion")
    public Integer resultadoUltimaFiscalizacion;

    @Column(name = "fecha_ultimo_psicofisico")
    public String fechaUltimoPsicofisico;

    @Column(name = "resultado_ultimo_psicofisico")
    public Integer resultadoUltimoPsicofisico;

    @Column(name = "fecha_ultima_alcoholemia")
    public String fechaUltimaAlcoholemia;

    @Column(name = "resultado_ultima_alcoholemia")
    public Integer resultadoUltimaAlcoholemia;

    @Column(name = "fecha_ultimo_test_sustancias")
    public String fechaUltimoTestSustancias;

    @Column(name = "resultado_ultimo_test_sustancias")
    public Integer resultadoUltimoTestSustancias;

    @Column(name = "inhabilitado_hasta")
    public String inhabilitadoHasta;

    @Column(name = "estado")
    public String estado;

    @Column(name = "vigencia_curso_p")
    public String vigenciaCursoP;

    @Column(name = "vigencia_curso_g")
    public String vigenciaCursoG;

    @Column(name = "vigencia_curso_c")
    public String vigenciaCursoC;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
