package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_vehiculo_cargas_habilitado")
public class VehiculoCargaHabilitado {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "dominio")
    public String dominio;

    @Column(name = "anio_modelo")
    public String anioModelo;

    @Column(name = "empresa_nro")
    public String empresaNumero;

    @Column(name = "paut")
    public String paut;

    @Column(name = "nro_chasis")
    public String numeroChasis;

    @Column(name = "cantidad_ejes")
    public String cantidadEjes;

    @Column(name = "tipo_vehiculo")
    public String tipoVehiculo;

    @Column(name = "chasis_marca")
    public String chasisMarca;

    @Column(name = "tipo_carroceria")
    public String tipoCarroceria;

    @Column(name = "razon_social")
    public String razonSocial;

    @Column(name = "tipo_documento_abrev")
    public String tipoDocumentoAbrev;

    @Column(name = "nro_documento")
    public String numeroDocumento;

    @Column(name = "vigencia_hasta_inspeccion_tecnica")
    public String vigenciaHastaInspeccionTecnica;

    @Column(name = "tipo_tecnica")
    public String tipoTecnica;

    @Column(name = "tecnica_nro")
    public String tecnicaNumero;

    @Column(name = "nro_ruta")
    public String numeroRuta;

    @Column(name = "fecha_emision")
    public String fechaEmision;

    @Column(name = "tacografo_nro")
    public String tacografoNumero;

    @ManyToOne(optional = true)
    @JoinColumn(name = "categoria_vehiculo_id")
    public CategoriaVehiculo categoriaVehiculo;

    @Column(name = "ultima_fisca_fecha")
    public String ultimaFiscaFecha;

    @Column(name = "ultima_fisca_resultado")
    public Integer ultimaFiscaResultado;

    @Column(name = "estado")
    public Integer estado;

    @Column(name = "permisos")
    public String permisos;

    @Column(name = "pais")
    public String pais;

    @Column(name = "fecha_baja")
    public String fechaBaja;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
