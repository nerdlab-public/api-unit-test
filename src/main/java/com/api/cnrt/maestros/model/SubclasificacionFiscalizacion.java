package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name  = "cnrt_subclasificacion_fiscalizacion")
public class SubclasificacionFiscalizacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "descripcion")
    public String descripcion;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_clasificacion_fiscalizacion")
    private ClasificacionFiscalizacion clasificacionFiscalizacion;

    @Column(name = "orden")
    public Integer orden;

    @Column(name = "ingreso_manual")
    public Integer ingresoManual;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
