package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cnrt_jurisdiccion")
public class Jurisdiccion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "fiscaliza")
    public Integer fiscaliza;

    @Column(name = "internacional")
    public Integer internacional;

    @Column(name = "abrev")
    public String abrev;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
