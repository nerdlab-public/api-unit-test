package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_tipo_orden_control")
public class TipoOrdenControl {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "abrev")
    public String abrev;

    @ManyToOne(optional = true)
    @JoinColumn(name = "tipo_transporte")
    public TipoTransporte tipoTransporte;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
