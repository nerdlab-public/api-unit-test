package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "permisos_pda")
public class PermisosPDA {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "id_logins")
    private Long idLogins;

    @Column(name = "acta_calidad")
    private Boolean actaCalidad;

    @Column(name = "acta_empresa")
    private Boolean actaEmpresa;

    @Column(name = "acta_pasajeros")
    private Boolean actaPasajeros;

    @Column(name = "acta_cargas")
    private Boolean actaCargas;

    @Column(name = "fmod")
    public LocalDateTime fmod;


}
