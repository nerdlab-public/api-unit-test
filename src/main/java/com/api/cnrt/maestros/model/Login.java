package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "logins")
public class Login {

    @Id
    @Column(name = "id_logins")
    public Long idLogin;

    @Column(name = "id_perfiles")
    public Long idPerfil;

    @Column(name = "id_app_devices")
    public Long idAPPDevice;

    @Column(name = "username")
    public String username;

    @Column(name = "email")
    public String email;

    @Column(name = "pass")
    public String pass;

    @Column(name = "nombre")
    public String nombre;

    @Column(name = "apellido")
    public String apellido;

    @Column(name = "celular")
    public String celular;

    @Column(name = "activo")
    public Integer activo;

    @Column(name = "last_login")
    public String lastLogin;

    @Column(name = "fecha_alta")
    public LocalDateTime fechaAlta;

    @Column(name = "fecha_modificacion")
    public LocalDateTime fechaModificacion;

    @Column(name = "autoriza")
    public Integer autoriza;

    @Column(name = "dni")
    public String dni;

    @ManyToOne(optional = true)
    @JoinColumn(name = "delegacion")
    public Delegacion delegacion;

    @Column(name = "calidad")
    public Integer calidad;

    @Column(name = "fmodif")
    public LocalDateTime fmodif;


}
