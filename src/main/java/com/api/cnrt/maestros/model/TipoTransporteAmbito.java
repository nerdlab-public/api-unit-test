package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_tipo_transporte_ambito")
public class TipoTransporteAmbito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @ManyToOne(optional = true)
    @JoinColumn(name = "tipo_transporte_id")
    public TipoTransporte tipoTransporte;

    @ManyToOne(optional = true)
    @JoinColumn(name = "ambito_id")
    public Ambito ambito;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
