package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_vehiculo_cnrt")
public class VehiculoCNRT {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "dominio")
    public String dominio;

    @Column(name = "marca")
    public String marca;

    @Column(name = "modelo")
    public String modelo;

    @Column(name = "anio")
    public String anio;

    @Column(name = "ubicacion")
    public String ubicacion;

    @Column(name = "nro_motor")
    public String numeroMotor;

    @Column(name = "nro_chasis")
    public String numeroChasis;

    @Column(name = "titular")
    public String titular;

    @Column(name = "cobertura")
    public String cobertura;

    @Column(name = "pointer")
    public Integer pointer;

    @Column(name = "estado")
    public String estado;

    @Column(name = "activo")
    public Integer activo;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
