package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cnrt_tipo_transporte")
public class TipoTransporte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "activo")
    public Integer activo;

    @Column(name = "dominio1")
    public Integer dominio1;

    @Column(name = "dominio2")
    public Integer dominio2;

    @Column(name = "dominio3")
    public Integer dominio3;

    @Column(name = "chofer1")
    public Integer chofer1;

    @Column(name = "chofer2")
    public Integer chofer2;

    @Column(name = "abrev")
    public String abrev;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
