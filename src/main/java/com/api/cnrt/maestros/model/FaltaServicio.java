package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_falta_servicio")
public class FaltaServicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "tipo_transporte_id")
    private Long tipoTransporteId;

    @Column(name = "jurisdiccion_id")
    private Long jurisdiccionId;

    @Column(name = "item_infraccion_id")
    private Long itemInfraccionId;

    @Column(name = "por_empresa")
    private Boolean porEmpresa;

    @Column(name = "por_dominio")
    private Boolean porDominio;

    @Column(name = "acta_calidad")
    private Boolean actaCalidad;

    @ManyToOne
    @JoinColumn(name = "clasificacion_fiscalizacion_id")
    private ClasificacionFiscalizacion clasificacionFiscalizacion;

    @ManyToOne
    @JoinColumn(name = "subclasificacion_fiscalizacion_id")
    private SubclasificacionFiscalizacion subclasificacionFiscalizacion;

    @ManyToOne
    @JoinColumn(name = "detalle_subclasificacion_fiscalizacion_id")
    private DetalleSubclasificacionFiscalizacion detalleSubclasificacionFiscalizacion;

    @Column(name = "en_partida")
    private Boolean enPartida;

    @Column(name = "en_viaje")
    private Boolean enViaje;

    @Column(name = "en_arribo")
    private Boolean enArribo;

    @Column(name = "agrupa")
    private Boolean agrupa;

    @Column(name = "retiene")
    private Boolean retiene;

    @Column(name = "paraliza")
    private Boolean paraliza;

    @Column(name = "desafecta_vehiculo")
    private Boolean desafectaVehiculo;

    @Column(name = "desafecta_chofer")
    private Boolean desafectaChofer;

    @Column(name = "cant_tope_1")
    private Integer cantTope1;

    @Column(name = "inf1")
    private Long inf1;

    @Column(name = "cant_tope_2")
    private Integer cantTope2;

    @Column(name = "inf2")
    private Long inf2;

    @Column(name = "cant_tope_3")
    private Integer cantTope3;

    @Column(name = "inf3")
    private Long inf3;

    @Column(name = "aplica_cantidad")
    private Boolean aplicaCantidad;

    @Column(name = "cnrt_servicios_id")
    private Long cnrtServiciosId;

    @Column(name = "fecha_modificacion")
    private LocalDateTime fechaModificacion;

    @Column(name = "relativo_chofer")
    private Boolean relativoChofer;

}
