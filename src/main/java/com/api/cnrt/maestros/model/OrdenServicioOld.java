package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import com.api.cnrt.common.OrdenServicioOldId;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ordenes_servicios")
@IdClass(OrdenServicioOldId.class)
public class OrdenServicioOld {

    @Id
    @Column(name = "dominio")
    public String dominio;

    @Column(name = "fecha_alta")
    public LocalDateTime fechaAlta;

    @Column(name = "ubicacion_origen")
    public String ubicacionOrigen;

    @Column(name = "ubicacion_destino")
    public String ubicacionDestino;

    @Column(name = "status")
    public Integer estado;

    @Column(name = "ubicacion")
    public String ubicacion;

    @Id
    @Column(name = "osi_alta")
    public String osiAlta;

    @Column(name = "osi_original")
    public String osiOriginal;

    @Column(name = "osi_cierre")
    public String osiCierre;

    @Column(name = "fecha_cierre")
    public LocalDateTime fechaCierre;

    @Column(name = "abrev")
    public String abrev;

    @Column(name = "fmod")
    public LocalDateTime fmod;

}
