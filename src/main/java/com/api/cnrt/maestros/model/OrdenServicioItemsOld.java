package com.api.cnrt.maestros.model;

import com.api.cnrt.common.OrdenServicioItemsOldId;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ordenes_servicios_items")
@IdClass(OrdenServicioItemsOldId.class)
public class OrdenServicioItemsOld {

    @Id
    @Column(name = "osi")
    private String osi;

    @Id
    @Column(name = "id_campo")
    private Integer idCampo;

    @Column(name = "observaciones", columnDefinition = "text")
    private String observaciones;


    @Column(name = "cantidad")
    private Integer cantidad;
}
