package com.api.cnrt.maestros.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_tipo_doc_cobrar")
public class TipoDocumentoCobrar {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "tipo_doc")
    public String tipoDocumento;

    @Column(name = "descripcion")
    public String descripcion;
    
    @Column(name = "fmod")
    public LocalDateTime fmod;

}
