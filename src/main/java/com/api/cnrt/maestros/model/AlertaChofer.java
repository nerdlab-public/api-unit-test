package com.api.cnrt.maestros.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_alerta_chofer")
public class AlertaChofer {

    @Id
    @Column(name = "dni")
    public String dni;
    
    @Column(name = "fecha")
    public LocalDateTime fecha;

    @Column(name = "inf_1")
    public Integer infraccion1;
    
    @Column(name = "inf_2")
    public Integer infraccion2;

    @Column(name = "inf_3")
    public Integer infraccion3;

    @Column(name = "fmod")
    public LocalDateTime fmod;

    @Column(name = "id_falta_servicio_1")
    public Long idFaltaServicioUno;

    @Column(name = "id_falta_servicio_2")
    public Long idFaltaServicioDos;

    @Column(name = "id_falta_servicio_3")
    public Long idFaltaServicioTres;

}
