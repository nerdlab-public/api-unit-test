package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_vehiculos_jn_habilitados")
public class VehiculoJNHabilitado {

    @Id
    @Column(name = "dominio")
    public String dominio;

    @Column(name = "interno")
    public String interno;

    @Column(name = "servicios")
    public String servicios;

    @Column(name = "anio_modelo")
    public String anioModelo;

    @Column(name = "cant_asientos")
    public Integer cantidadAsientos;

    @Column(name = "empresa_nro")
    public String empresaNumero;

    @Column(name = "razon_social")
    public String razonSocial;

    @Column(name = "vigencia_hasta")
    public String vigenciaHasta;

    @Column(name = "modelo")
    public String modelo;

    @Column(name = "marca")
    public String marca;

    @Column(name = "vigencia_hasta_inspeccion_tecnica")
    public String vigenciaHastaInspeccionTecnica;

    @Column(name = "tipo_tecnica")
    public String tipoTecnica;

    @Column(name = "categoria_aprob")
    public String categoriaAprob;

    @Column(name = "exp_orig")
    public String expOrig;

    @Column(name = "exp_modif")
    public String expModif;

    @Column(name = "disposicion")
    public String disposicion;

    @Column(name = "tacografo_marca")
    public String tacografoMarca;

    @Column(name = "tacografo_nro")
    public String tacografoNumero;

    @Column(name = "tecnica_nro")
    public String tecnicaNumero;

    @Column(name = "tipo_documento_abrev")
    public String tipoDocumentoAbrev;

    @Column(name = "cuit")
    public String cuit;

    @Column(name = "categoria_tecnica")
    public String categoriaTecnica;

    @Column(name = "aseguradora")
    public String aseguradora;

    @Column(name = "nro_poliza")
    public String numeroPoliza;

    @Column(name = "vigencia_hasta_poliza")
    public String vigenciaHastaPoliza;

    @Column(name = "fmod")
    public LocalDateTime fmod;

    @Column(name = "ultima_fisca_fecha")
    public String utimaFiscaFecha;

    @Column(name = "ultima_fisca_resultado")
    public String ultimaFiscaResultado;

    @Column(name = "pais")
    public String pais;

    @Column(name = "fecha_ultimo_rodillo")
    public String fechaUltimoRodillo;

    @Column(name = "resultado_ultimo_rodillo")
    public Integer resultadoUltimoRodillo;


}
