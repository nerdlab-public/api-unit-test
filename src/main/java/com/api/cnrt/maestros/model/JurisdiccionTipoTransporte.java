package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cnrt_jurisdiccion_tipo_transporte")
public class JurisdiccionTipoTransporte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @ManyToOne(optional = true)
    @JoinColumn(name = "tipo_transporte_id")
    public TipoTransporte tipoTransporte;

    @ManyToOne(optional = true)
    @JoinColumn(name = "jurisdiccion_id")
    public Jurisdiccion jurisdiccion;

    @Column(name = "fmod")
    public LocalDateTime fmod;

}
