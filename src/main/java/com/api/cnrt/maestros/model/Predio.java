package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_predio")
public class Predio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "genera_retencion")
    public Integer generaRetencion;

    @Column(name = "provincia")
    public String provincia;

    @Column(name = "activo")
    public Integer activo;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
