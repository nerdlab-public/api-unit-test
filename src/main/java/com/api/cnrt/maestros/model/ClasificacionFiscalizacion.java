package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name  = "cnrt_clasificacion_fiscalizacion")
public class ClasificacionFiscalizacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "descripcion")
    public String descripcion;

    @Column(name = "orden")
    public Integer orden;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
