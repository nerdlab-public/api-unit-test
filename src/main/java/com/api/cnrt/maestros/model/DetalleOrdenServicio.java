package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "d_orden_servicio_detalle")
public class DetalleOrdenServicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "id_orden_servicio")
    public OrdenServicio ordenServicio;

    @Column(name = "osi")
    public String osi;

    @Column(name = "observacion")
    public String observacion;

    @Column(name = "cantidad")
    public Integer cantidad;

    @Column(name = "id_falta_servicio")
    public Long idFaltaServicio;


    @Column(name = "fmod")
    public LocalDateTime fmod;

}
