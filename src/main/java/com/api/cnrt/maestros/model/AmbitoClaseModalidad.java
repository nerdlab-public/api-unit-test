package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_ambito_clase_modalidad")
public class AmbitoClaseModalidad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @ManyToOne(optional = true)
    @JoinColumn(name = "ambito_id")
   public Ambito ambito;

    @ManyToOne(optional = true)
    @JoinColumn(name = "clase_modalidad_id")
    public ClaseModalidad claseModalidad;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
