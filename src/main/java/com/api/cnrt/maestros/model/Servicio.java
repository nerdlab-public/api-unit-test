package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cnrt_servicios")
public class Servicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name ="servicio")
    public String servicio;

    @Column(name ="tipo")
    public String tipo;

    @Column(name ="abrev")
    public String abrev;

    @ManyToOne(optional = true)
    @JoinColumn(name = "tipo_orden_control_id")
    public TipoOrdenControl tipoOrdenControl;

    @ManyToOne(optional = true)
    @JoinColumn(name = "tipo_transporte_id")
    public TipoTransporte tipoTransporte;

    @ManyToOne(optional = true)
    @JoinColumn(name = "ambito_id")
    public Ambito ambito;

    @ManyToOne(optional = true)
    @JoinColumn(name = "clase_modalidad_id")
    public ClaseModalidad claseModalidad;

    @Column(name ="col_items")
    public String colItems;

    @Column(name = "lista_pasajeros")
    public Integer listaPasajeros;

    @Column(name = "conductor")
    public Integer conductor;


    @Column(name = "contrato")
    public Integer contrato;

    @Column(name = "declaracion")
    public Integer declaracion;

    @Column(name = "sis_electronico")
    public Integer sisElectronico;

    @Column(name = "comprobante")
    public Integer comprobante;

    @Column(name = "tacografo")
    public Integer tacografo;

    @Column(name = "activo")
    public Integer activo;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
