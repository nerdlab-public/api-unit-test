package com.api.cnrt.maestros.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "d_orden_servicio")
public class OrdenServicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "dominio")
    public String dominio;

    @Column(name = "fecha_alta")
    public LocalDateTime fechaAlta;


    @ManyToOne(optional = true)
    @JoinColumn(name = "id_localidad_origen")
    public Localidad localidadOrigen;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_localidad_destino")
    public Localidad localidadDestino;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_estado_orden_servicio")
    public EstadoOrdenServicio estadoOrdenServicio;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_estado_viaje")
    public EstadoViaje estadoViaje;

    @Column(name = "osi_alta")
    public String osiAlta;

    @Column(name = "osi_original")
    public String osiOriginal;

    @Column(name = "osi_cierre")
    public String osiCierre;

    @Column(name = "fecha_cierre")
    public LocalDateTime fechaCierre;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_servicio")
    public Servicio servicio;

    @Column(name = "fmod")
    public LocalDateTime fmod;
}
