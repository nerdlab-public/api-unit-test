package com.api.cnrt.maestros.controller;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.sentry.Sentry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.*;
import com.api.cnrt.maestros.model.OrdenServicioOld;
import com.api.cnrt.maestros.service.*;
import com.api.cnrt.validations.DateValidation;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/maestros")
@RequiredArgsConstructor
public class MaestrosController {
    private final IActaEstadoService<ActaEstadoDTO> actaEstadoService;
    private final IAmbitoService<AmbitoDTO> ambitoService;
    private final IClaseModalidadService<ClaseModalidadDTO> claseModalidadService;
    private final IAmbitoClaseModalidadService<AmbitoClaseModalidadDTO> ambitoClaseModalidadService;
    private final IZonaOperativoService<ZonaOperativoDTO> zonaOperativoService;
    private final ITipoUnidadService<TipoUnidadDTO> tipoUnidadService;
    private final ITipoCategoriaService<TipoCategoriaDTO> tipoCategoriaService;
    private final ITipoDocumentoCobrarService<TipoDocumentoCobrarDTO> tipoDocumentoCobrarService;
    private final ICategoriaChoferService<CategoriaChoferDTO> categoriaChoferService;
    private final ICategoriaVehiculoService<CategoriaVehiculoDTO> categoriaVehiculoService;
    private final IDelegacionService<DelegacionDTO> delegacionService;
    private final IPaisService<PaisDTO> paisService;
    private final IProvinciaService<ProvinciaDTO> provinciaService;
    private final ILocalidadService<LocalidadDTO> localidadService;
    private final IFiscalizadorService<FiscalizadorDTO> fiscalizadorService;
    private final IJurisdiccionService<JurisdiccionDTO> jurisdiccionService;
    private final ITipoTransporteService<TipoTransporteDTO> tipoTransporteService;
    private final IJurisdiccionTipoTransporteService<JurisdiccionTipoTransporteDTO> jurisdiccionTipoTransporteService;
    private final IPredioService<PredioDTO> predioService;
    private final IMotivoNoRetenerService<MotivoNoRetenerDTO> motivoNoRetenerService;
    private final ITipoDocumentoEmpresaService<TipoDocumentoEmpresaDTO> tipoDocumentoEmpresaService;
    private final ITipoDocumentoPersonaService<TipoDocumentoPersonaDTO> tipoDocumentoPersonaService;
    private final ITipoOrdenControlService<TipoOrdenControlDTO> tipoOrdenControlService;
    private final ILicenciaService<LicenciaDTO> licenciaService;
    private final IServicioService<ServicioDTO> servicioService;
    private final ITipoTransporteAmbitoService<TipoTransporteAmbitoDTO> tipoTransporteAmbitoService;
    private final IVehiculoCargaHabilitadoService<VehiculoCargasHabilitadoDTO> vehiculoCargaHabilitadoService;
    private final IVehiculoCNRTService<VehiculoCNRTDTO> vehiculoCNRTService;
    private final IVehiculoJNHabilitadoService<VehiculoJNHabilitadoDTO> vehiculoJNHabilitadoService;
    private final ILoginService<LoginDTO> loginService;
    private final ISustanciaService<SustanciaDTO> sustanciaService;
    private final IAlertaChoferService<AlertaChoferDTO> alertaChoferService;
    private final IEstadoViajeService<EstadoViajeDTO> estadoViajeService;
    private final IEstadoOrdenServicioService<EstadoOrdenServicioDTO> estadoOrdenServicioService;
    private final IEstadoFiscalizacionInfraccionService<EstadoFiscalizacionInfraccionDTO> estadoFiscalizacionInfraccionService;
    private final IResultadoFiscalizacionService<ResultadoFiscalizacionDTO> resultadoFiscalizacionService;
    private final IClasificacionFiscalizacionService<ClasificacionFiscalizacionDTO> clasificacionFiscalizacionService;
    private final ISubclasificacionFiscalizacionService<SubclasificacionFiscalizacionDTO> subclasificacionFiscalizacionService;
    private final IDetalleSubclasificacionFiscalizacionService<DetalleSubclasificacionFiscalizacionDTO> detalleSubclasificacionFiscalizacionService;
    private final IOrdenServicioService<OrdenServicioDTO> ordenServicioService;
    private final IDetalleOrdenServicioService<DetalleOrdenServicioDTO> detalleOrdenServicioService;
    private final ILugarService<LugarDTO> lugarService;
    private final IOrdenServicioOldService<OrdenServicioOldDTO> ordenServicioOldService;
    private final IOrdenServicioItemsOldService<OrdenServicioItemsOldDTO>  ordenServicioItemsOldService;
    private final IFaltaServicioService<FaltaServicioDTO> faltaServicioService;
    private final IPermisoPDAService<PermisosPDADTO> permisoPDAService;


    // Exepcion para la validacion de los campos obligatorios
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @GetMapping("/acta-estado")
    public Page<ActaEstadoDTO> getActaEstadoDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                    @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                    @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                    @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<ActaEstadoDTO> list = initial ? actaEstadoService.findAllBy(true, pageable) : actaEstadoService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/ambito")
    public Page<AmbitoDTO> getAmbitoDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                            @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                            @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                            @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);

        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<AmbitoDTO> list = initial ? ambitoService.findAllBy(true, pageable) : ambitoService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/clase-modalidad")
    public Page<ClaseModalidadDTO> getClaseModalidadDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                            @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                            @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                            @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<ClaseModalidadDTO> list = initial ? claseModalidadService.findAllBy(true, pageable) : claseModalidadService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/ambito-clase-modalidad")
    public Page<AmbitoClaseModalidadDTO> getAmbitoClaseModalidadList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                     @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                     @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                     @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<AmbitoClaseModalidadDTO> list = initial ? ambitoClaseModalidadService.findAllBy(true, pageable) : ambitoClaseModalidadService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/zona-operativo")
    public Page<ZonaOperativoDTO> getZonaOperativoDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<ZonaOperativoDTO> list = initial ? zonaOperativoService.findAllBy(true, pageable) : zonaOperativoService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipo-unidad")
    public Page<TipoUnidadDTO> getTipoUnidadDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                    @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                    @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                    @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoUnidadDTO> list = initial ? tipoUnidadService.findAllBy(true, pageable) : tipoUnidadService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipo-categoria")
    public Page<TipoCategoriaDTO> getTipoCategoriaDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoCategoriaDTO> list = initial ? tipoCategoriaService.findAllBy(true, pageable) : tipoCategoriaService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipo-documento-cobrar")
    public Page<TipoDocumentoCobrarDTO> getTipoDocumentoCobrarDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                      @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                      @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                      @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);

        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoDocumentoCobrarDTO> list = initial ? tipoDocumentoCobrarService.findAllBy(true, pageable) : tipoDocumentoCobrarService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/categoria-chofer")
    public Page<CategoriaChoferDTO> getCategoriaChoferDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                              @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                              @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                              @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);

        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<CategoriaChoferDTO> list = initial ? categoriaChoferService.findAllBy(true, pageable) : categoriaChoferService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/categoria-vehiculo")
    public Page<CategoriaVehiculoDTO> getCategoriaVehiculoDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                  @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                  @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                  @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);

        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<CategoriaVehiculoDTO> list = initial ? categoriaVehiculoService.findAllBy(true, pageable) : categoriaVehiculoService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/delegaciones")
    public Page<DelegacionDTO> getDelegacionDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                    @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                    @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                    @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<DelegacionDTO> list = initial ? delegacionService.findAllBy(true, pageable) : delegacionService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/paises")
    public Page<PaisDTO> getPaisDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<PaisDTO> list = initial ? paisService.findAllBy(true, pageable) : paisService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/provincias")
    public Page<ProvinciaDTO> getProvinciaDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                  @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                  @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                  @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<ProvinciaDTO> list = initial ? provinciaService.findAllBy(true, pageable) : provinciaService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/localidades")
    public Page<LocalidadDTO> getLocalidadDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                  @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                  @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                  @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<LocalidadDTO> list = initial ? localidadService.findAllBy(true, pageable) : localidadService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/fiscalizadores")
    public Page<FiscalizadorDTO> getFiscalizadorDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<FiscalizadorDTO> list = initial ? fiscalizadorService.findAllBy(true, pageable) : fiscalizadorService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/jurisdicciones")
    public Page<JurisdiccionDTO> getJurisdiccionDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<JurisdiccionDTO> list = initial ? jurisdiccionService.findAllBy(true, pageable) : jurisdiccionService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipos-transporte")
    public Page<TipoTransporteDTO> getTiposTranspoteDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                            @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                            @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                            @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoTransporteDTO> list = initial ? tipoTransporteService.findAllBy(true, pageable) : tipoTransporteService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/jurisdiccion-tipos-transporte")
    public Page<JurisdiccionTipoTransporteDTO> getJurisdiccionTiposTranspoteDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                                    @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                                    @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                                    @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<JurisdiccionTipoTransporteDTO> list = initial ? jurisdiccionTipoTransporteService.findAllBy(true, pageable) : jurisdiccionTipoTransporteService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/predios")
    public Page<PredioDTO> getPredioDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                            @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                            @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                            @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<PredioDTO> list = initial ? predioService.findAllBy(true, pageable) : predioService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/motivos-no-retener")
    public Page<MotivoNoRetenerDTO> getMotivoNoRetenerDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                              @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                              @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                              @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<MotivoNoRetenerDTO> list = initial ? motivoNoRetenerService.findAllBy(true, pageable) : motivoNoRetenerService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipos-documento-persona")
    public Page<TipoDocumentoPersonaDTO> getTipoDocumentoPersonaDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoDocumentoPersonaDTO> list = initial ? tipoDocumentoPersonaService.findAllBy(true, pageable) : tipoDocumentoPersonaService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipos-documento-empresa")
    public Page<TipoDocumentoEmpresaDTO> getTipoDocumentoEmpresaDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoDocumentoEmpresaDTO> list = initial ? tipoDocumentoEmpresaService.findAllBy(true, pageable) : tipoDocumentoEmpresaService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipos-orden-control")
    public Page<TipoOrdenControlDTO> getTipoOrdenControlDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoOrdenControlDTO> list = initial ? tipoOrdenControlService.findAllBy(true, pageable) : tipoOrdenControlService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/licencias")
    public Page<LicenciaDTO> getLicenciasDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                              @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                              @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                              @RequestParam(value = "sizePage", defaultValue = "10") int sizePage
    ) throws RepositoryException {
       LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<LicenciaDTO> licenciasPage = initial ? licenciaService.findAllBy(true, pageable): licenciaService.findAllBy(dateTime, pageable);
        return licenciasPage;
    }

    @GetMapping("/servicios")
    public Page<ServicioDTO> getServicioDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<ServicioDTO> list = initial ? servicioService.findAllBy(true, pageable) : servicioService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/tipos-transporte-ambito")
    public Page<TipoTransporteAmbitoDTO> getTipoTransporteAmbitoDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<TipoTransporteAmbitoDTO> list = initial ? tipoTransporteAmbitoService.findAllBy(true, pageable) : tipoTransporteAmbitoService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/vehiculos-carga-habilitados")
    public Page<VehiculoCargasHabilitadoDTO> getVehiculoCargaHabilitadoDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                               @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                               @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                               @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<VehiculoCargasHabilitadoDTO> list = initial ? vehiculoCargaHabilitadoService.findAllBy(true, pageable) : vehiculoCargaHabilitadoService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/vehiculos-jn-habilitados")
    public Page<VehiculoJNHabilitadoDTO> getVehiculoJNHabilitadoDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("dominio"));
        Page<VehiculoJNHabilitadoDTO> list = initial ? vehiculoJNHabilitadoService.findAllBy(true, pageable) : vehiculoJNHabilitadoService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/vehiculos-cnrt")
    public Page<VehiculoCNRTDTO> getVehiculoCNRTDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<VehiculoCNRTDTO> list = initial ? vehiculoCNRTService.findAllBy(true, pageable) : vehiculoCNRTService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/logins")
    public Page<LoginDTO> getLoginDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<LoginDTO> list = initial ? loginService.findAllBy(true, pageable) : loginService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/sustancias")
    public Page<SustanciaDTO> getSustanciaDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                  @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                  @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                  @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<SustanciaDTO> list = initial ? sustanciaService.findAllBy(true, pageable) : sustanciaService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/alertas-chofer")
    public Page<AlertaChoferDTO> getAlertaChoferDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("dni"));
        Page<AlertaChoferDTO> list = initial ? alertaChoferService.findAllBy(true, pageable) : alertaChoferService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/estado-viaje")
    public Page<EstadoViajeDTO> getEstadoViajeDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                      @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                      @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                      @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<EstadoViajeDTO> list = initial ? estadoViajeService.findAllBy(true, pageable) : estadoViajeService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/estado-orden-servicio")
    public Page<EstadoOrdenServicioDTO> getEstadoOrdenServicioDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                      @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                      @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                      @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<EstadoOrdenServicioDTO> list = initial ? estadoOrdenServicioService.findAllBy(true, pageable) : estadoOrdenServicioService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/estado-fiscalizacion-infraccion")
    public Page<EstadoFiscalizacionInfraccionDTO> getEstadoFiscalizacionInfraccionDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<EstadoFiscalizacionInfraccionDTO> list = initial ? estadoFiscalizacionInfraccionService.findAllBy(true, pageable) : estadoFiscalizacionInfraccionService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/resultado-fiscalizacion")
    public Page<ResultadoFiscalizacionDTO> getResultadoFiscalizacionDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                            @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                            @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                            @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<ResultadoFiscalizacionDTO> list = initial ? resultadoFiscalizacionService.findAllBy(true, pageable) : resultadoFiscalizacionService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/clasificacion-fiscalizacion")
    public Page<ClasificacionFiscalizacionDTO> getClasificacionFiscalizacionDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                                    @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                                    @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                                    @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<ClasificacionFiscalizacionDTO> list = initial ? clasificacionFiscalizacionService.findAllBy(true, pageable) : clasificacionFiscalizacionService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/subclasificacion-fiscalizacion")
    public Page<SubclasificacionFiscalizacionDTO> getSubclasificacionFiscalizacionDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<SubclasificacionFiscalizacionDTO> list = initial ? subclasificacionFiscalizacionService.findAllBy(true, pageable) : subclasificacionFiscalizacionService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/detalle-subclasificacion-fiscalizacion")
    public Page<DetalleSubclasificacionFiscalizacionDTO> getDetalleSubclasificacionFiscalizacionDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<DetalleSubclasificacionFiscalizacionDTO> list = initial ? detalleSubclasificacionFiscalizacionService.findAllBy(true, pageable) : detalleSubclasificacionFiscalizacionService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/ordenes-servicio")
    public Page<OrdenServicioDTO> getOrdenServicioDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<OrdenServicioDTO> list = initial ? ordenServicioService.findAllBy(true, pageable) : ordenServicioService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/detalle-ordenes-servicio")
    public Page<DetalleOrdenServicioDTO> getDetalleOrdenServicioDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                        @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                        @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<DetalleOrdenServicioDTO> list = initial ? detalleOrdenServicioService.findAllBy(true, pageable) : detalleOrdenServicioService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/lugares")
    public Page<LugarDTO> getLugarDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                        @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<LugarDTO> list = initial ? lugarService.findAllBy(true, pageable) : lugarService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/ordenes-servicio-old")
    public Page<OrdenServicioOldDTO> getOrdenServicioOldDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("osiAlta"));
        Page<OrdenServicioOldDTO> list = initial ? ordenServicioOldService.findAllBy(true, pageable) : ordenServicioOldService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/ordenes-servicio-items-old")
    public Page<OrdenServicioItemsOldDTO> getOrdenServicioItemsOldDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("osi"));
        Page<OrdenServicioItemsOldDTO> list = ordenServicioItemsOldService.getAllItems(pageable);
        return list;
    }

    @GetMapping("/faltas-servicios")
    public Page<FaltaServicioDTO> getFaltasServicioDTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                                          @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                                          @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                                          @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<FaltaServicioDTO> list = initial ? faltaServicioService.findAllBy(true, pageable) : faltaServicioService.findAllBy(dateTime, pageable);
        return list;
    }

    @GetMapping("/permisos-pda")
    public Page<PermisosPDADTO> getPermisosPDADTOList(@RequestParam(value = "initial", defaultValue = "false") boolean initial,
                                                    @RequestParam(name = "lastDateDb") @DateValidation(pattern = "yyyy-MM-dd", message = "Fecha no valida. Formato valido: yyyy-MM-dd.") String lastDateDb,
                                                    @RequestParam(value = "numberPage", defaultValue = "0") int numberPage,
                                                    @RequestParam(value = "sizePage", defaultValue = "10") int sizePage) throws RepositoryException {
        LocalDateTime dateTime = DateUtils.convertStringToLocalDateTime(lastDateDb);
        Pageable pageable = PageRequest.of(numberPage, sizePage, Sort.by("id"));
        Page<PermisosPDADTO> list = initial ? permisoPDAService.findAllBy(true, pageable) : permisoPDAService.findAllBy(dateTime, pageable);
        return list;
    }


    @GetMapping("/probando-sentry")
    public void probandoSentry(){
        try {
            throw new Exception("This is a test.");
        } catch (Exception e) {
            Sentry.captureException(e);
        }
    }

}
