package com.api.cnrt.maestros.service;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.SustanciaDTO;
import com.api.cnrt.maestros.mapper.SustanciaToSustanciaDTOMapper;
import com.api.cnrt.maestros.model.Sustancia;
import com.api.cnrt.maestros.repository.ISustanciaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SustanciaServiceImpl implements ISustanciaService<SustanciaDTO> {

    private final ISustanciaRepository sustanciaRepository;

    private final SustanciaToSustanciaDTOMapper sustanciaToSustanciaDTOMapper;

    @Override
    public Page<SustanciaDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Sustancia> list = sustanciaRepository.findAll(pageable);
            Page<SustanciaDTO> DTOList = list.map(sustanciaToSustanciaDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<SustanciaDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Sustancia> list = sustanciaRepository.findFromLastSincro(lastSyncro, pageable);
            Page<SustanciaDTO> DTOList = list.map(sustanciaToSustanciaDTOMapper::map);
            return DTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
