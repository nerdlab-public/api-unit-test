package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ProvinciaDTO;
import com.api.cnrt.maestros.mapper.ProvinciaToProvinciaDTOMapper;
import com.api.cnrt.maestros.model.Provincia;
import com.api.cnrt.maestros.repository.IProvinciaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProvinciaServiceImpl implements IProvinciaService<ProvinciaDTO>{

	private final IProvinciaRepository provinciaRepository;
	private final ProvinciaToProvinciaDTOMapper provinciaToProvinciaDTOMapper;

    @Override
    public Page<ProvinciaDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Provincia> provinciaList = provinciaRepository.findAll(pageable);

            Page<ProvinciaDTO> provinciaDTOList = provinciaList.map(provinciaToProvinciaDTOMapper::map);

            return provinciaDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
    
    @Override
    public Page<ProvinciaDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
            Page<Provincia> provinciaList = provinciaRepository.findFromLastSincro(lastDateDb, pageable);

            Page<ProvinciaDTO> provinciaDTOList = provinciaList.map(provinciaToProvinciaDTOMapper::map);

            return provinciaDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    
}
