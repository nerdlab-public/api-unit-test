package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.LugarDTO;
import com.api.cnrt.maestros.mapper.LugaresToLugarDTOMapper;
import com.api.cnrt.maestros.model.Localidad;
import com.api.cnrt.maestros.model.Lugar;
import com.api.cnrt.maestros.repository.ILocalidadRepository;
import com.api.cnrt.maestros.repository.ILugarRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LugarServiceImpl implements ILugarService<LugarDTO> {

    private final ILugarRepository repository;

    private final LugaresToLugarDTOMapper mapper;

    private final ILocalidadRepository localidadRepository;


    @Override
    public Page<LugarDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Lugar> list = repository.findAll(pageable);
            Page<LugarDTO> instanceDTOList = list.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<LugarDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Lugar> list = repository.findFromLastSincro(lastSyncro, pageable);
            Page<LugarDTO> instanceDTOList = list.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    public Long grabarNuevoLugar(String lugarDescripcion, Long idLocalidad) throws RepositoryException {
        try{
            Localidad localidad = localidadRepository.findById(idLocalidad)
                    .orElseThrow(() -> new RepositoryException("Localidad no encontrada con ID: " + idLocalidad));

            Lugar lugar = new Lugar();
            lugar.setDescripcion(lugarDescripcion);
            lugar.setLocalidad(localidad);

            repository.save(lugar);

            return lugar.getId();

        }catch(Exception ex){
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
