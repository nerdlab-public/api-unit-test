package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.mapper.FiscalizadorToFiscalizadorDTOMapper;
import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.repository.IFiscalizadorRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FiscalizadorServiceImpl implements IFiscalizadorService<FiscalizadorDTO> {

    private final IFiscalizadorRepository fiscalizadorRepository;

    private final FiscalizadorToFiscalizadorDTOMapper fiscalizadorToFiscalizadorDTOMapper;


    @Override
    public Page<FiscalizadorDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Fiscalizador> list = fiscalizadorRepository.findAll(pageable);
            Page<FiscalizadorDTO> fiscalizadorDTOList = list.map(fiscalizadorToFiscalizadorDTOMapper::map);

            return fiscalizadorDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<FiscalizadorDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Fiscalizador> list = fiscalizadorRepository.findFromLastSincro(lastSyncro, pageable);
            Page<FiscalizadorDTO> fiscalizadorDTOList = list.map(fiscalizadorToFiscalizadorDTOMapper::map);
            return fiscalizadorDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
