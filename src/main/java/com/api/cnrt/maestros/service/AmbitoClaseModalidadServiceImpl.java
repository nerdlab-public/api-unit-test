package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.AmbitoClaseModalidadDTO;
import com.api.cnrt.maestros.mapper.AmbitoClaseModalidadToAmbitoClaseModalidadDTOMapper;
import com.api.cnrt.maestros.model.AmbitoClaseModalidad;
import com.api.cnrt.maestros.repository.IAmbitoClaseModalidadRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AmbitoClaseModalidadServiceImpl  implements IAmbitoClaseModalidadService<AmbitoClaseModalidadDTO>{

    private final IAmbitoClaseModalidadRepository ambitoClaseModalidadRepository;
    private final AmbitoClaseModalidadToAmbitoClaseModalidadDTOMapper ambitoClaseModalidadDTOMapper;


    @Override
    public Page<AmbitoClaseModalidadDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<AmbitoClaseModalidad> list = ambitoClaseModalidadRepository.findAll(pageable);

            Page<AmbitoClaseModalidadDTO> ambitoClaseModalidadDTOList = list.map(ambitoClaseModalidadDTOMapper::map);

            return ambitoClaseModalidadDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<AmbitoClaseModalidadDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
            Page<AmbitoClaseModalidad> list = ambitoClaseModalidadRepository.findFromLastSincro(lastDateDb, pageable);

            Page<AmbitoClaseModalidadDTO> ambitoClaseModalidadDTOList = list.map(ambitoClaseModalidadDTOMapper::map);

            return ambitoClaseModalidadDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }


}
