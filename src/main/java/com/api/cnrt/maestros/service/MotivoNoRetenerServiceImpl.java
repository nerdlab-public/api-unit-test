package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.MotivoNoRetenerDTO;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.mapper.MotivoNoRetenerToMotivoNoRetenerDTOMapper;
import com.api.cnrt.maestros.mapper.PredioToPredioDTOMapper;
import com.api.cnrt.maestros.model.MotivoNoRetener;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.repository.IMotivoNoRetenerRepository;
import com.api.cnrt.maestros.repository.IPredioRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MotivoNoRetenerServiceImpl implements IMotivoNoRetenerService<MotivoNoRetenerDTO> {

    private final IMotivoNoRetenerRepository motivoNoRetenerRepository;

    private final MotivoNoRetenerToMotivoNoRetenerDTOMapper motivoNoRetenerToMotivoNoRetenerDTOMapper;


    @Override
    public Page<MotivoNoRetenerDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<MotivoNoRetener> motivoList = motivoNoRetenerRepository.findAll(pageable);
            Page<MotivoNoRetenerDTO> motivoDTOList = motivoList.map(motivoNoRetenerToMotivoNoRetenerDTOMapper::map);

            return motivoDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<MotivoNoRetenerDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<MotivoNoRetener> motivoList = motivoNoRetenerRepository.findFromLastSincro(lastSyncro, pageable);
            Page<MotivoNoRetenerDTO> motivoDTOList = motivoList.map(motivoNoRetenerToMotivoNoRetenerDTOMapper::map);
            return motivoDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
