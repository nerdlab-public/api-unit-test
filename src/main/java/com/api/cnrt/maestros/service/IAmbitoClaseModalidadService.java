package com.api.cnrt.maestros.service;

import java.time.LocalDateTime;
import java.util.List;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.AmbitoClaseModalidadDTO;
import com.api.cnrt.maestros.dto.AmbitoDTO;
import com.api.cnrt.maestros.service.common.ICommonGetService;
import com.api.cnrt.maestros.service.common.ICommonPageableService;

public interface IAmbitoClaseModalidadService<T> extends ICommonPageableService<T> {
}
