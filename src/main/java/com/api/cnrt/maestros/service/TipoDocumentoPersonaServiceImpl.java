package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoDocumentoEmpresaDTO;
import com.api.cnrt.maestros.dto.TipoDocumentoPersonaDTO;
import com.api.cnrt.maestros.mapper.TipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper;
import com.api.cnrt.maestros.mapper.TipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper;
import com.api.cnrt.maestros.model.TipoDocumentoEmpresa;
import com.api.cnrt.maestros.model.TipoDocumentoPersona;
import com.api.cnrt.maestros.repository.ITipoDocumentoEmpresaRepository;
import com.api.cnrt.maestros.repository.ITipoDocumentoPersonaRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TipoDocumentoPersonaServiceImpl implements ITipoDocumentoPersonaService<TipoDocumentoPersonaDTO> {

    private final ITipoDocumentoPersonaRepository tipoDocumentoPersonaRepository;

    private final TipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper tipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper;


    @Override
    public Page<TipoDocumentoPersonaDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoDocumentoPersona> instanceList = tipoDocumentoPersonaRepository.findAll(pageable);
            Page<TipoDocumentoPersonaDTO> instanceDTOList = instanceList.map(tipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<TipoDocumentoPersonaDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoDocumentoPersona> instanceList = tipoDocumentoPersonaRepository.findFromLastSincro(lastSyncro, pageable);
            Page<TipoDocumentoPersonaDTO> instanceDTOList = instanceList.map(tipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
