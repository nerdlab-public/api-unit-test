package com.api.cnrt.maestros.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.service.common.ICommonPageableService;

public interface IOrdenServicioItemsOldService<T>  {
    Page<T> getAllItems(Pageable pageable) throws RepositoryException;
}

