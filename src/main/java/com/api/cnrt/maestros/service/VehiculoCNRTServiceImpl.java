package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.dto.VehiculoCNRTDTO;
import com.api.cnrt.maestros.mapper.FiscalizadorToFiscalizadorDTOMapper;
import com.api.cnrt.maestros.mapper.VehiculoCNRTToVehiculoCNRTDTOMapper;
import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.VehiculoCNRT;
import com.api.cnrt.maestros.repository.IFiscalizadorRepository;
import com.api.cnrt.maestros.repository.IVehiculoCNRTRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VehiculoCNRTServiceImpl implements IVehiculoCNRTService<VehiculoCNRTDTO> {

    private final IVehiculoCNRTRepository vehiculoCNRTRepository;
    private final VehiculoCNRTToVehiculoCNRTDTOMapper vehiculoCNRTToVehiculoCNRTDTOMapper;

    @Override
    public Page<VehiculoCNRTDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<VehiculoCNRT> list = vehiculoCNRTRepository.findAll(pageable);
            Page<VehiculoCNRTDTO> DTOList = list.map(vehiculoCNRTToVehiculoCNRTDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<VehiculoCNRTDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<VehiculoCNRT> list = vehiculoCNRTRepository.findFromLastSincro(lastSyncro, pageable);
            Page<VehiculoCNRTDTO> DTOList = list.map(vehiculoCNRTToVehiculoCNRTDTOMapper::map);
            return DTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
