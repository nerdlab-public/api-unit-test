package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.LocalidadDTO;
import com.api.cnrt.maestros.mapper.LocalidadToLocalidadDTOMapper;
import com.api.cnrt.maestros.model.Localidad;
import com.api.cnrt.maestros.repository.ILocalidadRepository;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LocalidadServiceImpl implements ILocalidadService<LocalidadDTO> {

    private final ILocalidadRepository localidadRepository;
    private final LocalidadToLocalidadDTOMapper localidadToLocalidadDTOMapper;

    @Override
    public Page<LocalidadDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Localidad> list = localidadRepository.findAll(pageable);

            Page<LocalidadDTO> localidadDTOList = list.map(localidadToLocalidadDTOMapper::map);

            return localidadDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<LocalidadDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
            Page<Localidad> list = localidadRepository.findFromLastSincro(lastDateDb, pageable);

            Page<LocalidadDTO> localidadDTOList = list.map(localidadToLocalidadDTOMapper::map);

            return localidadDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }


    public boolean isIdLocalidadInProvincia(Long idLocalidad, Long idProvincia) {
        return localidadRepository.findById(idLocalidad)
                .map(localidad -> idProvincia.equals(localidad.getProvinciaId()))
                .orElse(false);
    }
}
