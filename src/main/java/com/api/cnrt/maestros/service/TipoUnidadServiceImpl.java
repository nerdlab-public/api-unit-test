package com.api.cnrt.maestros.service;


import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoUnidadDTO;
import com.api.cnrt.maestros.mapper.TipoUnidadToTipoUnidadDTOMapper;
import com.api.cnrt.maestros.model.TipoUnidad;
import com.api.cnrt.maestros.repository.ITipoUnidadRepository;

@Service
@RequiredArgsConstructor
public class TipoUnidadServiceImpl implements ITipoUnidadService<TipoUnidadDTO>{

	private final ITipoUnidadRepository tipoUnidadRepository;
	private final TipoUnidadToTipoUnidadDTOMapper tipoUnidadToTipoUnidadDTOMapper;



    @Override
    public Page<TipoUnidadDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoUnidad> list = tipoUnidadRepository.findAll(pageable);

            Page<TipoUnidadDTO> tipoUnidadDTOList = list.map(tipoUnidadToTipoUnidadDTOMapper::map);

            return tipoUnidadDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<TipoUnidadDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoUnidad> list = tipoUnidadRepository.findFromLastSincro(lastSyncro, pageable);

            Page<TipoUnidadDTO> tipoUnidadDTOList = list.map(tipoUnidadToTipoUnidadDTOMapper::map);

            return tipoUnidadDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
