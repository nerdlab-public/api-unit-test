package com.api.cnrt.maestros.service;

import com.api.cnrt.maestros.service.common.ICommonGetService;
import com.api.cnrt.maestros.service.common.ICommonPageableService;

import jakarta.validation.constraints.NotNull;

public interface ILocalidadService<T> extends ICommonPageableService<T> {
}
