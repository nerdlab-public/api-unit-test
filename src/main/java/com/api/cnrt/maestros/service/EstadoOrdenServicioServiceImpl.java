package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoOrdenServicioDTO;
import com.api.cnrt.maestros.dto.ResultadoFiscalizacionDTO;
import com.api.cnrt.maestros.mapper.EstadoOrdenServicioToEstadoOrdenServicioDTOMapper;
import com.api.cnrt.maestros.mapper.ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.EstadoOrdenServicio;
import com.api.cnrt.maestros.model.ResultadoFiscalizacion;
import com.api.cnrt.maestros.repository.IEstadoOrdenServicioRepository;
import com.api.cnrt.maestros.repository.IResultadoFiscalizacionRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EstadoOrdenServicioServiceImpl implements IEstadoOrdenServicioService<EstadoOrdenServicioDTO> {

    private final IEstadoOrdenServicioRepository repository;

    private final EstadoOrdenServicioToEstadoOrdenServicioDTOMapper mapper;


    @Override
    public Page<EstadoOrdenServicioDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<EstadoOrdenServicio> list = repository.findAll(pageable);
            Page<EstadoOrdenServicioDTO> instanceDTOList = list.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<EstadoOrdenServicioDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<EstadoOrdenServicio> list = repository.findFromLastSincro(lastSyncro, pageable);
            Page<EstadoOrdenServicioDTO> instanceDTOList = list.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
