package com.api.cnrt.maestros.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.model.Fiscalizacion;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionDominio;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.dto.VehiculoCargasHabilitadoDTO;
import com.api.cnrt.maestros.mapper.FiscalizadorToFiscalizadorDTOMapper;
import com.api.cnrt.maestros.mapper.VehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper;
import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.VehiculoCargaHabilitado;
import com.api.cnrt.maestros.repository.IFiscalizadorRepository;
import com.api.cnrt.maestros.repository.IVehiculoCargasHabilitadoRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VehiculoCargaHabilitadoServiceImpl implements IVehiculoCargaHabilitadoService<VehiculoCargasHabilitadoDTO> {

    private final IVehiculoCargasHabilitadoRepository vehiculoCargasHabilitadoRepository;

    private final VehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper vehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper;


    @Override
    public Page<VehiculoCargasHabilitadoDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            // TODO: si nos saben decir que significan los valores de estado, llevar a una constante
            Page<VehiculoCargaHabilitado> list = vehiculoCargasHabilitadoRepository.findByEstado(0, pageable);
            Page<VehiculoCargasHabilitadoDTO> DTOList = list.map(vehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<VehiculoCargasHabilitadoDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<VehiculoCargaHabilitado> list = vehiculoCargasHabilitadoRepository.findFromLastSincro(lastSyncro, pageable);
            Page<VehiculoCargasHabilitadoDTO> DTOList = list.map(vehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper::map);
            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Transactional
    public List<VehiculoCargaHabilitado> updateVehiculosCargas(Fiscalizacion fiscaCabeceraSaved, List<FiscalizacionDominio> fiscaDominiosList) {
        List<VehiculoCargaHabilitado> vehiculosCargaUpdated = new ArrayList<>();

        for (FiscalizacionDominio dominio : fiscaDominiosList) {
            String dominioVehiculo = dominio.getDominio();

            if (dominioVehiculo != null) {
                VehiculoCargaHabilitado vehiculoCarga = vehiculoCargasHabilitadoRepository.findByDominio(dominioVehiculo);

                if (vehiculoCarga != null) {
                    vehiculoCarga.setFmod(LocalDateTime.now());
                    vehiculoCarga.setUltimaFiscaFecha(DateUtils.convertLocalDateTimeToString(fiscaCabeceraSaved.getFechaCierre()));
                    vehiculoCarga.setUltimaFiscaResultado(dominio.getUltimaFiscaResultado());
                    vehiculoCargasHabilitadoRepository.save(vehiculoCarga);
                    vehiculosCargaUpdated.add(vehiculoCarga);
                }
            }
        }

        return vehiculosCargaUpdated;




    }
}
