package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.CategoriaChoferDTO;
import com.api.cnrt.maestros.mapper.CategoriaChoferToCategoriaChoferDTOMapper;
import com.api.cnrt.maestros.model.CategoriaChofer;
import com.api.cnrt.maestros.repository.ICategoriaChoferRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoriaChoferServiceImpl implements ICategoriaChoferService<CategoriaChoferDTO> {


    private final ICategoriaChoferRepository categoriaChoferRepository;

    private final CategoriaChoferToCategoriaChoferDTOMapper categoriaChoferToCategoriaChoferDTOMapper;



    @Override
    public Page<CategoriaChoferDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<CategoriaChofer> list = categoriaChoferRepository.findAll(pageable);

            Page<CategoriaChoferDTO> categoriaChoferDTOList = list.map(categoriaChoferToCategoriaChoferDTOMapper::map);

            return categoriaChoferDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<CategoriaChoferDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
            Page<CategoriaChofer> list = categoriaChoferRepository.findFromLastSincro(lastDateDb, pageable);

            Page<CategoriaChoferDTO> categoriaChoferDTOList = list.map(categoriaChoferToCategoriaChoferDTOMapper::map);

            return categoriaChoferDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }


}
