package com.api.cnrt.maestros.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.model.Fiscalizacion;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionDominio;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionInfraccion;
import com.api.cnrt.maestros.dto.OrdenServicioDTO;
import com.api.cnrt.maestros.mapper.OrdenServicioToOrdenServicioDTOMapper;
import com.api.cnrt.maestros.model.*;
import com.api.cnrt.maestros.repository.IDetalleOrdenServicioRepository;
import com.api.cnrt.maestros.repository.IOrdenServicioRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrdenServicioServiceImpl implements IOrdenServicioService<OrdenServicioDTO> {

    private final IOrdenServicioRepository repository;

    private final OrdenServicioToOrdenServicioDTOMapper mapper;

    private final IDetalleOrdenServicioRepository detalleOrdenServicioRepository;


    @Override
    public Page<OrdenServicioDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<OrdenServicio> instanceList = repository.findAll(pageable);
            Page<OrdenServicioDTO> instanceDTOList = instanceList.map( mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<OrdenServicioDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<OrdenServicio> instanceList = repository.findFromLastSincro(lastSyncro, pageable);
            Page<OrdenServicioDTO> instanceDTOList = instanceList.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    public List<OrdenServicio> buscarOrdenServicioExistente(FiscalizacionDominio fiscaDominio) {

            List<OrdenServicio> ordenServicioByDominioList = repository.findByDominioAndFechaCierreIsNull(fiscaDominio.getDominio());

        return ordenServicioByDominioList;

    }

    @Transactional
    public OrdenServicio crearNuevaOrdenServicio(String dominio, List<FiscalizacionInfraccion> fiscaInfracionList, Fiscalizacion fiscaCabeceraSaved, String osiOriginal) {

        OrdenServicio nuevaOrdenServicio = new OrdenServicio();
        nuevaOrdenServicio.setDominio(dominio);
        nuevaOrdenServicio.setFechaAlta(LocalDateTime.now());
        nuevaOrdenServicio.setLocalidadOrigen(fiscaCabeceraSaved.getLocalidadOrigen());
        nuevaOrdenServicio.setLocalidadDestino(fiscaCabeceraSaved.getLocalidadDestino());
        EstadoOrdenServicio estadoOrdenServicio = new EstadoOrdenServicio();
        estadoOrdenServicio.setId(1L);
        nuevaOrdenServicio.setEstadoOrdenServicio(estadoOrdenServicio);
        nuevaOrdenServicio.setEstadoViaje(fiscaCabeceraSaved.getEstadoViaje());
        nuevaOrdenServicio.setOsiAlta(fiscaCabeceraSaved.getOsi());
        nuevaOrdenServicio.setOsiOriginal(osiOriginal != null ? osiOriginal : fiscaCabeceraSaved.getOsi());
        nuevaOrdenServicio.setServicio(fiscaCabeceraSaved.getServicio());
        nuevaOrdenServicio.setFmod(LocalDateTime.now());


        OrdenServicio ordenServicioSaved = repository.save(nuevaOrdenServicio);

        List<DetalleOrdenServicio> detallesOrdenServicioList = new ArrayList<>();
        for (FiscalizacionInfraccion fiscaInfraccion : fiscaInfracionList) {
            DetalleOrdenServicio detalleOrdenServicio = new DetalleOrdenServicio();
            detalleOrdenServicio.setOrdenServicio(nuevaOrdenServicio);
            detalleOrdenServicio.setOsi(fiscaInfraccion.getOsi());
            detalleOrdenServicio.setObservacion(fiscaInfraccion.getObservacion());
            detalleOrdenServicio.setCantidad(fiscaInfraccion.getCantidad());
            detalleOrdenServicio.setFmod(LocalDateTime.now());
            detalleOrdenServicio.setIdFaltaServicio(fiscaInfraccion.getIdFaltaServicio());
            detallesOrdenServicioList.add(detalleOrdenServicio);
        }

        detalleOrdenServicioRepository.saveAll(detallesOrdenServicioList);

        return ordenServicioSaved;

    }


    public List<OrdenServicio> cerrarOrdenesServicio(List<OrdenServicio> existingOsiListToClose, String osi) {
        existingOsiListToClose.forEach(item -> {
            item.setOsiCierre(osi);
            item.setFechaCierre(LocalDateTime.now());
            item.setFmod(LocalDateTime.now());
            repository.save(item);
        });

        return existingOsiListToClose;
    }


    @Transactional
    public OrdenServicio cerrarYCrearOrdenesServicio(List<OrdenServicio> existingOsiList, Fiscalizacion fiscaCabeceraSaved, List<FiscalizacionInfraccion> fiscaInfracionList, String dominio) {
        try {
            List<OrdenServicio> osiClosed = cerrarOrdenesServicio(existingOsiList, fiscaCabeceraSaved.getOsi());
            return crearNuevaOrdenServicio(dominio, fiscaInfracionList, fiscaCabeceraSaved, osiClosed.get(0).getOsiOriginal());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error while closing and creating orders of service", e);
        }
    }

    public List<FiscalizacionInfraccion> generaOrdenServicio(List<FiscalizacionInfraccion> fiscaInfracionList, Fiscalizacion fiscaCabeceraSaved) {
        return fiscaInfracionList.stream()
                .filter(infraccion -> infraccion.getDominio() != null)
                .collect(Collectors.toList());

    }

    public List<FiscalizacionInfraccion> generaAlertaChofer(List<FiscalizacionInfraccion> fiscaInfracionList, Fiscalizacion fiscaCabeceraSaved) {
        return fiscaInfracionList.stream()
                .filter(infraccion -> infraccion.getDni() != null)
                .collect(Collectors.toList());
    }
}
