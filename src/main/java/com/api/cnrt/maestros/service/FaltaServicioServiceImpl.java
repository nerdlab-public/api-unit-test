package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ClasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.dto.FaltaServicioDTO;
import com.api.cnrt.maestros.mapper.ClasificacionFiscalizacionToClasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.mapper.FaltaServicioToFaltaServicioDTOMapper;
import com.api.cnrt.maestros.model.ClasificacionFiscalizacion;
import com.api.cnrt.maestros.model.FaltaServicio;
import com.api.cnrt.maestros.repository.IClasificacionFiscalizacionRepository;
import com.api.cnrt.maestros.repository.IFaltaServicioRepository;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class FaltaServicioServiceImpl implements IFaltaServicioService<FaltaServicioDTO> {

    private final IFaltaServicioRepository repository;

    private final FaltaServicioToFaltaServicioDTOMapper mapper;


    @Override
    public Page<FaltaServicioDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<FaltaServicio> list = repository.findAll(pageable);
            Page<FaltaServicioDTO> instanceDTOList = list.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<FaltaServicioDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<FaltaServicio> list = repository.findFromLastSincro(lastSyncro, pageable);
            Page<FaltaServicioDTO> instanceDTOList = list.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
