package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.CategoriaVehiculoDTO;
import com.api.cnrt.maestros.mapper.CategoriaVehiculoToCategoriaVehiculoDTOMapper;
import com.api.cnrt.maestros.model.CategoriaVehiculo;
import com.api.cnrt.maestros.repository.ICategoriaVehiculoRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoriaVehiculoServiceImpl implements ICategoriaVehiculoService<CategoriaVehiculoDTO>{

    private final ICategoriaVehiculoRepository categoriaVehiculoRepository;

    private final CategoriaVehiculoToCategoriaVehiculoDTOMapper categoriaVehiculoToCategoriaVehiculoDTOMapper;



    @Override
    public Page<CategoriaVehiculoDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<CategoriaVehiculo> list = categoriaVehiculoRepository.findAll(pageable);

            Page<CategoriaVehiculoDTO> categoriaVehiculoDTOList = list.map(categoriaVehiculoToCategoriaVehiculoDTOMapper::map);

            return categoriaVehiculoDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<CategoriaVehiculoDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
            Page<CategoriaVehiculo> list = categoriaVehiculoRepository.findFromLastSincro(lastDateDb, pageable);

            Page<CategoriaVehiculoDTO> CategoriaVehiculoDTOList = list.map(categoriaVehiculoToCategoriaVehiculoDTOMapper::map);

            return CategoriaVehiculoDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

}
