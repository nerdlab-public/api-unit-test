package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ClasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.mapper.ClasificacionFiscalizacionToClasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.mapper.EstadoViajeToEstadoViajeDTOMapper;
import com.api.cnrt.maestros.model.ClasificacionFiscalizacion;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.repository.IClasificacionFiscalizacionRepository;
import com.api.cnrt.maestros.repository.IEstadoViajeRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClasificacionFiscalizacionServiceImpl implements IClasificacionFiscalizacionService<ClasificacionFiscalizacionDTO> {

    private final IClasificacionFiscalizacionRepository repository;

    private final ClasificacionFiscalizacionToClasificacionFiscalizacionDTOMapper mapper;


    @Override
    public Page<ClasificacionFiscalizacionDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<ClasificacionFiscalizacion> list = repository.findAll(pageable);
            Page<ClasificacionFiscalizacionDTO> instanceDTOList = list.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<ClasificacionFiscalizacionDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<ClasificacionFiscalizacion> list = repository.findFromLastSincro(lastSyncro, pageable);
            Page<ClasificacionFiscalizacionDTO> instanceDTOList = list.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
