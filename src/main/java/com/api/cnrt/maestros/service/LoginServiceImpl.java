package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.LoginDTO;
import com.api.cnrt.maestros.dto.ServicioDTO;
import com.api.cnrt.maestros.mapper.LoginToLoginDTOMapper;
import com.api.cnrt.maestros.mapper.ServicioToServicioDTOMapper;
import com.api.cnrt.maestros.model.Login;
import com.api.cnrt.maestros.model.Servicio;
import com.api.cnrt.maestros.repository.ILoginRepository;
import com.api.cnrt.maestros.repository.IServicioRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements ILoginService<LoginDTO> {

    private final ILoginRepository loginRepository;

    private final LoginToLoginDTOMapper loginToLoginDTOMapper;


    @Override
    public Page<LoginDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Login> list = loginRepository.findAllByIdGreaterThanOne(pageable);
            Page<LoginDTO> DTOList = list.map(loginToLoginDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<LoginDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Login> list = loginRepository.findFromLastSincro(lastSyncro, pageable);
            Page<LoginDTO> DTOList = list.map(loginToLoginDTOMapper::map);
            return DTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
