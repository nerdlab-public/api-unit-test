package com.api.cnrt.maestros.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.common.DateUtils;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.model.Fiscalizacion;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionChofer;
import com.api.cnrt.maestros.dto.LicenciaDTO;
import com.api.cnrt.maestros.mapper.LicenciaToLicenciaDTOMapper;
import com.api.cnrt.maestros.model.Licencia;
import com.api.cnrt.maestros.repository.ILicenciaRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LicenciaServiceImpl implements ILicenciaService<LicenciaDTO> {

    private final ILicenciaRepository licenciaRepository;

    private final LicenciaToLicenciaDTOMapper licenciaToLicenciaDTOMapper;


    @Override
    public Page<LicenciaDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Licencia> licenciasPage = licenciaRepository.findAll(pageable);
            Page<LicenciaDTO> licenciasDTOPage = licenciasPage.map(licenciaToLicenciaDTOMapper::map);
            return licenciasDTOPage;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<LicenciaDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Licencia> licenciasPage = licenciaRepository.findFromLastSincro(lastSyncro, pageable);
            Page<LicenciaDTO> licenciasDTOPage = licenciasPage.map(licenciaToLicenciaDTOMapper::map);
            return licenciasDTOPage;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }


    @Transactional
    public List<Licencia> updateLicencias(Fiscalizacion fiscaCabeceraSaved, List<FiscalizacionChofer> fiscaChoferesList) {


        List<Licencia> licenciaUpdated = new ArrayList<>();
         for (FiscalizacionChofer chofer: fiscaChoferesList){
            String dni = chofer.getDni();

            if (dni != null){

                Licencia licenciaChofer = licenciaRepository.findByDni(chofer.getDni());

                if(licenciaChofer != null){
                    licenciaChofer.setFmod(LocalDateTime.now());
                    licenciaChofer.setFechaUltimaFiscalizacion(DateUtils.convertLocalDateTimeToString(fiscaCabeceraSaved.getFechaCierre()));
                    licenciaChofer.setResultadoUltimaFiscalizacion(chofer.getResultadoFisca());
                    licenciaRepository.save(licenciaChofer);
                    licenciaUpdated.add(licenciaChofer);
                }
            }
        }
            return licenciaUpdated;

    }
}
