package com.api.cnrt.maestros.service.common;

import java.time.LocalDateTime;
import java.util.List;

import com.api.cnrt.exceptions.RepositoryException;

public interface ICommonGetService <T> {
    List<T> findAllBy(Boolean initial) throws RepositoryException;
    List<T> findAllBy(LocalDateTime lastSyncro) throws RepositoryException;
}
