package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.dto.TipoDocumentoEmpresaDTO;
import com.api.cnrt.maestros.mapper.PredioToPredioDTOMapper;
import com.api.cnrt.maestros.mapper.TipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.model.TipoDocumentoEmpresa;
import com.api.cnrt.maestros.repository.IPredioRepository;
import com.api.cnrt.maestros.repository.ITipoDocumentoEmpresaRepository;
import com.api.cnrt.maestros.repository.ITipoTransporteRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TipoDocumentoEmpresaServiceImpl implements ITipoDocumentoEmpresaService<TipoDocumentoEmpresaDTO> {

    private final ITipoDocumentoEmpresaRepository tipoDocumentoEmpresaRepository;

    private final TipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper tipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper;


    @Override
    public Page<TipoDocumentoEmpresaDTO> findAllBy(Boolean initial, org.springframework.data.domain.Pageable pageable) throws RepositoryException {
        try {
            Page<TipoDocumentoEmpresa> instanceList = tipoDocumentoEmpresaRepository.findAll(pageable);
            Page<TipoDocumentoEmpresaDTO> instanceDTOList = instanceList.map(tipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<TipoDocumentoEmpresaDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoDocumentoEmpresa> instanceList = tipoDocumentoEmpresaRepository.findFromLastSincro(lastSyncro, pageable);
            Page<TipoDocumentoEmpresaDTO> instanceDTOList = instanceList.map(tipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
