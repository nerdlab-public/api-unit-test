package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.dto.TipoTransporteAmbitoDTO;
import com.api.cnrt.maestros.mapper.FiscalizadorToFiscalizadorDTOMapper;
import com.api.cnrt.maestros.mapper.TipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper;
import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.TipoTransporteAmbito;
import com.api.cnrt.maestros.repository.ITipoTransporteAmbitoRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TipoTransporteAmbitoServiceImpl implements ITipoTransporteAmbitoService<TipoTransporteAmbitoDTO> {

    private final ITipoTransporteAmbitoRepository tipoTransporteAmbitoRepository;

    private final TipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper tipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper;


    @Override
    public Page<TipoTransporteAmbitoDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoTransporteAmbito> list = tipoTransporteAmbitoRepository.findAll(pageable);
            Page<TipoTransporteAmbitoDTO> DTOList = list.map(tipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<TipoTransporteAmbitoDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoTransporteAmbito> list = tipoTransporteAmbitoRepository.findFromLastSincro(lastSyncro, pageable);
            Page<TipoTransporteAmbitoDTO> DTOList = list.map(tipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper::map);
            return DTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
