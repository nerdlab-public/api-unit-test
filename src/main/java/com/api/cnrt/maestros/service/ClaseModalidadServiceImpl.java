package com.api.cnrt.maestros.service;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ClaseModalidadDTO;
import com.api.cnrt.maestros.mapper.ClaseModalidadToClaseModalidadDTOMapper;
import com.api.cnrt.maestros.model.ClaseModalidad;
import com.api.cnrt.maestros.repository.IClaseModalidadRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ClaseModalidadServiceImpl implements IClaseModalidadService<ClaseModalidadDTO> {

	private final IClaseModalidadRepository claseModalidadRepository;
	private final ClaseModalidadToClaseModalidadDTOMapper claseModalidadToClaseModalidadDTOMapper;

	@Override
	public Page<ClaseModalidadDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
		try {
			Page<ClaseModalidad> list = claseModalidadRepository.findAll(pageable);
			Page<ClaseModalidadDTO> DTOList = list.map(claseModalidadToClaseModalidadDTOMapper::map);

			return DTOList;
		} catch (Exception ex) {
			throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
		}
	}

	@Override
	public Page<ClaseModalidadDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
		try {
			Page<ClaseModalidad> list = claseModalidadRepository.findFromLastSincro(lastSyncro, pageable);
			Page<ClaseModalidadDTO> DTOList = list.map(claseModalidadToClaseModalidadDTOMapper::map);
			return DTOList;

		} catch (Exception ex) {
			throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
		}
	}
}
