package com.api.cnrt.maestros.service;

import com.api.cnrt.maestros.service.common.ICommonGetService;
import com.api.cnrt.maestros.service.common.ICommonPageableService;

public interface IPredioService<T> extends ICommonPageableService<T> {
}
