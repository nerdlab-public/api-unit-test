package com.api.cnrt.maestros.service.common;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.api.cnrt.exceptions.RepositoryException;

import java.time.LocalDateTime;
import java.util.List;

public interface ICommonPageableService<T> {
    Page<T> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException;
    Page<T> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException;
}
