package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoFiscalizacionInfraccionDTO;
import com.api.cnrt.maestros.dto.EstadoOrdenServicioDTO;
import com.api.cnrt.maestros.mapper.EstadoFiscalizacionInfraccionToEstadoFiscalizacionInfraccionDTOMapper;
import com.api.cnrt.maestros.mapper.EstadoOrdenServicioToEstadoOrdenServicioDTOMapper;
import com.api.cnrt.maestros.model.EstadoFiscalizacionInfraccion;
import com.api.cnrt.maestros.model.EstadoOrdenServicio;
import com.api.cnrt.maestros.repository.IEstadoFiscalizacionInfraccionRepository;
import com.api.cnrt.maestros.repository.IEstadoOrdenServicioRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EstadoFiscalizacionInfraccionServiceImpl implements IEstadoFiscalizacionInfraccionService<EstadoFiscalizacionInfraccionDTO> {

    private final IEstadoFiscalizacionInfraccionRepository repository;

    private final EstadoFiscalizacionInfraccionToEstadoFiscalizacionInfraccionDTOMapper mapper;


    @Override
    public Page<EstadoFiscalizacionInfraccionDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<EstadoFiscalizacionInfraccion> list = repository.findAll(pageable);
            Page<EstadoFiscalizacionInfraccionDTO> instanceDTOList = list.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<EstadoFiscalizacionInfraccionDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<EstadoFiscalizacionInfraccion> list = repository.findFromLastSincro(lastSyncro, pageable);
            Page<EstadoFiscalizacionInfraccionDTO> instanceDTOList = list.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
