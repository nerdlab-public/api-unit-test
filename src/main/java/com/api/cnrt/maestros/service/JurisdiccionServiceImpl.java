package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.dto.JurisdiccionDTO;
import com.api.cnrt.maestros.mapper.FiscalizadorToFiscalizadorDTOMapper;
import com.api.cnrt.maestros.mapper.JurisdiccionToJurisdiccionDTOMapper;
import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.Jurisdiccion;
import com.api.cnrt.maestros.repository.IJurisdiccionRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class JurisdiccionServiceImpl implements IJurisdiccionService<JurisdiccionDTO> {

    private final IJurisdiccionRepository jurisdiccionRepository;

    private final JurisdiccionToJurisdiccionDTOMapper jurisdiccionToJurisdiccionDTOMapper;


    @Override
    public Page<JurisdiccionDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Jurisdiccion> list = jurisdiccionRepository.findAll(pageable);
            Page<JurisdiccionDTO> jurisdiccionDTOList = list.map(jurisdiccionToJurisdiccionDTOMapper::map);

            return jurisdiccionDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<JurisdiccionDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Jurisdiccion> list = jurisdiccionRepository.findFromLastSincro(lastSyncro, pageable);
            Page<JurisdiccionDTO> jurisdiccionDTOList = list.map(jurisdiccionToJurisdiccionDTOMapper::map);
            return jurisdiccionDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
