package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.PermisosPDADTO;
import com.api.cnrt.maestros.mapper.PermisosPDAToPermisosPDADTOMapper;
import com.api.cnrt.maestros.model.PermisosPDA;
import com.api.cnrt.maestros.repository.IPermisosPDARepository;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class PermisoPDAServiceImpl implements IPermisoPDAService<PermisosPDADTO> {

	private final IPermisosPDARepository repository;
	private final PermisosPDAToPermisosPDADTOMapper mapper;

	@Override
	public Page<PermisosPDADTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
		try {
			Page<PermisosPDA> list = repository.findAll(pageable);
			Page<PermisosPDADTO> DTOList = list.map(mapper::map);

			return DTOList;
		} catch (Exception ex) {
			throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
		}
	}

	@Override
	public Page<PermisosPDADTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
		try {
			Page<PermisosPDA> list = repository.findFromLastSincro(lastSyncro, pageable);
			Page<PermisosPDADTO> DTOList = list.map(mapper::map);
			return DTOList;

		} catch (Exception ex) {
			throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
		}
	}
}
