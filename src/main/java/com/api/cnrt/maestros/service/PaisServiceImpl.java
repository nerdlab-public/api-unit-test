package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.mapper.PaisToPaisDTOMapper;
import com.api.cnrt.maestros.model.Pais;
import com.api.cnrt.maestros.model.TipoCategoria;
import com.api.cnrt.maestros.repository.IPaisRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PaisServiceImpl implements IPaisService<PaisDTO>{

	private final IPaisRepository paisRepository;
	private final PaisToPaisDTOMapper paisToPaisDTOMapper;


    @Override
    public Page<PaisDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Pais> paisList = paisRepository.findAll(pageable);

            Page<PaisDTO> paisDTOList = paisList.map(paisToPaisDTOMapper::map);

            return paisDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
    
    @Override
    public Page<PaisDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
            Page<Pais> paisList = paisRepository.findFromLastSincro(lastDateDb, pageable);

            Page<PaisDTO> paisDTOList = paisList.map(paisToPaisDTOMapper::map);

            return paisDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
    
}
