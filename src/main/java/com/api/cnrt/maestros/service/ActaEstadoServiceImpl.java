package com.api.cnrt.maestros.service;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.mapper.ActaEstadoToActaEstadoDTOMapper;
import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.repository.IActaEstadoRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ActaEstadoServiceImpl implements IActaEstadoService<ActaEstadoDTO> {

	private final IActaEstadoRepository actaEstadoRepository;
	private final ActaEstadoToActaEstadoDTOMapper actaEstadoToActaEstadoDTOMapper;

	@Override
	public Page<ActaEstadoDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
		try {
			Page<ActaEstado> list = actaEstadoRepository.findAll(pageable);
			Page<ActaEstadoDTO> DTOList = list.map(actaEstadoToActaEstadoDTOMapper::map);

			return DTOList;
		} catch (Exception ex) {
			throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
		}
	}

	@Override
	public Page<ActaEstadoDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
		try {
			Page<ActaEstado> list = actaEstadoRepository.findFromLastSincro(lastSyncro, pageable);
			Page<ActaEstadoDTO> DTOList = list.map(actaEstadoToActaEstadoDTOMapper::map);
			return DTOList;

		} catch (Exception ex) {
			throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
		}
	}
}
