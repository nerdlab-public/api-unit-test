package com.api.cnrt.maestros.service;



import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.DetalleOrdenServicioDTO;
import com.api.cnrt.maestros.mapper.DetalleOrdenServicioToDetalleOrdenServicioDTOMapper;
import com.api.cnrt.maestros.model.DetalleOrdenServicio;
import com.api.cnrt.maestros.repository.IDetalleOrdenServicioRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class DetalleOrdenServicioServiceImpl implements IDetalleOrdenServicioService<DetalleOrdenServicioDTO> {

    private final IDetalleOrdenServicioRepository repository;

    private final DetalleOrdenServicioToDetalleOrdenServicioDTOMapper mapper;


    @Override
    public Page<DetalleOrdenServicioDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<DetalleOrdenServicio> list = repository.findAll(pageable);
            Page<DetalleOrdenServicioDTO> instanceDTOList = list.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<DetalleOrdenServicioDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<DetalleOrdenServicio> list = repository.findFromLastSincro(lastSyncro, pageable);
            Page<DetalleOrdenServicioDTO> instanceDTOList = list.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
