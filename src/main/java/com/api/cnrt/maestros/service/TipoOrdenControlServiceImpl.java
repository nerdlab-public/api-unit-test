package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoOrdenControlDTO;
import com.api.cnrt.maestros.mapper.TipoOrdenControlToTipoOrdenControlDTOMapper;
import com.api.cnrt.maestros.model.TipoOrdenControl;
import com.api.cnrt.maestros.repository.ITipoOrdenControlRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TipoOrdenControlServiceImpl implements ITipoOrdenControlService<TipoOrdenControlDTO> {

    private final ITipoOrdenControlRepository ordenControlRepository;

    private final TipoOrdenControlToTipoOrdenControlDTOMapper tipoOrdenControlToTipoOrdenControlDTOMapper;


    @Override
    public Page<TipoOrdenControlDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoOrdenControl> instanceList = ordenControlRepository.findAll(pageable);
            Page<TipoOrdenControlDTO> instanceDTOList = instanceList.map(tipoOrdenControlToTipoOrdenControlDTOMapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<TipoOrdenControlDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoOrdenControl> instanceList = ordenControlRepository.findFromLastSincro(lastSyncro, pageable);
            Page<TipoOrdenControlDTO> instanceDTOList = instanceList.map(tipoOrdenControlToTipoOrdenControlDTOMapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
