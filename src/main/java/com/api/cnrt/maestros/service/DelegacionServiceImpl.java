package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.DelegacionDTO;
import com.api.cnrt.maestros.mapper.DelegacionToDelegacionDTOMapper;
import com.api.cnrt.maestros.model.Delegacion;
import com.api.cnrt.maestros.repository.IDelegacionRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DelegacionServiceImpl implements IDelegacionService<DelegacionDTO> {

    private final IDelegacionRepository delegacionRepository;

    private final DelegacionToDelegacionDTOMapper delegacionDTOMapper;



    @Override
    public Page<DelegacionDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Delegacion> list = delegacionRepository.findAll(pageable);

            Page<DelegacionDTO> delegacionDTOList = list.map(delegacionDTOMapper::map);

            return delegacionDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<DelegacionDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Delegacion> list = delegacionRepository.findFromLastSincro(lastSyncro, pageable);
            Page<DelegacionDTO> delegacionDTOList = list.map(delegacionDTOMapper::map);
            return delegacionDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }


}
