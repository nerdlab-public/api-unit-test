package com.api.cnrt.maestros.service;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionInfraccion;
import com.api.cnrt.maestros.dto.AlertaChoferDTO;
import com.api.cnrt.maestros.mapper.AlertaChoferToAlertaChoferDTOMapper;
import com.api.cnrt.maestros.model.AlertaChofer;
import com.api.cnrt.maestros.repository.IAlertaChoferRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AlertaChoferServiceImpl implements IAlertaChoferService<AlertaChoferDTO> {

    private final IAlertaChoferRepository alertaChoferRepository;
    private final AlertaChoferToAlertaChoferDTOMapper alertaChoferToAlertaChoferDTOMapper;


    @Override
    public Page<AlertaChoferDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<AlertaChofer> list = alertaChoferRepository.findAll(pageable);
            Page<AlertaChoferDTO> DTOList = list.map(alertaChoferToAlertaChoferDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<AlertaChoferDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<AlertaChofer> list = alertaChoferRepository.findFromLastSincro(lastSyncro, pageable);
            Page<AlertaChoferDTO> DTOList = list.map(alertaChoferToAlertaChoferDTOMapper::map);
            return DTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    public List<AlertaChofer> updateAlertaChofer(List<FiscalizacionInfraccion> infraccionToAlertaChoferList) throws RepositoryException {

        try {
            // Mapa para agrupar infracciones por DNI
            Map<String, List<FiscalizacionInfraccion>> dniToInfraccionesMap = new HashMap<>();
            List<AlertaChofer> createdAlertaChoferList = new ArrayList<>();
            // Agrupar las infracciones por DNI
            for (FiscalizacionInfraccion fiscaInfraccion : infraccionToAlertaChoferList) {
                dniToInfraccionesMap
                        .computeIfAbsent(fiscaInfraccion.getDni(), key -> new ArrayList<>())
                        .add(fiscaInfraccion);
            }

            // Iterar sobre el mapa y realizar las operaciones necesarias
            for (Map.Entry<String, List<FiscalizacionInfraccion>> entry : dniToInfraccionesMap.entrySet()) {
                String dni = entry.getKey();
                List<FiscalizacionInfraccion> infracciones = entry.getValue();

                // Eliminar la AlertaChofer para el DNI específico
                alertaChoferRepository.deleteById(dni);

                // Crear una nueva AlertaChofer con las infracciones correspondientes
                AlertaChofer nuevaAlertaChofer = new AlertaChofer();
                nuevaAlertaChofer.setDni(dni);
                // TODO: VER QUE FECHA
                nuevaAlertaChofer.setFecha(LocalDateTime.now());
                nuevaAlertaChofer.setFmod(LocalDateTime.now());
                nuevaAlertaChofer.setIdFaltaServicioUno(infracciones.isEmpty() ? null : infracciones.get(0).getIdFaltaServicio());
                nuevaAlertaChofer.setIdFaltaServicioDos(infracciones.size() > 1 ? infracciones.get(1).getIdFaltaServicio() : null);
                nuevaAlertaChofer.setIdFaltaServicioTres(infracciones.size() > 2 ? infracciones.get(2).getIdFaltaServicio() : null);


                AlertaChofer createdAlerta = alertaChoferRepository.save(nuevaAlertaChofer);
                createdAlertaChoferList.add(createdAlerta);
            }

            return createdAlertaChoferList;

        } catch (Exception ex) {
            throw new RepositoryException("Error durante la actualización de AlertaChofer", ex);
        }






    }
}
