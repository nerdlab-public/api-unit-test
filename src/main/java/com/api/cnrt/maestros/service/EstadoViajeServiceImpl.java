package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.dto.TipoDocumentoPersonaDTO;
import com.api.cnrt.maestros.mapper.EstadoViajeToEstadoViajeDTOMapper;
import com.api.cnrt.maestros.mapper.TipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.model.TipoDocumentoPersona;
import com.api.cnrt.maestros.repository.IEstadoViajeRepository;
import com.api.cnrt.maestros.repository.ITipoDocumentoPersonaRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EstadoViajeServiceImpl implements IEstadoViajeService<EstadoViajeDTO> {

    private final IEstadoViajeRepository estadoViajeRepository;

    private final EstadoViajeToEstadoViajeDTOMapper estadoViajeToEstadoViajeDTOMapper;


    @Override
    public Page<EstadoViajeDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<EstadoViaje> list = estadoViajeRepository.findAll(pageable);
            Page<EstadoViajeDTO> instanceDTOList = list.map(estadoViajeToEstadoViajeDTOMapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<EstadoViajeDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<EstadoViaje> list = estadoViajeRepository.findFromLastSincro(lastSyncro, pageable);
            Page<EstadoViajeDTO> instanceDTOList = list.map(estadoViajeToEstadoViajeDTOMapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
