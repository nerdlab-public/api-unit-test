package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.dto.ServicioDTO;
import com.api.cnrt.maestros.mapper.FiscalizadorToFiscalizadorDTOMapper;
import com.api.cnrt.maestros.mapper.ServicioToServicioDTOMapper;
import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.model.Servicio;
import com.api.cnrt.maestros.repository.IFiscalizadorRepository;
import com.api.cnrt.maestros.repository.IServicioRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ServicioServiceImpl implements IServicioService<ServicioDTO> {

    private final IServicioRepository servicioRepository;

    private final ServicioToServicioDTOMapper servicioToServicioDTOMapper;

    @Override
    public Page<ServicioDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Servicio> list = servicioRepository.findAll(pageable);
            Page<ServicioDTO> DTOList = list.map(servicioToServicioDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<ServicioDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Servicio> list = servicioRepository.findFromLastSincro(lastSyncro, pageable);
            Page<ServicioDTO> DTOList = list.map(servicioToServicioDTOMapper::map);
            return DTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
