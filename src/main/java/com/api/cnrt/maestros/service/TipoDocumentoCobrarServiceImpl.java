package com.api.cnrt.maestros.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoDocumentoCobrarDTO;
import com.api.cnrt.maestros.mapper.TipoDocumentoCobrarToTipoDocumentoCobrarDTOMapper;
import com.api.cnrt.maestros.model.TipoDocumentoCobrar;
import com.api.cnrt.maestros.repository.ITipoDocumentoCobrarRepository;

@Service
@RequiredArgsConstructor
public class TipoDocumentoCobrarServiceImpl implements ITipoDocumentoCobrarService<TipoDocumentoCobrarDTO>{

	private final ITipoDocumentoCobrarRepository tipoDocumentoCobrarRepository;
	private final TipoDocumentoCobrarToTipoDocumentoCobrarDTOMapper mapper;
	



    @Override
    public Page<TipoDocumentoCobrarDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoDocumentoCobrar> list = tipoDocumentoCobrarRepository.findAll(pageable);

            Page<TipoDocumentoCobrarDTO> tipoDocumentoCobrarDTOList = list.map(mapper::map);

            return tipoDocumentoCobrarDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
    
    @Override
    public Page<TipoDocumentoCobrarDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
        	Page<TipoDocumentoCobrar> list = tipoDocumentoCobrarRepository.findFromLastSincro(lastDateDb, pageable);

            Page<TipoDocumentoCobrarDTO> tipoDocumentoCobrarDTOList = list.map(mapper::map);

            return tipoDocumentoCobrarDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    
    
}
