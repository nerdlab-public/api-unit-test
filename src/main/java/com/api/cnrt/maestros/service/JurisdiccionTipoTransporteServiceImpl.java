package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.JurisdiccionTipoTransporteDTO;
import com.api.cnrt.maestros.dto.TipoTransporteDTO;
import com.api.cnrt.maestros.mapper.JurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper;
import com.api.cnrt.maestros.mapper.TIpoTransporteToTipoTransporteDTOMapper;
import com.api.cnrt.maestros.model.JurisdiccionTipoTransporte;
import com.api.cnrt.maestros.model.TipoTransporte;
import com.api.cnrt.maestros.repository.IJurisdiccionTipoTransporteRepository;
import com.api.cnrt.maestros.repository.ITipoTransporteRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class JurisdiccionTipoTransporteServiceImpl implements IJurisdiccionTipoTransporteService<JurisdiccionTipoTransporteDTO> {

    private final IJurisdiccionTipoTransporteRepository jurisdiccionTipoTransporteRepository;

    private final JurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper jurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper;


    @Override
    public Page<JurisdiccionTipoTransporteDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<JurisdiccionTipoTransporte> list = jurisdiccionTipoTransporteRepository.findAll(pageable);
            Page<JurisdiccionTipoTransporteDTO> jurisdiccionTipoTransporteDTOList = list.map(jurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper::map);

            return jurisdiccionTipoTransporteDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<JurisdiccionTipoTransporteDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<JurisdiccionTipoTransporte> list = jurisdiccionTipoTransporteRepository.findFromLastSincro(lastSyncro, pageable);
            Page<JurisdiccionTipoTransporteDTO> jurisdiccionTipoTransporteDTOList = list.map(jurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper::map);
            return jurisdiccionTipoTransporteDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
