package com.api.cnrt.maestros.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.fiscalizaciones.model.Fiscalizacion;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionDominio;
import com.api.cnrt.fiscalizaciones.model.FiscalizacionInfraccion;
import com.api.cnrt.maestros.dto.OrdenServicioOldDTO;
import com.api.cnrt.maestros.mapper.OrdenServicioOldToOrdenServicioOldDTOMapper;
import com.api.cnrt.maestros.model.OrdenServicio;
import com.api.cnrt.maestros.model.OrdenServicioOld;
import com.api.cnrt.maestros.repository.IOrdenServicioOldRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrdenServicioOldServiceImpl implements IOrdenServicioOldService<OrdenServicioOldDTO> {

    private final IOrdenServicioOldRepository repository;

    private final OrdenServicioOldToOrdenServicioOldDTOMapper mapper;

    private final OrdenServicioServiceImpl ordenServicioService;


    @Override
    public Page<OrdenServicioOldDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<OrdenServicioOld> instanceList = repository.findByEstado(1, pageable);
            Page<OrdenServicioOldDTO> instanceDTOList = instanceList.map( mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<OrdenServicioOldDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<OrdenServicioOld> instanceList = repository.findFromLastSincro(lastSyncro, pageable);
            Page<OrdenServicioOldDTO> instanceDTOList = instanceList.map(mapper::map);
            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    public List<OrdenServicioOld> buscarOrdenServicioOldExistente(FiscalizacionDominio fiscaDominio) {

            List<OrdenServicioOld> ordenServicioByDominioList = repository.findByDominioAndFechaCierreIsNull(fiscaDominio.getDominio());

        return ordenServicioByDominioList;
    }

    public List<OrdenServicioOld> cerrarOrdenesServicio(List<OrdenServicioOld> existingOsiListToClose, String osi) {
        existingOsiListToClose.forEach(item -> {
            item.setOsiCierre(osi);
            item.setFechaCierre(LocalDateTime.now());
            item.setFmod(LocalDateTime.now());
            repository.save(item);
        });

        return existingOsiListToClose;


    }

    @Transactional
    public OrdenServicio cerrarYCrearOrdenesServicioDesdeOld(List<OrdenServicioOld> existingOsiOldList, Fiscalizacion fiscaCabeceraSaved, List<FiscalizacionInfraccion> fiscaInfracionList, String dominio) {
        try {
            List<OrdenServicioOld> ordenClosedList = cerrarOrdenesServicio(existingOsiOldList, fiscaCabeceraSaved.getOsi());
            return ordenServicioService.crearNuevaOrdenServicio(dominio, fiscaInfracionList, fiscaCabeceraSaved, ordenClosedList.get(0).getOsiOriginal());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error while closing and creating orders of service", e);
        }

    }
}
