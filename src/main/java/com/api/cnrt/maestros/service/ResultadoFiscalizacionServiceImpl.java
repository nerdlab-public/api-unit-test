package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.dto.ResultadoFiscalizacionDTO;
import com.api.cnrt.maestros.mapper.EstadoViajeToEstadoViajeDTOMapper;
import com.api.cnrt.maestros.mapper.ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.model.ResultadoFiscalizacion;
import com.api.cnrt.maestros.repository.IEstadoViajeRepository;
import com.api.cnrt.maestros.repository.IResultadoFiscalizacionRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ResultadoFiscalizacionServiceImpl implements IResultadoFiscalizacionService<ResultadoFiscalizacionDTO> {

    private final IResultadoFiscalizacionRepository repository;

    private final ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper mapper;


    @Override
    public Page<ResultadoFiscalizacionDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<ResultadoFiscalizacion> instanceList = repository.findAll(pageable);
            Page<ResultadoFiscalizacionDTO> instanceDTOList = instanceList.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<ResultadoFiscalizacionDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<ResultadoFiscalizacion> instanceList = repository.findFromLastSincro(lastSyncro, pageable);
            Page<ResultadoFiscalizacionDTO> instanceDTOList = instanceList.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
