package com.api.cnrt.maestros.service;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.AmbitoDTO;
import com.api.cnrt.maestros.mapper.AmbitoToAmbitoDTOMapper;
import com.api.cnrt.maestros.model.Ambito;
import com.api.cnrt.maestros.repository.IAmbitoRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AmbitoServiceImpl implements IAmbitoService<AmbitoDTO> {

    private final IAmbitoRepository ambitoRepository;
    private final AmbitoToAmbitoDTOMapper ambitoToAmbitoDTOMapper;


    @Override
    public Page<AmbitoDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<Ambito> list = ambitoRepository.findAll(pageable);
            Page<AmbitoDTO> DTOList = list.map(ambitoToAmbitoDTOMapper::map);

            return DTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<AmbitoDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<Ambito> list = ambitoRepository.findFromLastSincro(lastSyncro, pageable);
            Page<AmbitoDTO> DTOList = list.map(ambitoToAmbitoDTOMapper::map);
            return DTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
