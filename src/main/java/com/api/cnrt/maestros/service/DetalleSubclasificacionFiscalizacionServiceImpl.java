package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.DetalleSubclasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.mapper.DetalleSubclasificacionFiscalizacionToDetalleSubclasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.mapper.EstadoViajeToEstadoViajeDTOMapper;
import com.api.cnrt.maestros.model.DetalleSubclasificacionFiscalizacion;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.repository.IDetalleSubclasificacionFiscalizacionRepository;
import com.api.cnrt.maestros.repository.IEstadoViajeRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DetalleSubclasificacionFiscalizacionServiceImpl implements IDetalleSubclasificacionFiscalizacionService<DetalleSubclasificacionFiscalizacionDTO> {

    private final IDetalleSubclasificacionFiscalizacionRepository repository;

    private final DetalleSubclasificacionFiscalizacionToDetalleSubclasificacionFiscalizacionDTOMapper mapper;


    @Override
    public Page<DetalleSubclasificacionFiscalizacionDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<DetalleSubclasificacionFiscalizacion> list = repository.findAll(pageable);
            Page<DetalleSubclasificacionFiscalizacionDTO> instanceDTOList = list.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<DetalleSubclasificacionFiscalizacionDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<DetalleSubclasificacionFiscalizacion> list = repository.findFromLastSincro(lastSyncro, pageable);
            Page<DetalleSubclasificacionFiscalizacionDTO> instanceDTOList = list.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
