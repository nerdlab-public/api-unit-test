package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.JurisdiccionDTO;
import com.api.cnrt.maestros.dto.TipoTransporteDTO;
import com.api.cnrt.maestros.mapper.JurisdiccionToJurisdiccionDTOMapper;
import com.api.cnrt.maestros.mapper.TIpoTransporteToTipoTransporteDTOMapper;
import com.api.cnrt.maestros.model.Jurisdiccion;
import com.api.cnrt.maestros.model.TipoTransporte;
import com.api.cnrt.maestros.repository.IJurisdiccionRepository;
import com.api.cnrt.maestros.repository.ITipoTransporteRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TipoTransporteServiceImpl implements ITipoTransporteService<TipoTransporteDTO> {

    private final ITipoTransporteRepository tipoTransporteRepository;

    private final TIpoTransporteToTipoTransporteDTOMapper tIpoTransporteToTipoTransporteDTOMapper;


    @Override
    public Page<TipoTransporteDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoTransporte> tipoTransporteList = tipoTransporteRepository.findAll(pageable);
            Page<TipoTransporteDTO> tipoTransporteDTOList = tipoTransporteList.map(tIpoTransporteToTipoTransporteDTOMapper::map);

            return tipoTransporteDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<TipoTransporteDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoTransporte> tipoTransporteList = tipoTransporteRepository.findFromLastSincro(lastSyncro, pageable);
            Page<TipoTransporteDTO> tipoTransporteDTOList = tipoTransporteList.map(tIpoTransporteToTipoTransporteDTOMapper::map);
            return tipoTransporteDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
