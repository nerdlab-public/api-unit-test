package com.api.cnrt.maestros.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ZonaOperativoDTO;
import com.api.cnrt.maestros.mapper.ZonaOperativoToZonaOperativoDTOMapper;
import com.api.cnrt.maestros.model.ZonaOperativo;
import com.api.cnrt.maestros.repository.IZonaOperativoRepository;

@Service
@RequiredArgsConstructor
public class ZonaOperativoServiceImpl implements IZonaOperativoService<ZonaOperativoDTO>{

	private final IZonaOperativoRepository zonaOperativoRepository;
	private final ZonaOperativoToZonaOperativoDTOMapper zonaOperativoToZonaOperativoDTOMapper;



    @Override
    public Page<ZonaOperativoDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<ZonaOperativo> list = zonaOperativoRepository.findAll(pageable);

            Page<ZonaOperativoDTO> zonaOperativoDTOList = list.map(zonaOperativoToZonaOperativoDTOMapper::map);

            return zonaOperativoDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
    
    @Override
    public Page<ZonaOperativoDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
        	Page<ZonaOperativo> list = zonaOperativoRepository.findFromLastSincro(lastDateDb, pageable);

            Page<ZonaOperativoDTO> zonaOperativoDTOList = list.map(zonaOperativoToZonaOperativoDTOMapper::map);

            return zonaOperativoDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

}
