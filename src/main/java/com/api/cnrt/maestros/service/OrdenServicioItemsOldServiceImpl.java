package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.dto.OrdenServicioItemsOldDTO;
import com.api.cnrt.maestros.mapper.EstadoViajeToEstadoViajeDTOMapper;
import com.api.cnrt.maestros.mapper.OrdenServicioItemsOldToOrdenServicioItemsOldDTOMapper;
import com.api.cnrt.maestros.mapper.OrdenServicioToOrdenServicioDTOMapper;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.model.OrdenServicioItemsOld;
import com.api.cnrt.maestros.repository.IEstadoViajeRepository;
import com.api.cnrt.maestros.repository.IOrdenServicioItemsOldRepository;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class OrdenServicioItemsOldServiceImpl implements IOrdenServicioItemsOldService<OrdenServicioItemsOldDTO> {

    private final IOrdenServicioItemsOldRepository repository;

    private final OrdenServicioItemsOldToOrdenServicioItemsOldDTOMapper mapper;


    @Override
    public Page<OrdenServicioItemsOldDTO> getAllItems(Pageable pageable) throws RepositoryException {
        try {
            Page<OrdenServicioItemsOld> items = repository.findAll(pageable);

            return items.map(mapper::map);
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }


}
