package com.api.cnrt.maestros.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.dto.SubclasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.mapper.EstadoViajeToEstadoViajeDTOMapper;
import com.api.cnrt.maestros.mapper.SubclasificacionFiscalizacionToSubclasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.model.SubclasificacionFiscalizacion;
import com.api.cnrt.maestros.repository.IEstadoViajeRepository;
import com.api.cnrt.maestros.repository.ISubclasificacionFiscalizacionRepository;

import static com.api.cnrt.common.MapperUtil.mapList;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SubclasificacionFiscalizacionServiceImpl implements ISubclasificacionFiscalizacionService<SubclasificacionFiscalizacionDTO> {

    private final ISubclasificacionFiscalizacionRepository repository;

    private final SubclasificacionFiscalizacionToSubclasificacionFiscalizacionDTOMapper mapper;


    @Override
    public Page<SubclasificacionFiscalizacionDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<SubclasificacionFiscalizacion> instanceList = repository.findAll(pageable);
            Page<SubclasificacionFiscalizacionDTO> instanceDTOList = instanceList.map(mapper::map);

            return instanceDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }

    @Override
    public Page<SubclasificacionFiscalizacionDTO> findAllBy(LocalDateTime lastSyncro, Pageable pageable) throws RepositoryException {
        try {
            Page<SubclasificacionFiscalizacion> instanceList = repository.findFromLastSincro(lastSyncro, pageable);
            Page<SubclasificacionFiscalizacionDTO> instanceDTOList = instanceList.map(mapper::map);
            return instanceDTOList;

        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
}
