package com.api.cnrt.maestros.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.mapper.TipoCategoriaToTipoCategoriaDTOMapper;
import com.api.cnrt.maestros.model.TipoCategoria;
import com.api.cnrt.maestros.repository.ITipoCategoriaRepository;

@Service
@RequiredArgsConstructor
public class TipoCategoriaServiceImpl implements ITipoCategoriaService<TipoCategoriaDTO>{

	private final ITipoCategoriaRepository tipoCategoriaRepository;
	private final TipoCategoriaToTipoCategoriaDTOMapper mapper;
	


    @Override
    public Page<TipoCategoriaDTO> findAllBy(Boolean initial, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoCategoria> list = tipoCategoriaRepository.findAll(pageable);

            Page<TipoCategoriaDTO> tipoCategoriaDTOList = list.map(mapper::map);

            return tipoCategoriaDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
    
    @Override
    public Page<TipoCategoriaDTO> findAllBy(LocalDateTime lastDateDb, Pageable pageable) throws RepositoryException {
        try {
            Page<TipoCategoria> list = tipoCategoriaRepository.findFromLastSincro(lastDateDb, pageable);

            Page<TipoCategoriaDTO> tipoCategoriaDTOList = list.map(mapper::map);

            return tipoCategoriaDTOList;
        } catch (Exception ex) {
            throw new RepositoryException("Error mientras se obtienen datos de la base de datos", ex);
        }
    }
    

    
    
}
