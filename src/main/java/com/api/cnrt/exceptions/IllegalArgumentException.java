package com.api.cnrt.exceptions;

public class IllegalArgumentException extends BusinessException {

    public IllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalArgumentException(String message) {
        super(message);
    }
}
