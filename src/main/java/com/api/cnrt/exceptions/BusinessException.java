package com.api.cnrt.exceptions;

public class BusinessException extends Exception {

	static final long serialVersionUID = 3387516993124229948L;

	public BusinessException() {
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(String message) {
		super(message);
	}

}