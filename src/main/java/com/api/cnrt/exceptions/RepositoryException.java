package com.api.cnrt.exceptions;

public class RepositoryException extends BusinessException {

	  public RepositoryException(String message, Throwable cause) {
	    super(message, cause);
	  }

	  public RepositoryException(String message) {
	    super(message);
	  }

	}
