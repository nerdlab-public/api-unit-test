package com.api.cnrt.exceptions;

public class EntityNotFoundException extends BusinessException {

	  public EntityNotFoundException(String message, Throwable cause) {
	    super(message, cause);
	  }

	  public EntityNotFoundException(String message) {
	    super(message);
	  }

	}

