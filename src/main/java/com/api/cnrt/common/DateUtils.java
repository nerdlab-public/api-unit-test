package com.api.cnrt.common;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateUtils {
    public static LocalDateTime convertTimestampToDateTime(Long timestamp) {
        Instant instant = Instant.ofEpochSecond(timestamp);
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
    
    public static LocalDateTime convertStringToLocalDateTime(String date) {
        if (date.length() == 10) {
        	date = date + " 00:00:00";
		}     	
    	return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
    
    public static String convertLocalDateTimeToString(LocalDateTime date) {
    	if (date != null) {
    		return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		} else {
			return null;
		}
    }
    
}