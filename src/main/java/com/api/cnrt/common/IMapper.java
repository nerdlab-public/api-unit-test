package com.api.cnrt.common;

public interface IMapper  <I, O>{
    public O map(I in);
}
