package com.api.cnrt.common;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MapperUtil {

    public static <T, U> List<U> mapList(List<T> sourceList, Function<T, U> mapper) {
        return sourceList.stream()
                .map(mapper)
                .collect(Collectors.toList());
    }
}
