package com.api.cnrt.common;

public class CommonUtils {

    public static Long getUnixTime(){
        return System.currentTimeMillis() / 1000L;
    }

    public static String getPrefijo(Long idAppDevice) {
        String prefijo = "8" + String.format("%04d", idAppDevice);
        return prefijo;
    }
}
