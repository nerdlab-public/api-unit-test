package com.api.cnrt.common;

public class ConstantCommonUtil {
    public static final long FISCALIZACION_OK = 1L;
    public static final long ACTA = 2L;
    public static final long ACTA_RETENER = 3L;
    public static final long ACTA_NO_RETENER = 4L;

    public static final long ESTADO_INFRACCION_FALTA = 1L;
    public static final long ESTADO_INFRACCION_SANEADA = 2L;
}
