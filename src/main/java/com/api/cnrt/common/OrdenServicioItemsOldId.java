package com.api.cnrt.common;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrdenServicioItemsOldId implements Serializable {
    private String osi;
    private Integer idCampo;

}