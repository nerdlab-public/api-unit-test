package com.api.cnrt.common;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrdenServicioOldId implements Serializable {
    private String dominio;
    private String osiAlta;
}
