package com.api.cnrt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnrtApplication {

	public static void main(String[] args) {
		SpringApplication.run(CnrtApplication.class, args);
	}
	
}
