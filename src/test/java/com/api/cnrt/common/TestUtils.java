package com.api.cnrt.common;

import com.api.cnrt.maestros.model.*;
import org.checkerframework.checker.units.qual.A;

import java.time.LocalDateTime;

public class TestUtils {


    public static AlertaChofer createMockAlertaChofer() {
        AlertaChofer alerta = new AlertaChofer();
        alerta.setDni("92610564");
        alerta.setFecha(LocalDateTime.now());
        alerta.setInfraccion1(2);
        alerta.setFmod(LocalDateTime.now());
        return alerta;
    }

    public static AmbitoClaseModalidad createMockAmbitoClaseModalidad() {
        AmbitoClaseModalidad ambitoClaseModalidad = new AmbitoClaseModalidad();
        ambitoClaseModalidad.setAmbito(createMockAmbito());
        ambitoClaseModalidad.setFmod(LocalDateTime.now());
        return ambitoClaseModalidad;
    }

    public static Ambito createMockAmbito() {
        Ambito ambito = new Ambito();
        ambito.setDescripcion("ambito prueba");
        ambito.setAbrev("ap");
        ambito.setActivo("true");
        ambito.setFmod(LocalDateTime.now());
        return ambito;
    }

    public static CategoriaChofer createMockCategoriaChofer() {
        CategoriaChofer cateChofer = new CategoriaChofer();
        cateChofer.setDescripcion("cate 1");
        cateChofer.setAbrev("c1");
        cateChofer.setFmod(LocalDateTime.now());
        return cateChofer;
    }

    public static CategoriaVehiculo createMockCategoriaVehiculo() {
        CategoriaVehiculo cateVehiculo = new CategoriaVehiculo();
        cateVehiculo.setDescripcion("cate 1");
        cateVehiculo.setAbrev("c1");
        cateVehiculo.setFmod(LocalDateTime.now());
        return cateVehiculo;
    }

    public static ClaseModalidad createMockClaseModalidad() {
        ClaseModalidad claseModalidad = new ClaseModalidad();
        claseModalidad.setDescripcion("calse 1");
        claseModalidad.setAbrev("c1");
        claseModalidad.setFmod(LocalDateTime.now());
        claseModalidad.setActivo("True");
        return claseModalidad;
    }

    public static ClasificacionFiscalizacion createMockClasificacionFiscalizacion() {
        ClasificacionFiscalizacion clasificacionFiscalizacion = new ClasificacionFiscalizacion();
        clasificacionFiscalizacion.setDescripcion("Clsi 1");
        clasificacionFiscalizacion.setOrden(1);
        clasificacionFiscalizacion.setFmod(LocalDateTime.now());
        return clasificacionFiscalizacion;
    }


    public static Delegacion createMockDelegacion() {
        Delegacion delegacion = new Delegacion();
        delegacion.setActivo(1);
        delegacion.setDescripcion("Retiro");
        delegacion.setFmod(LocalDateTime.now());
        return delegacion;
    }

    public static DetalleOrdenServicio createMockDetalleOrdenServicio() {
        DetalleOrdenServicio detalleOrdenServicio = new DetalleOrdenServicio();
        detalleOrdenServicio.setId(1L);
        detalleOrdenServicio.setFmod(LocalDateTime.now());
        return detalleOrdenServicio;
    }

    public static DetalleSubclasificacionFiscalizacion createMockDetalleSubclasificacionFiscalizacion() {
        DetalleSubclasificacionFiscalizacion detalleSubclasificacionFiscalizacion = new DetalleSubclasificacionFiscalizacion();
        detalleSubclasificacionFiscalizacion.setId(1L);
        detalleSubclasificacionFiscalizacion.setFmod(LocalDateTime.now());
        return detalleSubclasificacionFiscalizacion;
    }

    public static EstadoFiscalizacionInfraccion createMockEstadoFiscalizacionInfraccion() {
        EstadoFiscalizacionInfraccion estadoFiscalizacionInfraccion = new EstadoFiscalizacionInfraccion();
        estadoFiscalizacionInfraccion.setId(1L);
        estadoFiscalizacionInfraccion.setFmod(LocalDateTime.now());
        return estadoFiscalizacionInfraccion;
    }

    public static EstadoOrdenServicio createMockEstadoOrdenServicio() {
        EstadoOrdenServicio estadoOrdenServicio = new EstadoOrdenServicio();
        estadoOrdenServicio.setId(1L);
        estadoOrdenServicio.setFmod(LocalDateTime.now());
        return estadoOrdenServicio;
    }

    public static EstadoViaje createMockEstadoViaje() {
        EstadoViaje estadoViaje = new EstadoViaje();
        estadoViaje.setId(1L);
        estadoViaje.setFmod(LocalDateTime.now());
        return estadoViaje;
    }

    public static FaltaServicio createMockFaltaServicio() {
        FaltaServicio faltaServicio = new FaltaServicio();
        faltaServicio.setId(1L);
        faltaServicio.setFechaModificacion(LocalDateTime.now());
        return faltaServicio;
    }

    public static Fiscalizador createMockFiscalizador() {
        Fiscalizador fiscalizador = new Fiscalizador();
        fiscalizador.setId(1L);
        fiscalizador.setFmod(LocalDateTime.now());
        return fiscalizador;
    }

    public static Jurisdiccion createMockJurisdiccion() {
        Jurisdiccion jurisdiccion = new Jurisdiccion();
        jurisdiccion.setId(1L);
        jurisdiccion.setFmod(LocalDateTime.now());
        return jurisdiccion;
    }

    public static JurisdiccionTipoTransporte createMockJurisdiccionTipoTransporte() {
        JurisdiccionTipoTransporte jurisdiccionTipoTransporte = new JurisdiccionTipoTransporte();
        jurisdiccionTipoTransporte.setId(1L);
        jurisdiccionTipoTransporte.setFmod(LocalDateTime.now());
        return jurisdiccionTipoTransporte;
    }

    public static Licencia createMockLicencia() {
        Licencia licencia = new Licencia();
        licencia.setId(1L);
        licencia.setFmod(LocalDateTime.now());
        return licencia;
    }

    public static Localidad createMockLocalidad() {
        Localidad localidad = new Localidad();
        localidad.setId(1L);
        localidad.setFmod(LocalDateTime.now());
        return localidad;
    }

    public static Login createMockLogin() {
        Login login = new Login();
        login.setIdLogin(1L);
        login.setFmodif(LocalDateTime.now());
        return login;
    }

    public static Lugar createMockLugar() {
        Lugar lugar = new Lugar();
        lugar.setId(1L);
        lugar.setFmod(LocalDateTime.now());
        return lugar;
    }

    public static MotivoNoRetener createMockMotivoNoRetener() {
        MotivoNoRetener motivoNoRetener = new MotivoNoRetener();
        motivoNoRetener.setId(1L);
        motivoNoRetener.setFmod(LocalDateTime.now());
        return motivoNoRetener;
    }


    public static ActaEstado createMockActaEstado() {
        ActaEstado actaEstado = new ActaEstado();
        actaEstado.setId(1L);
        actaEstado.setDescripcion("Inicial");
        actaEstado.setFmod(LocalDateTime.now());
        return actaEstado;
    }

    public static ZonaOperativo createMockZonaOperativo() {
        ZonaOperativo zonaOperativo = new ZonaOperativo();
        zonaOperativo.setId(1L);
        zonaOperativo.setActivo(1);
        zonaOperativo.setDescripcion("Inicial");
        zonaOperativo.setFmod(LocalDateTime.now());
        return zonaOperativo;
    }

    public static VehiculoJNHabilitado createMockZonaVehiculoJNHabilitado() {
        VehiculoJNHabilitado vehiculoJNHabilitado = new VehiculoJNHabilitado();
        vehiculoJNHabilitado.setDominio("ABC123");
        vehiculoJNHabilitado.setInterno("0");
        vehiculoJNHabilitado.setServicios("[{\"abrev\":\"ISP\",\"descripcion\":\"INTERNACIONAL SERVICIO P\\u00daBLICO\",\"ambito\":\"I\",\"vigenciaHasta\":\"2027-12-31\"}]");
        vehiculoJNHabilitado.setAnioModelo("1990");
        vehiculoJNHabilitado.setCantidadAsientos(30);
        vehiculoJNHabilitado.setEmpresaNumero("10232");
        vehiculoJNHabilitado.setRazonSocial("Viajes Empresa");
        vehiculoJNHabilitado.setVigenciaHasta("");
        vehiculoJNHabilitado.setModelo("");
        vehiculoJNHabilitado.setMarca("Mercedes");
        vehiculoJNHabilitado.setVigenciaHastaInspeccionTecnica("2025-01-12");
        vehiculoJNHabilitado.setTipoTecnica("CENT");
        vehiculoJNHabilitado.setCategoriaAprob("{\"id\":12,\"descripcion\":\"INTERURBANO CAMA SUITE\"}");
        vehiculoJNHabilitado.setExpOrig("94547/2015");
        vehiculoJNHabilitado.setExpModif("94547/2018");
        vehiculoJNHabilitado.setDisposicion("0092/15");
        vehiculoJNHabilitado.setTacografoMarca("VDO");
        vehiculoJNHabilitado.setTacografoNumero("09447312");
        vehiculoJNHabilitado.setTecnicaNumero("664667");
        vehiculoJNHabilitado.setTipoDocumentoAbrev("CUIT");
        vehiculoJNHabilitado.setCuit("30611338844");
        vehiculoJNHabilitado.setCategoriaTecnica("M3");
        vehiculoJNHabilitado.setAseguradora("PROTECCION MUTUAL  DEL TPTE PUB DE PASAJEROS");
        vehiculoJNHabilitado.setNumeroPoliza("168048");
        vehiculoJNHabilitado.setVigenciaHastaPoliza("2024-02-27");
        vehiculoJNHabilitado.setFmod(LocalDateTime.now());
        vehiculoJNHabilitado.setUltimaFiscaResultado(null);
        vehiculoJNHabilitado.setPais("AR");
        vehiculoJNHabilitado.setFechaUltimoRodillo(null);
        vehiculoJNHabilitado.setResultadoUltimoRodillo(null);
        return vehiculoJNHabilitado;
    }

    public static VehiculoCNRT createMockVehiculoCNRT() {
        VehiculoCNRT vehiculoCNRT = new VehiculoCNRT();
        vehiculoCNRT.setId(1L);
        vehiculoCNRT.setDominio("MBN052");
        vehiculoCNRT.setMarca("VW");
        vehiculoCNRT.setModelo("SURAN");
        vehiculoCNRT.setAnio("2013");
        vehiculoCNRT.setUbicacion("Predio");
        vehiculoCNRT.setNumeroMotor("CFZ292725");
        vehiculoCNRT.setNumeroChasis("8AWPB45Z3DA513372");
        vehiculoCNRT.setTitular("Ministerio del interior");
        vehiculoCNRT.setCobertura("Nacion Seguros");
        vehiculoCNRT.setPointer(1);
        vehiculoCNRT.setEstado(null);
        vehiculoCNRT.setActivo(1);
        vehiculoCNRT.setFmod(LocalDateTime.now());
        return vehiculoCNRT;
    }

    public static VehiculoCargaHabilitado createMockVehiculoCargaHabilitado() {
        VehiculoCargaHabilitado vehiculoCargaHabilitado = new VehiculoCargaHabilitado();
        vehiculoCargaHabilitado.setId(1L);
        vehiculoCargaHabilitado.setFmod(LocalDateTime.now());
        return vehiculoCargaHabilitado;
    }

    public static TipoUnidad createMockTipoUnidad() {
        TipoUnidad tipoUnidad = new TipoUnidad();
        tipoUnidad.setId(1L);
        tipoUnidad.setDiaCoeficiente("abc");
        tipoUnidad.setDescripcion("ABC123");
        tipoUnidad.setFmod(LocalDateTime.now());
        return tipoUnidad;
    }

    public static TipoTransporte createMockTipoTransporte() {
        TipoTransporte tipoTransporte = new TipoTransporte();
        tipoTransporte.setId(1L);
        tipoTransporte.setActivo(1);
        tipoTransporte.setDescripcion("ABC123");
        tipoTransporte.setFmod(LocalDateTime.now());
        return tipoTransporte;
    }

    public static TipoTransporteAmbito createMockTipoTransporteAmbito() {
        TipoTransporteAmbito tipoTransporteAmbito = new TipoTransporteAmbito();
        tipoTransporteAmbito.setId(1L);
        tipoTransporteAmbito.setFmod(LocalDateTime.now());
        return tipoTransporteAmbito;
    }

    public static TipoOrdenControl createMockTipoOrdenControl() {
        TipoOrdenControl tipoOrdenControl = new TipoOrdenControl();
        tipoOrdenControl.setId(1L);
        tipoOrdenControl.setFmod(LocalDateTime.now());
        return tipoOrdenControl;
    }

    public static TipoDocumentoPersona createMockTipoDocumentoPersona() {
        TipoDocumentoPersona tipoDocumentoPersona = new TipoDocumentoPersona();
        tipoDocumentoPersona.setId(1L);
        tipoDocumentoPersona.setFmod(LocalDateTime.now());
        return tipoDocumentoPersona;
    }

    public static TipoDocumentoEmpresa createMockTipoDocumentoEmpresa() {
        TipoDocumentoEmpresa tipoDocumentoEmpresa = new TipoDocumentoEmpresa();
        tipoDocumentoEmpresa.setId(1L);
        tipoDocumentoEmpresa.setFmod(LocalDateTime.now());
        return tipoDocumentoEmpresa;
    }

    public static TipoDocumentoCobrar createMockTipoDocumentoCobrar() {
        TipoDocumentoCobrar tipoDocumentoCobrar = new TipoDocumentoCobrar();
        tipoDocumentoCobrar.setId(1L);
        tipoDocumentoCobrar.setFmod(LocalDateTime.now());
        return tipoDocumentoCobrar;
    }

    public static TipoCategoria createMockTipoCategoria() {
        TipoCategoria tipoCategoria = new TipoCategoria();
        tipoCategoria.setId(1L);
        tipoCategoria.setFmod(LocalDateTime.now());
        return tipoCategoria;
    }

    public static Sustancia createMockSustancia() {
        Sustancia sustancia = new Sustancia();
        sustancia.setId(1L);
        sustancia.setActivo(1);
        sustancia.setFmod(LocalDateTime.now());
        return sustancia;
    }

    public static SubclasificacionFiscalizacion createMockSubclasificacionFiscalizacion() {
        SubclasificacionFiscalizacion subclasificacionFiscalizacion = new SubclasificacionFiscalizacion();
        subclasificacionFiscalizacion.setId(1L);
        subclasificacionFiscalizacion.setFmod(LocalDateTime.now());
        return subclasificacionFiscalizacion;
    }

    public static Servicio createMockServicio() {
        Servicio servicio = new Servicio();
        servicio.setId(1L);
        servicio.setFmod(LocalDateTime.now());
        return servicio;
    }

    public static ResultadoFiscalizacion createMockResultadoFiscalizacion() {
        ResultadoFiscalizacion resultadoFiscalizacion = new ResultadoFiscalizacion();
        resultadoFiscalizacion.setId(1L);
        resultadoFiscalizacion.setFmod(LocalDateTime.now());
        return resultadoFiscalizacion;
    }

    public static Provincia createMockProvincia() {
        Provincia provincia = new Provincia();
        provincia.setId(1L);
        provincia.setFmod(LocalDateTime.now());
        return provincia;
    }

    public static Predio createMockPredio() {
        Predio predio = new Predio();
        predio.setId(1L);
        predio.setFmod(LocalDateTime.now());
        return predio;
    }

    public static PermisosPDA createMockPermisosPDA() {
        PermisosPDA permisosPDA = new PermisosPDA();
        permisosPDA.setId(1L);
        permisosPDA.setFmod(LocalDateTime.now());
        return permisosPDA;
    }

    public static Pais createMockPais() {
        Pais pais = new Pais();
        pais.setId(1L);
        pais.setFmod(LocalDateTime.now());
        return pais;
    }

    public static OrdenServicio createMockOrdenServicio() {
        OrdenServicio ordenServicio = new OrdenServicio();
        ordenServicio.setId(1L);
        ordenServicio.setFmod(LocalDateTime.now());
        return ordenServicio;
    }

    public static OrdenServicioOld createMockOrdenServicioOld() {
        OrdenServicioOld ordenServicioOld = new OrdenServicioOld();
        ordenServicioOld.setDominio("ABC123");
        ordenServicioOld.setFmod(LocalDateTime.now());
        return ordenServicioOld;
    }

    public static OrdenServicioItemsOld createMockOrdenServicioItemsOld() {
        OrdenServicioItemsOld ordenServicioItemsOld = new OrdenServicioItemsOld();
        ordenServicioItemsOld.setIdCampo(1);
        // ordenServicioItemsOld.setFmod(LocalDateTime.now());
        return ordenServicioItemsOld;
    }
}
