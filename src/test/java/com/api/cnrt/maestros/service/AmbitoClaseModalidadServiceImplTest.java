package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.AmbitoClaseModalidadDTO;
import com.api.cnrt.maestros.mapper.AmbitoClaseModalidadToAmbitoClaseModalidadDTOMapper;
import com.api.cnrt.maestros.model.AmbitoClaseModalidad;
import com.api.cnrt.maestros.repository.IAmbitoClaseModalidadRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AmbitoClaseModalidadServiceImplTest {

    private IAmbitoClaseModalidadRepository repositoryMock;
    private AmbitoClaseModalidadToAmbitoClaseModalidadDTOMapper mapperMock;
    private AmbitoClaseModalidadServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IAmbitoClaseModalidadRepository.class);
        mapperMock = mock(AmbitoClaseModalidadToAmbitoClaseModalidadDTOMapper.class);
        service = new AmbitoClaseModalidadServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<AmbitoClaseModalidad> ambitoClaseModalidadList = new ArrayList<>();
        ambitoClaseModalidadList.add(new AmbitoClaseModalidad());
        Page<AmbitoClaseModalidad> ambitoClaseModalidadPage = new PageImpl<>(ambitoClaseModalidadList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(ambitoClaseModalidadPage);

        List<AmbitoClaseModalidadDTO> ambitoClaseModalidadDTOList = new ArrayList<>();
        ambitoClaseModalidadDTOList.add(new AmbitoClaseModalidadDTO());
        Page<AmbitoClaseModalidadDTO> ambitoClaseModalidadDTOPage = new PageImpl<>(ambitoClaseModalidadDTOList);
        when(mapperMock.map(any(AmbitoClaseModalidad.class))).thenReturn(new AmbitoClaseModalidadDTO());

        Page<AmbitoClaseModalidadDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(ambitoClaseModalidadDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<AmbitoClaseModalidad> ambitoClaseModalidadList = new ArrayList<>();
        ambitoClaseModalidadList.add(new AmbitoClaseModalidad());
        ambitoClaseModalidadList.add(new AmbitoClaseModalidad());

        List<AmbitoClaseModalidadDTO> ambitoClaseModalidadDTOList = new ArrayList<>();
        ambitoClaseModalidadDTOList.add(new AmbitoClaseModalidadDTO());
        ambitoClaseModalidadDTOList.add(new AmbitoClaseModalidadDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<AmbitoClaseModalidad> ambitoClaseModalidadPage = new PageImpl<>(ambitoClaseModalidadList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(ambitoClaseModalidadPage);


        when(mapperMock.map(any(AmbitoClaseModalidad.class))).thenReturn(ambitoClaseModalidadDTOList.get(0), ambitoClaseModalidadDTOList.get(1));

        Page<AmbitoClaseModalidadDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(ambitoClaseModalidadDTOList, result.getContent());
    }
}