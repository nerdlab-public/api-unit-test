package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.VehiculoJNHabilitadoDTO;
import com.api.cnrt.maestros.dto.ZonaOperativoDTO;
import com.api.cnrt.maestros.mapper.VehiculoJNHabilitadoToVehiculoJNHabilitadoDTOMapper;
import com.api.cnrt.maestros.mapper.ZonaOperativoToZonaOperativoDTOMapper;
import com.api.cnrt.maestros.model.VehiculoJNHabilitado;
import com.api.cnrt.maestros.model.ZonaOperativo;
import com.api.cnrt.maestros.repository.IVehiculoJNHabilitadoRepository;
import com.api.cnrt.maestros.repository.IZonaOperativoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class VehiculoJNHabilitadoServiceImplTest {

    private IVehiculoJNHabilitadoRepository repositoryMock;
    private VehiculoJNHabilitadoToVehiculoJNHabilitadoDTOMapper mapperMock;
    private VehiculoJNHabilitadoServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IVehiculoJNHabilitadoRepository.class);
        mapperMock = mock(VehiculoJNHabilitadoToVehiculoJNHabilitadoDTOMapper.class);
        service = new VehiculoJNHabilitadoServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<VehiculoJNHabilitado> list = new ArrayList<>();
        list.add(new VehiculoJNHabilitado());
        Page<VehiculoJNHabilitado> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<VehiculoJNHabilitadoDTO> dtoList = new ArrayList<>();
        dtoList.add(new VehiculoJNHabilitadoDTO());
        Page<VehiculoJNHabilitadoDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(VehiculoJNHabilitado.class))).thenReturn(new VehiculoJNHabilitadoDTO());

        Page<VehiculoJNHabilitadoDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<VehiculoJNHabilitado> list = new ArrayList<>();
        list.add(new VehiculoJNHabilitado());
        list.add(new VehiculoJNHabilitado());

        List<VehiculoJNHabilitadoDTO> dtoList = new ArrayList<>();
        dtoList.add(new VehiculoJNHabilitadoDTO());
        dtoList.add(new VehiculoJNHabilitadoDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<VehiculoJNHabilitado> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(VehiculoJNHabilitado.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<VehiculoJNHabilitadoDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
