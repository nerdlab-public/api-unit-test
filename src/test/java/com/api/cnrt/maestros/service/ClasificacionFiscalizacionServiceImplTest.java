package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ClasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.mapper.ClasificacionFiscalizacionToClasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.ClasificacionFiscalizacion;
import com.api.cnrt.maestros.repository.IClasificacionFiscalizacionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class ClasificacionFiscalizacionServiceImplTest {

    private IClasificacionFiscalizacionRepository repositoryMock;
    private ClasificacionFiscalizacionToClasificacionFiscalizacionDTOMapper mapperMock;
    private ClasificacionFiscalizacionServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IClasificacionFiscalizacionRepository.class);
        mapperMock = mock(ClasificacionFiscalizacionToClasificacionFiscalizacionDTOMapper.class);
        service = new ClasificacionFiscalizacionServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<ClasificacionFiscalizacion> clasificacionFiscalizacionList = new ArrayList<>();
        clasificacionFiscalizacionList.add(new ClasificacionFiscalizacion());
        Page<ClasificacionFiscalizacion> clasificacionFiscalizacionPage = new PageImpl<>(clasificacionFiscalizacionList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(clasificacionFiscalizacionPage);

        List<ClasificacionFiscalizacionDTO> clasificacionFiscalizacionDTOList = new ArrayList<>();
        clasificacionFiscalizacionDTOList.add(new ClasificacionFiscalizacionDTO());
        Page<ClasificacionFiscalizacionDTO> clasificacionFiscalizacionDTOPage = new PageImpl<>(clasificacionFiscalizacionDTOList);
        when(mapperMock.map(any(ClasificacionFiscalizacion.class))).thenReturn(new ClasificacionFiscalizacionDTO());

        Page<ClasificacionFiscalizacionDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(clasificacionFiscalizacionDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<ClasificacionFiscalizacion> clasificacionFiscalizacionList = new ArrayList<>();
        clasificacionFiscalizacionList.add(new ClasificacionFiscalizacion());
        clasificacionFiscalizacionList.add(new ClasificacionFiscalizacion());

        List<ClasificacionFiscalizacionDTO> clasificacionFiscalizacionDTOList = new ArrayList<>();
        clasificacionFiscalizacionDTOList.add(new ClasificacionFiscalizacionDTO());
        clasificacionFiscalizacionDTOList.add(new ClasificacionFiscalizacionDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<ClasificacionFiscalizacion> clasificacionFiscalizacionPage = new PageImpl<>(clasificacionFiscalizacionList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(clasificacionFiscalizacionPage);


        when(mapperMock.map(any(ClasificacionFiscalizacion.class))).thenReturn(clasificacionFiscalizacionDTOList.get(0), clasificacionFiscalizacionDTOList.get(1));

        Page<ClasificacionFiscalizacionDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(clasificacionFiscalizacionDTOList, result.getContent());
    }
}