package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.DetalleSubclasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.mapper.DetalleSubclasificacionFiscalizacionToDetalleSubclasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.DetalleSubclasificacionFiscalizacion;
import com.api.cnrt.maestros.repository.IDetalleSubclasificacionFiscalizacionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class DetalleSubclasificacionFiscalizacionServiceImplTest {

    private IDetalleSubclasificacionFiscalizacionRepository repositoryMock;
    private DetalleSubclasificacionFiscalizacionToDetalleSubclasificacionFiscalizacionDTOMapper mapperMock;
    private DetalleSubclasificacionFiscalizacionServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IDetalleSubclasificacionFiscalizacionRepository.class);
        mapperMock = mock(DetalleSubclasificacionFiscalizacionToDetalleSubclasificacionFiscalizacionDTOMapper.class);
        service = new DetalleSubclasificacionFiscalizacionServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<DetalleSubclasificacionFiscalizacion> detalleSubclasificacionFiscalizacionList = new ArrayList<>();
        detalleSubclasificacionFiscalizacionList.add(new DetalleSubclasificacionFiscalizacion());
        Page<DetalleSubclasificacionFiscalizacion> detalleSubclasificacionFiscalizacionPage = new PageImpl<>(detalleSubclasificacionFiscalizacionList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(detalleSubclasificacionFiscalizacionPage);

        List<DetalleSubclasificacionFiscalizacionDTO> detalleSubclasificacionFiscalizacionDTOList = new ArrayList<>();
        detalleSubclasificacionFiscalizacionDTOList.add(new DetalleSubclasificacionFiscalizacionDTO());
        Page<DetalleSubclasificacionFiscalizacionDTO> detalleSubclasificacionFiscalizacionDTOPage = new PageImpl<>(detalleSubclasificacionFiscalizacionDTOList);
        when(mapperMock.map(any(DetalleSubclasificacionFiscalizacion.class))).thenReturn(new DetalleSubclasificacionFiscalizacionDTO());

        Page<DetalleSubclasificacionFiscalizacionDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(detalleSubclasificacionFiscalizacionDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<DetalleSubclasificacionFiscalizacion> detalleSubclasificacionFiscalizacionList = new ArrayList<>();
        detalleSubclasificacionFiscalizacionList.add(new DetalleSubclasificacionFiscalizacion());
        detalleSubclasificacionFiscalizacionList.add(new DetalleSubclasificacionFiscalizacion());

        List<DetalleSubclasificacionFiscalizacionDTO> detalleSubclasificacionFiscalizacionDTOList = new ArrayList<>();
        detalleSubclasificacionFiscalizacionDTOList.add(new DetalleSubclasificacionFiscalizacionDTO());
        detalleSubclasificacionFiscalizacionDTOList.add(new DetalleSubclasificacionFiscalizacionDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<DetalleSubclasificacionFiscalizacion> detalleSubclasificacionFiscalizacionPage = new PageImpl<>(detalleSubclasificacionFiscalizacionList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(detalleSubclasificacionFiscalizacionPage);


        when(mapperMock.map(any(DetalleSubclasificacionFiscalizacion.class))).thenReturn(detalleSubclasificacionFiscalizacionDTOList.get(0), detalleSubclasificacionFiscalizacionDTOList.get(1));

        Page<DetalleSubclasificacionFiscalizacionDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(detalleSubclasificacionFiscalizacionDTOList, result.getContent());
    }
}