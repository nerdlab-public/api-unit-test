package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.LugarDTO;
import com.api.cnrt.maestros.mapper.LugaresToLugarDTOMapper;
import com.api.cnrt.maestros.model.Lugar;
import com.api.cnrt.maestros.repository.ILocalidadRepository;
import com.api.cnrt.maestros.repository.ILugarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class LugarServiceImplTest {

    private ILugarRepository repositoryMock;

    private ILocalidadRepository localidadRepository;
    private LugaresToLugarDTOMapper mapperMock;
    private LugarServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ILugarRepository.class);
        mapperMock = mock(LugaresToLugarDTOMapper.class);
        service = new LugarServiceImpl(repositoryMock, mapperMock, localidadRepository);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Lugar> lugarList = new ArrayList<>();
        lugarList.add(new Lugar());
        Page<Lugar> lugarPage = new PageImpl<>(lugarList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(lugarPage);

        List<LugarDTO> lugarDTOList = new ArrayList<>();
        lugarDTOList.add(new LugarDTO());
        Page<LugarDTO> lugarDTOPage = new PageImpl<>(lugarDTOList);
        when(mapperMock.map(any(Lugar.class))).thenReturn(new LugarDTO());

        Page<LugarDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(lugarDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Lugar> lugarList = new ArrayList<>();
        lugarList.add(new Lugar());
        lugarList.add(new Lugar());

        List<LugarDTO> lugarDTOList = new ArrayList<>();
        lugarDTOList.add(new LugarDTO());
        lugarDTOList.add(new LugarDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Lugar> lugarPage = new PageImpl<>(lugarList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(lugarPage);


        when(mapperMock.map(any(Lugar.class))).thenReturn(lugarDTOList.get(0), lugarDTOList.get(1));

        Page<LugarDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(lugarDTOList, result.getContent());
    }
}