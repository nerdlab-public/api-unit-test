package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.FaltaServicioDTO;
import com.api.cnrt.maestros.mapper.FaltaServicioToFaltaServicioDTOMapper;
import com.api.cnrt.maestros.model.FaltaServicio;
import com.api.cnrt.maestros.repository.IFaltaServicioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class FaltaServicioServiceImplTest {

    private IFaltaServicioRepository repositoryMock;
    private FaltaServicioToFaltaServicioDTOMapper mapperMock;
    private FaltaServicioServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IFaltaServicioRepository.class);
        mapperMock = mock(FaltaServicioToFaltaServicioDTOMapper.class);
        service = new FaltaServicioServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<FaltaServicio> faltaServicioList = new ArrayList<>();
        faltaServicioList.add(new FaltaServicio());
        Page<FaltaServicio> faltaServicioPage = new PageImpl<>(faltaServicioList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(faltaServicioPage);

        List<FaltaServicioDTO> faltaServicioDTOList = new ArrayList<>();
        faltaServicioDTOList.add(new FaltaServicioDTO());
        Page<FaltaServicioDTO> faltaServicioDTOPage = new PageImpl<>(faltaServicioDTOList);
        when(mapperMock.map(any(FaltaServicio.class))).thenReturn(new FaltaServicioDTO());

        Page<FaltaServicioDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(faltaServicioDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<FaltaServicio> faltaServicioList = new ArrayList<>();
        faltaServicioList.add(new FaltaServicio());
        faltaServicioList.add(new FaltaServicio());

        List<FaltaServicioDTO> faltaServicioDTOList = new ArrayList<>();
        faltaServicioDTOList.add(new FaltaServicioDTO());
        faltaServicioDTOList.add(new FaltaServicioDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<FaltaServicio> faltaServicioPage = new PageImpl<>(faltaServicioList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(faltaServicioPage);


        when(mapperMock.map(any(FaltaServicio.class))).thenReturn(faltaServicioDTOList.get(0), faltaServicioDTOList.get(1));

        Page<FaltaServicioDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(faltaServicioDTOList, result.getContent());
    }
}