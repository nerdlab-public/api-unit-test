package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoOrdenControlDTO;
import com.api.cnrt.maestros.mapper.TipoOrdenControlToTipoOrdenControlDTOMapper;
import com.api.cnrt.maestros.model.TipoOrdenControl;
import com.api.cnrt.maestros.repository.ITipoOrdenControlRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoOrdenControlServiceImplTest {

    private ITipoOrdenControlRepository repositoryMock;
    private TipoOrdenControlToTipoOrdenControlDTOMapper mapperMock;
    private TipoOrdenControlServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoOrdenControlRepository.class);
        mapperMock = mock(TipoOrdenControlToTipoOrdenControlDTOMapper.class);
        service = new TipoOrdenControlServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoOrdenControl> list = new ArrayList<>();
        list.add(new TipoOrdenControl());
        Page<TipoOrdenControl> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoOrdenControlDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoOrdenControlDTO());
        Page<TipoOrdenControlDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoOrdenControl.class))).thenReturn(new TipoOrdenControlDTO());

        Page<TipoOrdenControlDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoOrdenControl> list = new ArrayList<>();
        list.add(new TipoOrdenControl());
        list.add(new TipoOrdenControl());

        List<TipoOrdenControlDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoOrdenControlDTO());
        dtoList.add(new TipoOrdenControlDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoOrdenControl> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoOrdenControl.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoOrdenControlDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
