package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.VehiculoCNRTDTO;
import com.api.cnrt.maestros.dto.VehiculoJNHabilitadoDTO;
import com.api.cnrt.maestros.mapper.VehiculoCNRTToVehiculoCNRTDTOMapper;
import com.api.cnrt.maestros.mapper.VehiculoJNHabilitadoToVehiculoJNHabilitadoDTOMapper;
import com.api.cnrt.maestros.model.VehiculoCNRT;
import com.api.cnrt.maestros.model.VehiculoJNHabilitado;
import com.api.cnrt.maestros.repository.IVehiculoCNRTRepository;
import com.api.cnrt.maestros.repository.IVehiculoJNHabilitadoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class VehiculoCNRTServiceImplTest {

    private IVehiculoCNRTRepository repositoryMock;
    private VehiculoCNRTToVehiculoCNRTDTOMapper mapperMock;
    private VehiculoCNRTServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IVehiculoCNRTRepository.class);
        mapperMock = mock(VehiculoCNRTToVehiculoCNRTDTOMapper.class);
        service = new VehiculoCNRTServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<VehiculoCNRT> list = new ArrayList<>();
        list.add(new VehiculoCNRT());
        Page<VehiculoCNRT> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<VehiculoCNRTDTO> dtoList = new ArrayList<>();
        dtoList.add(new VehiculoCNRTDTO());
        Page<VehiculoCNRTDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(VehiculoCNRT.class))).thenReturn(new VehiculoCNRTDTO());

        Page<VehiculoCNRTDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<VehiculoCNRT> list = new ArrayList<>();
        list.add(new VehiculoCNRT());
        list.add(new VehiculoCNRT());

        List<VehiculoCNRTDTO> dtoList = new ArrayList<>();
        dtoList.add(new VehiculoCNRTDTO());
        dtoList.add(new VehiculoCNRTDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<VehiculoCNRT> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(VehiculoCNRT.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<VehiculoCNRTDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
