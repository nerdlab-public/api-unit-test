package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoCategoriaDTO;
import com.api.cnrt.maestros.mapper.TipoCategoriaToTipoCategoriaDTOMapper;
import com.api.cnrt.maestros.model.TipoCategoria;
import com.api.cnrt.maestros.repository.ITipoCategoriaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoCategoriaServiceImplTest {

    private ITipoCategoriaRepository repositoryMock;
    private TipoCategoriaToTipoCategoriaDTOMapper mapperMock;
    private TipoCategoriaServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoCategoriaRepository.class);
        mapperMock = mock(TipoCategoriaToTipoCategoriaDTOMapper.class);
        service = new TipoCategoriaServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoCategoria> list = new ArrayList<>();
        list.add(new TipoCategoria());
        Page<TipoCategoria> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoCategoriaDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoCategoriaDTO());
        Page<TipoCategoriaDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoCategoria.class))).thenReturn(new TipoCategoriaDTO());

        Page<TipoCategoriaDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoCategoria> list = new ArrayList<>();
        list.add(new TipoCategoria());
        list.add(new TipoCategoria());

        List<TipoCategoriaDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoCategoriaDTO());
        dtoList.add(new TipoCategoriaDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoCategoria> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoCategoria.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoCategoriaDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
