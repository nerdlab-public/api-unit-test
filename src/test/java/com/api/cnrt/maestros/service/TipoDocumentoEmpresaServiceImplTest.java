package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoDocumentoEmpresaDTO;
import com.api.cnrt.maestros.mapper.TipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper;
import com.api.cnrt.maestros.model.TipoDocumentoEmpresa;
import com.api.cnrt.maestros.repository.ITipoDocumentoEmpresaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoDocumentoEmpresaServiceImplTest {

    private ITipoDocumentoEmpresaRepository repositoryMock;
    private TipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper mapperMock;
    private TipoDocumentoEmpresaServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoDocumentoEmpresaRepository.class);
        mapperMock = mock(TipoDocumentoEmpresaToTipoDocumentoEmpresaDTOMapper.class);
        service = new TipoDocumentoEmpresaServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoDocumentoEmpresa> list = new ArrayList<>();
        list.add(new TipoDocumentoEmpresa());
        Page<TipoDocumentoEmpresa> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoDocumentoEmpresaDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoDocumentoEmpresaDTO());
        Page<TipoDocumentoEmpresaDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoDocumentoEmpresa.class))).thenReturn(new TipoDocumentoEmpresaDTO());

        Page<TipoDocumentoEmpresaDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoDocumentoEmpresa> list = new ArrayList<>();
        list.add(new TipoDocumentoEmpresa());
        list.add(new TipoDocumentoEmpresa());

        List<TipoDocumentoEmpresaDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoDocumentoEmpresaDTO());
        dtoList.add(new TipoDocumentoEmpresaDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoDocumentoEmpresa> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoDocumentoEmpresa.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoDocumentoEmpresaDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
