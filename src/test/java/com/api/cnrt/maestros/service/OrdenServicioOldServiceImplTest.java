package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.OrdenServicioOldDTO;
import com.api.cnrt.maestros.mapper.OrdenServicioOldToOrdenServicioOldDTOMapper;
import com.api.cnrt.maestros.mapper.OrdenServicioToOrdenServicioDTOMapper;
import com.api.cnrt.maestros.model.OrdenServicioOld;
import com.api.cnrt.maestros.repository.IDetalleOrdenServicioRepository;
import com.api.cnrt.maestros.repository.IOrdenServicioOldRepository;
import com.api.cnrt.maestros.repository.IOrdenServicioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class OrdenServicioOldServiceImplTest {

    private IOrdenServicioOldRepository repositoryMock;
    private OrdenServicioOldToOrdenServicioOldDTOMapper mapperMock;
    private OrdenServicioOldServiceImpl service;
    private OrdenServicioServiceImpl ordenServicioService;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IOrdenServicioOldRepository.class);
        mapperMock = mock(OrdenServicioOldToOrdenServicioOldDTOMapper.class);
        ordenServicioService = mock(OrdenServicioServiceImpl.class);
        service = new OrdenServicioOldServiceImpl(repositoryMock, mapperMock, ordenServicioService);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<OrdenServicioOld> list = new ArrayList<>();
        list.add(new OrdenServicioOld());
        Page<OrdenServicioOld> page = new PageImpl<>(list);
        when(repositoryMock.findByEstado(eq(1),any(Pageable.class))).thenReturn(page);

        List<OrdenServicioOldDTO> dtoList = new ArrayList<>();
        dtoList.add(new OrdenServicioOldDTO());
        Page<OrdenServicioOldDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(OrdenServicioOld.class))).thenReturn(new OrdenServicioOldDTO());

        Page<OrdenServicioOldDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<OrdenServicioOld> list = new ArrayList<>();
        list.add(new OrdenServicioOld());
        list.add(new OrdenServicioOld());

        List<OrdenServicioOldDTO> dtoList = new ArrayList<>();
        dtoList.add(new OrdenServicioOldDTO());
        dtoList.add(new OrdenServicioOldDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<OrdenServicioOld> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(OrdenServicioOld.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<OrdenServicioOldDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
