package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.JurisdiccionDTO;
import com.api.cnrt.maestros.mapper.JurisdiccionToJurisdiccionDTOMapper;
import com.api.cnrt.maestros.model.Jurisdiccion;
import com.api.cnrt.maestros.repository.IJurisdiccionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class JurisdiccionServiceImplTest {

    private IJurisdiccionRepository repositoryMock;
    private JurisdiccionToJurisdiccionDTOMapper mapperMock;
    private JurisdiccionServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IJurisdiccionRepository.class);
        mapperMock = mock(JurisdiccionToJurisdiccionDTOMapper.class);
        service = new JurisdiccionServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Jurisdiccion> jurisdiccionList = new ArrayList<>();
        jurisdiccionList.add(new Jurisdiccion());
        Page<Jurisdiccion> jurisdiccionPage = new PageImpl<>(jurisdiccionList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(jurisdiccionPage);

        List<JurisdiccionDTO> jurisdiccionDTOList = new ArrayList<>();
        jurisdiccionDTOList.add(new JurisdiccionDTO());
        Page<JurisdiccionDTO> jurisdiccionDTOPage = new PageImpl<>(jurisdiccionDTOList);
        when(mapperMock.map(any(Jurisdiccion.class))).thenReturn(new JurisdiccionDTO());

        Page<JurisdiccionDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(jurisdiccionDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Jurisdiccion> jurisdiccionList = new ArrayList<>();
        jurisdiccionList.add(new Jurisdiccion());
        jurisdiccionList.add(new Jurisdiccion());

        List<JurisdiccionDTO> jurisdiccionDTOList = new ArrayList<>();
        jurisdiccionDTOList.add(new JurisdiccionDTO());
        jurisdiccionDTOList.add(new JurisdiccionDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Jurisdiccion> jurisdiccionPage = new PageImpl<>(jurisdiccionList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(jurisdiccionPage);


        when(mapperMock.map(any(Jurisdiccion.class))).thenReturn(jurisdiccionDTOList.get(0), jurisdiccionDTOList.get(1));

        Page<JurisdiccionDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(jurisdiccionDTOList, result.getContent());
    }
}