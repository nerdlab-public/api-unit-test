package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.CategoriaVehiculoDTO;
import com.api.cnrt.maestros.mapper.CategoriaVehiculoToCategoriaVehiculoDTOMapper;
import com.api.cnrt.maestros.model.CategoriaVehiculo;
import com.api.cnrt.maestros.repository.ICategoriaVehiculoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@SpringBootTest
class CategoriaVehiculoServiceImplTest {

    private ICategoriaVehiculoRepository repositoryMock;
    private CategoriaVehiculoToCategoriaVehiculoDTOMapper mapperMock;
    private CategoriaVehiculoServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ICategoriaVehiculoRepository.class);
        mapperMock = mock(CategoriaVehiculoToCategoriaVehiculoDTOMapper.class);
        service = new CategoriaVehiculoServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<CategoriaVehiculo> categoriaVehiculoList = new ArrayList<>();
        categoriaVehiculoList.add(new CategoriaVehiculo());
        Page<CategoriaVehiculo> categoriaVehiculoPage = new PageImpl<>(categoriaVehiculoList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(categoriaVehiculoPage);

        List<CategoriaVehiculoDTO> categoriaVehiculoDTOList = new ArrayList<>();
        categoriaVehiculoDTOList.add(new CategoriaVehiculoDTO());
        Page<CategoriaVehiculoDTO> categoriaVehiculoDTOPage = new PageImpl<>(categoriaVehiculoDTOList);
        when(mapperMock.map(any(CategoriaVehiculo.class))).thenReturn(new CategoriaVehiculoDTO());

        Page<CategoriaVehiculoDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(categoriaVehiculoDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<CategoriaVehiculo> categoriaVehiculoList = new ArrayList<>();
        categoriaVehiculoList.add(new CategoriaVehiculo());
        categoriaVehiculoList.add(new CategoriaVehiculo());

        List<CategoriaVehiculoDTO> categoriaVehiculoDTOList = new ArrayList<>();
        categoriaVehiculoDTOList.add(new CategoriaVehiculoDTO());
        categoriaVehiculoDTOList.add(new CategoriaVehiculoDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<CategoriaVehiculo> categoriaVehiculoPage = new PageImpl<>(categoriaVehiculoList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(categoriaVehiculoPage);


        when(mapperMock.map(any(CategoriaVehiculo.class))).thenReturn(categoriaVehiculoDTOList.get(0), categoriaVehiculoDTOList.get(1));

        Page<CategoriaVehiculoDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(categoriaVehiculoDTOList, result.getContent());
    }
}