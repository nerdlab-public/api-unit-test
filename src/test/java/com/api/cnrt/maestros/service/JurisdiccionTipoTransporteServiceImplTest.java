package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.JurisdiccionTipoTransporteDTO;
import com.api.cnrt.maestros.mapper.JurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper;
import com.api.cnrt.maestros.model.JurisdiccionTipoTransporte;
import com.api.cnrt.maestros.repository.IJurisdiccionTipoTransporteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class JurisdiccionTipoTransporteServiceImplTest {

    private IJurisdiccionTipoTransporteRepository repositoryMock;
    private JurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper mapperMock;
    private JurisdiccionTipoTransporteServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IJurisdiccionTipoTransporteRepository.class);
        mapperMock = mock(JurisdiccionTipoTransporteToJurisdiccionTIpoTransporteDTOMapper.class);
        service = new JurisdiccionTipoTransporteServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<JurisdiccionTipoTransporte> jurisdiccionTipoList = new ArrayList<>();
        jurisdiccionTipoList.add(new JurisdiccionTipoTransporte());
        Page<JurisdiccionTipoTransporte> jurisdiccionTipoPage = new PageImpl<>(jurisdiccionTipoList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(jurisdiccionTipoPage);

        List<JurisdiccionTipoTransporteDTO> jurisdiccionTipoDTOList = new ArrayList<>();
        jurisdiccionTipoDTOList.add(new JurisdiccionTipoTransporteDTO());
        Page<JurisdiccionTipoTransporteDTO> jurisdiccionTipoDTOPage = new PageImpl<>(jurisdiccionTipoDTOList);
        when(mapperMock.map(any(JurisdiccionTipoTransporte.class))).thenReturn(new JurisdiccionTipoTransporteDTO());

        Page<JurisdiccionTipoTransporteDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(jurisdiccionTipoDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<JurisdiccionTipoTransporte> jurisdiccionTipoList = new ArrayList<>();
        jurisdiccionTipoList.add(new JurisdiccionTipoTransporte());
        jurisdiccionTipoList.add(new JurisdiccionTipoTransporte());

        List<JurisdiccionTipoTransporteDTO> jurisdiccionTipoDTOList = new ArrayList<>();
        jurisdiccionTipoDTOList.add(new JurisdiccionTipoTransporteDTO());
        jurisdiccionTipoDTOList.add(new JurisdiccionTipoTransporteDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<JurisdiccionTipoTransporte> jurisdiccionTipoPage = new PageImpl<>(jurisdiccionTipoList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(jurisdiccionTipoPage);


        when(mapperMock.map(any(JurisdiccionTipoTransporte.class))).thenReturn(jurisdiccionTipoDTOList.get(0), jurisdiccionTipoDTOList.get(1));

        Page<JurisdiccionTipoTransporteDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(jurisdiccionTipoDTOList, result.getContent());
    }
}