package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.AmbitoDTO;
import com.api.cnrt.maestros.mapper.AmbitoToAmbitoDTOMapper;
import com.api.cnrt.maestros.model.Ambito;
import com.api.cnrt.maestros.repository.IAmbitoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class AmbitoServiceImplTest {

    private IAmbitoRepository repositoryMock;
    private AmbitoToAmbitoDTOMapper mapperMock;
    private AmbitoServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IAmbitoRepository.class);
        mapperMock = mock(AmbitoToAmbitoDTOMapper.class);
        service = new AmbitoServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Ambito> ambitoList = new ArrayList<>();
        ambitoList.add(new Ambito());
        Page<Ambito> ambitoPage = new PageImpl<>(ambitoList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(ambitoPage);

        List<AmbitoDTO> ambitoDTOList = new ArrayList<>();
        ambitoDTOList.add(new AmbitoDTO());
        Page<AmbitoDTO> ambitoDTOPage = new PageImpl<>(ambitoDTOList);
        when(mapperMock.map(any(Ambito.class))).thenReturn(new AmbitoDTO());

        Page<AmbitoDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(ambitoDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Ambito> ambitoList = new ArrayList<>();
        ambitoList.add(new Ambito());
        ambitoList.add(new Ambito());

        List<AmbitoDTO> ambitoDTOList = new ArrayList<>();
        ambitoDTOList.add(new AmbitoDTO());
        ambitoDTOList.add(new AmbitoDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Ambito> ambitoPage = new PageImpl<>(ambitoList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(ambitoPage);


        when(mapperMock.map(any(Ambito.class))).thenReturn(ambitoDTOList.get(0), ambitoDTOList.get(1));

        Page<AmbitoDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(ambitoDTOList, result.getContent());
    }
}