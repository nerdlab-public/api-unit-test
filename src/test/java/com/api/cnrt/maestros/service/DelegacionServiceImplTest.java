package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.DelegacionDTO;
import com.api.cnrt.maestros.mapper.DelegacionToDelegacionDTOMapper;
import com.api.cnrt.maestros.model.Delegacion;
import com.api.cnrt.maestros.repository.IDelegacionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class DelegacionServiceImplTest {

    private IDelegacionRepository repositoryMock;
    private DelegacionToDelegacionDTOMapper mapperMock;
    private DelegacionServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IDelegacionRepository.class);
        mapperMock = mock(DelegacionToDelegacionDTOMapper.class);
        service = new DelegacionServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Delegacion> delegacionList = new ArrayList<>();
        delegacionList.add(new Delegacion());
        Page<Delegacion> delegacionPage = new PageImpl<>(delegacionList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(delegacionPage);

        List<DelegacionDTO> delegacionDTOList = new ArrayList<>();
        delegacionDTOList.add(new DelegacionDTO());
        Page<DelegacionDTO> delegacionDTOPage = new PageImpl<>(delegacionDTOList);
        when(mapperMock.map(any(Delegacion.class))).thenReturn(new DelegacionDTO());

        Page<DelegacionDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(delegacionDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Delegacion> delegacionList = new ArrayList<>();
        delegacionList.add(new Delegacion());
        delegacionList.add(new Delegacion());

        List<DelegacionDTO> delegacionDTOList = new ArrayList<>();
        delegacionDTOList.add(new DelegacionDTO());
        delegacionDTOList.add(new DelegacionDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Delegacion> delegacionPage = new PageImpl<>(delegacionList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(delegacionPage);


        when(mapperMock.map(any(Delegacion.class))).thenReturn(delegacionDTOList.get(0), delegacionDTOList.get(1));

        Page<DelegacionDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(delegacionDTOList, result.getContent());
    }
}