package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoDocumentoPersonaDTO;
import com.api.cnrt.maestros.mapper.TipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper;
import com.api.cnrt.maestros.model.TipoDocumentoPersona;
import com.api.cnrt.maestros.repository.ITipoDocumentoPersonaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoDocumentoPersonaServiceImplTest {

    private ITipoDocumentoPersonaRepository repositoryMock;
    private TipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper mapperMock;
    private TipoDocumentoPersonaServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoDocumentoPersonaRepository.class);
        mapperMock = mock(TipoDocumentoPersonaToTipoDocumentoPersonaDTOMapper.class);
        service = new TipoDocumentoPersonaServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoDocumentoPersona> list = new ArrayList<>();
        list.add(new TipoDocumentoPersona());
        Page<TipoDocumentoPersona> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoDocumentoPersonaDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoDocumentoPersonaDTO());
        Page<TipoDocumentoPersonaDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoDocumentoPersona.class))).thenReturn(new TipoDocumentoPersonaDTO());

        Page<TipoDocumentoPersonaDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoDocumentoPersona> list = new ArrayList<>();
        list.add(new TipoDocumentoPersona());
        list.add(new TipoDocumentoPersona());

        List<TipoDocumentoPersonaDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoDocumentoPersonaDTO());
        dtoList.add(new TipoDocumentoPersonaDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoDocumentoPersona> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoDocumentoPersona.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoDocumentoPersonaDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
