package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.VehiculoCNRTDTO;
import com.api.cnrt.maestros.dto.VehiculoCargasHabilitadoDTO;
import com.api.cnrt.maestros.mapper.VehiculoCNRTToVehiculoCNRTDTOMapper;
import com.api.cnrt.maestros.mapper.VehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper;
import com.api.cnrt.maestros.model.VehiculoCNRT;
import com.api.cnrt.maestros.model.VehiculoCargaHabilitado;
import com.api.cnrt.maestros.repository.IVehiculoCNRTRepository;
import com.api.cnrt.maestros.repository.IVehiculoCargasHabilitadoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class VehiculoCargasHabilitadoServiceImplTest {

    private IVehiculoCargasHabilitadoRepository repositoryMock;
    private VehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper mapperMock;
    private VehiculoCargaHabilitadoServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IVehiculoCargasHabilitadoRepository.class);
        mapperMock = mock(VehiculoCargaHabilitadoToVehiculoCargaHabilitadoDTOMapper.class);
        service = new VehiculoCargaHabilitadoServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<VehiculoCargaHabilitado> list = new ArrayList<>();
        list.add(new VehiculoCargaHabilitado());
        Page<VehiculoCargaHabilitado> page = new PageImpl<>(list);
        when(repositoryMock.findByEstado(eq(0),any(Pageable.class))).thenReturn(page);

        List<VehiculoCargasHabilitadoDTO> dtoList = new ArrayList<>();
        dtoList.add(new VehiculoCargasHabilitadoDTO());
        Page<VehiculoCargasHabilitadoDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(VehiculoCargaHabilitado.class))).thenReturn(new VehiculoCargasHabilitadoDTO());

        Page<VehiculoCargasHabilitadoDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<VehiculoCargaHabilitado> list = new ArrayList<>();
        list.add(new VehiculoCargaHabilitado());
        list.add(new VehiculoCargaHabilitado());

        List<VehiculoCargasHabilitadoDTO> dtoList = new ArrayList<>();
        dtoList.add(new VehiculoCargasHabilitadoDTO());
        dtoList.add(new VehiculoCargasHabilitadoDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<VehiculoCargaHabilitado> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(VehiculoCargaHabilitado.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<VehiculoCargasHabilitadoDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
