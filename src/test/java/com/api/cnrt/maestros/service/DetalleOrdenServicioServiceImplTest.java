package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.DetalleOrdenServicioDTO;
import com.api.cnrt.maestros.mapper.DetalleOrdenServicioToDetalleOrdenServicioDTOMapper;
import com.api.cnrt.maestros.model.DetalleOrdenServicio;
import com.api.cnrt.maestros.repository.IDetalleOrdenServicioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class DetalleOrdenServicioServiceImplTest {

    private IDetalleOrdenServicioRepository repositoryMock;
    private DetalleOrdenServicioToDetalleOrdenServicioDTOMapper mapperMock;
    private DetalleOrdenServicioServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IDetalleOrdenServicioRepository.class);
        mapperMock = mock(DetalleOrdenServicioToDetalleOrdenServicioDTOMapper.class);
        service = new DetalleOrdenServicioServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<DetalleOrdenServicio> detalleOrdenServicioList = new ArrayList<>();
        detalleOrdenServicioList.add(new DetalleOrdenServicio());
        Page<DetalleOrdenServicio> detalleOrdenServicioPage = new PageImpl<>(detalleOrdenServicioList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(detalleOrdenServicioPage);

        List<DetalleOrdenServicioDTO> detalleOrdenServicioDTOList = new ArrayList<>();
        detalleOrdenServicioDTOList.add(new DetalleOrdenServicioDTO());
        Page<DetalleOrdenServicioDTO> detalleOrdenServicioDTOPage = new PageImpl<>(detalleOrdenServicioDTOList);
        when(mapperMock.map(any(DetalleOrdenServicio.class))).thenReturn(new DetalleOrdenServicioDTO());

        Page<DetalleOrdenServicioDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(detalleOrdenServicioDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<DetalleOrdenServicio> detalleOrdenServicioList = new ArrayList<>();
        detalleOrdenServicioList.add(new DetalleOrdenServicio());
        detalleOrdenServicioList.add(new DetalleOrdenServicio());

        List<DetalleOrdenServicioDTO> detalleOrdenServicioDTOList = new ArrayList<>();
        detalleOrdenServicioDTOList.add(new DetalleOrdenServicioDTO());
        detalleOrdenServicioDTOList.add(new DetalleOrdenServicioDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<DetalleOrdenServicio> detalleOrdenServicioPage = new PageImpl<>(detalleOrdenServicioList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(detalleOrdenServicioPage);


        when(mapperMock.map(any(DetalleOrdenServicio.class))).thenReturn(detalleOrdenServicioDTOList.get(0), detalleOrdenServicioDTOList.get(1));

        Page<DetalleOrdenServicioDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(detalleOrdenServicioDTOList, result.getContent());
    }
}