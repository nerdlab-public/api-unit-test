package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.FiscalizadorDTO;
import com.api.cnrt.maestros.mapper.FiscalizadorToFiscalizadorDTOMapper;
import com.api.cnrt.maestros.model.Fiscalizador;
import com.api.cnrt.maestros.repository.IFiscalizadorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class FiscalizadorServiceImplTest {


    private IFiscalizadorRepository repositoryMock;
    private FiscalizadorToFiscalizadorDTOMapper mapperMock;
    private FiscalizadorServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IFiscalizadorRepository.class);
        mapperMock = mock(FiscalizadorToFiscalizadorDTOMapper.class);
        service = new FiscalizadorServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Fiscalizador> fiscalizadorList = new ArrayList<>();
        fiscalizadorList.add(new Fiscalizador());
        Page<Fiscalizador> fiscalizadorPage = new PageImpl<>(fiscalizadorList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(fiscalizadorPage);

        List<FiscalizadorDTO> fiscalizadorDTOList = new ArrayList<>();
        fiscalizadorDTOList.add(new FiscalizadorDTO());
        Page<FiscalizadorDTO> fiscalizadorDTOPage = new PageImpl<>(fiscalizadorDTOList);
        when(mapperMock.map(any(Fiscalizador.class))).thenReturn(new FiscalizadorDTO());

        Page<FiscalizadorDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(fiscalizadorDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Fiscalizador> fiscalizadorList = new ArrayList<>();
        fiscalizadorList.add(new Fiscalizador());
        fiscalizadorList.add(new Fiscalizador());

        List<FiscalizadorDTO> fiscalizadorDTOList = new ArrayList<>();
        fiscalizadorDTOList.add(new FiscalizadorDTO());
        fiscalizadorDTOList.add(new FiscalizadorDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Fiscalizador> fiscalizadorPage = new PageImpl<>(fiscalizadorList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(fiscalizadorPage);


        when(mapperMock.map(any(Fiscalizador.class))).thenReturn(fiscalizadorDTOList.get(0), fiscalizadorDTOList.get(1));

        Page<FiscalizadorDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(fiscalizadorDTOList, result.getContent());
    }
}