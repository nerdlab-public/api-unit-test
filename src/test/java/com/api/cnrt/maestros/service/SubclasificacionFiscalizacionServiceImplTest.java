package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.SubclasificacionFiscalizacionDTO;
import com.api.cnrt.maestros.mapper.SubclasificacionFiscalizacionToSubclasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.SubclasificacionFiscalizacion;
import com.api.cnrt.maestros.repository.ISubclasificacionFiscalizacionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class SubclasificacionFiscalizacionServiceImplTest {

    private ISubclasificacionFiscalizacionRepository repositoryMock;
    private SubclasificacionFiscalizacionToSubclasificacionFiscalizacionDTOMapper mapperMock;
    private SubclasificacionFiscalizacionServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ISubclasificacionFiscalizacionRepository.class);
        mapperMock = mock(SubclasificacionFiscalizacionToSubclasificacionFiscalizacionDTOMapper.class);
        service = new SubclasificacionFiscalizacionServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<SubclasificacionFiscalizacion> list = new ArrayList<>();
        list.add(new SubclasificacionFiscalizacion());
        Page<SubclasificacionFiscalizacion> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<SubclasificacionFiscalizacionDTO> dtoList = new ArrayList<>();
        dtoList.add(new SubclasificacionFiscalizacionDTO());
        Page<SubclasificacionFiscalizacionDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(SubclasificacionFiscalizacion.class))).thenReturn(new SubclasificacionFiscalizacionDTO());

        Page<SubclasificacionFiscalizacionDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<SubclasificacionFiscalizacion> list = new ArrayList<>();
        list.add(new SubclasificacionFiscalizacion());
        list.add(new SubclasificacionFiscalizacion());

        List<SubclasificacionFiscalizacionDTO> dtoList = new ArrayList<>();
        dtoList.add(new SubclasificacionFiscalizacionDTO());
        dtoList.add(new SubclasificacionFiscalizacionDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<SubclasificacionFiscalizacion> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(SubclasificacionFiscalizacion.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<SubclasificacionFiscalizacionDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
