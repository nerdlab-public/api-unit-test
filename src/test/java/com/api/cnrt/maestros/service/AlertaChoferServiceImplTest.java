package com.api.cnrt.maestros.service;


import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.AlertaChoferDTO;
import com.api.cnrt.maestros.mapper.AlertaChoferToAlertaChoferDTOMapper;
import com.api.cnrt.maestros.model.AlertaChofer;
import com.api.cnrt.maestros.repository.IAlertaChoferRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class AlertaChoferServiceImplTest {

    private IAlertaChoferRepository repositoryMock;
    private AlertaChoferToAlertaChoferDTOMapper mapperMock;
    private AlertaChoferServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IAlertaChoferRepository.class);
        mapperMock = mock(AlertaChoferToAlertaChoferDTOMapper.class);
        service = new AlertaChoferServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<AlertaChofer> alertaChoferList = new ArrayList<>();
        alertaChoferList.add(new AlertaChofer());
        Page<AlertaChofer> alertaChoferPage = new PageImpl<>(alertaChoferList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(alertaChoferPage);

        List<AlertaChoferDTO> alertaChoferDTOList = new ArrayList<>();
        alertaChoferDTOList.add(new AlertaChoferDTO());
        Page<AlertaChoferDTO> alertaChoferDTOPage = new PageImpl<>(alertaChoferDTOList);
        when(mapperMock.map(any(AlertaChofer.class))).thenReturn(new AlertaChoferDTO());

        Page<AlertaChoferDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(alertaChoferDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<AlertaChofer> alertaChoferList = new ArrayList<>();
        alertaChoferList.add(new AlertaChofer());
        alertaChoferList.add(new AlertaChofer());

        List<AlertaChoferDTO> alertaChoferDTOList = new ArrayList<>();
        alertaChoferDTOList.add(new AlertaChoferDTO());
        alertaChoferDTOList.add(new AlertaChoferDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<AlertaChofer> alertaChoferPage = new PageImpl<>(alertaChoferList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(alertaChoferPage);


        when(mapperMock.map(any(AlertaChofer.class))).thenReturn(alertaChoferDTOList.get(0), alertaChoferDTOList.get(1));

        Page<AlertaChoferDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(alertaChoferDTOList, result.getContent());
    }
}