package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.OrdenServicioDTO;
import com.api.cnrt.maestros.mapper.OrdenServicioToOrdenServicioDTOMapper;
import com.api.cnrt.maestros.mapper.PaisToPaisDTOMapper;
import com.api.cnrt.maestros.model.OrdenServicio;
import com.api.cnrt.maestros.repository.IDetalleOrdenServicioRepository;
import com.api.cnrt.maestros.repository.IOrdenServicioRepository;
import com.api.cnrt.maestros.repository.IPaisRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class OrdenServicioServiceImplTest {

    private IOrdenServicioRepository repositoryMock;
    private OrdenServicioToOrdenServicioDTOMapper mapperMock;
    private OrdenServicioServiceImpl service;
    private IDetalleOrdenServicioRepository detalleOrdenServicioRepository;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IOrdenServicioRepository.class);
        mapperMock = mock(OrdenServicioToOrdenServicioDTOMapper.class);
        detalleOrdenServicioRepository = mock(IDetalleOrdenServicioRepository.class);
        service = new OrdenServicioServiceImpl(repositoryMock, mapperMock, detalleOrdenServicioRepository);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<OrdenServicio> list = new ArrayList<>();
        list.add(new OrdenServicio());
        Page<OrdenServicio> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<OrdenServicioDTO> dtoList = new ArrayList<>();
        dtoList.add(new OrdenServicioDTO());
        Page<OrdenServicioDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(OrdenServicio.class))).thenReturn(new OrdenServicioDTO());

        Page<OrdenServicioDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<OrdenServicio> list = new ArrayList<>();
        list.add(new OrdenServicio());
        list.add(new OrdenServicio());

        List<OrdenServicioDTO> dtoList = new ArrayList<>();
        dtoList.add(new OrdenServicioDTO());
        dtoList.add(new OrdenServicioDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<OrdenServicio> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(OrdenServicio.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<OrdenServicioDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
