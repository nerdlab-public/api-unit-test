package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.LoginDTO;
import com.api.cnrt.maestros.mapper.LoginToLoginDTOMapper;
import com.api.cnrt.maestros.model.Login;
import com.api.cnrt.maestros.repository.ILoginRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@SpringBootTest
class LoginServiceImplTest {

    private ILoginRepository repositoryMock;
    private LoginToLoginDTOMapper mapperMock;
    private LoginServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ILoginRepository.class);
        mapperMock = mock(LoginToLoginDTOMapper.class);
        service = new LoginServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Login> loginList = new ArrayList<>();
        loginList.add(new Login());
        Page<Login> loginPage = new PageImpl<>(loginList);
        when(repositoryMock.findAllByIdGreaterThanOne(any(Pageable.class))).thenReturn(loginPage);


        List<LoginDTO> loginDTOList = new ArrayList<>();
        loginDTOList.add(new LoginDTO());
        Page<LoginDTO> loginDTOPage = new PageImpl<>(loginDTOList);
        when(mapperMock.map(any(Login.class))).thenReturn(new LoginDTO());

        Page<LoginDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(loginDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Login> loginList = new ArrayList<>();
        loginList.add(new Login());
        loginList.add(new Login());

        List<LoginDTO> loginDTOList = new ArrayList<>();
        loginDTOList.add(new LoginDTO());
        loginDTOList.add(new LoginDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Login> loginPage = new PageImpl<>(loginList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(loginPage);


        when(mapperMock.map(any(Login.class))).thenReturn(loginDTOList.get(0), loginDTOList.get(1));

        Page<LoginDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(loginDTOList, result.getContent());
    }
}