package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.LocalidadDTO;
import com.api.cnrt.maestros.mapper.LocalidadToLocalidadDTOMapper;
import com.api.cnrt.maestros.model.Localidad;
import com.api.cnrt.maestros.repository.ILocalidadRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class LocalidadServiceImplTest {

    private ILocalidadRepository repositoryMock;
    private LocalidadToLocalidadDTOMapper mapperMock;
    private LocalidadServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ILocalidadRepository.class);
        mapperMock = mock(LocalidadToLocalidadDTOMapper.class);
        service = new LocalidadServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Localidad> localidadList = new ArrayList<>();
        localidadList.add(new Localidad());
        Page<Localidad> localidadPage = new PageImpl<>(localidadList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(localidadPage);

        List<LocalidadDTO> localidadDTOList = new ArrayList<>();
        localidadDTOList.add(new LocalidadDTO());
        Page<LocalidadDTO> localidadDTOPage = new PageImpl<>(localidadDTOList);
        when(mapperMock.map(any(Localidad.class))).thenReturn(new LocalidadDTO());

        Page<LocalidadDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(localidadDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Localidad> localidadList = new ArrayList<>();
        localidadList.add(new Localidad());
        localidadList.add(new Localidad());

        List<LocalidadDTO> localidadDTOList = new ArrayList<>();
        localidadDTOList.add(new LocalidadDTO());
        localidadDTOList.add(new LocalidadDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Localidad> localidadPage = new PageImpl<>(localidadList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(localidadPage);


        when(mapperMock.map(any(Localidad.class))).thenReturn(localidadDTOList.get(0), localidadDTOList.get(1));

        Page<LocalidadDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(localidadDTOList, result.getContent());
    }
}