package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.LicenciaDTO;
import com.api.cnrt.maestros.mapper.LicenciaToLicenciaDTOMapper;
import com.api.cnrt.maestros.model.Licencia;
import com.api.cnrt.maestros.repository.ILicenciaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class LicenciaServiceImplTest {
    private ILicenciaRepository repositoryMock;
    private LicenciaToLicenciaDTOMapper mapperMock;
    private LicenciaServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ILicenciaRepository.class);
        mapperMock = mock(LicenciaToLicenciaDTOMapper.class);
        service = new LicenciaServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Licencia> licenciaList = new ArrayList<>();
        licenciaList.add(new Licencia());
        Page<Licencia> licenciaPage = new PageImpl<>(licenciaList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(licenciaPage);

        List<LicenciaDTO> licenciaDTOList = new ArrayList<>();
        licenciaDTOList.add(new LicenciaDTO());
        Page<LicenciaDTO> licenciaDTOPage = new PageImpl<>(licenciaDTOList);
        when(mapperMock.map(any(Licencia.class))).thenReturn(new LicenciaDTO());

        Page<LicenciaDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(licenciaDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Licencia> licenciaList = new ArrayList<>();
        licenciaList.add(new Licencia());
        licenciaList.add(new Licencia());

        List<LicenciaDTO> licenciaDTOList = new ArrayList<>();
        licenciaDTOList.add(new LicenciaDTO());
        licenciaDTOList.add(new LicenciaDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Licencia> licenciaPage = new PageImpl<>(licenciaList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(licenciaPage);


        when(mapperMock.map(any(Licencia.class))).thenReturn(licenciaDTOList.get(0), licenciaDTOList.get(1));

        Page<LicenciaDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(licenciaDTOList, result.getContent());
    }
}