package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ResultadoFiscalizacionDTO;
import com.api.cnrt.maestros.mapper.ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper;
import com.api.cnrt.maestros.mapper.ServicioToServicioDTOMapper;
import com.api.cnrt.maestros.model.ResultadoFiscalizacion;
import com.api.cnrt.maestros.repository.IResultadoFiscalizacionRepository;
import com.api.cnrt.maestros.repository.IServicioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ResultadoFiscalizacionServiceImplTest {

    private IResultadoFiscalizacionRepository repositoryMock;
    private ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper mapperMock;
    private ResultadoFiscalizacionServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IResultadoFiscalizacionRepository.class);
        mapperMock = mock(ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper.class);
        service = new ResultadoFiscalizacionServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<ResultadoFiscalizacion> list = new ArrayList<>();
        list.add(new ResultadoFiscalizacion());
        Page<ResultadoFiscalizacion> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<ResultadoFiscalizacionDTO> dtoList = new ArrayList<>();
        dtoList.add(new ResultadoFiscalizacionDTO());
        Page<ResultadoFiscalizacionDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(ResultadoFiscalizacion.class))).thenReturn(new ResultadoFiscalizacionDTO());

        Page<ResultadoFiscalizacionDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<ResultadoFiscalizacion> list = new ArrayList<>();
        list.add(new ResultadoFiscalizacion());
        list.add(new ResultadoFiscalizacion());

        List<ResultadoFiscalizacionDTO> dtoList = new ArrayList<>();
        dtoList.add(new ResultadoFiscalizacionDTO());
        dtoList.add(new ResultadoFiscalizacionDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<ResultadoFiscalizacion> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(ResultadoFiscalizacion.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<ResultadoFiscalizacionDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
