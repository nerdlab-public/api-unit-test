package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoUnidadDTO;
import com.api.cnrt.maestros.mapper.TipoUnidadToTipoUnidadDTOMapper;
import com.api.cnrt.maestros.mapper.VehiculoCNRTToVehiculoCNRTDTOMapper;
import com.api.cnrt.maestros.model.TipoUnidad;
import com.api.cnrt.maestros.repository.ITipoUnidadRepository;
import com.api.cnrt.maestros.repository.IVehiculoCNRTRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoUnidadServiceImplTest {

    private ITipoUnidadRepository repositoryMock;
    private TipoUnidadToTipoUnidadDTOMapper mapperMock;
    private TipoUnidadServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoUnidadRepository.class);
        mapperMock = mock(TipoUnidadToTipoUnidadDTOMapper.class);
        service = new TipoUnidadServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoUnidad> list = new ArrayList<>();
        list.add(new TipoUnidad());
        Page<TipoUnidad> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoUnidadDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoUnidadDTO());
        Page<TipoUnidadDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoUnidad.class))).thenReturn(new TipoUnidadDTO());

        Page<TipoUnidadDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoUnidad> list = new ArrayList<>();
        list.add(new TipoUnidad());
        list.add(new TipoUnidad());

        List<TipoUnidadDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoUnidadDTO());
        dtoList.add(new TipoUnidadDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoUnidad> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoUnidad.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoUnidadDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
