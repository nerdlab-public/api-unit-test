package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.PermisosPDADTO;
import com.api.cnrt.maestros.mapper.PermisosPDAToPermisosPDADTOMapper;
import com.api.cnrt.maestros.mapper.PredioToPredioDTOMapper;
import com.api.cnrt.maestros.model.PermisosPDA;
import com.api.cnrt.maestros.repository.IPermisosPDARepository;
import com.api.cnrt.maestros.repository.IPredioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class PermisosPDAServiceImplTest {

    private IPermisosPDARepository repositoryMock;
    private PermisosPDAToPermisosPDADTOMapper mapperMock;
    private PermisoPDAServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IPermisosPDARepository.class);
        mapperMock = mock(PermisosPDAToPermisosPDADTOMapper.class);
        service = new PermisoPDAServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<PermisosPDA> list = new ArrayList<>();
        list.add(new PermisosPDA());
        Page<PermisosPDA> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<PermisosPDADTO> dtoList = new ArrayList<>();
        dtoList.add(new PermisosPDADTO());
        Page<PermisosPDADTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(PermisosPDA.class))).thenReturn(new PermisosPDADTO());

        Page<PermisosPDADTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<PermisosPDA> list = new ArrayList<>();
        list.add(new PermisosPDA());
        list.add(new PermisosPDA());

        List<PermisosPDADTO> dtoList = new ArrayList<>();
        dtoList.add(new PermisosPDADTO());
        dtoList.add(new PermisosPDADTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<PermisosPDA> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(PermisosPDA.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<PermisosPDADTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
