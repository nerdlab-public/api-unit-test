package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ServicioDTO;
import com.api.cnrt.maestros.mapper.ServicioToServicioDTOMapper;
import com.api.cnrt.maestros.mapper.SubclasificacionFiscalizacionToSubclasificacionFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.Servicio;
import com.api.cnrt.maestros.repository.IServicioRepository;
import com.api.cnrt.maestros.repository.ISubclasificacionFiscalizacionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ServicioServiceImplTest {

    private IServicioRepository repositoryMock;
    private ServicioToServicioDTOMapper mapperMock;
    private ServicioServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IServicioRepository.class);
        mapperMock = mock(ServicioToServicioDTOMapper.class);
        service = new ServicioServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Servicio> list = new ArrayList<>();
        list.add(new Servicio());
        Page<Servicio> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<ServicioDTO> dtoList = new ArrayList<>();
        dtoList.add(new ServicioDTO());
        Page<ServicioDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(Servicio.class))).thenReturn(new ServicioDTO());

        Page<ServicioDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Servicio> list = new ArrayList<>();
        list.add(new Servicio());
        list.add(new Servicio());

        List<ServicioDTO> dtoList = new ArrayList<>();
        dtoList.add(new ServicioDTO());
        dtoList.add(new ServicioDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Servicio> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(Servicio.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<ServicioDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
