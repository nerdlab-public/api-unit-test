package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.CategoriaChoferDTO;
import com.api.cnrt.maestros.mapper.CategoriaChoferToCategoriaChoferDTOMapper;
import com.api.cnrt.maestros.model.CategoriaChofer;
import com.api.cnrt.maestros.repository.ICategoriaChoferRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class CategoriaChoferServiceImplTest {

    private ICategoriaChoferRepository repositoryMock;
    private CategoriaChoferToCategoriaChoferDTOMapper mapperMock;
    private CategoriaChoferServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ICategoriaChoferRepository.class);
        mapperMock = mock(CategoriaChoferToCategoriaChoferDTOMapper.class);
        service = new CategoriaChoferServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<CategoriaChofer> categoriaChoferList = new ArrayList<>();
        categoriaChoferList.add(new CategoriaChofer());
        Page<CategoriaChofer> categoriaChoferPage = new PageImpl<>(categoriaChoferList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(categoriaChoferPage);

        List<CategoriaChoferDTO> categoriaChoferDTOList = new ArrayList<>();
        categoriaChoferDTOList.add(new CategoriaChoferDTO());
        Page<CategoriaChoferDTO> categoriaChoferDTOPage = new PageImpl<>(categoriaChoferDTOList);
        when(mapperMock.map(any(CategoriaChofer.class))).thenReturn(new CategoriaChoferDTO());

        Page<CategoriaChoferDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(categoriaChoferDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<CategoriaChofer> categoriaChoferList = new ArrayList<>();
        categoriaChoferList.add(new CategoriaChofer());
        categoriaChoferList.add(new CategoriaChofer());

        List<CategoriaChoferDTO> categoriaChoferDTOList = new ArrayList<>();
        categoriaChoferDTOList.add(new CategoriaChoferDTO());
        categoriaChoferDTOList.add(new CategoriaChoferDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<CategoriaChofer> categoriaChoferPage = new PageImpl<>(categoriaChoferList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(categoriaChoferPage);


        when(mapperMock.map(any(CategoriaChofer.class))).thenReturn(categoriaChoferDTOList.get(0), categoriaChoferDTOList.get(1));

        Page<CategoriaChoferDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(categoriaChoferDTOList, result.getContent());
    }
}