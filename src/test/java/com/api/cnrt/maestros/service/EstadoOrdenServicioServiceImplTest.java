package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoOrdenServicioDTO;
import com.api.cnrt.maestros.mapper.EstadoOrdenServicioToEstadoOrdenServicioDTOMapper;
import com.api.cnrt.maestros.model.EstadoOrdenServicio;
import com.api.cnrt.maestros.repository.IEstadoOrdenServicioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@SpringBootTest
class EstadoOrdenServicioServiceImplTest {

    private IEstadoOrdenServicioRepository repositoryMock;
    private EstadoOrdenServicioToEstadoOrdenServicioDTOMapper mapperMock;
    private EstadoOrdenServicioServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IEstadoOrdenServicioRepository.class);
        mapperMock = mock(EstadoOrdenServicioToEstadoOrdenServicioDTOMapper.class);
        service = new EstadoOrdenServicioServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<EstadoOrdenServicio> estadoOrdenServicioList = new ArrayList<>();
        estadoOrdenServicioList.add(new EstadoOrdenServicio());
        Page<EstadoOrdenServicio> estadoOrdenServicioPage = new PageImpl<>(estadoOrdenServicioList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(estadoOrdenServicioPage);

        List<EstadoOrdenServicioDTO> estadoOrdenServicioDTOList = new ArrayList<>();
        estadoOrdenServicioDTOList.add(new EstadoOrdenServicioDTO());
        Page<EstadoOrdenServicioDTO> estadoOrdenServicioDTOPage = new PageImpl<>(estadoOrdenServicioDTOList);
        when(mapperMock.map(any(EstadoOrdenServicio.class))).thenReturn(new EstadoOrdenServicioDTO());

        Page<EstadoOrdenServicioDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(estadoOrdenServicioDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<EstadoOrdenServicio> estadoOrdenServicioList = new ArrayList<>();
        estadoOrdenServicioList.add(new EstadoOrdenServicio());
        estadoOrdenServicioList.add(new EstadoOrdenServicio());

        List<EstadoOrdenServicioDTO> estadoOrdenServicioDTOList = new ArrayList<>();
        estadoOrdenServicioDTOList.add(new EstadoOrdenServicioDTO());
        estadoOrdenServicioDTOList.add(new EstadoOrdenServicioDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<EstadoOrdenServicio> estadoOrdenServicioPage = new PageImpl<>(estadoOrdenServicioList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(estadoOrdenServicioPage);


        when(mapperMock.map(any(EstadoOrdenServicio.class))).thenReturn(estadoOrdenServicioDTOList.get(0), estadoOrdenServicioDTOList.get(1));

        Page<EstadoOrdenServicioDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(estadoOrdenServicioDTOList, result.getContent());
    }
}