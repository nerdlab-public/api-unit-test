package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoTransporteAmbitoDTO;
import com.api.cnrt.maestros.mapper.TipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper;
import com.api.cnrt.maestros.model.TipoTransporteAmbito;
import com.api.cnrt.maestros.repository.ITipoTransporteAmbitoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoTransporteAmbitoServiceImplTest {

    private ITipoTransporteAmbitoRepository repositoryMock;
    private TipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper mapperMock;
    private TipoTransporteAmbitoServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoTransporteAmbitoRepository.class);
        mapperMock = mock(TipoTransporteAmbitoToTipoTransporteAmbitoDTOMapper.class);
        service = new TipoTransporteAmbitoServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoTransporteAmbito> list = new ArrayList<>();
        list.add(new TipoTransporteAmbito());
        Page<TipoTransporteAmbito> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoTransporteAmbitoDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoTransporteAmbitoDTO());
        Page<TipoTransporteAmbitoDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoTransporteAmbito.class))).thenReturn(new TipoTransporteAmbitoDTO());

        Page<TipoTransporteAmbitoDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoTransporteAmbito> list = new ArrayList<>();
        list.add(new TipoTransporteAmbito());
        list.add(new TipoTransporteAmbito());

        List<TipoTransporteAmbitoDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoTransporteAmbitoDTO());
        dtoList.add(new TipoTransporteAmbitoDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoTransporteAmbito> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoTransporteAmbito.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoTransporteAmbitoDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
