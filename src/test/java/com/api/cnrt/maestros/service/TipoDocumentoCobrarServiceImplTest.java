package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoDocumentoCobrarDTO;
import com.api.cnrt.maestros.mapper.TipoDocumentoCobrarToTipoDocumentoCobrarDTOMapper;
import com.api.cnrt.maestros.model.TipoDocumentoCobrar;
import com.api.cnrt.maestros.repository.ITipoDocumentoCobrarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoDocumentoCobrarServiceImplTest {

    private ITipoDocumentoCobrarRepository repositoryMock;
    private TipoDocumentoCobrarToTipoDocumentoCobrarDTOMapper mapperMock;
    private TipoDocumentoCobrarServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoDocumentoCobrarRepository.class);
        mapperMock = mock(TipoDocumentoCobrarToTipoDocumentoCobrarDTOMapper.class);
        service = new TipoDocumentoCobrarServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoDocumentoCobrar> list = new ArrayList<>();
        list.add(new TipoDocumentoCobrar());
        Page<TipoDocumentoCobrar> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoDocumentoCobrarDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoDocumentoCobrarDTO());
        Page<TipoDocumentoCobrarDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoDocumentoCobrar.class))).thenReturn(new TipoDocumentoCobrarDTO());

        Page<TipoDocumentoCobrarDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoDocumentoCobrar> list = new ArrayList<>();
        list.add(new TipoDocumentoCobrar());
        list.add(new TipoDocumentoCobrar());

        List<TipoDocumentoCobrarDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoDocumentoCobrarDTO());
        dtoList.add(new TipoDocumentoCobrarDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoDocumentoCobrar> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoDocumentoCobrar.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoDocumentoCobrarDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
