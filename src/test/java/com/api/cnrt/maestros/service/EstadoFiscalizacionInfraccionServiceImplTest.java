package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoFiscalizacionInfraccionDTO;
import com.api.cnrt.maestros.mapper.EstadoFiscalizacionInfraccionToEstadoFiscalizacionInfraccionDTOMapper;
import com.api.cnrt.maestros.model.EstadoFiscalizacionInfraccion;
import com.api.cnrt.maestros.repository.IEstadoFiscalizacionInfraccionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@SpringBootTest
class EstadoFiscalizacionInfraccionServiceImplTest {

    private IEstadoFiscalizacionInfraccionRepository repositoryMock;
    private EstadoFiscalizacionInfraccionToEstadoFiscalizacionInfraccionDTOMapper mapperMock;
    private EstadoFiscalizacionInfraccionServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IEstadoFiscalizacionInfraccionRepository.class);
        mapperMock = mock(EstadoFiscalizacionInfraccionToEstadoFiscalizacionInfraccionDTOMapper.class);
        service = new EstadoFiscalizacionInfraccionServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<EstadoFiscalizacionInfraccion> estadoFiscalizacionInfraccionList = new ArrayList<>();
        estadoFiscalizacionInfraccionList.add(new EstadoFiscalizacionInfraccion());
        Page<EstadoFiscalizacionInfraccion> estadoFiscalizacionInfraccionPage = new PageImpl<>(estadoFiscalizacionInfraccionList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(estadoFiscalizacionInfraccionPage);

        List<EstadoFiscalizacionInfraccionDTO> estadoFiscalizacionInfraccionDTOList = new ArrayList<>();
        estadoFiscalizacionInfraccionDTOList.add(new EstadoFiscalizacionInfraccionDTO());
        Page<EstadoFiscalizacionInfraccionDTO> estadoFiscalizacionInfraccionDTOPage = new PageImpl<>(estadoFiscalizacionInfraccionDTOList);
        when(mapperMock.map(any(EstadoFiscalizacionInfraccion.class))).thenReturn(new EstadoFiscalizacionInfraccionDTO());

        Page<EstadoFiscalizacionInfraccionDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(estadoFiscalizacionInfraccionDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<EstadoFiscalizacionInfraccion> estadoFiscalizacionInfraccionList = new ArrayList<>();
        estadoFiscalizacionInfraccionList.add(new EstadoFiscalizacionInfraccion());
        estadoFiscalizacionInfraccionList.add(new EstadoFiscalizacionInfraccion());

        List<EstadoFiscalizacionInfraccionDTO> estadoFiscalizacionInfraccionDTOList = new ArrayList<>();
        estadoFiscalizacionInfraccionDTOList.add(new EstadoFiscalizacionInfraccionDTO());
        estadoFiscalizacionInfraccionDTOList.add(new EstadoFiscalizacionInfraccionDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<EstadoFiscalizacionInfraccion> estadoFiscalizacionInfraccionPage = new PageImpl<>(estadoFiscalizacionInfraccionList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(estadoFiscalizacionInfraccionPage);


        when(mapperMock.map(any(EstadoFiscalizacionInfraccion.class))).thenReturn(estadoFiscalizacionInfraccionDTOList.get(0), estadoFiscalizacionInfraccionDTOList.get(1));

        Page<EstadoFiscalizacionInfraccionDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(estadoFiscalizacionInfraccionDTOList, result.getContent());
    }
}