package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.dto.ZonaOperativoDTO;
import com.api.cnrt.maestros.mapper.ActaEstadoToActaEstadoDTOMapper;
import com.api.cnrt.maestros.mapper.ZonaOperativoToZonaOperativoDTOMapper;
import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.model.ZonaOperativo;
import com.api.cnrt.maestros.repository.IActaEstadoRepository;
import com.api.cnrt.maestros.repository.IZonaOperativoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ZonaOperativoServiceImplTest {

    private IZonaOperativoRepository repositoryMock;
    private ZonaOperativoToZonaOperativoDTOMapper mapperMock;
    private ZonaOperativoServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IZonaOperativoRepository.class);
        mapperMock = mock(ZonaOperativoToZonaOperativoDTOMapper.class);
        service = new ZonaOperativoServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<ZonaOperativo> list = new ArrayList<>();
        list.add(new ZonaOperativo());
        Page<ZonaOperativo> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<ZonaOperativoDTO> dtoList = new ArrayList<>();
        dtoList.add(new ZonaOperativoDTO());
        Page<ZonaOperativoDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(ZonaOperativo.class))).thenReturn(new ZonaOperativoDTO());

        Page<ZonaOperativoDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<ZonaOperativo> list = new ArrayList<>();
        list.add(new ZonaOperativo());
        list.add(new ZonaOperativo());

        List<ZonaOperativoDTO> dtoList = new ArrayList<>();
        dtoList.add(new ZonaOperativoDTO());
        dtoList.add(new ZonaOperativoDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<ZonaOperativo> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(ZonaOperativo.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<ZonaOperativoDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
