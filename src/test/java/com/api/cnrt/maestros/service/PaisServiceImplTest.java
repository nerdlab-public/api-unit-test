package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.PaisDTO;
import com.api.cnrt.maestros.mapper.PaisToPaisDTOMapper;
import com.api.cnrt.maestros.mapper.PermisosPDAToPermisosPDADTOMapper;
import com.api.cnrt.maestros.model.Pais;
import com.api.cnrt.maestros.repository.IPaisRepository;
import com.api.cnrt.maestros.repository.IPermisosPDARepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class PaisServiceImplTest {

    private IPaisRepository repositoryMock;
    private PaisToPaisDTOMapper mapperMock;
    private PaisServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IPaisRepository.class);
        mapperMock = mock(PaisToPaisDTOMapper.class);
        service = new PaisServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Pais> list = new ArrayList<>();
        list.add(new Pais());
        Page<Pais> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<PaisDTO> dtoList = new ArrayList<>();
        dtoList.add(new PaisDTO());
        Page<PaisDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(Pais.class))).thenReturn(new PaisDTO());

        Page<PaisDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Pais> list = new ArrayList<>();
        list.add(new Pais());
        list.add(new Pais());

        List<PaisDTO> dtoList = new ArrayList<>();
        dtoList.add(new PaisDTO());
        dtoList.add(new PaisDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Pais> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(Pais.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<PaisDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
