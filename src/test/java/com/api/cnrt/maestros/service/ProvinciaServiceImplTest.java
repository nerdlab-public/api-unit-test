package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ProvinciaDTO;
import com.api.cnrt.maestros.mapper.ProvinciaToProvinciaDTOMapper;
import com.api.cnrt.maestros.mapper.ResultadoFiscalizacionToResultadoFiscalizacionDTOMapper;
import com.api.cnrt.maestros.model.Provincia;
import com.api.cnrt.maestros.repository.IProvinciaRepository;
import com.api.cnrt.maestros.repository.IResultadoFiscalizacionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ProvinciaServiceImplTest {

    private IProvinciaRepository repositoryMock;
    private ProvinciaToProvinciaDTOMapper mapperMock;
    private ProvinciaServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IProvinciaRepository.class);
        mapperMock = mock(ProvinciaToProvinciaDTOMapper.class);
        service = new ProvinciaServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Provincia> list = new ArrayList<>();
        list.add(new Provincia());
        Page<Provincia> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<ProvinciaDTO> dtoList = new ArrayList<>();
        dtoList.add(new ProvinciaDTO());
        Page<ProvinciaDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(Provincia.class))).thenReturn(new ProvinciaDTO());

        Page<ProvinciaDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Provincia> list = new ArrayList<>();
        list.add(new Provincia());
        list.add(new Provincia());

        List<ProvinciaDTO> dtoList = new ArrayList<>();
        dtoList.add(new ProvinciaDTO());
        dtoList.add(new ProvinciaDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Provincia> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(Provincia.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<ProvinciaDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
