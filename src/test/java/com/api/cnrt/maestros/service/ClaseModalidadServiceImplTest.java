package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ClaseModalidadDTO;
import com.api.cnrt.maestros.mapper.ClaseModalidadToClaseModalidadDTOMapper;
import com.api.cnrt.maestros.model.ClaseModalidad;
import com.api.cnrt.maestros.repository.IClaseModalidadRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@SpringBootTest
class ClaseModalidadServiceImplTest {

    private IClaseModalidadRepository repositoryMock;
    private ClaseModalidadToClaseModalidadDTOMapper mapperMock;
    private ClaseModalidadServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IClaseModalidadRepository.class);
        mapperMock = mock(ClaseModalidadToClaseModalidadDTOMapper.class);
        service = new ClaseModalidadServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<ClaseModalidad> claseModalidadList = new ArrayList<>();
        claseModalidadList.add(new ClaseModalidad());
        Page<ClaseModalidad> claseModalidadPage = new PageImpl<>(claseModalidadList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(claseModalidadPage);

        List<ClaseModalidadDTO> claseModalidadDTOList = new ArrayList<>();
        claseModalidadDTOList.add(new ClaseModalidadDTO());
        Page<ClaseModalidadDTO> claseModalidadDTOPage = new PageImpl<>(claseModalidadDTOList);
        when(mapperMock.map(any(ClaseModalidad.class))).thenReturn(new ClaseModalidadDTO());

        Page<ClaseModalidadDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(claseModalidadDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<ClaseModalidad> claseModalidadList = new ArrayList<>();
        claseModalidadList.add(new ClaseModalidad());
        claseModalidadList.add(new ClaseModalidad());

        List<ClaseModalidadDTO> claseModalidadDTOList = new ArrayList<>();
        claseModalidadDTOList.add(new ClaseModalidadDTO());
        claseModalidadDTOList.add(new ClaseModalidadDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<ClaseModalidad> claseModalidadPage = new PageImpl<>(claseModalidadList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(claseModalidadPage);


        when(mapperMock.map(any(ClaseModalidad.class))).thenReturn(claseModalidadDTOList.get(0), claseModalidadDTOList.get(1));

        Page<ClaseModalidadDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(claseModalidadDTOList, result.getContent());
    }
}