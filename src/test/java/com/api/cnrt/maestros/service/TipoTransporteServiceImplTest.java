package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.TipoTransporteDTO;
import com.api.cnrt.maestros.mapper.TIpoTransporteToTipoTransporteDTOMapper;
import com.api.cnrt.maestros.mapper.TipoUnidadToTipoUnidadDTOMapper;
import com.api.cnrt.maestros.model.TipoTransporte;
import com.api.cnrt.maestros.repository.ITipoTransporteRepository;
import com.api.cnrt.maestros.repository.ITipoUnidadRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TipoTransporteServiceImplTest {

    private ITipoTransporteRepository repositoryMock;
    private TIpoTransporteToTipoTransporteDTOMapper mapperMock;
    private TipoTransporteServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ITipoTransporteRepository.class);
        mapperMock = mock(TIpoTransporteToTipoTransporteDTOMapper.class);
        service = new TipoTransporteServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<TipoTransporte> list = new ArrayList<>();
        list.add(new TipoTransporte());
        Page<TipoTransporte> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<TipoTransporteDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoTransporteDTO());
        Page<TipoTransporteDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(TipoTransporte.class))).thenReturn(new TipoTransporteDTO());

        Page<TipoTransporteDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<TipoTransporte> list = new ArrayList<>();
        list.add(new TipoTransporte());
        list.add(new TipoTransporte());

        List<TipoTransporteDTO> dtoList = new ArrayList<>();
        dtoList.add(new TipoTransporteDTO());
        dtoList.add(new TipoTransporteDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<TipoTransporte> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(TipoTransporte.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<TipoTransporteDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
