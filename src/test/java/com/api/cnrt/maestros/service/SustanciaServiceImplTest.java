package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.SustanciaDTO;
import com.api.cnrt.maestros.mapper.SustanciaToSustanciaDTOMapper;
import com.api.cnrt.maestros.mapper.TipoCategoriaToTipoCategoriaDTOMapper;
import com.api.cnrt.maestros.model.Sustancia;
import com.api.cnrt.maestros.repository.ISustanciaRepository;
import com.api.cnrt.maestros.repository.ITipoCategoriaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class SustanciaServiceImplTest {

    private ISustanciaRepository repositoryMock;
    private SustanciaToSustanciaDTOMapper mapperMock;
    private SustanciaServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(ISustanciaRepository.class);
        mapperMock = mock(SustanciaToSustanciaDTOMapper.class);
        service = new SustanciaServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Sustancia> list = new ArrayList<>();
        list.add(new Sustancia());
        Page<Sustancia> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<SustanciaDTO> dtoList = new ArrayList<>();
        dtoList.add(new SustanciaDTO());
        Page<SustanciaDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(Sustancia.class))).thenReturn(new SustanciaDTO());

        Page<SustanciaDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Sustancia> list = new ArrayList<>();
        list.add(new Sustancia());
        list.add(new Sustancia());

        List<SustanciaDTO> dtoList = new ArrayList<>();
        dtoList.add(new SustanciaDTO());
        dtoList.add(new SustanciaDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Sustancia> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(Sustancia.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<SustanciaDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
