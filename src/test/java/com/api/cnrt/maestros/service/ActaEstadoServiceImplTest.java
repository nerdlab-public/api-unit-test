package com.api.cnrt.maestros.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.ActaEstadoDTO;
import com.api.cnrt.maestros.mapper.ActaEstadoToActaEstadoDTOMapper;
import com.api.cnrt.maestros.model.ActaEstado;
import com.api.cnrt.maestros.repository.IActaEstadoRepository;

@SpringBootTest
public class ActaEstadoServiceImplTest {

    private IActaEstadoRepository repositoryMock;
    private ActaEstadoToActaEstadoDTOMapper mapperMock;
    private ActaEstadoServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IActaEstadoRepository.class);
        mapperMock = mock(ActaEstadoToActaEstadoDTOMapper.class);
        service = new ActaEstadoServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<ActaEstado> actaEstadoList = new ArrayList<>();
        actaEstadoList.add(new ActaEstado());
        Page<ActaEstado> actaEstadoPage = new PageImpl<>(actaEstadoList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(actaEstadoPage);

        List<ActaEstadoDTO> actaEstadoDTOList = new ArrayList<>();
        actaEstadoDTOList.add(new ActaEstadoDTO());
        Page<ActaEstadoDTO> actaEstadoDTOPage = new PageImpl<>(actaEstadoDTOList);
        when(mapperMock.map(any(ActaEstado.class))).thenReturn(new ActaEstadoDTO());

        Page<ActaEstadoDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(actaEstadoDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<ActaEstado> actaEstadoList = new ArrayList<>();
        actaEstadoList.add(new ActaEstado());
        actaEstadoList.add(new ActaEstado());

        List<ActaEstadoDTO> actaEstadoDTOList = new ArrayList<>();
        actaEstadoDTOList.add(new ActaEstadoDTO());
        actaEstadoDTOList.add(new ActaEstadoDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<ActaEstado> actaEstadoPage = new PageImpl<>(actaEstadoList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(actaEstadoPage);


        when(mapperMock.map(any(ActaEstado.class))).thenReturn(actaEstadoDTOList.get(0), actaEstadoDTOList.get(1));

        Page<ActaEstadoDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(actaEstadoDTOList, result.getContent());
    }
}
