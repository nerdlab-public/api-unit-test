package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.MotivoNoRetenerDTO;
import com.api.cnrt.maestros.mapper.MotivoNoRetenerToMotivoNoRetenerDTOMapper;
import com.api.cnrt.maestros.model.MotivoNoRetener;
import com.api.cnrt.maestros.repository.IMotivoNoRetenerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class MotivoNoRetenerServiceImplTest {

    private IMotivoNoRetenerRepository repositoryMock;
    private MotivoNoRetenerToMotivoNoRetenerDTOMapper mapperMock;
    private MotivoNoRetenerServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IMotivoNoRetenerRepository.class);
        mapperMock = mock(MotivoNoRetenerToMotivoNoRetenerDTOMapper.class);
        service = new MotivoNoRetenerServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<MotivoNoRetener> motivoNoRetenerList = new ArrayList<>();
        motivoNoRetenerList.add(new MotivoNoRetener());
        Page<MotivoNoRetener> motivoNoRetenerPage = new PageImpl<>(motivoNoRetenerList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(motivoNoRetenerPage);

        List<MotivoNoRetenerDTO> motivoNoRetenerDTOList = new ArrayList<>();
        motivoNoRetenerDTOList.add(new MotivoNoRetenerDTO());
        Page<MotivoNoRetenerDTO> motivoNoRetenerDTOPage = new PageImpl<>(motivoNoRetenerDTOList);
        when(mapperMock.map(any(MotivoNoRetener.class))).thenReturn(new MotivoNoRetenerDTO());

        Page<MotivoNoRetenerDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(motivoNoRetenerDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<MotivoNoRetener> motivoNoRetenerList = new ArrayList<>();
        motivoNoRetenerList.add(new MotivoNoRetener());
        motivoNoRetenerList.add(new MotivoNoRetener());

        List<MotivoNoRetenerDTO> motivoNoRetenerDTOList = new ArrayList<>();
        motivoNoRetenerDTOList.add(new MotivoNoRetenerDTO());
        motivoNoRetenerDTOList.add(new MotivoNoRetenerDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<MotivoNoRetener> motivoNoRetenerPage = new PageImpl<>(motivoNoRetenerList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(motivoNoRetenerPage);


        when(mapperMock.map(any(MotivoNoRetener.class))).thenReturn(motivoNoRetenerDTOList.get(0), motivoNoRetenerDTOList.get(1));

        Page<MotivoNoRetenerDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(motivoNoRetenerDTOList, result.getContent());
    }
}