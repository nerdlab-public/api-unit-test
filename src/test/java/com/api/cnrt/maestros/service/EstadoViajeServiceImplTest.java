package com.api.cnrt.maestros.service;
import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.EstadoViajeDTO;
import com.api.cnrt.maestros.mapper.EstadoViajeToEstadoViajeDTOMapper;
import com.api.cnrt.maestros.model.EstadoViaje;
import com.api.cnrt.maestros.repository.IEstadoViajeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class EstadoViajeServiceImplTest {

    private IEstadoViajeRepository repositoryMock;
    private EstadoViajeToEstadoViajeDTOMapper mapperMock;
    private EstadoViajeServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IEstadoViajeRepository.class);
        mapperMock = mock(EstadoViajeToEstadoViajeDTOMapper.class);
        service = new EstadoViajeServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<EstadoViaje> estadoViajeList = new ArrayList<>();
        estadoViajeList.add(new EstadoViaje());
        Page<EstadoViaje> estadoViajePage = new PageImpl<>(estadoViajeList);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(estadoViajePage);

        List<EstadoViajeDTO> estadoViajeDTOList = new ArrayList<>();
        estadoViajeDTOList.add(new EstadoViajeDTO());
        Page<EstadoViajeDTO> estadoViajeDTOPage = new PageImpl<>(estadoViajeDTOList);
        when(mapperMock.map(any(EstadoViaje.class))).thenReturn(new EstadoViajeDTO());

        Page<EstadoViajeDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(estadoViajeDTOPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<EstadoViaje> estadoViajeList = new ArrayList<>();
        estadoViajeList.add(new EstadoViaje());
        estadoViajeList.add(new EstadoViaje());

        List<EstadoViajeDTO> estadoViajeDTOList = new ArrayList<>();
        estadoViajeDTOList.add(new EstadoViajeDTO());
        estadoViajeDTOList.add(new EstadoViajeDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<EstadoViaje> estadoViajePage = new PageImpl<>(estadoViajeList);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(estadoViajePage);


        when(mapperMock.map(any(EstadoViaje.class))).thenReturn(estadoViajeDTOList.get(0), estadoViajeDTOList.get(1));

        Page<EstadoViajeDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(estadoViajeDTOList, result.getContent());
    }
}