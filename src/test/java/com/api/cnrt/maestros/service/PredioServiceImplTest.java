package com.api.cnrt.maestros.service;

import com.api.cnrt.exceptions.RepositoryException;
import com.api.cnrt.maestros.dto.PredioDTO;
import com.api.cnrt.maestros.mapper.PredioToPredioDTOMapper;
import com.api.cnrt.maestros.mapper.ProvinciaToProvinciaDTOMapper;
import com.api.cnrt.maestros.model.Predio;
import com.api.cnrt.maestros.repository.IPredioRepository;
import com.api.cnrt.maestros.repository.IProvinciaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class PredioServiceImplTest {

    private IPredioRepository repositoryMock;
    private PredioToPredioDTOMapper mapperMock;
    private PredioServiceImpl service;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(IPredioRepository.class);
        mapperMock = mock(PredioToPredioDTOMapper.class);
        service = new PredioServiceImpl(repositoryMock, mapperMock);
    }

    @Test
    public void testFindAllByInitial() throws RepositoryException {
        List<Predio> list = new ArrayList<>();
        list.add(new Predio());
        Page<Predio> page = new PageImpl<>(list);
        when(repositoryMock.findAll(any(Pageable.class))).thenReturn(page);

        List<PredioDTO> dtoList = new ArrayList<>();
        dtoList.add(new PredioDTO());
        Page<PredioDTO> dtoPage = new PageImpl<>(dtoList);
        when(mapperMock.map(any(Predio.class))).thenReturn(new PredioDTO());

        Page<PredioDTO> result = service.findAllBy(true, Pageable.unpaged());

        assertEquals(dtoPage, result);
    }

    @Test
    public void testFindAllByLastSyncro() throws RepositoryException {
        List<Predio> list = new ArrayList<>();
        list.add(new Predio());
        list.add(new Predio());

        List<PredioDTO> dtoList = new ArrayList<>();
        dtoList.add(new PredioDTO());
        dtoList.add(new PredioDTO());

        Pageable pageable = Pageable.unpaged();
        LocalDateTime lastSyncro = LocalDateTime.now().minusDays(7);
        Page<Predio> page = new PageImpl<>(list);
        when(repositoryMock.findFromLastSincro(lastSyncro, pageable)).thenReturn(page);


        when(mapperMock.map(any(Predio.class))).thenReturn(dtoList.get(0), dtoList.get(1));

        Page<PredioDTO> result = service.findAllBy(lastSyncro, pageable);

        assertEquals(dtoList, result.getContent());
    }
}
