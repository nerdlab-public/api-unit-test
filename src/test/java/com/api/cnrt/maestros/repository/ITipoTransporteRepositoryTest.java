package com.api.cnrt.maestros.repository;

import com.api.cnrt.common.TestUtils;
import com.api.cnrt.maestros.model.TipoTransporte;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@DataJpaTest
@ExtendWith(MockitoExtension.class)
class ITipoTransporteRepositoryTest {

    @Mock
    private ITipoTransporteRepository tipoTransporteRepository;

    private LocalDateTime lastSyncro;

    @BeforeEach
    void setUp() {
        lastSyncro = LocalDateTime.now().minusDays(3);
        List<TipoTransporte> tipoTransporteList = Collections.singletonList(TestUtils.createMockTipoTransporte());

        when(tipoTransporteRepository.findFromLastSincro(eq(lastSyncro), eq(PageRequest.of(0, 10))))
                .thenReturn(new PageImpl<>(tipoTransporteList));
    }


    @Test
    void findFromLastSincro() {
        Page<TipoTransporte> result = tipoTransporteRepository.findFromLastSincro(lastSyncro, PageRequest.of(0, 10));
        assertEquals(1, result.getTotalElements());
    }
}