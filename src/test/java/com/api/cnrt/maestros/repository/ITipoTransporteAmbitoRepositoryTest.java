package com.api.cnrt.maestros.repository;

import com.api.cnrt.common.TestUtils;
import com.api.cnrt.maestros.model.TipoTransporteAmbito;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@DataJpaTest
@ExtendWith(MockitoExtension.class)
class ITipoTransporteAmbitoRepositoryTest {

    @Mock
    private ITipoTransporteAmbitoRepository tipoTransporteAmbitoRepository;

    private LocalDateTime lastSyncro;

    @BeforeEach
    void setUp() {
        lastSyncro = LocalDateTime.now().minusDays(3);
        List<TipoTransporteAmbito> tipoTransporteAmbitoList = Collections.singletonList(TestUtils.createMockTipoTransporteAmbito());

        when(tipoTransporteAmbitoRepository.findFromLastSincro(eq(lastSyncro), eq(PageRequest.of(0, 10))))
                .thenReturn(new PageImpl<>(tipoTransporteAmbitoList));
    }


    @Test
    void findFromLastSincro() {
        Page<TipoTransporteAmbito> result = tipoTransporteAmbitoRepository.findFromLastSincro(lastSyncro, PageRequest.of(0, 10));
        assertEquals(1, result.getTotalElements());
    }
}