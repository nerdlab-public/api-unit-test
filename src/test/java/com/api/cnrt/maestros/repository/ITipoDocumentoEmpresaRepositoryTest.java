package com.api.cnrt.maestros.repository;

import com.api.cnrt.common.TestUtils;
import com.api.cnrt.maestros.model.TipoDocumentoEmpresa;
import com.api.cnrt.maestros.model.TipoDocumentoPersona;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@DataJpaTest
@ExtendWith(MockitoExtension.class)
class ITipoDocumentoEmpresaRepositoryTest {

    @Mock
    private ITipoDocumentoEmpresaRepository tipoDocumentoEmpresaRepository;

    private LocalDateTime lastSyncro;

    @BeforeEach
    void setUp() {
        lastSyncro = LocalDateTime.now().minusDays(3);
        List<TipoDocumentoEmpresa> tipoDocumentoEmpresaList = Collections.singletonList(TestUtils.createMockTipoDocumentoEmpresa());

        when(tipoDocumentoEmpresaRepository.findFromLastSincro(eq(lastSyncro), eq(PageRequest.of(0, 10))))
                .thenReturn(new PageImpl<>(tipoDocumentoEmpresaList));
    }


    @Test
    void findFromLastSincro() {
        Page<TipoDocumentoEmpresa> result = tipoDocumentoEmpresaRepository.findFromLastSincro(lastSyncro, PageRequest.of(0, 10));
        assertEquals(1, result.getTotalElements());
    }
}