package com.api.cnrt.maestros.repository;

import com.api.cnrt.common.TestUtils;
import com.api.cnrt.maestros.model.Ambito;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@DataJpaTest
@ExtendWith(MockitoExtension.class)
class IAmbitoRepositoryTest {

    @Mock
    private IAmbitoRepository repo;

    private LocalDateTime lastSync;

    @BeforeEach
    void setUp() {
        lastSync = LocalDateTime.now().minusDays(3);
        List<Ambito> list = Collections.singletonList(TestUtils.createMockAmbito());

        when(repo.findFromLastSincro(eq(lastSync), eq(PageRequest.of(0, 10))))
                .thenReturn(new PageImpl<>(list));
    }

    @Test
    void findFromLastSincro() {
        Page<Ambito> result = repo.findFromLastSincro(lastSync, PageRequest.of(0, 10));

        assertEquals(1, result.getTotalElements());
    }
}
