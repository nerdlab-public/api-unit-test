package com.api.cnrt.maestros.repository;

import com.api.cnrt.common.TestUtils;
import com.api.cnrt.maestros.model.ClaseModalidad;
import com.api.cnrt.maestros.model.Delegacion;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@DataJpaTest
@ExtendWith(MockitoExtension.class)
class IDelegacionRepositoryTest {
    @Mock
    private IDelegacionRepository repo;

    private LocalDateTime lastSync;

    @BeforeEach
    void setUp() {
        lastSync = LocalDateTime.now().minusDays(3);
        List<Delegacion> list = Collections.singletonList(TestUtils.createMockDelegacion());

        when(repo.findFromLastSincro(eq(lastSync), eq(PageRequest.of(0, 10))))
                .thenReturn(new PageImpl<>(list));
    }

    @Test
    void findFromLastSincro() {
        Page<Delegacion> result = repo.findFromLastSincro(lastSync, PageRequest.of(0, 10));

        assertEquals(1, result.getTotalElements());
    }
}