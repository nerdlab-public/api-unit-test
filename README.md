# api-cnrt-fis

## Descripción
El proyecto "api-cnrt-fis" se ha creado para registrar fiscalizaciones desde dispositivos móviles y proporcionar estadísticas.

## Tabla de contenidos
- [Getting Started](#getting-started)
- [Despliegue](#despliegue)
- [Contribución](#contribución)
- [Licencia](#licencia)

# Getting Started
| Tecnología         | Versión |
|--------------------|-|
| Spring Boot        | 3.1.3 |
| Maven              | 3.6.3 |
| JPA (Java Persistence API) | |
| JWT (JSON Web Token)  | |
| Java               | 17 |
| ...                | ... |

- Clonar el proyecto desde Gitlab
- IDE recomendado IntelliJ IDEA

# Despliegue

Estos son los pasos necesarios para implementar el proyecto en un entorno de producción.

## Paso 1: Generar el archivo JAR

Ejecuta el siguiente comando en la raíz de tu proyecto Maven para compilar y empaquetar el proyecto en un archivo JAR:

```
mvn clean package
```
## Paso 2: Copiar el JAR al servidor de DataCloud
Utiliza scp para copiar el archivo JAR generado a tu servidor en DataCloud. Sustituye backend-0.0.1-SNAPSHOT.jar con el nombre de tu archivo JAR y asegúrate de tener acceso SSH al servidor:

```
scp target/backend-0.0.1-SNAPSHOT.jar ubuntu@186.33.226.26:/opt
```

## Paso 3: Conéctate al servidor de DataCloud
Abre una terminal y conéctate a tu servidor DataCloud utilizando SSH.

## Paso 4: Ver procesos anteriores y su PID
Para asegurarte de que no haya una instancia previa en ejecución del mismo proceso, puedes verificar los procesos que se están ejecutando utilizando ps y filtrando por java:

```
ps aux | grep java
```
Esto te mostrará una lista de procesos en ejecución relacionados con Java. Anota el PID del proceso que corresponde a tu aplicación.

## Paso 5: Matar el proceso existente (si es necesario)
Si encuentras un proceso previo que debe detenerse, utiliza el comando kill para detenerlo. Reemplaza <PID> con el PID del proceso que deseas detener:

```
kill -9 <PID>
```

## Paso 6: Mover el JAR al directorio de implementación
Navega al directorio donde copiaste el archivo JAR. Por lo general, es el directorio /opt en el servidor de DataCloud:

```
cd /opt
```

## Paso 7: Iniciar la aplicación en segundo plano
Usa el comando nohup para ejecutar la aplicación JAR en segundo plano y mantenerla en ejecución incluso cuando cierres la terminal SSH:

```
nohup java -jar backend-0.0.1-SNAPSHOT.jar &
```
Esto iniciará tu aplicación en el servidor de DataCloud en un entorno de producción. Asegúrate de haber configurado las variables de entorno y ajustes de configuración necesarios para que tu aplicación funcione correctamente en este entorno. También, considera configurar un servicio de inicio automático para que tu aplicación se inicie automáticamente en caso de reinicios del servidor.

## Contribución
Invita a otros desarrolladores a colaborar en el proyecto. Puedes proporcionar información sobre cómo realizar contribuciones, cómo reportar problemas o cómo ponerse en contacto con el equipo de desarrollo.

## Licencia
Indica la licencia bajo la cual se distribuye el proyecto. Asegúrate de cumplir con las regulaciones de licencia adecuadas para tu proyecto.

---

Siéntete libre de personalizar este README.md según las necesidades específicas de tu proyecto. Puedes agregar más secciones, como "Uso", "Tecnologías Utilizadas", "Ejemplos de API", "Documentación", etc. Además, puedes proporcionar enlaces a la documentación, repositorio de código, sitio web o cualquier otro recurso relacionado con tu proyecto.


